//include can
#include <mcp_can.h>
#include <mcp_can_dfs.h>

const int SPI_CS_PIN = 9;

// Declare class for CAN Shield
MCP_CAN CAN(SPI_CS_PIN);

unsigned char buf[7];
unsigned char len = 3;
unsigned int canID = 0x03;


void setup() {
  // Initialize serial for troubleshooting.
  Serial.begin(9600);
  
  // init can bus : baudrate = 500k
  while(CAN.begin(CAN_250KBPS) == CAN_FAILINIT)
  {
      delay(100);
  }

  Serial.println("Begin");

  buf[0] = 0;         //torque low byte
  buf[1] = 6;        //torque high byte, not set you probably dont need full torque
  buf[2] = 10;        //speed command low byte
  //buf[3] = 0x07;
  // buf[4] = 0x00;
  //buf[5] = 0x00;
  //buf[6] = 0x39;
 
}

int val;

void loop() {
  val = analogRead(A0);     // read the input pin

  Serial.println(val);

  if(val > 1000){
    CAN.sendMsgBuf(canID, 0, len, buf);
    Serial.println("Message Sent");
    delay(500);
  }
  delay(50);
}
