Installation content
====================

*README-FIRST.txt : This file
*SPC5Studio v5.0.exe : Spc5studio installer
*SPC5Studio_installation_procedure.pdf : Installation procedure of SPC5Studio
*PLS_Debugger_Installation_driver_on_SPC560X_Discovery_board.pdf : how to install the driver on SPC560X Discovery Board for 
FTDI driver and COM port
*How to Import and export projects in SPC5Studio.pdf : Procedure to export Old project before 
removing old SPC5Studio installation (if any)
* SPC5Studio License agreement.pdf 


How to install ?
================

- If you are already using SPC5Studio, please save all your existing projects exporting all your all project following
 "How to Import and export projects in SPC5Studio.pdf"
- Follow "SPC5Studio_installation_procedure.pdf" instructions.
- Follow "PPLS_Debugger_Installation_driver_on_SPC560X_Discovery_board.pdf" to enable JTAG debug and serial port.
- Download Universal debuger(UDE)if needed to debug with PLS debugger.
- Once SPC5Studio properly installed import your existing projects previously saved.

