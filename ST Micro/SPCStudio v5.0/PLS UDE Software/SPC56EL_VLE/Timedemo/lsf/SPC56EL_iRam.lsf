/* SPC56EL_iRam.lcf - Simple minimal SPC56EL link file using 64/128 KB SRAM */
/* Aug 03 2010 initial version */

/* define heap and stack size */
__HEAP_SIZE = DEFINED(__HEAD_SIZE) ? __HEAP_SIZE : 1K ;
__STACK_SIZE = DEFINED(__STACK_SIZE) ? __HEAP_SIZE : 4K ;

MEMORY
{
  interrupts_ram (rx) : org = 0x40000000, len = 0x2000
  internal_cram (rxw) : org = 0x40002000, len = 0x3000
  internal_ram (rxw)  : org = 0x40005000, len = 0xB000
}

/* map all used regions to existing memory ares */
REGION_ALIAS("INTR", interrupts_ram)
REGION_ALIAS("CODE", internal_cram)
REGION_ALIAS("RODATA", internal_cram)
REGION_ALIAS("RAM", internal_ram)

/* L2 SRAM Location (used for L2 SRAM initialization) */

L2SRAM_LOCATION = 0x40000000;

SECTIONS
{
  .traptable   : ALIGN(4096)
  {
    /*        *(.traptable) */
    /* For INTC in HW Vector Mode   */
    /*        . = ALIGN(16) ;   */
    *(.interrupt_table)
  } > INTR

  /* Handlers for core interrupts */
  .ivor_handlers        : {
    *(.ivor_handlers)
  } > INTR

  .intc_sw_isr_vector_table : {} /* For INTC in SW Vector Mode */

  .startup : FLAGS(ax)
  {
    *(.startup)
  } > CODE

  .text : FLAGS(ax)
  {
    *(.text)
    *(.init)
    *(.fini)
    *(.eini)
    . = ALIGN(16);
  } > CODE

  .rodata :
  {
    *(.rodata)
  } > RODATA

  .rom_copy_info :
  {
    PROVIDE(__rom_copy_info = . ) ;
    LONG(LOADADDR(.data)) ; LONG(0 + ADDR(.data)) ; LONG(SIZEOF(.data));
    LONG(LOADADDR(.sdata)) ; LONG(0 + ADDR(.sdata)) ; LONG(SIZEOF(.sdata));
    LONG(LOADADDR(.sdata2)) ; LONG(0 + ADDR(.sdata2)) ; LONG(SIZEOF(.sdata2));
    LONG(0) ; LONG(0) ; LONG(0) ;
  } > CODE

  .bss_init_info :
  {
    PROVIDE(__bss_init_info = . ) ;
    LONG(0 + ADDR(.bss)); LONG(SIZEOF(.bss));
    LONG(0 + ADDR(.sbss)); LONG(SIZEOF(.sbss));
    LONG(0 + ADDR(.sbss2)); LONG(SIZEOF(.sbss2));
    LONG(0) ; LONG(0) ; LONG(0) ;
  } > CODE

  .data   : FLAGS(aw)
  {
    *(.data)
    *(.data.*)
    *(.ctors)
    *(.dtors)
  }  > RAM AT>CODE

  .sdata2       :
  {
    _SDA2_BASE_ = ABSOLUTE(.);
    *(.sdata2)
    *(.sdata2.*)
  } > RAM AT>CODE

  .sbss2    (NOLOAD)   :
  {
    *(.sbss2)
    *(.sbss2.*)
  } >RAM

  extab      : {}> RAM
  extabindex : {}> RAM


  .sdata  :
  {
    _SDA_BASE_ = ABSOLUTE(.);
    *(.sdata)
    *(.sdata.*)
  } > RAM

  .sbss  (NOLOAD)  :
  {
    *(.sbss)
    *(.sbss.*)
  } > RAM

  .bss   (NOLOAD)  :
  {
    *(.bss)
    *(.bss*)
  } > RAM

  .stack (NOLOAD) : ALIGN(16) FLAGS(aw)
  {
    __HEAP = . ;
    . += __HEAP_SIZE ;
    __HEAP_END = . ;
    _stack_end = . ;
    . +=  __STACK_SIZE ;
    _stack_addr = . ;
    . += 4;
  } > RAM
}



