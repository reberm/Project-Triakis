/*

  File:     SPC56EL_HWInit.h
  Created:  19.08.2010
  Desc.:
    This file contains all SPC56EL derivative needed initializations, 
    and all initializations for the SPC56EL boards which are supported.
    This includes setting up the External Bus Interface to allow access to 
    memory on the external bus, and ensuring there is a valid entry in the
    MMU for the external memory access.

  Rev.: 1.0
  
*/


#ifndef _SPC56EL_HWINIT_H_
#define _SPC56EL_HWINIT_H_


/*
* Device overview (simplified):
*
*  Device   Code Flash(KB)    RAM(KB)   Clock(Mhz)
*  SPC56EL60  1024        64/128    120 (DP/LS)
*
* Please note, that this sample is configured to run with:
*
* SPC56EL60 1024        128     120    (Dual-Processor Mode)
*
* To switch the device just change "INTERNAL_RAM_SIZE" and the "MEMORY"
* section in the "*.lcf" file.
*
* Also configure the target mode (Lock-Step / Dual Procssor Mode) by
* setting "LS_ON" in the Makefile!!
*
*/ 


/*----------------------------------------------------------------------------*/
/* Includes                                                                   */
/*----------------------------------------------------------------------------*/

#ifdef __cplusplus
extern "C" {
#endif

#define ASM_FUNC      __attribute__((naked))
#define STARTUP_SEC   __attribute__((section(".startup")))

/*******************************************************/
/* Board specific definitions                          */
/*******************************************************/
/* Lock Step / Dual Processor Mode */

#ifdef LS_MODE
#define INTERNAL_RAM_SIZE (128*1024)
#else
#define INTERNAL_RAM_SIZE (64*1024)
#endif

#define LED1_CR  SIUL.PCR52.R
#define LED2_CR  SIUL.PCR53.R
#define LED3_CR  SIUL.PCR54.R
#define LED4_CR  SIUL.PCR55.R

#define LED1_DR  SIUL.GPDO52_55.B.PDO52
#define LED2_DR  SIUL.GPDO52_55.B.PDO53
#define LED3_DR  SIUL.GPDO52_55.B.PDO54
#define LED4_DR  SIUL.GPDO52_55.B.PDO55


/*******************************************************/
/* Startup initializations                             */
/*******************************************************/
void INIT_ExternalBusAndMemory(void);
void INIT_Derivative(void); 
void __initMMU(void);


/*******************************************************/
/* Post startup initializations                        */
/*******************************************************/
void initModesAndClock(void);
void initPIT(void);
void disableWatchdog(void);
void initIrqVectors(void);
void initINTC(void);
void initSwIrq4(void);
void enableIrq(void);

/*----------------------------------------------------------------------------*/
/* Used Board selection                                                       */
/*----------------------------------------------------------------------------*/

/* Supported Boards */
/* Lock Step Mode switch */
#define NO_EXTERNAL_MEMORY      0

/* Used board */
/*$UsedboardMacro$*/

#ifndef INIT_USED_BOARD
#error INIT_USED_BOARD should be defined !
#endif


/*----------------------------------------------------------------------------*/
/* MMU Table Entries Defines                                                  */
/*----------------------------------------------------------------------------*/

/**
* Generate MMU Assist 0 value from the parameters provided.
* In accordance with the PowerPC Zen core specification the TLBSEL value 
* is always set to 01b to maintain future compatibility.
*
*/
#define MAS0_VALUE(eselcam) ((unsigned long)(0x10000000 | (eselcam << 16)))

  /** 
* Generate MMU Assist 1 value from the parameters provided
*
* parameter valid:   1 if the MMU entry is valid, otherwise \c 0 (invalid).
* parameter iprot:   Invalidation protection value
* parameter tid:     the translation ID
* parameter ts:      the translation space value
* parameter tsize:   the translation size
*/
#define MAS1_VALUE(valid, iprot, tid, tsize) \
    ((unsigned long)((valid << 31) | (iprot << 30) | (tid << 16) | (0 << 12) | (tsize << 8)))

/** V TLB valid bit */
#define V_INVALID 0
#define V_VALID   1

/** IPROT TLB Invalidate protect bit */
#define IPROT_NOTPROTECTED 0
#define IPROT_PROTECTED    1

/** Translation ID defines the TID as global and matches all process IDs */
#define TID_GLOBAL          0

/** Translation size */
#define TSIZE_4KB           1
#define TSIZE_16KB          2
#define TSIZE_64KB          3
#define TSIZE_256KB         4
#define TSIZE_1MB           5
#define TSIZE_4MB           6
#define TSIZE_16MB          7
#define TSIZE_64MB          8
#define TSIZE_256MB         9
#define TSIZE_1GB           10
#define TSIZE_4GB           11

/**
* Generate MMU Assist 2 value from the parameters provided
*
* Effective Page Number (start address of logical memory region) 
* must be computed directely in the assembly code.
* 
* parameter   epn: Effective page number 
* parameter   a:   Alternate coherency mode
* parameter   v:   VLE mode
* parameter   w:   Write-through Required
* parameter   i:   Cache Inhibited
* parameter   m:   Memory Coherency Required
* parameter   g:   Guarded
* parameter   e:   Endianness
*/
#define MAS2_VALUE(epn, a, v, w, i, m, g, e) \
  ((unsigned long)((epn)| (a << 6)| (v << 5)| (w << 4)| (i << 3)| (m << 2)| (g << 1)| (e)))

/** MAS2[EPN]: Effective page number */

/** MAS2[ACM]: Alternate coherency mode */
#define NOT_ACM       0
#define ACM         1

/** MAS2[VLE]: PPC mode */
#define NOT_VLE       0
/** MAS2[VLE]: VLE (Variable Length Encoding) mode */
#define VLE         1

/** MAS2[W]: Update data in the cache only */
#define WRITE_BACK          0
/** MAS2[W]: All stores performed are written through to memory */
#define WRITE_THROUGH       1

/** MAS2[I]: The page is considered cacheable */
#define CACHEABLE           0
/** MAS2[I]: The page is cache-inhibited */
#define CACHE_INHIBIT       1

/** MAS2[M]: Memory Coherence is not-required */
#define MEM_COHERENCE_NREQ  0
/** MAS2[M]: Memory Coherence is required */
#define MEM_COHENRECE_REQ   1

/** MAS2[G]: Access to page is not guarded */
#define NOT_GUARDED         0
/** MAS2[G]: All loads and stores are performed without speculation */
#define GUARDED             1

/** MAS2[E]: Page is accessed in big-endian order */
#define BIG_ENDIAN          0
/** MAS2[E]: Page is accessed in little-endian order */
#define LITTLE_ENDIAN       1

/**
* Generate MMU Assist 3 flags from the parameters provided
*
* Real Page Number (start address of physical memory region) 
* must be computed directely in the assembly code
*
* parameter permissions:  Permission bits
*/
#define MAS3_VALUE(rpn, permissions) \
  ((unsigned long)((rpn)| permissions))

/** MAS3[RPN]: Real page number */

/** MAS3[U/S{XWR}]: Read. Write and Execute permission */
#define READ_WRITE_EXECUTE  0x3f
/** MAS3[U/S{XR}]: Read and Execute permission */
#define READ_EXECUTE        0x33


#ifdef __cplusplus
}
#endif

#endif
