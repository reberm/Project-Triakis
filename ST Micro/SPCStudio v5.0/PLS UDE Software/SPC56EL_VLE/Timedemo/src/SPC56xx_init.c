/*

  File:     SPC56xx_init.c
  Created:  19.08.2010
  Desc.:
    This file contains the SPC56xx derivative needed initializations. 
    usr_init() is called by the startup code of the application at initialization time
    You can add needed hardware initializations here.
    This file also contains the RCHW and Reset Vector setup:
    The chip is by default setup to boot from internal Flash and the watchdog is disabled.

  Rev.: 1.0
  
*/


#include "spc56el.h"
#include "SPC56EL_HWInit.h"

#define ASM_FUNC        __attribute__((naked))

//#pragma section code_type ".init"
#ifdef INIT_HARDWARE
#define INIT_BOARD_INTERNAL_SETUP 1
#define INIT_DERIVATIVE_INTERNAL_SETUP 1
#define INIT_EXTERNAL_BUS_INTERFACE_SETUP 0
#else
#define INIT_BOARD_INTERNAL_SETUP 0
#define INIT_DERIVATIVE_INTERNAL_SETUP 0
#define INIT_EXTERNAL_BUS_INTERFACE_SETUP 0
#endif

#ifdef __cplusplus
extern "C" {
#endif

extern void _start();
void usr_init();
/*lint -esym(752,__start) */

#ifdef __cplusplus
}
#endif

/*****************************************************************/
/* usr_init():                                                   */
/*   Define here the needed hardware initializations at startup  */

ASM_FUNC void usr_init()
{
  /* Add needed hardware initializations in this function */
  asm volatile ("mflr     r30");                         /* Save off return address in NV reg */

#if INIT_BOARD_INTERNAL_SETUP==1
  asm volatile ("bl      __initMMU");                    /* Board specific hardware initializations */
#endif

  SWT.SR.R = 0x0000C520;     /* Write keys to clear soft lock bit */
  SWT.SR.R = 0x0000D928; 
  SWT.CR.R = 0xFF00000A;     /* Clear watchdog enable (WEN) */

#if INIT_DERIVATIVE_INTERNAL_SETUP==1
  asm volatile ("bl      INIT_Derivative");              /* Derivative specific hardware initializations */
#endif
#if INIT_EXTERNAL_BUS_INTERFACE_SETUP==1
  asm volatile ("bl      INIT_ExternalBusAndMemory");    /* Set up access to external memory (inc. chip select and MMU) */
#endif

  asm volatile ("mtlr    r30");                          /* Get saved return address */

  asm volatile ("blr");
}

#ifdef __cplusplus
extern "C" {
#endif

/**************************************************************/
/* RCHW and Reset Vector setup:                               */
/*   The chip is by default setup to boot from internal Flash */
/*   and the watchdog is disabled.                            */ 

typedef void (*resetfuncptr)(void);

//#pragma push /* Save the current state */
//#pragma section sconst_type ".boot"
#define BOOT_SEC __attribute__ ((section(".boot")))

BOOT_SEC extern const unsigned long bam_rchw;
BOOT_SEC extern const resetfuncptr bam_resetvector;

/* RCHW_VALUE Flags */
#define RCHW_WTE 0x0400L        /* Enable Watchdog */
#define RCHW_VLE 0x0100L        /* VLE Indicator   */
#define RCHW_PS0_32BITS 0x0000L /* Boot from External Bus CS0, 32-bit CS0 port size. */
#define RCHW_PS0_16BITS 0x0200L /* Boot from External Bus CS0, 16-bit CS0 port size. */
#define RCHW_BOOTIDENTIFIER 0x005AL

/* Used RCHW value: boot from internal flash, watchdog disabled */
#ifdef __PPC_VLE__
#define RCHW_VALUE RCHW_BOOTIDENTIFIER | RCHW_VLE
#else
#define RCHW_VALUE RCHW_BOOTIDENTIFIER
#endif

BOOT_SEC const unsigned long bam_rchw = ( ((RCHW_VALUE) << 16) | RCHW_VALUE );
BOOT_SEC const resetfuncptr bam_resetvector = _start;

//#pragma pop

#ifdef __cplusplus
}
#endif

