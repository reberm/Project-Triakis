/*

  File:     handler.c
  Created:  19.08.2010

  Rev.: 1.0
  
*/

#include "spc56el.h"


#define INTC_EOIR       0xFFF48018   // Single Core: End Of Interrupt Reg. addr
#define INTC_EOIR_PRC0  0xFFF48018   // Dual Core: End Of Interrupt Reg. addr

#ifdef __cplusplus
extern "C" {
#endif

extern void SwIrq4ISR(void);
extern void Pit1ISR(void);


#define INTR_PROLOG \
  __asm__ volatile ("" \
  "stwu    r1, -0x50 (r1)   # Create stack frame and store back chain\n\t" \
  "stw     r3,  0x28 (r1)   # Store a working register\n\t" \
  "mfsrr0  r3               # Store SRR0:1 (must be done before enabling EE)\n\t" \
  "stw     r3,  0x0C (r1)\n\t" \
  "mfsrr1  r3\n\t" \
  "stw     r3,  0x10 (r1)\n\t" \
  "wrteei  1                 # Set MSR[EE]=1       (must wait a couple clocks after reading IACKR)\n\t" \
  "stw     r12, 0x4C (r1)    # Store rest of gprs\n\t" \
  "stw     r11, 0x48 (r1)\n\t" \
  "stw     r10, 0x44 (r1)\n\t" \
  "stw     r9,  0x40 (r1)\n\t" \
  "stw     r8,  0x3C (r1)\n\t" \
  "stw     r7,  0x38 (r1)\n\t" \
  "stw     r6,  0x34 (r1)\n\t" \
  "stw     r5,  0x30 (r1)\n\t" \
  "stw     r4,  0x2C (r1)\n\t" \
  "stw     r0,  0x24 (r1)\n\t" \
  "mfcr    r3                 # Store CR\n\t" \
  "stw     r3,  0x20 (r1)\n\t" \
  "mfxer   r3                 # Store XER\n\t" \
  "stw     r3,  0x1C (r1)\n\t" \
  "mfctr   r3                 # Store CTR\n\t" \
  "stw     r3,  0x18 (r1)\n\t" \
  "mflr    r3\n\t" \
  "stw     r3,  0x14 (r1)     # Store LR\n\t" \
  "")

#define INTR_EPILOG \
    __asm__ volatile ("" \
    "lwz     r3,  0x14 (r1)   # Restore LR\n\t" \
    "mtlr    r3\n\t" \
    "lwz     r3,  0x18 (r1)   # Restore CTR\n\t" \
    "mtctr   r3\n\t" \
    "lwz     r3,  0x1C (r1)   # Restore XER\n\t" \
    "mtxer   r3\n\t" \
    "lwz     r3,  0x20 (r1)   # Restore CR\n\t" \
    "mtcrf   0xff, r3\n\t" \
    "lwz     r0,  0x24 (r1)   # Restore other gprs except working registers\n\t" \
    "lwz     r5,  0x30 (r1)\n\t" \
    "lwz     r6,  0x34 (r1)\n\t" \
    "lwz     r7,  0x38 (r1)\n\t" \
    "lwz     r8,  0x3C (r1)\n\t" \
    "lwz     r9,  0x40 (r1)\n\t" \
    "lwz     r10, 0x44 (r1)\n\t" \
    "lwz     r11, 0x48 (r1)\n\t" \
    "lwz     r12, 0x4C (r1)\n\t" \
    "mbar    0                 # Ensure store to clear flag bit has completed\n\t" \
    "                                  # Use one of the following lines:\n\t" \
    "lis     r3, %0@ha # MPC551x: Load upper half of proc'r 0 EIOR addr. to r3\n\t" \
    "#       lis     r3, INTC_EOIR@ha  # MPC555x: Load upper half of EIOR address to r3\n\t" \
    "li      r4, 0           \n\t" \
    "wrteei  0                 # Disable interrupts for rest of handler\n\t" \
    "                                  # Use one of the following lines:\n\t" \
    "stw     r4, %0@l(r3)        # MPC551x: Write 0 to proc'r 0 INTC_EOIR\n\t" \
    "#       stw     r4, INTC_EOIR@l(r3)             # MPC555x: Write 0 to INTC_EOIR \n\t" \
    "lwz     r3,  0x0C (r1)      # Restore SRR0\n\t" \
    "mtsrr0  r3\n\t" \
    "lwz     r3,  0x10 (r1)      # Restore SRR1\n\t" \
    "mtsrr1  r3\n\t" \
    "lwz     r4,  0x2C (r1)      # Restore working registers\n\t" \
    "lwz     r3,  0x28 (r1)      \n\t" \
    "addi    r1, r1, 0x50        # Delete stack frame\n\t" \
    "rfi                         # End of Interrupt\n\t" \
    "": : "i" (INTC_EOIR))

__attribute__ ((naked))
void SwIrq4Handler(void)
{
  INTR_PROLOG;
  SwIrq4ISR();
  INTR_EPILOG;
}
__attribute__ ((naked))
void Pit1Handler(void)
{
  INTR_PROLOG;
  Pit1ISR();
  INTR_EPILOG;
}
#ifdef __cplusplus
}
#endif
