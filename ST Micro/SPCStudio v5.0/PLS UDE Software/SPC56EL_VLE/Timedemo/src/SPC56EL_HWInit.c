/*

  File:     SPC56EL_HWInit.c
  Created:  19.08.2010
  Desc.:
    This file contains all SPC56EL derivative needed initializations,
    and all initializations for the SPC56EL boards which are supported.
    This includes setting up the External Bus Interface to allow access
    to memory on the external bus, and ensuring there is a valid entry in
    the MMU for the external memory access.

  Rev.: 1.0
  
*/


/*----------------------------------------------------------------------------*/
/* Includes                                                                   */
/*----------------------------------------------------------------------------*/

#include <unistd.h>
#include "spc56el.h"        /* SPC56EL platform development header            */
#include "SPC56EL_HWInit.h"

#define STARTUP_SEC   __attribute__((section(".startup")))

#ifdef __cplusplus
extern "C" {
#endif

/*******************************************************/
/* External Memory Locations from lcf file             */
/*******************************************************/
/* Symbol L2SRAM_LOCATION is defined in the application linker command file (.lcf) 
It is defined to the start of the L2SRAM of the SPC56EL.
*/
extern long L2SRAM_LOCATION[];


/*******************************************************/
/* Function declarations                               */
/*******************************************************/
/* Initialize a set of contiguous PCRs */
void InitPCRs(volatile unsigned short *pcr, unsigned short val, unsigned int count);

/* Initialize the SIU External Bus Interface */
void __initSIUExtBusInterface(void);

/* Initialize the used EBI Chip Selects */
void __initEBIChipSelects(void);

/* Write one MMU Table Entry */
void WriteMMUTableEntry(unsigned int MAS0, unsigned int MAS1, unsigned int MAS2, unsigned int MAS3);

/* Initialize the needed MMU Table entries */
void __initMMUExternalMemory(void);


/*******************************************************/
/* Startup initializations                             */
/*******************************************************/
/*
* SPC56EL L2SRAM initialization
*/
ASM_FUNC void INIT_Derivative(void) 
{
  /* Loop counter to get all of L2SRAM; 128k/4 bytes/32 GPRs = 1024 */

  asm volatile ("mtctr %0\n"
  "0:\n"
  "stmw r0,0(%1)\n"          /* Write all 32 GPRs to L2SRAM */
  "addi %1,%1,128\n"         /* Inc the ram ptr; 32 GPRs * 4 bytes = 128 */
  "bdnz 0b\n"    /* Loop for 128k of L2SRAM */
  "blr": : "r" (INTERNAL_RAM_SIZE/(32*4)), "r" (L2SRAM_LOCATION));
}


/*******************************************************/
/* Post startup initializations                        */
/*******************************************************/
/*
* SPC56EL clock and modes initialization
*/
void initModesAndClock(void) 
{
  RGM.FES.R = 0xFFFF;             /* Clear any fault status */
  ME.IS.R = 0x0000000F;

  ME.MER.R = 0x000005FF;          /* Enable DRUN, RUN0, SAFE, RESET modes */

  ME.RUN_PC0.R = 0xFE;            /* Peri. Cfg. for RUN0, DRUN, SAFE modes */
  ME.LP_PC0.R = 0x500;            /* Peri. Cfg. for Low Power */

  ME.DRUN_MC.R = 0x001F0032;      // switch on osscillators

  ME.MCTL.R = 0x30005AF0;         /* Enter DRUN Mode & Key */
  ME.MCTL.R = 0x3000A50F;         /* Enter DRUN Mode & Inverted Key */  
  while (ME.GS.B.S_MTRANS) {}     /* Wait for mode transition to complete */    

  CMU_0.CSR.R = 0x6;
  CMU_0.LFREFR_A.R = 0x1;
  CMU_0.HFREFR_A.R = 0xFFE;

  CGM.AC3_SC.B.SELCTL = 0x1;      /* Set PLL0 ref. clock */
  CGM.AC4_SC.B.SELCTL = 0x1;      /* Set PLL1 ref. clock */

  FMPLL_0.CR.B.IDF = 3;           /* 40 MHz xtal: Set PLL0 to 120 MHz */   
  FMPLL_0.CR.B.ODF = 1;
  FMPLL_0.CR.B.NDIV = 48;
 
  FMPLL_1.CR.B.IDF = 3;           /* 40 MHz xtal: Set PLL0 to 80 MHz */   
  FMPLL_1.CR.B.ODF = 1;
  FMPLL_1.CR.B.NDIV = 32;

  ME.DRUN_MC.R = 0x001F00F2;      // switch on PLLs

  ME.MCTL.R = 0x30005AF0;         /* Enter DRUN Mode & Key */
  ME.MCTL.R = 0x3000A50F;         /* Enter DRUN Mode & Inverted Key */  
  while (ME.GS.B.S_MTRANS) {}     /* Wait for mode transition to complete */    

  CGM.SC_DC0.R = 0x81;            // 40 MHz peripheral set 0

  CGM.AC0_SC.R = 0x08000000;
  CGM.AC0_DC0.R = 0x80;           // 80MHz motor control
  CGM.AC0_DC1.R = 0x83;           // 20MHz Sine Wave Gen

  CGM.AC1_SC.R = 0x08000000;
  CGM.AC1_DC0.R = 0x81;           // 40MHz FlexRay

  CGM.AC2_SC.R = 0x08000000;
  CGM.AC2_DC0.R = 0x81;           // 40MHz FlexCAN

  ME.DRUN_MC.R = 0x001F00F4;      // switch to PLL0 clock
  ME.MCTL.R = 0x30005AF0;         /* Enter DRUN Mode & Key */
  ME.MCTL.R = 0x3000A50F;         /* Enter DRUN Mode & Inverted Key */  
  while (ME.GS.B.S_MTRANS) {}     /* Wait for mode transition to complete */    
  
  CGM.OCDS_SC.R = 0x32;           /* Set and enable output clock, 120/8 = 15MHz */
  CGM.OC_EN.R = 0x1;
  SIUL.PCR22.R = 0x0604;

  SSCM.ERROR.R = 0x2;
}

/*
* Initialize the Periodic Interrupt Timer
*/
void initPIT(void) 
{
  PIT.PITMCR.R = 0x00000001;        /* Enable PIT and configure to stop in debug mode */

  PIT.TIMER1_LDVAL.R = 60000*100;   /* Timeout = 6000000 sysclks x 1sec/60M sysclks = 100 ms */
  PIT.TIMER1_TCTRL.R = 0x000000003; /* Enable PIT1 interrupt & start PIT counting */ 
  INTC.PSR60.R = 0x01;              /* PIT 1 interrupt vector with priority 1 */
}


void disableWatchdog(void) 
{
  SWT.SR.R = 0x0000c520;     /* Write keys to clear soft lock bit */
  SWT.SR.R = 0x0000d928; 
  SWT.CR.R = 0xFF00000A;     /* Clear watchdog enable (WEN) */
}        

extern void IntcHandlerBranchTable(void);
extern void IVOR_table(void);

void initIrqVectors(void)
{
  __asm__ volatile ("mtivpr  %0"::"r" (IntcHandlerBranchTable));
}

extern unsigned int IVOR0trap;
extern unsigned int IVOR1trap;
extern unsigned int IVOR2trap;
extern unsigned int IVOR3trap;
extern unsigned int IVOR4trap;
extern unsigned int IVOR5trap;
extern unsigned int IVOR6trap;
extern unsigned int IVOR7trap;
extern unsigned int IVOR8trap;
extern unsigned int IVOR9trap;
extern unsigned int IVOR10trap;
extern unsigned int IVOR11trap;
extern unsigned int IVOR12trap;
extern unsigned int IVOR13trap;
extern unsigned int IVOR14trap;
extern unsigned int IVOR15trap;
extern unsigned int IVOR32trap;
extern unsigned int IVOR33trap;
extern unsigned int IVOR34trap;

void initINTC(void)
{  
  INTC.BCR.B.VTES_PRC0 = 0;
  INTC.BCR.B.HVEN_PRC0 = 1;       /* Initialize for HW vector mode */

  INTC.IACKR_PRC0.R = (unsigned int)IntcHandlerBranchTable;

  asm volatile ("mtspr   400, %0"::"r" (&IVOR0trap));
  asm volatile ("mtspr   401, %0"::"r" (&IVOR1trap));
  asm volatile ("mtspr   402, %0"::"r" (&IVOR2trap));
  asm volatile ("mtspr   403, %0"::"r" (&IVOR3trap));
  asm volatile ("mtspr   404, %0"::"r" (&IVOR4trap));
  asm volatile ("mtspr   405, %0"::"r" (&IVOR5trap));
  asm volatile ("mtspr   406, %0"::"r" (&IVOR6trap));
  asm volatile ("mtspr   407, %0"::"r" (&IVOR7trap));
  asm volatile ("mtspr   408, %0"::"r" (&IVOR8trap));
  asm volatile ("mtspr   409, %0"::"r" (&IVOR9trap));
  asm volatile ("mtspr   410, %0"::"r" (&IVOR10trap));
  asm volatile ("mtspr   411, %0"::"r" (&IVOR11trap));
  asm volatile ("mtspr   412, %0"::"r" (&IVOR12trap));
  asm volatile ("mtspr   413, %0"::"r" (&IVOR13trap));
  asm volatile ("mtspr   414, %0"::"r" (&IVOR14trap));
  asm volatile ("mtspr   415, %0"::"r" (&IVOR15trap));

  asm volatile ("mtspr   528, %0"::"r" (&IVOR32trap));
  asm volatile ("mtspr   529, %0"::"r" (&IVOR33trap));
  asm volatile ("mtspr   530, %0"::"r" (&IVOR34trap));
}

void initSwIrq4(void) 
{
  INTC.PSR4.R = 2;    /* Software interrupt 4 IRQ priority = 2 */
}

void enableIrq(void) 
{
  INTC.CPR_PRC0.B.PRI = 0;          /* Single Core: Lower INTC's current priority */
  __asm__ volatile(" wrteei 1");           /* Enable external interrupts */
}

/** This macro allows to use C defined address with the inline assembler */
//#define MAKE_HLI_COMPATIBLE(hli_name, c_expr) enum { hli_name=/*lint -e30*/((int)(c_expr)) };
//#define MAKE_HLI_COMPATIBLE(hli_name, c_expr) int hli_name=/*lint -e30*/((int)(c_expr));

#define _STRINGIFY(x) #x
#define MAKE_HLI_COMPATIBLE(name, addr) \
  __asm__ (".set  " #name ", %0":: "i" (addr));   \
  extern int name;


/*******************************************************/
/* Function implementations                            */
/*******************************************************/
#pragma GCC optimize ("Os")
ASM_FUNC void INIT_ExternalBusAndMemory(void)
{
  asm volatile ("mflr     r29");
  
  /* Initialize the SIU External Bus Interface */
  asm volatile ("bl __initSIUExtBusInterface");
  /* Initialize the used EBI Chip Selects */
  asm volatile ("bl __initEBIChipSelects");
  /* Initialize the needed MMU Table entries */
  asm volatile ("bl __initMMUExternalMemory");
  
  asm volatile ("mtlr     r29");
  
  asm volatile ("blr");
}


/*----------------------------------------------------------------------------*/
/* SIU External Bus Interface                                                 */
/*----------------------------------------------------------------------------*/

/* Initialize a set of contiguous PCRs:               */
/* r3: the firts PCR to initialize                    */
/* r4: the value to write in the PCRs                 */ 
/* r5: the number of PCRs to initialize               */
#pragma GCC push_options
#pragma GCC optimize ("Os")
void InitPCRs(volatile unsigned short *pcr, unsigned short val, unsigned int count)
{
  while(count-- > 0)
  *pcr++ = val;
}

/* Initialize the SIU External Bus Interface */
ASM_FUNC void __initSIUExtBusInterface(void)
{ 
  asm volatile ("blr");
}

/*----------------------------------------------------------------------------*/
/* EBI Chip Selects                                                           */
/*----------------------------------------------------------------------------*/

/* Initialize the used EBI Chip Selects */
ASM_FUNC void __initEBIChipSelects(void)
{
  asm volatile ("blr");
}

/*----------------------------------------------------------------------------*/
/* Writing to MMU Table Entries                                               */
/*----------------------------------------------------------------------------*/
/* Write one MMU Table Entry:               */
/* r3, r4, r5 and r6 must hold              */
/* the values of MAS0, MAS1, MAS2 and MAS3  */
ASM_FUNC void WriteMMUTableEntry(unsigned int MAS0,unsigned int MAS1,unsigned int  MAS2,unsigned int MAS3)

{ 
  /* Write MMU Assist Register 0 (MAS0); SPR 624 */
  asm volatile ("mtspr   624, %0"::"r" (MAS0));
  /* Write MMU Assist Register 1 (MAS1); SPR 625 */
  asm volatile ("mtspr   625, %0"::"r" (MAS1));
  /* Write MMU Assist Register 2 (MAS2); SPR 626 */
  asm volatile ("mtspr   626, %0"::"r" (MAS2));
  /* Write MMU Assist Register 3 (MAS3); SPR 627 */
  asm volatile ("mtspr   627, %0"::"r" (MAS3));
  /* Write the table entry */
  asm volatile ("tlbwe");

  asm volatile ("blr");
}

#define SET_PARAMS(a,b,c,d) \
  __asm__ volatile ("" \
  "lis r3,%0@ha \n\t" \
  "ori r3,r3,%0@l \n\t" \
  "lis r4,%1@ha \n\t" \
  "ori r4,r4,%1@l \n\t" \
  "lis r5,%2@ha \n\t" \
  "ori r5,r5,%2@l \n\t" \
  "lis r6,%3@ha \n\t" \
  "ori r6,r6,%3@l"::"i"(a),"i"(b),"i"(c),"i"(d))

#ifdef __PPC_VLE__
#define VLE_FLG     (VLE)
#else
#define VLE_FLG     (NOT_VLE)
#endif

ASM_FUNC void __initMMU(void)
{
  asm volatile ("mflr r29");

  /* select TLB1 */
  SET_PARAMS(0x10000108, 0, 0, 0);
  asm volatile ("mtspr   628, r3");

  /* FLASH entry */
  SET_PARAMS(MAS0_VALUE(0),
  MAS1_VALUE(V_VALID, IPROT_PROTECTED, TID_GLOBAL, TSIZE_16MB),
  MAS2_VALUE(0x00000000, NOT_ACM, VLE_FLG, WRITE_BACK,CACHE_INHIBIT, MEM_COHERENCE_NREQ, NOT_GUARDED, BIG_ENDIAN),
  MAS3_VALUE(0x00000000, READ_WRITE_EXECUTE));
  asm volatile ("bl  WriteMMUTableEntry");

  /* SRAM entry */
  SET_PARAMS(MAS0_VALUE(1),
  MAS1_VALUE(V_VALID, IPROT_PROTECTED, TID_GLOBAL, TSIZE_16MB),
  MAS2_VALUE(0x40000000, NOT_ACM, VLE_FLG, WRITE_BACK, CACHE_INHIBIT, MEM_COHERENCE_NREQ, NOT_GUARDED, BIG_ENDIAN),
  MAS3_VALUE(0x40000000, READ_WRITE_EXECUTE));
  asm volatile ("bl  WriteMMUTableEntry");

  /* On Platform 1 Peripherals */
  SET_PARAMS(MAS0_VALUE(2),
  MAS1_VALUE(V_VALID, IPROT_PROTECTED, TID_GLOBAL, TSIZE_1MB),
  MAS2_VALUE(0x8FF00000, NOT_ACM, NOT_VLE, WRITE_BACK, CACHE_INHIBIT, MEM_COHERENCE_NREQ, NOT_GUARDED, BIG_ENDIAN),
  MAS3_VALUE(0x8FF00000, READ_WRITE_EXECUTE));
  asm volatile ("bl  WriteMMUTableEntry"); 

  /* Peripheral A modules */
  SET_PARAMS(MAS0_VALUE(3),
  MAS1_VALUE(V_VALID, IPROT_PROTECTED, TID_GLOBAL, TSIZE_1MB),
  MAS2_VALUE(0xC3F00000, NOT_ACM, NOT_VLE, WRITE_BACK, CACHE_INHIBIT, MEM_COHERENCE_NREQ, GUARDED, BIG_ENDIAN),
  MAS3_VALUE(0xC3F00000, READ_WRITE_EXECUTE));
  asm volatile ("bl  WriteMMUTableEntry");
  
  /* Off Platform peripherals */
  SET_PARAMS(MAS0_VALUE(4),
  MAS1_VALUE(V_VALID, IPROT_PROTECTED, TID_GLOBAL, TSIZE_1MB),
  MAS2_VALUE(0xFFE00000, NOT_ACM, NOT_VLE, WRITE_BACK, CACHE_INHIBIT, MEM_COHERENCE_NREQ, NOT_GUARDED, BIG_ENDIAN),
  MAS3_VALUE(0xFFE00000, READ_WRITE_EXECUTE)); 
  asm volatile ("bl  WriteMMUTableEntry"); 
  
  /* On Platform 0 Peripherals */
  SET_PARAMS(MAS0_VALUE(5),
  MAS1_VALUE(V_VALID, IPROT_PROTECTED, TID_GLOBAL, TSIZE_1MB),
  MAS2_VALUE(0xFFF00000, NOT_ACM, VLE_FLG, WRITE_BACK, CACHE_INHIBIT, MEM_COHERENCE_NREQ, NOT_GUARDED, BIG_ENDIAN),
  MAS3_VALUE(0xFFF00000, READ_WRITE_EXECUTE)); 
  asm volatile ("bl  WriteMMUTableEntry");

  asm volatile ("mtlr r29");

  asm volatile ("blr");
}

/* Initialize needed MMU Table entries */
ASM_FUNC void __initMMUExternalMemory(void)
{
  asm volatile ("blr");
}

#ifdef __cplusplus
}
#endif
