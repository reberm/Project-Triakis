/*

  File:     main.c
  Created:  19.08.2010
  Desc.:
    Hardware vector mode program using C isr

  Rev.: 1.0

*/

#include "spc56el.h" /* Use proper include file */
#include "SPC56EL_HWInit.h"

uint32_t Pit1Ctr = 0;   /* Counter for PIT 1 interrupts */
uint32_t SWirq4Ctr = 0; /* Counter for software interrupt 4 */

//SIUL_PCR register masks
#define PCR_OBE_MASK 0x0200             // Output Enable
#define PCR_ODE_MASK 0x0020             // Open Drain Enable
#define PCR_PA_ALTERNATE1_MASK 0x0400   // Alternate function 1
#define PCR_IBE_MASK 0x0100             // Input Enable
#define PCR_HYS_MASK 0x0010             // Hysteresis Enable

//***************
//*  init_LEDs  *
//***************
//Configures PC12 to PC15 for PWM output
void init_LEDs(void)
{
  LED1_CR = (PCR_OBE_MASK);
  LED2_CR = (PCR_OBE_MASK);
  LED3_CR = (PCR_OBE_MASK);
  LED4_CR = (PCR_OBE_MASK);

  LED1_DR = 1;
  LED2_DR = 1;
  LED3_DR = 1;
  LED4_DR = 1;
}

unsigned char Buffer[10];
unsigned char Seconds;
unsigned char Minutes;
unsigned char Hours;
unsigned int volatile byTimeFlag;

volatile __attribute__((vector_size(8))) float vd0={1,1},vd1={1,1},vd2={1,1};

unsigned int volatile uiDummyAddressA;

void ExecuteDmaTransfer()
{
  // program eDMA to transfer STM_0_CR to dummy location
  EDMA.TCD00_SADDR.R = (unsigned int)&STM.CNT;
  EDMA.TCD00_M1.B.SMOD = 0;
  EDMA.TCD00_M1.B.SSIZE = 2; // 32 Bit
  EDMA.TCD00_M1.B.DMOD = 0;
  EDMA.TCD00_M1.B.DSIZE = 2; // 32 Bit
  EDMA.TCD00_M1.B.SOFF = 0;
  EDMA.TCD00_NBYTES.R = 4;
  EDMA.TCD00_DADDR.R = (unsigned int)&uiDummyAddressA;
  EDMA.TCD00_M2.B.DOFF = 0;
  EDMA.TCD00_M2.B.CITER = 1; // 1 major loop
  EDMA.TCD00_M3.B.D_REG = 1;
  EDMA.TCD00_M3.B.START = 1;
}

//volatile  int vd0=1,vd1=1,vd2=1;
int main (void) 
{
  vuint32_t i = 0;

  disableWatchdog();    /* Disable watchdog */
  initModesAndClock();  /* Initialize mode entries, set sysclk */
  initIrqVectors();     /* Initialize exceptions: only need to load IVPR */
  initINTC();           /* Initialize INTC for software vector mode */
  initPIT();            /* Initialize PIT1 for 1KHz IRQ, priority 2 */
  initSwIrq4();         /* Initialize software interrupt 4 */
  init_LEDs();
  STM.CR.B.TEN = 1;   // start STM
  STM.CR.B.FRZ = 1;
  // make DMA a trusted master
  AIPS_LITE.MPROT0.B.MTR2 = 1;
  AIPS_LITE.MPROT0.B.MTW2 = 1;
  AIPS_LITE.MPROT0.B.MPL2 = 1;
  
  enableIrq();          /* Ensure INTC current prority=0 & enable IRQ */

  while (1) 
  {
    if (byTimeFlag == 1) 
    {
      ExecuteDmaTransfer();
      byTimeFlag = 0;
      Buffer[i % 10] = i;
      if (0 == ++i % 10)
      {
        // update timer
        if (59 == Seconds) 
        {
          Seconds = 0;
          if (59 == Minutes) 
          {
            Minutes = 0;
            if (23 == Hours)
            {
              Hours = 0;
            } 
            else 
            {
              Hours++;
            }
          } 
          else 
          {
            Minutes++;
          }
        }
        else 
        {
          Seconds++;
        }
      }
      /* SPE sample */
      vd1 += vd2;
      vd2 = (vd1 / vd2);
    }
  }
}

void Pit1ISR(void)
{
  Pit1Ctr++;                  /* Increment interrupt counter */
  INTC.SSCIR4.R = 2;          /*  then invoke software interrupt 4 */
  PIT.TIMER1_TFLG.B.TIF = 1;  /* MPC56xxP/B/S: CLear PIT 1 flag by writing 1 */
}
void SwIrq4ISR(void)
{
  SWirq4Ctr++;          /* Increment interrupt counter */
  LED1_DR = (SWirq4Ctr & 1)?1:0;
  LED2_DR = (SWirq4Ctr & 2)?1:0;
  LED3_DR = (SWirq4Ctr & 4)?1:0;
  LED4_DR = (SWirq4Ctr & 8)?1:0;
  byTimeFlag = 1;
  INTC.SSCIR4.R = 1;    /* Clear channel's flag */
}

