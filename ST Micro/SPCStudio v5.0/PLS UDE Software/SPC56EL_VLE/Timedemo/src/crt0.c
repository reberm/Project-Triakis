/*

  File:     crt0.c
  Created:  19.08.2010

  Rev.: 1.0
  
*/

#include <spc56el.h>
#include <string.h>

#define ASM_FUNC      __attribute__((naked))
#define STARTUP_SEC   __attribute__((section(".startup")))

/***************************************************************************/
/*
*  external declarations
*/
/***************************************************************************/
#ifdef __cplusplus
extern "C" {
#endif

extern int main(int,char **,char**);
extern void usr_init(void);
extern void initINTC(void);
extern void initIrqVectors(void);

#ifdef __cpluspluc
}
#endif

/***************************************************************************/
/*
*  function declarations
*/
/***************************************************************************/

#ifdef __cplusplus
extern "C" {
#endif

  void data_init(void);
  extern void exit(int);
  void IVOR_table(void);

  STARTUP_SEC ASM_FUNC void init_registers(void);
#ifdef __cplusplus
}
#endif

/***************************************************************************/
/*
*  _start
*
*  PowerPC EABI Runtime Initialization.  Initialize pointers,
*  initialize data, and call main().
*
*  This function is the very first location executed in a program, or
*  the first location called by the board-level initialization.
*  Memory access is not guaranteed to be safe until after usr_init().
*/
/***************************************************************************/
STARTUP_SEC ASM_FUNC void _start()
{
  /*
  *  Set the MSR[SPE] 
  */
  asm volatile("mfmsr %r3");
  asm volatile("oris  %r3, %r3, 0x00000200");
  asm volatile("mtmsr %r3");

  /*
  * Init stack pointer and small data pointers
  */
  init_registers();

  /*
  *  board-level initialization
  */
  usr_init();
  /* Memory access is safe now. */

  /*
  *  Prepare a terminating stack record.
  */
#if 0
  asm volatile ("stwu %r1, -16(%r1)");    /* e500 required SP to always be 16-byte aligned */
#else
  asm volatile ("stwu %r1, -8(%r1)");   /* required SP to always be 8-byte aligned */
#endif

  asm volatile ("li %r0, 0x0000");    /* load up r0 with 0x00000000 */
  asm volatile ("stw  %r0,  0(%r1)"); /* SysVr4 EABI Supp indicated that initial back */
  asm volatile ("li %r0, -1");      /* load up r0 with 0xFFFFFFFF */
  asm volatile ("stw  %r0, 4(%r1)");    /* Make an illegal return address of 0xFFFFFFFF */
  
  /*
  * set up the trap vector
    */
  asm volatile("mtivpr %0":: "r" (IVOR_table));
  
  /*
  *  Data initialization: copy ROM data to RAM as necessary
  */
  data_init();

  /*
  *  Put here your initializations before main
  */

  /*
  *  branch to main program
  */
  exit(main(0, 0, 0));
}

#pragma weak init_register
STARTUP_SEC ASM_FUNC void init_registers(void)
{
  asm volatile(" li %r0,0");
  asm volatile(" li %r1,0");
  asm volatile(" li %r2,0");
  asm volatile(" li %r3,0");
  asm volatile(" li %r4,0");
  asm volatile(" li %r5,0");
  asm volatile(" li %r6,0");
  asm volatile(" li %r7,0");
  asm volatile(" li %r8,0");
  asm volatile(" li %r9,0");
  asm volatile(" li %r10,0");
  asm volatile(" li %r11,0");
  asm volatile(" li %r12,0");
  asm volatile(" li %r13,0");
  asm volatile(" li %r14,0");
  asm volatile(" li %r15,0");
  asm volatile(" li %r16,0");
  asm volatile(" li %r17,0");
  asm volatile(" li %r18,0");
  asm volatile(" li %r19,0");
  asm volatile(" li %r20,0");
  asm volatile(" li %r21,0");
  asm volatile(" li %r22,0");
  asm volatile(" li %r23,0");
  asm volatile(" li %r24,0");
  asm volatile(" li %r25,0");
  asm volatile(" li %r26,0");
  asm volatile(" li %r27,0");
  asm volatile(" li %r28,0");
  asm volatile(" li %r29,0");
  asm volatile(" li %r30,0");
  asm volatile(" li %r31,0");

  asm volatile("mtspr 400,%r30");
  asm volatile("mtspr 401,%r30");
  asm volatile("mtspr 402,%r30");
  asm volatile("mtspr 403,%r30");
  asm volatile("mtspr 404,%r30");
  asm volatile("mtspr 405,%r30");
  asm volatile("mtspr 406,%r30");
  asm volatile("mtspr 407,%r30");
  asm volatile("mtspr 408,%r30");
  asm volatile("mtspr 409,%r30");
  asm volatile("mtspr 410,%r30");
  asm volatile("mtspr 411,%r30");
  asm volatile("mtspr 412,%r30");
  asm volatile("mtspr 413,%r30");
  asm volatile("mtspr 414,%r30");
  asm volatile("mtspr 415,%r30");
  
  asm volatile("mtspr 528,%r30");
  asm volatile("mtspr 529,%r30");
  asm volatile("mtspr 530,%r30");

  /*  asm volatile("mtvscr vr0"); */
  asm volatile("mtcrf 0xFF,%r31");
  asm volatile("mtspr 284,%r31");  /* TBL */
  asm volatile("mtspr 285,%r31");  /* TBU */
  asm volatile("mtspr 272,%r31");  /* SPRG0-7 */
  asm volatile("mtspr 273,%r31");
  asm volatile("mtspr 274,%r31");
  asm volatile("mtspr 275,%r31");
  asm volatile("mtspr 276,%r31");
  asm volatile("mtspr 277,%r31");
  asm volatile("mtspr 278,%r31");
  asm volatile("mtspr 279,%r31");
  asm volatile("mtspr 604,%r31");  /* SPRG8-9 */
  asm volatile("mtspr 605,%r31");
  asm volatile("mtspr 26,%r31");  /* SRR0-1 */
  asm volatile("mtspr 27,%r31");
  asm volatile("mtspr 58,%r31");  /* CSRR0-1 */
  asm volatile("mtspr 59,%r31");
  asm volatile("mtspr 63,%r31");  /* IVPR */
  asm volatile("mtspr 61,%r31");  /* DEAR */
  asm volatile("mtspr 62,%r31");  /* ESR */
  asm volatile("mtspr 570,%r31"); /* MCSRR0 */
  asm volatile("mtspr 571,%r31"); /* MCSRR1 */
  asm volatile("mtspr 572,%r31"); /* MCSR */
  asm volatile("mtspr 573,%r31");
  asm volatile("mtspr 574,%r31"); /* DSRR0 */
  asm volatile("mtspr 575,%r31"); /* DSRR1 */
  asm volatile("mtspr 340,%r31"); /* TCR */
  asm volatile("mtspr 336,%r31"); /* TSR */
  asm volatile("mtspr 512,%r31"); /* SPEFSCR */

  asm volatile("mtspr 1,%r31");   /* XER */
  asm volatile("mtspr 256,%r31"); /* USPRG0 */
  asm volatile("mtspr 9,%r31");   /* CTR */
  //    asm volatile("mtspr 8,r31");  /* LR */
  asm volatile("mtspr 308,%r31"); /* DBCR0-6 */
  asm volatile("mtspr 309,%r31");
  asm volatile("mtspr 310,%r31");
  asm volatile("mtspr 561,%r31");
  asm volatile("mtspr 563,%r31");
  asm volatile("mtspr 564,%r31");
  asm volatile("mtspr 603,%r31");

  /*
  *  initialize stack pointer
  */
  asm volatile ("lis      %r1, _stack_addr@ha");
  /* _stack_addr is generated by linker */
  asm volatile ("addi     %r1, %r1, _stack_addr@l");

  /*
  *  initialize small data area pointers (EABI)
  */
  asm volatile ("lis  %r2, _SDA2_BASE_@ha");
  /* _SDA2_BASE_ is generated by linker */
  asm volatile ("addi %r2, %r2, _SDA2_BASE_@l");

  asm volatile ("lis  %r13, _SDA_BASE_@ha");
  /* _SDA_BASE_ is generated by linker */
  asm volatile ("addi %r13, %r13, _SDA_BASE_@l");
  asm volatile ("blr");

}
/**************************************************************************/
/* branch table for the 16 MPC56xx core interrupts/traps */
/**************************************************************************/
#ifdef __PPC_VLE__
#define TRAP(n) \
  __asm__ volatile (".global IVOR"#n"trap"); \
  __asm__ volatile (".align 4"); \
  __asm__ volatile ("IVOR"#n"trap:       se_illegal\n\tb  IVOR"#n"trap");
#else
#define TRAP(n) \
  __asm__ volatile (".global IVOR"#n"trap"); \
  __asm__ volatile (".align 4"); \
  __asm__ volatile ("IVOR"#n"trap:       b  IVOR"#n"trap");
#endif

__attribute__((section(".traptable"),naked))
void IVOR_table(void)
{
  TRAP(0)
  TRAP(1)
  TRAP(2)
  TRAP(3)
  TRAP(4)
  TRAP(5)
  TRAP(6)
  TRAP(7)
  TRAP(8)
  TRAP(9)
  TRAP(10)
  TRAP(11)
  TRAP(12)
  TRAP(13)
  TRAP(14)
  TRAP(15)
  TRAP(32)
  TRAP(33)
  TRAP(34)
}

/***************************************************************************/
/*
  
*/
/***************************************************************************/


/***************************************************************************/
/*
*  __copy_rom_section
*
*  Copy the ROM section to RAM if dst and src are different and size
*  is nonzero.
*
*  dst     destination RAM address
*  src     source ROM address
*  size    number of bytes to copy
*/
/***************************************************************************/
extern void __copy_rom_section(void* dst, const void* src, unsigned long size)
{
  if (size && (dst != src)) {
    memcpy(dst, src, size);
    //    __flush_cache( dst, size );
  }
}


/***************************************************************************/
/*
*  __init_bss_section
*
*  Initialize the RAM section to zeros if size is greater than zero.
*
*  dst     destination RAM address
*  size    number of bytes to zero
*/
/***************************************************************************/
extern void __init_bss_section(void* dst, unsigned long size)
{
  if (size) {
    __builtin_memset(dst, 0, size);
  }
}

/***************************************************************************/
/*
*  data_init
*
*  Initialize all (RAM) data sections, copying ROM sections as necessary.
*
*  dst     destination RAM address
*  size    number of bytes to zero
*
*  we now overload the function in the event that the user wants pic pid
*
*/
/***************************************************************************/
typedef struct {
  char *  rom_addr; /* address in ROM */
  char *  ram_addr; /* address in RAM */
  unsigned int size;  /* size of section */
} __rom_copy_info_t;

typedef struct {
  char *  ram_addr; /* address in RAM */
  unsigned int size;  /* size of section */
} __bss_init_info_t;

extern __rom_copy_info_t  __rom_copy_info[];
extern __bss_init_info_t  __bss_init_info[];

void
data_init(void)
{
  __rom_copy_info_t *prci;
  __bss_init_info_t *pbii;

  /* copy initialized section to RAM */
  for (prci = __rom_copy_info; prci->rom_addr != 0 || prci->ram_addr != 0
  || prci->size != 0; prci++)
  {
    __copy_rom_section(prci->ram_addr, prci->rom_addr, prci->size);
  }

  for (pbii = __bss_init_info; pbii->ram_addr != 0 || pbii->size != 0; pbii++)
  {
    __init_bss_section(pbii->ram_addr, pbii->size);
  }
}

int exit_status = -1;

#pragma weak _exit
void _exit(int i)
{
  exit_status = i;
#ifdef __PPC_VLE__
  asm volatile ("se_illegal");
#endif
  while(1);
}
void exit(int i)
{
  _exit(i);
}

void abort(void)
{
  exit_status = -127;
#ifdef __PPC_VLE__
  asm volatile ("se_illegal");
#endif
  while(1);
}

#pragma weak write
int write (int d, char *bp, int len)
{
  d = d;
  bp = bp;
  return len;
}
