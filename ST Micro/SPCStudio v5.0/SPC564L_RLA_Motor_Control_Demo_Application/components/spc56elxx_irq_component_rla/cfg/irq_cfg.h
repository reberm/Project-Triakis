/*
    SPC5 RLA - Copyright (C) 2015 STMicroelectronics

    Licensed under the Apache License, Version 2.0 (the "License").
    You may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
    Portion of the code in this module have been taken from the ChibiOS
    project (ChibiOS - Copyright (C) 2006..2015) licensed under Apache
    2.0 license.
*/

/**
 * @file    irq_cfg.h
 * @brief   IRQ configuration macros and structures.
 *
 * @addtogroup IRQ
 * @{
 */

#ifndef _IRQ_CFG_H_
#define _IRQ_CFG_H_

/*===========================================================================*/
/* Module constants.                                                         */
/*===========================================================================*/

/**
 * @name    Common constants
 * @{
 */
#if !defined(FALSE) || defined(__DOXYGEN__)
#define FALSE                       0U
#endif

#if !defined(TRUE) || defined(__DOXYGEN__)
#define TRUE                        1U
#endif

/**
 * @name    ISR vectors identifiers
 * @{
 */
#define ISR_VECTOR_SYSTASKS                 30
#define ISR_VECTOR_SPD_ENCODER_INDEX        169
#define ISR_VECTOR_ADC_DMA_CTU_END_CONV     11
#define ISR_VECTOR_SPD_HALL_S1              157
#define ISR_VECTOR_SPD_HALL_S2              159
#define ISR_VECTOR_SPD_HALL_S3              158
#define ISR_VECTOR_ADC_USER_ISR             31
#define ISR_VECTOR_ADC_DMA_CTU_END_CONV_USER_CONV 14
/** @} */

/**
 * @name    ISR vectors names overrides
 * @{
 */
#define vector30                            SysTasks_Handler_Vector
#define vector169                           SPD_eTIMER1_TC1IR_IRQHandler_Vector
#define vector11                            CTU_FIF0_1_DMAComplete_IRQHandler_Vector
#define vector157                           SPD_eTIMER_InputCapture_HALL_S1_IRQHandler_Vector
#define vector159                           SPD_eTIMER_InputCapture_HALL_S2_IRQHandler_Vector
#define vector158                           SPD_eTIMER_InputCapture_HALL_S3_IRQHandler_Vector
#define vector31                            ADC_STM_User_Vector
#define vector14                            CTU_FIF0_2_DMAComplete_IRQHandler_Vector
/** @} */

/*===========================================================================*/
/* Module pre-compile time settings.                                         */
/*===========================================================================*/

/*===========================================================================*/
/* Derived constants and error checks.                                       */
/*===========================================================================*/

/*===========================================================================*/
/* Module data structures and types.                                         */
/*===========================================================================*/

/*===========================================================================*/
/* Module macros.                                                            */
/*===========================================================================*/

/*===========================================================================*/
/* External declarations.                                                    */
/*===========================================================================*/

#if !defined(_FROM_ASM_)
#ifdef __cplusplus
extern "C" {
#endif
  void SysTasks_Handler_Vector(void);
  void SPD_eTIMER1_TC1IR_IRQHandler_Vector(void);
  void CTU_FIF0_1_DMAComplete_IRQHandler_Vector(void);
  void SPD_eTIMER_InputCapture_HALL_S1_IRQHandler_Vector(void);
  void SPD_eTIMER_InputCapture_HALL_S2_IRQHandler_Vector(void);
  void SPD_eTIMER_InputCapture_HALL_S3_IRQHandler_Vector(void);
  void ADC_STM_User_Vector(void);
  void CTU_FIF0_2_DMAComplete_IRQHandler_Vector(void);
  void irq_cfg_init(void);
#ifdef __cplusplus
}
#endif
#endif /* !defined(_FROM_ASM_) */

#endif /* _IRQ_CFG_H_ */

/** @} */
