/*
    SPC5 RLA - Copyright (C) 2015 STMicroelectronics

    Licensed under the Apache License, Version 2.0 (the "License").
    You may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
    Portion of the code in this module have been taken from the ChibiOS
    project (ChibiOS - Copyright (C) 2006..2015) licensed under Apache
    2.0 license.
*/

/**
 * @file    irq_cfg.c
 * @brief   IRQ configuration code.
 *
 * @addtogroup IRQ
 * @{
 */

#include "irq.h"

#include "Spc5xxx_MC_it.h"

/*===========================================================================*/
/* Module local definitions.                                                 */
/*===========================================================================*/

/*===========================================================================*/
/* Module exported variables.                                                */
/*===========================================================================*/

/*===========================================================================*/
/* Module local types.                                                       */
/*===========================================================================*/

/*===========================================================================*/
/* Module local variables.                                                   */
/*===========================================================================*/

/*===========================================================================*/
/* Module local functions.                                                   */
/*===========================================================================*/

/*===========================================================================*/
/* Module ISRs.                                                              */
/*===========================================================================*/

/*
 * SYSTASKS ISR function (vector 30).
 */
IRQ_HANDLER(SysTasks_Handler_Vector) {

  IRQ_PROLOGUE();

  /* ISR code here.*/
  SysTasks_Handler();

  IRQ_EPILOGUE();
}

/*
 * SPD_ENCODER_INDEX ISR function (vector 169).
 */
IRQ_HANDLER(SPD_eTIMER1_TC1IR_IRQHandler_Vector) {

  IRQ_PROLOGUE();

  /* ISR code here.*/
  SPD_eTIMER1_TC1IR_IRQHandler();

  IRQ_EPILOGUE();
}

/*
 * ADC_DMA_CTU_END_CONV ISR function (vector 11).
 */
IRQ_HANDLER(CTU_FIF0_1_DMAComplete_IRQHandler_Vector) {

  IRQ_PROLOGUE();

  /* ISR code here.*/
  CTU_FIF0_1_DMAComplete_IRQHandler();

  IRQ_EPILOGUE();
}

/*
 * SPD_HALL_S1 ISR function (vector 157).
 */
IRQ_HANDLER(SPD_eTIMER_InputCapture_HALL_S1_IRQHandler_Vector) {

  IRQ_PROLOGUE();

  /* ISR code here.*/
  SPD_eTIMER_InputCapture_HALL_S1_IRQHandler();

  IRQ_EPILOGUE();
}

/*
 * SPD_HALL_S2 ISR function (vector 159).
 */
IRQ_HANDLER(SPD_eTIMER_InputCapture_HALL_S2_IRQHandler_Vector) {

  IRQ_PROLOGUE();

  /* ISR code here.*/
  SPD_eTIMER_InputCapture_HALL_S2_IRQHandler();

  IRQ_EPILOGUE();
}

/*
 * SPD_HALL_S3 ISR function (vector 158).
 */
IRQ_HANDLER(SPD_eTIMER_InputCapture_HALL_S3_IRQHandler_Vector) {

  IRQ_PROLOGUE();

  /* ISR code here.*/
  SPD_eTIMER_InputCapture_HALL_S3_IRQHandler();

  IRQ_EPILOGUE();
}

/*
 * ADC_USER_ISR ISR function (vector 31).
 */
IRQ_HANDLER(ADC_STM_User_Vector) {

  IRQ_PROLOGUE();

  /* ISR code here.*/
  ADC_STM_User_Handler();

  IRQ_EPILOGUE();
}

/*
 * ADC_DMA_CTU_END_CONV_USER_CONV ISR function (vector 14).
 */
IRQ_HANDLER(CTU_FIF0_2_DMAComplete_IRQHandler_Vector) {

  IRQ_PROLOGUE();

  /* ISR code here.*/
  CTU_FIF0_2_DMAComplete_IRQHandler();

  IRQ_EPILOGUE();
}

/*===========================================================================*/
/* Module exported functions.                                                */
/*===========================================================================*/

/**
 * @brief   Generated initialization code.
 *
 * @special
 */
void irq_cfg_init(void) {

  INTC_PSR(ISR_VECTOR_SYSTASKS) = INTC_PSR_ENABLE(INTC_PSR_MAINCORE, 14);
  INTC_PSR(ISR_VECTOR_SPD_ENCODER_INDEX) = INTC_PSR_ENABLE(INTC_PSR_MAINCORE, 13);
  INTC_PSR(ISR_VECTOR_ADC_DMA_CTU_END_CONV) = INTC_PSR_ENABLE(INTC_PSR_MAINCORE, 15);
  INTC_PSR(ISR_VECTOR_SPD_HALL_S1) = INTC_PSR_ENABLE(INTC_PSR_MAINCORE, 12);
  INTC_PSR(ISR_VECTOR_SPD_HALL_S2) = INTC_PSR_ENABLE(INTC_PSR_MAINCORE, 12);
  INTC_PSR(ISR_VECTOR_SPD_HALL_S3) = INTC_PSR_ENABLE(INTC_PSR_MAINCORE, 12);
  INTC_PSR(ISR_VECTOR_ADC_USER_ISR) = INTC_PSR_ENABLE(INTC_PSR_MAINCORE, 10);
  INTC_PSR(ISR_VECTOR_ADC_DMA_CTU_END_CONV_USER_CONV) = INTC_PSR_ENABLE(INTC_PSR_MAINCORE, 15);
}

/** @} */
