/*
    SPC5 RLA - Copyright (C) 2017 STMicroelectronics

    Licensed under the Apache License, Version 2.0 (the "License").
    You may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

/**
 * @file    ivor.s
 * @brief   Kernel ISRs.
 *
 * @addtogroup PPC_CORE
 * @{
 */

/*
 * Imports the configuration headers.
 */
#define _FROM_ASM_
#include "platform.h"
#include "irq_cfg.h"
#include "osal_cfg.h"
#include "ivor_cfg.h"
#include "boot.h"

#if !defined(__DOXYGEN__)

#if (OSAL_OS_ENABLE == FALSE)

/* Context save.*/
.macro SAVE_CONTEXT
        e_stwu      sp, -80(sp)             /* Size of the extctx structure.*/
        e_stmvsrrw  8(sp)                   /* Saves PC, MSR (SRR0, SRR1).  */
        e_stmvsprw  16(sp)                  /* Saves CR, LR, CTR, XER.      */
        e_stmvgprw  32(sp)                  /* Saves GPR0, GPR3...GPR12.    */
.endm

/* Context restore.*/
.macro RESTORE_CONTEXT
        e_lmvgprw   32(sp)                  /* Restores GPR0, GPR3...GPR12.  */
        e_lmvsprw   16(sp)                  /* Restores CR, LR, CTR, XER.    */
        e_lmvsrrw   8(sp)                   /* Restores PC, MSR (SRR0, SRR1).*/
        e_addi      sp, sp, 80              /* Back to the previous frame.   */
.endm

/* Context save for critical class interrupt.*/
.macro SAVE_CONTEXT_CRITICAL
        e_stwu      sp, -80(sp)             /* Size of the extctx structure.*/
        e_stmvcsrrw  8(sp)                  /* Saves PC, MSR (CSRR0, CSRR1).*/
        e_stmvsprw  16(sp)                  /* Saves CR, LR, CTR, XER.      */
        e_stmvgprw  32(sp)                  /* Saves GPR0, GPR3...GPR12.    */
.endm

/* Context restore for critical class interrupt.*/
.macro RESTORE_CONTEXT_CRITICAL
        e_lmvgprw   32(sp)                  /* Restores GPR0, GPR3...GPR12.    */
        e_lmvsprw   16(sp)                  /* Restores CR, LR, CTR, XER.      */
        e_lmvcsrrw   8(sp)                  /* Restores PC, MSR (CSRR0, CSRR1).*/
        e_addi      sp, sp, 80              /* Back to the previous frame.     */
.endm

/*
 * Prepare SRR0 and SRR1 registers as callback's parameters, moreover do some stuff
 * to calculate the returning address after handling IVOR exceptions, to avoid the
 * exception is raised again. The returning address is the next instruction of SRR0.
 */
.macro LOAD_SSR0_SSR1_PLUS
		mfspr 		%r5,26				/* Load SRR0 -> r5 */
		se_mr		%r3,%r5				/* Prepare 1st parameter (SRR0) for the callback. */
		mfspr		%r4,27				/* Prepare 2nd parameter (SRR1) for the callback. */
        se_lhz 		%r6,0(%r5)			/* Determine opcode @ SRR0 */
        e_andi. 	%r7,%r6,0x9000
        e_cmpli 	0x0,%r7,0x1000		/* Check bit 31,28 only */

		se_bc		0x1,0x0,0x4			/* Skip next instruction if CR is true. */
        se_addi 	%r5,2				/* 0xx1 => 32 bit */

        se_addi 	%r5,2				/* All others just 16 bit long */

        e_stw       %r5, 8(sp)			/* Override the SRR0 previously saved in stack with the new one */
.endm

/*
 * Prepare SRR0 and SRR1 registers as callback's parameters.
 */
.macro LOAD_SSR0_SSR1
		mfspr 		%r3,26				/* Prepare 1st parameter (SRR0) for the callback. */
		mfspr		%r4,27				/* Prepare 2nd parameter (SRR1) for the callback. */
.endm

/*
 * Prepare CSRR0 and CSRR1 registers as callback's parameters.
 */
.macro LOAD_CSSR0_CSSR1
		mfspr 		%r3,58				/* Prepare 1st parameter (CSRR0) for the callback. */
		mfspr		%r4,59				/* Prepare 2nd parameter (CSRR1) for the callback. */
.endm

        .section    .handlers, "axv"
_VLE

        /*
         * _IVOR0 handler (Critical input Interrupt).
         */
#if IVOR0_HANDLER
        .align      4
        .globl      _IVOR0
        .type       _IVOR0, @function
_IVOR0:
        SAVE_CONTEXT_CRITICAL

		LOAD_CSSR0_CSSR1

		e_bl		_ivor0_callback		/* Branch to the proper callback*/

        RESTORE_CONTEXT_CRITICAL
        se_rfci
#endif

        /*
         * _IVOR1 handler (Machine Check Interrupt).
         */
#if IVOR1_HANDLER
        .align      4
        .globl      _IVOR1
        .type       _IVOR1, @function
_IVOR1:
        SAVE_CONTEXT_CRITICAL

		LOAD_CSSR0_CSSR1

		e_bl		_ivor1_callback		/* Branch to the proper callback*/

        RESTORE_CONTEXT_CRITICAL
        se_rfci
#endif

        /*
         * _IVOR2 handler (Data Storage Interrupt).
         */
#if IVOR2_HANDLER
        .align      4
        .globl      _IVOR2
        .type       _IVOR2, @function
_IVOR2:
        SAVE_CONTEXT

		LOAD_SSR0_SSR1_PLUS

		e_bl		_ivor2_callback		/* Branch to the proper callback*/

        RESTORE_CONTEXT
        se_rfi
#endif

        /*
         * _IVOR3 handler (Instruction Storage Interrupt).
         */
#if IVOR3_HANDLER
        .align      4
        .globl      _IVOR3
        .type       _IVOR3, @function
_IVOR3:
        SAVE_CONTEXT

		LOAD_SSR0_SSR1_PLUS

		e_bl		_ivor3_callback		/* Branch to the proper callback*/

        RESTORE_CONTEXT
        se_rfi
#endif

#if (CORE_SUPPORTS_DECREMENTER == 1)
        /*
         * _IVOR10 handler (Book-E decrementer).
         */
        .align      4
        .globl      _IVOR10
        .type       _IVOR10, @function
_IVOR10:
#if (BOOT_CORE0 == 1)
_C0_IVOR10:
#endif
        SAVE_CONTEXT

        /* Reset DIE bit in TSR register.*/
        e_lis       %r3, 0x0800             /* DIS bit mask.                */
        mtspr       336, %r3                /* TSR register.                */

        /* Restoring pre-IRQ MSR register value.*/
        mfSRR1      %r0

        /* No preemption, keeping EE disabled.*/
        se_bclri    %r0, 16                 /* EE = bit 16.                 */
        mtMSR       %r0

        RESTORE_CONTEXT
        se_rfi
#endif /* CORE_SUPPORTS_DECREMENTER */

        /*
         * _IVOR4 handler (Book-E external interrupt).
         */
        .align      4
        .globl      _IVOR4
        .type       _IVOR4, @function
_IVOR4:
        SAVE_CONTEXT

        /* Software vector address from the INTC register.*/
        e_lis       %r3, HI(INTC_IACKR_BASE) /*IACKR register address.      */
        e_or2i      %r3, LO(INTC_IACKR_BASE)
        se_lwz      %r3, 0(%r3)             /* IACKR register value.        */
        se_lwz      %r3, 0(%r3)
        se_mtCTR    %r3                     /* Software handler address.    */

        /* Restoring pre-IRQ MSR register value.*/
        mfSRR1      %r0
        /* No preemption, keeping EE disabled.*/
        se_bclri    %r0, 16                 /* EE = bit 16.                 */
        mtMSR       %r0

#if (OSAL_ENABLE_IRQ_PREEMPTION == TRUE)
        /* Allows preemption while executing the software handler.*/
        wrteei      1
#endif /* OSAL_ENABLE_IRQ_PREEMPTION */

        /* Executes the software handler.*/
        se_bctrl

#if (OSAL_ENABLE_IRQ_PREEMPTION == TRUE)
        /* Prevents preemption again.*/
        wrteei      0
#endif /* OSAL_ENABLE_IRQ_PREEMPTION */

        /* Informs the INTC that the interrupt has been served.*/
        mbar        0
        e_lis       %r3, HI(INTC_EOIR_BASE)
        e_or2i      %r3, LO(INTC_EOIR_BASE)
        se_stw      %r3, 0(%r3)             /* Writing any value should do. */

        RESTORE_CONTEXT
        se_rfi

#if (BOOT_CORE0 == 1)
        /*
         * _C0_IVOR4 handler (Book-E external interrupt).
         */
        .align      4
        .globl      _C0_IVOR4
        .type       _C0_IVOR4, @function
_C0_IVOR4:
        SAVE_CONTEXT

        /* Software vector address from the INTC register.*/
        e_lis       %r3, HI(INTC_IACKR_BASE_0) /*IACKR register address.    */
        e_or2i      %r3, LO(INTC_IACKR_BASE_0)
        se_lwz      %r3, 0(%r3)             /* IACKR register value.        */
        se_lwz      %r3, 0(%r3)
        se_mtCTR    %r3                     /* Software handler address.    */

        /* Restoring pre-IRQ MSR register value.*/
        mfSRR1      %r0
        /* No preemption, keeping EE disabled.*/
        se_bclri    %r0, 16                 /* EE = bit 16.                 */
        mtMSR       %r0

#if (OSAL_ENABLE_IRQ_PREEMPTION == TRUE)
        /* Allows preemption while executing the software handler.*/
        wrteei      1
#endif /* OSAL_ENABLE_IRQ_PREEMPTION */

        /* Executes the software handler.*/
        se_bctrl

#if (OSAL_ENABLE_IRQ_PREEMPTION == TRUE)
        /* Prevents preemption again.*/
        wrteei      0
#endif /* OSAL_ENABLE_IRQ_PREEMPTION */

        /* Informs the INTC that the interrupt has been served.*/
        mbar        0
        e_lis       %r3, HI(INTC_EOIR_BASE_0)
        e_or2i      %r3, LO(INTC_EOIR_BASE_0)
        se_stw      %r3, 0(%r3)             /* Writing any value should do. */

        RESTORE_CONTEXT
        se_rfi
#endif /* BOOT_CORE0 */

        /*
         * _IVOR5 handler (Alignement Exception).
         */
#if IVOR5_HANDLER
        .align      4
        .globl      _IVOR5
        .type       _IVOR5, @function
_IVOR5:
        SAVE_CONTEXT

		LOAD_SSR0_SSR1_PLUS

		e_bl		_ivor5_callback		/* Branch to the proper callback */

        RESTORE_CONTEXT
        se_rfi
#endif

        /*
         * _IVOR6 handler (Program Interrupt Exception).
         */
#if IVOR6_HANDLER
        .align      4
        .globl      _IVOR6
        .type       _IVOR6, @function
_IVOR6:
        SAVE_CONTEXT

		LOAD_SSR0_SSR1_PLUS

		e_bl		_ivor6_callback		/* Branch to the proper callback */

        RESTORE_CONTEXT
        se_rfi
#endif

        /*
         * _IVOR7 handler (Floating-Point Interrupt Exception).
         */
#if IVOR7_HANDLER
        .align      4
        .globl      _IVOR7
        .type       _IVOR7, @function
_IVOR7:
        SAVE_CONTEXT

		LOAD_SSR0_SSR1_PLUS

		e_bl		_ivor7_callback		/* Branch to the proper callback */

        RESTORE_CONTEXT
        se_rfi
#endif

        /*
         * _IVOR8 handler (System Call Interrupt Exception).
         */
#if IVOR8_HANDLER
        .align      4
        .globl      _IVOR8
        .type       _IVOR8, @function
_IVOR8:
        SAVE_CONTEXT

		LOAD_SSR0_SSR1

		e_bl		_ivor8_callback		/* Branch to the proper callback */

        RESTORE_CONTEXT
        se_rfi
#endif

        /*
         * _IVOR9 handler (Auxiliary Processor Unavailable Interrupt Exception).
         */
#if IVOR9_HANDLER
        .align      4
        .globl      _IVOR9
        .type       _IVOR9, @function
_IVOR9:
        SAVE_CONTEXT

		LOAD_SSR0_SSR1_PLUS

		e_bl		_ivor9_callback		/* Branch to the proper callback */

        RESTORE_CONTEXT
        se_rfi
#endif

        /*
         * _IVOR11 handler (Fixed Interval Timer Interrupt Exception).
         */
#if IVOR11_HANDLER
        .align      4
        .globl      _IVOR11
        .type       _IVOR11, @function
_IVOR11:
        SAVE_CONTEXT

		LOAD_SSR0_SSR1

		e_bl		_ivor11_callback		/* Branch to the proper callback */
#if 0
        /* Reset FIS bit in TSR register.*/
        e_lis       %r3, 0x0400             /* FIS bit mask.                */
        mtspr       336, %r3                /* TSR register.                */

        /* Restoring pre-IRQ MSR register value.*/
        mfSRR1      %r0

        /* No preemption, keeping EE disabled.*/
        se_bclri    %r0, 16                 /* EE = bit 16.                 */
        mtMSR       %r0
#endif
        RESTORE_CONTEXT
        se_rfi
#endif

        /*
         * _IVOR12 handler (Watchdog Timer Interrupt Exception).
         */
#if IVOR12_HANDLER
        .align      4
        .globl      _IVOR12
        .type       _IVOR12, @function
_IVOR12:
        SAVE_CONTEXT

		LOAD_SSR0_SSR1

		e_bl		_ivor12_callback		/* Branch to the proper callback */
#if 0
        /* Reset WIS bit in TSR register.*/
        e_lis       %r3, 0x4000             /* WIS bit mask.                */
        mtspr       336, %r3                /* TSR register.                */

        /* Restoring pre-IRQ MSR register value.*/
        mfSRR1      %r0

        /* No preemption, keeping EE disabled.*/
        se_bclri    %r0, 16                 /* EE = bit 16.                 */
        mtMSR       %r0
#endif
        RESTORE_CONTEXT
        se_rfi
#endif

        /*
         * _IVOR13 handler (Data TLB Error Interrupt Exception).
         */
#if IVOR13_HANDLER
        .align      4
        .globl      _IVOR13
        .type       _IVOR13, @function
_IVOR13:
        SAVE_CONTEXT

		LOAD_SSR0_SSR1_PLUS

		e_bl		_ivor13_callback		/* Branch to the proper callback */

        RESTORE_CONTEXT
        se_rfi
#endif

        /*
         * _IVOR14 handler (Instruction TLB Error Interrupt Exception).
         */
#if IVOR14_HANDLER
        .align      4
        .globl      _IVOR14
        .type       _IVOR14, @function
_IVOR14:
        SAVE_CONTEXT

		LOAD_SSR0_SSR1_PLUS

		e_bl		_ivor14_callback		/* Branch to the proper callback */

        RESTORE_CONTEXT
        se_rfi
#endif

        /*
         * _IVOR15 handler (Debug Interrupt Exception).
         */
#if IVOR15_HANDLER
        .align      4
        .globl      _IVOR15
        .type       _IVOR15, @function
_IVOR15:
        SAVE_CONTEXT_CRITICAL

		LOAD_CSSR0_CSSR1

		e_bl		_ivor15_callback		/* Branch to the proper callback */

        RESTORE_CONTEXT_CRITICAL

        se_rfci
#endif

#if CORE_SUPPORTS_IVORS
        .align      4
        .globl      _spr_init
        .type       _spr_init, @function
_spr_init:

        e_lis       %r3, HI(_IVOR0)
        e_or2i      %r3, LO(_IVOR0)
        mtIVOR0     %r3

        e_lis       %r3, HI(_IVOR1)
        e_or2i      %r3, LO(_IVOR1)
        mtIVOR1     %r3

        e_lis       %r3, HI(_IVOR2)
        e_or2i      %r3, LO(_IVOR2)
        mtIVOR2     %r3

        e_lis       %r3, HI(_IVOR3)
        e_or2i      %r3, LO(_IVOR3)
        mtIVOR3     %r3

        e_lis       %r3, HI(_IVOR4)
        e_or2i      %r3, LO(_IVOR4)
        mtIVOR4     %r3

        e_lis       %r3, HI(_IVOR5)
        e_or2i      %r3, LO(_IVOR5)
        mtIVOR5     %r3

        e_lis       %r3, HI(_IVOR6)
        e_or2i      %r3, LO(_IVOR6)
        mtIVOR6     %r3

        e_lis       %r3, HI(_IVOR7)
        e_or2i      %r3, LO(_IVOR7)
        mtIVOR7     %r3

        e_lis       %r3, HI(_IVOR8)
        e_or2i      %r3, LO(_IVOR8)
        mtIVOR8     %r3

        e_lis       %r3, HI(_IVOR9)
        e_or2i      %r3, LO(_IVOR9)
        mtIVOR9     %r3

        e_lis       %r3, HI(_IVOR10)
        e_or2i      %r3, LO(_IVOR10)
        mtIVOR10    %r3

        e_lis       %r3, HI(_IVOR11)
        e_or2i      %r3, LO(_IVOR11)
        mtIVOR11     %r3

        e_lis       %r3, HI(_IVOR12)
        e_or2i      %r3, LO(_IVOR12)
        mtIVOR12     %r3

        e_lis       %r3, HI(_IVOR13)
        e_or2i      %r3, LO(_IVOR13)
        mtIVOR13     %r3

        e_lis       %r3, HI(_IVOR14)
        e_or2i      %r3, LO(_IVOR14)
        mtIVOR14     %r3

        e_lis       %r3, HI(_IVOR15)
        e_or2i      %r3, LO(_IVOR15)
        mtIVOR15     %r3

        e_lis       %r3, HI(_IVOR32)
        e_or2i      %r3, LO(_IVOR32)
        mtIVOR32     %r3

        e_lis       %r3, HI(_IVOR33)
        e_or2i      %r3, LO(_IVOR33)
        mtIVOR33     %r3

        e_lis       %r3, HI(_IVOR34)
        e_or2i      %r3, LO(_IVOR34)
        mtIVOR34     %r3

        se_blr
#endif /* CORE_SUPPORTS_IVORS */

#endif /* !defined(__DOXYGEN__) */

#endif /* OSAL_OS_ENABLE */

/** @} */
