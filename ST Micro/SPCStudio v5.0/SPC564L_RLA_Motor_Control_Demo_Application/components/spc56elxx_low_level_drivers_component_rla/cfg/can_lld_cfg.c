/*
    SPC5 RLA - Copyright (C) 2015 STMicroelectronics

    Licensed under the Apache License, Version 2.0 (the "License").
    You may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

/**
 * @file    can_lld_cfg.c
 * @brief   CAN Driver configuration code.
 *
 * @addtogroup CAN
 * @{
 */

#include "can_lld_cfg.h"

#if (LLD_USE_CAN == TRUE) || defined(__DOXYGEN__)

/*===========================================================================*/
/* Driver local definitions.                                                 */
/*===========================================================================*/

/*===========================================================================*/
/* Driver exported variables.                                                */
/*===========================================================================*/


/**
 * @brief   Structure defining the CAN configuration "to250kbps".
 */
const CANConfig can_config_to250kbps = {

  0,
  CAN_CTRL_PROPSEG(2) | CAN_CTRL_PSEG2(5) |
  CAN_CTRL_PSEG1(5) | CAN_CTRL_PRESDIV(9),
#if (SPC5_CAN_USE_FILTERS == TRUE)
  {
   {0, 0x1},
   {0, 0x2},
   {0, 0x3},
   {0, 0x6},
   {0, 0x7},
   {0, 0x8},
   {0, 0x9},
   {0, 0xa},
  },
#endif
  /* RX fifo Enabled */
  FALSE,
  /* RX fifo ID Acceptance Mask */
  CAN_RX_FIFO_FORMAT_A,
  /*FIFO Filters*/
  {
  {0,0},               /* filter unused 0 */
  {0,0},               /* filter unused 1 */
  {0,0},               /* filter unused 2 */
  {0,0},               /* filter unused 3 */
  {0,0},               /* filter unused 4 */
  {0,0},               /* filter unused 5 */
  {0,0},               /* filter unused 6 */
  {0,0},               /* filter unused 7 */
  {0,0},               /* filter unused 8 */
  {0,0},               /* filter unused 9 */
  {0,0},               /* filter unused 10 */
  {0,0},               /* filter unused 11 */
  {0,0},               /* filter unused 12 */
  {0,0},               /* filter unused 13 */
  {0,0},               /* filter unused 14 */
  {0,0},               /* filter unused 15 */
  {0,0},               /* filter unused 16 */
  {0,0},               /* filter unused 17 */
  {0,0},               /* filter unused 18 */
  {0,0},               /* filter unused 19 */
  {0,0},               /* filter unused 20 */
  {0,0},               /* filter unused 21 */
  {0,0},               /* filter unused 22 */
  {0,0},               /* filter unused 23 */
  {0,0},               /* filter unused 24 */
  {0,0},               /* filter unused 25 */
  {0,0},               /* filter unused 26 */
  {0,0},               /* filter unused 27 */
  {0,0},               /* filter unused 28 */
  {0,0},               /* filter unused 29 */
  {0,0},               /* filter unused 30 */
  {0,0},               /* filter unused 31 */
  },
  /* First MB available for RX and TX */
  0U,
  /* FIFO RX CallBack */
  NULL,
};


/**
 * @brief   Structure defining the CAN configuration "to500kbps".
 */
const CANConfig can_config_to500kbps = {

  0,
  CAN_CTRL_PROPSEG(2) | CAN_CTRL_PSEG2(5) |
  CAN_CTRL_PSEG1(5) | CAN_CTRL_PRESDIV(4),
#if (SPC5_CAN_USE_FILTERS == TRUE)
  {
   {0, 0x1},
   {0, 0x2},
   {0, 0x3},
   {0, 0x6},
   {0, 0x7},
   {0, 0x8},
   {0, 0x9},
   {0, 0xa},
  },
#endif
  /* RX fifo Enabled */
  FALSE,
  /* RX fifo ID Acceptance Mask */
  CAN_RX_FIFO_FORMAT_A,
  /*FIFO Filters*/
  {
  {0,0},               /* filter unused 0 */
  {0,0},               /* filter unused 1 */
  {0,0},               /* filter unused 2 */
  {0,0},               /* filter unused 3 */
  {0,0},               /* filter unused 4 */
  {0,0},               /* filter unused 5 */
  {0,0},               /* filter unused 6 */
  {0,0},               /* filter unused 7 */
  {0,0},               /* filter unused 8 */
  {0,0},               /* filter unused 9 */
  {0,0},               /* filter unused 10 */
  {0,0},               /* filter unused 11 */
  {0,0},               /* filter unused 12 */
  {0,0},               /* filter unused 13 */
  {0,0},               /* filter unused 14 */
  {0,0},               /* filter unused 15 */
  {0,0},               /* filter unused 16 */
  {0,0},               /* filter unused 17 */
  {0,0},               /* filter unused 18 */
  {0,0},               /* filter unused 19 */
  {0,0},               /* filter unused 20 */
  {0,0},               /* filter unused 21 */
  {0,0},               /* filter unused 22 */
  {0,0},               /* filter unused 23 */
  {0,0},               /* filter unused 24 */
  {0,0},               /* filter unused 25 */
  {0,0},               /* filter unused 26 */
  {0,0},               /* filter unused 27 */
  {0,0},               /* filter unused 28 */
  {0,0},               /* filter unused 29 */
  {0,0},               /* filter unused 30 */
  {0,0},               /* filter unused 31 */
  },
  /* First MB available for RX and TX */
  0U,
  /* FIFO RX CallBack */
  NULL,
};

/*===========================================================================*/
/* Driver local types.                                                       */
/*===========================================================================*/

/*===========================================================================*/
/* Driver local variables.                                                   */
/*===========================================================================*/

/*===========================================================================*/
/* Driver local functions.                                                   */
/*===========================================================================*/

/*===========================================================================*/
/* Driver exported functions.                                                */
/*===========================================================================*/

#endif /* LLD_USE_CAN */

/** @} */
