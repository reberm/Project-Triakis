/*
    SPC5 RLA - Copyright (C) 2015 STMicroelectronics

    Licensed under the Apache License, Version 2.0 (the "License").
    You may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

/**
 * @file    spi_lld_cfg.c
 * @brief   SPI Driver configuration code.
 *
 * @addtogroup SPI
 * @{
 */

#include "spi_lld_cfg.h"

#if (LLD_USE_SPI == TRUE) || defined(__DOXYGEN__)

/*===========================================================================*/
/* Driver local definitions.                                                 */
/*===========================================================================*/

/*===========================================================================*/
/* Driver exported variables.                                                */
/*===========================================================================*/

/**
 * @brief   Structure defining the SPI configuration "L9907".
 */
const SPIConfig spi_config_L9907 = {
  NULL,
  PORT_C,
  4,
  0UL | SPC5_CTAR_FMSZ(16) | SPC5_CTAR_CPHA |
        SPC5_CTAR_PCSSCK_PRE1 | SPC5_CTAR_PASC_PRE1 |
        SPC5_CTAR_PDT_PRE1 | SPC5_CTAR_PBR_PRE2 |
        SPC5_CTAR_CSSCK_DIV256 | SPC5_CTAR_ASC_DIV256 |
        SPC5_CTAR_DT_DIV256 | SPC5_CTAR_BR_DIV64,
  0UL | SPC5_PUSHR_CONT | SPC5_PUSHR_CTAS(0) | SPC5_PUSHR_PCS(0)
};

 /*===========================================================================*/
/* Driver local types.                                                       */
/*===========================================================================*/

/*===========================================================================*/
/* Driver local variables.                                                   */
/*===========================================================================*/

 /*===========================================================================*/
/* Driver local functions.                                                   */
/*===========================================================================*/

/*===========================================================================*/
/* Driver exported functions.                                                */
/*===========================================================================*/

#endif /* LLD_USE_SPI */

/** @} */
