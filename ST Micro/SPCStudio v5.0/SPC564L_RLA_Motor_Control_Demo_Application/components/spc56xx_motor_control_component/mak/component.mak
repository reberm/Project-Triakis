##############################################################################
# Source and include file located under ./lib and ./cfg  are automatically added.
##############################################################################
 
# C specific options here (added to USE_OPT).
USE_COPT += -DAUTOSAR_OS_NOT_USED
 
#rwildcard = $(wildcard $1$2) $(foreach d,$(wildcard $1*),$(call rwildcard,$d/,$2))
#LIB_C_SRC = $(call rwildcard,./components/spc560pxx_motor_control_component/lib/,*.c)

LIB_C_SRC += ./components/spc56xx_motor_control_component/lib/TimeBase/Timebase.c \
             ./components/spc56xx_motor_control_component/lib/System_Drive_Params/src/MC_Math.c \
             ./components/spc56xx_motor_control_component/lib/System_Drive_Params/src/Spc5xxx_MC_it.c \
             ./components/spc56xx_motor_control_component/lib/System/src/MC_Lib_Setup.c \
             ./components/spc56xx_motor_control_component/lib/MC_Library/CVBS/src/BusVoltageSensorClass.c \
             ./components/spc56xx_motor_control_component/lib/MC_Library/CVBS/src/Virtual_BusVoltageSensorClass.c \
             ./components/spc56xx_motor_control_component/lib/MC_Library/CTSNS/src/TemperatureSensorClass.c \
             ./components/spc56xx_motor_control_component/lib/MC_Library/CTSNS/src/Virtual_TemperatureSensorClass.c \
             ./components/spc56xx_motor_control_component/lib/MC_Library/CSTM/src/StateMachineClass.c \
             ./components/spc56xx_motor_control_component/lib/MC_Library/CSTC/src/SpeednTorqCtrlClass.c \
             ./components/spc56xx_motor_control_component/lib/MC_Library/CSPD/src/ENCODER_SpeednPosFdbkClass.c \
             ./components/spc56xx_motor_control_component/lib/MC_Library/CEAC/src/EncAlignCtrlClass.c \
             ./components/spc56xx_motor_control_component/lib/MC_Library/CSPD/src/HALL_SpeednPosFdbkClass.c \
             ./components/spc56xx_motor_control_component/lib/MC_Library/CSPD/src/SpeednPosFdbkClass.c \
             ./components/spc56xx_motor_control_component/lib/MC_Library/CSPD/src/STO_SpeednPosFdbkClass.c \
             ./components/spc56xx_motor_control_component/lib/MC_Library/CSPD/src/VirtualSpeedSensor_SpeednPosFdbkClass.c \
             ./components/spc56xx_motor_control_component/lib/MC_Library/CRUC/src/RevupCtrlClass.c \
             ./components/spc56xx_motor_control_component/lib/MC_Library/CPWMC/src/ICS_PWMnCurrFdbkClass.c \
             ./components/spc56xx_motor_control_component/lib/MC_Library/CPWMC/src/PWMnCurrFdbkClass.c \
             ./components/spc56xx_motor_control_component/lib/MC_Library/CPI/src/PID_PIRegulatorClass.c \
             ./components/spc56xx_motor_control_component/lib/MC_Library/CPI/src/PIRegulatorClass.c \
             ./components/spc56xx_motor_control_component/lib/MC_Library/COL/src/OpenLoopClass.c \
             ./components/spc56xx_motor_control_component/lib/MC_Library/CMPM/src/MotorPowerMeasurementClass.c \
             ./components/spc56xx_motor_control_component/lib/MC_Library/CMPM/src/PQD_MotorPowerMeasurementClass.c \
             ./components/spc56xx_motor_control_component/lib/MC_Library/CMCIRQ/src/MCIRQHandlerClass.c \
             ./components/spc56xx_motor_control_component/lib/MC_Library/CLM/src/CircleLimitationClass.c \
             ./components/spc56xx_motor_control_component/lib/MC_Application/src/MCInterfaceClass.c \
             ./components/spc56xx_motor_control_component/lib/MC_Application/src/MCTaskFunction.c \
             ./components/spc56xx_motor_control_component/lib/MC_Application/src/MCTasks.c \
             ./components/spc56xx_motor_control_component/lib/MC_Application/src/MCTuningClass.c \
			 ./components/spc56xx_motor_control_component/lib/LLD_Library/SPC56EL/src/Mcu_Dmamux_LLD.c \
             ./components/spc56xx_motor_control_component/lib/LLD_Library/SPC56EL/src/Mcu_Dma_LLD.c \
             ./components/spc56xx_motor_control_component/lib/LLD_Library/SPC56EL/src/Reg_eSys_eTimer_GPT.c
LIB_C_SRC += ./components/spc56xx_motor_control_component/lib/UILibrary/src/UserInterfaceClass.c \
            ./components/spc56xx_motor_control_component/lib/UILibrary/src/MotorControlProtocolClass.c \
             ./components/spc56xx_motor_control_component/lib/UILibrary/src/FrameCommunicationProtocolClass.c \
            ./components/spc56xx_motor_control_component/lib/UILibrary/src/PhysicalLayerCommunication_Class.c \
            ./components/spc56xx_motor_control_component/lib/UILibrary/src/UIIRQHandlerClass.c \
            ./components/spc56xx_motor_control_component/lib/UILibrary/src/UITask.c \
            ./components/spc56xx_motor_control_component/lib/UILibrary/src/USART_PhysicalLayerCommunication_Class.c \

#LIB_INCLUDES += $(sort $(dir $(call rwildcard,./components/spc560pxx_motor_control_component/lib/,*)))
LIB_INCLUDES += ./components/spc56xx_motor_control_component/lib/TimeBase/
LIB_INCLUDES += ./components/spc56xx_motor_control_component/lib/System_Drive_Params/interface/
LIB_INCLUDES += ./components/spc56xx_motor_control_component/lib/System_Drive_Params/include/
LIB_INCLUDES += ./components/spc56xx_motor_control_component/lib/System/interface
LIB_INCLUDES += ./components/spc56xx_motor_control_component/lib/System/include
LIB_INCLUDES += ./components/spc56xx_motor_control_component/lib/Math_Lib/interface/
LIB_INCLUDES += ./components/spc56xx_motor_control_component/lib/Math_Lib/include/
LIB_INCLUDES += ./components/spc56xx_motor_control_component/lib/LLD_Library/SPC56EL/interface/
LIB_INCLUDES += ./components/spc56xx_motor_control_component/lib/LLD_Library/SPC56EL/include/
LIB_INCLUDES += ./components/spc56xx_motor_control_component/lib/MC_Library/CVBS/interface/
LIB_INCLUDES += ./components/spc56xx_motor_control_component/lib/MC_Library/CTSNS/interface/
LIB_INCLUDES += ./components/spc56xx_motor_control_component/lib/MC_Library/CSTM/interface/
LIB_INCLUDES += ./components/spc56xx_motor_control_component/lib/MC_Library/CSTC/interface/
LIB_INCLUDES += ./components/spc56xx_motor_control_component/lib/MC_Library/CSPD/interface/
LIB_INCLUDES += ./components/spc56xx_motor_control_component/lib/MC_Library/CDOUT/interface/
LIB_INCLUDES += ./components/spc56xx_motor_control_component/lib/MC_Library/CRUC/interface/
LIB_INCLUDES += ./components/spc56xx_motor_control_component/lib/MC_Library/CPWMC/interface/
LIB_INCLUDES += ./components/spc56xx_motor_control_component/lib/MC_Library/CPWMC/interface/
LIB_INCLUDES += ./components/spc56xx_motor_control_component/lib/MC_Library/CPROT/interface/
LIB_INCLUDES += ./components/spc56xx_motor_control_component/lib/MC_Library/CPI/interface/
LIB_INCLUDES += ./components/spc56xx_motor_control_component/lib/MC_Library/COL/interface/
LIB_INCLUDES += ./components/spc56xx_motor_control_component/lib/MC_Library/CMPM/interface/
LIB_INCLUDES += ./components/spc56xx_motor_control_component/lib/MC_Library/CMPM/interface/
LIB_INCLUDES += ./components/spc56xx_motor_control_component/lib/MC_Library/CMCIRQ/interface/
LIB_INCLUDES += ./components/spc56xx_motor_control_component/lib/MC_Library/CLM/interface/
LIB_INCLUDES += ./components/spc56xx_motor_control_component/lib/MC_Library/CEAC/interface/
LIB_INCLUDES += ./components/spc56xx_motor_control_component/lib/MC_Library/CVBS/include/
LIB_INCLUDES += ./components/spc56xx_motor_control_component/lib/MC_Library/CTSNS/include/
LIB_INCLUDES += ./components/spc56xx_motor_control_component/lib/MC_Library/CSTM/include/
LIB_INCLUDES += ./components/spc56xx_motor_control_component/lib/MC_Library/CSTC/include/
LIB_INCLUDES += ./components/spc56xx_motor_control_component/lib/MC_Library/CSPD/include/
LIB_INCLUDES += ./components/spc56xx_motor_control_component/lib/MC_Library/CSPD/include/
LIB_INCLUDES += ./components/spc56xx_motor_control_component/lib/MC_Library/CRUC/include/
LIB_INCLUDES += ./components/spc56xx_motor_control_component/lib/MC_Library/CPWMC/include/
LIB_INCLUDES += ./components/spc56xx_motor_control_component/lib/MC_Library/CPWMC/include/
LIB_INCLUDES += ./components/spc56xx_motor_control_component/lib/MC_Library/CPROT/include/
LIB_INCLUDES += ./components/spc56xx_motor_control_component/lib/MC_Library/CPI/include/
LIB_INCLUDES += ./components/spc56xx_motor_control_component/lib/MC_Library/COL/include/
LIB_INCLUDES += ./components/spc56xx_motor_control_component/lib/MC_Library/CMPM/include/
LIB_INCLUDES += ./components/spc56xx_motor_control_component/lib/MC_Library/CMPM/include/
LIB_INCLUDES += ./components/spc56xx_motor_control_component/lib/MC_Library/CMCIRQ/include/
LIB_INCLUDES += ./components/spc56xx_motor_control_component/lib/MC_Library/CLM/include/
LIB_INCLUDES += ./components/spc56xx_motor_control_component/lib/MC_Library/CEAC/include/
LIB_INCLUDES += ./components/spc56xx_motor_control_component/lib/MC_Application/interface/
LIB_INCLUDES += ./components/spc56xx_motor_control_component/lib/MC_Application/include/
LIB_INCLUDES += ./components/spc56xx_motor_control_component/lib/UILibrary/inc/
LIB_INCLUDES += ./components/spc56xx_motor_control_component/lib/UILibrary/interface/




#LIB_INCLUDES +=  $(sort $(dir $(call rwildcard,./components/spc560pxx_motor_control_component/cfg/,*)))

############################################
# References to the application locations.

APP_C_SRC       += 

APP_CPP_SRC     +=

APP_ASM_SRC     +=

APP_INCLUDES    += ./components/spc56xx_motor_control_component/cfg

##############################################################################
# Default directories, definitions and libraries.

# C/C++ definitions (-D....).
DDEFS   =

# Assembled definitions (-D....).
DADEFS  =

# Include files search directories separated by spaces.
DINCDIR =

# Library files search directories separated by spaces.
DLIBDIR =

# Libraries (-l...).
DLIBS   =
