/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    Power stage parameters.h
*   @version BETA 0.9.1
*   @brief   This file contains motor parameters
*          
*   @details This file contains motor parameters.
*
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __POWER_STAGE_PARAMETERS_H
#define __POWER_STAGE_PARAMETERS_H

/************* PWM Driving signals section **************/
#define PHASE_UH_POLARITY             FLEXPWM_Polarity_High 
#define PHASE_VH_POLARITY             FLEXPWM_Polarity_High
#define PHASE_WH_POLARITY             FLEXPWM_Polarity_High

#define HW_COMPLEMENTED_LOW_SIDE      DISABLE 

#define PHASE_UL_POLARITY             FLEXPWM_NPolarity_Low 
#define PHASE_VL_POLARITY             FLEXPWM_NPolarity_Low 
#define PHASE_WL_POLARITY             FLEXPWM_NPolarity_Low 

#define HW_DEAD_TIME_NS              100 /*!< Dead-time inserted 
                                                         by HW if low side signals 
                                                         are not used */
/********** Inrush current limiter signal section *******/
#define INRUSH_CURR_LIMITER_POLARITY  DOUT_ACTIVE_HIGH 

/******* Dissipative brake driving signal section *******/
#define DISSIPATIVE_BRAKE_POLARITY    DOUT_ACTIVE_HIGH

/*********** Bus voltage sensing section ****************/
#define VBUS_PARTITIONING_FACTOR      0.0066 /*!< It expresses how 
                                                  much the Vbus is attenuated  
                                                  before being converted into 
                                                  digital value */
#define NOMINAL_BUS_VOLTAGE_V         12

/******** Current reading parameters section ******/
/*** Topology ***/
/* #define THREE_SHUNT */
/* #define SINGLE_SHUNT */
#define ICS_SENSORS


#define RSHUNT                        0.22 

/*  ICSs gains in case of isolated current sensors,
        amplification gain for shunts based sensing */
#define AMPLIFICATION_GAIN            0.04 
/*Current sensing on legs as configured */
#define CURR_MEASURED_ON_UW_PHASES
/*** Noise parameters ***/
#define TNOISE_NS                     2550 
#define TRISE_NS                      2550 

/******** Application Task section ******/
#define CHARGE_BOOT_CAP_ENABLING      ENABLE
#define CHARGE_BOOT_CAP_ENABLING2     DISABLE
#define CHARGE_BOOT_CAP_MS            10
#define CHARGE_BOOT_CAP_MS2           10
#define OFFCALIBRWAIT_MS              0
#define OFFCALIBRWAIT_MS2             0
#define STOPPERMANENCY_MS             400
#define STOPPERMANENCY_MS2            400

/*********** Over-current protection section ************/
#define OVERCURR_FEEDBACK_POLARITY       EMSTOP_ACTIVE_LOW
#define OVERCURR_PROTECTION_HW_DISABLING  DOUT_ACTIVE_HIGH

/************ Temperature sensing section ***************/
/* V[V]=V0+dV/dT[V/Celsius]*(T-T0)[Celsius]*/
#define V0_V                          0.29 /*!< in Volts */
#define T0_C                          25.0 /*!< in Celsius degrees */
#define dV_dT                         0.025 /*!< V/Celsius degrees */
#define T_MAX                         70 /*!< Sensor measured 
                                                       temperature at maximum 
                                                       power stage working 
                                                       temperature, Celsius degrees */
#endif /*__POWER_STAGE_PARAMETERS_H*/
