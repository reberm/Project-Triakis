/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    PMSM motor parameters.h
*   @version BETA 0.9.1
*   @brief   This file contains motor parameters
*          
*   @details This file contains motor parameters.
*
*/
 
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __PMSM_MOTOR_PARAMETERS_H
#define __PMSM_MOTOR_PARAMETERS_H

#define MOTOR_TYPE             PMSM

/***************** MOTOR ELECTRICAL PARAMETERS  ******************************/
#define POLE_PAIR_NUM          1 /* Number of motor pole pairs */
#define RS                     0.07 /* Stator resistance , ohm*/
#define LS                     0.000015 /* Stator inductance, H
                                                 For I-PMSM it is equal to Lq */

/* When using Id = 0, NOMINAL_CURRENT is utilized to saturate the output of the 
   PID for speed regulation (i.e. reference torque). 
   Transformation of real currents (A) into s16 format must be done accordingly with 
   formula:
   Phase current (s16 0-to-peak) = (Phase current (A 0-to-peak)* 32767 * Rshunt *
                                   *Amplifying network gain)/(MCU supply voltage/2)
*/

#define NOMINAL_CURRENT         6354
#define MOTOR_MAX_SPEED_RPM     10300 /*!< Maximum rated speed  */
#define MOTOR_VOLTAGE_CONSTANT  1.2 /*!< Volts RMS ph-ph /kRPM */
#define ID_DEMAG                -6354 /*!< Demagnetization current */

/***************** MOTOR SENSORS PARAMETERS  ******************************/
/* Motor sensors parameters are always generated but really meaningful only 
   if the corresponding sensor is actually present in the motor         */

/*** Hall sensors ***/
#define HALL_SENSORS_AVAILABLE  TRUE
#define HALL_SENSORS_PLACEMENT  DEGREES_120 /*!<Define here the  
                                                 mechanical position of the sensors
                                                 with reference to an electrical cycle. 
                                                 It can be either DEGREES_120 or 
                                                 DEGREES_60 */
                                                                                   
#define HALL_PHASE_SHIFT        300 /*!< Define here in degrees  
                                                 the electrical phase shift between 
                                                 the low to high transition of 
                                                 signal H1 and the maximum of 
                                                 the Bemf induced on phase A */ 
/*** Quadrature encoder ***/ 
#define ENCODER_AVAILABLE       TRUE
#define ENCODER_PPR             500  /*!< Number of pulses per 
                                            revolution */
/*** Resolver ***/
#define RESOLVER_SENSORS_AVAILABLE  FALSE
#define RESOLVER_PHASE_SHIFT        0 /*!< Define here in degrees
                                                 the electrical phase shift between
                                                 the Resolver Channel A and the Bemf
                                                 induced on phase A*/
#define RESOLVER_PPMR               2            /*!< Define here the number of full period
                                                 per mechanical revolution */

#endif /*__PMSM_MOTOR_PARAMETERS_H*/
