/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    Control stage parameters.h
*   @version BETA 0.9.1
*   @brief   This file contains motor parameters
*          
*   @details This file contains motor parameters.
*
*/
  
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __CONTROL_STAGE_PARAMETERS_H
#define __CONTROL_STAGE_PARAMETERS_H

#include "platform.h"

/***************************** MCU SELECTION SECTION **************************/
#define SPC56EL               /* Leopard */

#define PACKAGE_SELECTION     NOT_USED   

/***************************** MCU supply voltage *****************************/
#define MCU_SUPPLY_VOLTAGE    3.3

/***************************** CLOCK SETTINGS SECTION *************************/
#define CLOCK_SOURCE          EXTERNAL  /* EXTERNAL or INTERNAL */

#define CPU_CLK_120_MHZ
   
/* ext. clock frequency */
#define EXT_CLK_8_MHZ
                        
/************************ DIGITAL I/O DEFINITION SECTION  *********************/

/* DMA channels used for ADC conversions via CTU */
#define DMA_CHANNEL_FOR_CURRENT_SENSING   (0U) /*!< Dma channel used for Current Sensing */
#define DMA_CHANNEL_FOR_RESOLVER          (1U) /*!< Dma channel used for Resolver */
#define DMA_CHANNEL_FOR_VBUS_TEMPERATURE  (2U) /*!< Dma channel used for VBus and Temperature */
#define DMA_CHANNEL_FOR_ADC_CONVERSIONS   (3U) /*!< Dma channel used for User Adc Conversions */

/* FlexPWM module 0 */
#define FLEXPWM_Module_0                  0U
/* FlexPWM module 1 */
#define FLEXPWM_Module_1                  1U
/* PWM Module section */
#define PWM_MODULE_SELECTION              FLEXPWM_Module_0
/* PWM generation configuration (SUB_MOD_1/2/3) - standard */ 
#define FLEXPWM_PHASE_U                   1U
#define FLEXPWM_PHASE_V                   2U
#define FLEXPWM_PHASE_W                   3U

#define FLEXPWM_SUBMODULES_USED           FLEXPWM_SUBMOD_NB
#define FLEXPWM_SUB_MOD_INDEX             1U
#define FLEXPWM_SUB_RUN_MASK              0x0F00U 

#define FLEXPWM_SUB_MOD_EN_OUT_ALL        0x0EE0U 
#define FLEXPWM_SUB_MOD_EN_OUT_LS         0x0E00U

#define PWM_TIMER_SELECTION               PWM_TIM1 /* PWM_TIM1 or PWM_TIM8 */ 
#define PWM_TIMER_REMAPPING               PWM_FULL_REMAP /* PWM_NO_REMAP,  
                                                           PWM_FULL_REMAP, 
                                                           PWM_PARTIAL_REMAP */
#define PHASE_UH_GPIO_PORT                GPIOE
#define PHASE_UH_GPIO_PIN                 GPIO_Pin_9
#define PHASE_VH_GPIO_PORT                GPIOE
#define PHASE_VH_GPIO_PIN                 GPIO_Pin_11
#define PHASE_WH_GPIO_PORT                GPIOE
#define PHASE_WH_GPIO_PIN                 GPIO_Pin_13
#define PHASE_UL_GPIO_PORT                GPIOE
#define PHASE_UL_GPIO_PIN                 GPIO_Pin_8
#define PHASE_VL_GPIO_PORT                GPIOE
#define PHASE_VL_GPIO_PIN                 GPIO_Pin_10
#define PHASE_WL_GPIO_PORT                GPIOE
#define PHASE_WL_GPIO_PIN                 GPIO_Pin_12
#define EMERGENCY_STOP_GPIO_PORT          GPIOE
#define EMERGENCY_STOP_GPIO_PIN           GPIO_Pin_15

#define BKIN_MODE                         NONE /* NONE, INT_MODE, EXT_MODE */
#define BKIN2_MODE                        NONE /* NONE, INT_MODE, EXT_MODE */
#define EMERGENCY2_STOP_GPIO_PORT         GPIOE
#define EMERGENCY2_STOP_GPIO_PIN          GPIO_Pin_15
#define PHASE_UH_GPIO_AF                  GPIO_AF_2
#define PHASE_VH_GPIO_AF                  GPIO_AF_2
#define PHASE_WH_GPIO_AF                  GPIO_AF_2
#define PHASE_UL_GPIO_AF                  GPIO_AF_2
#define PHASE_VL_GPIO_AF                  GPIO_AF_2
#define PHASE_WL_GPIO_AF                  GPIO_AF_2
#define BRKIN_GPIO_AF                     GPIO_AF_2
#define BRKIN2_GPIO_AF                    GPIO_AF_2

/* Hall timer section */
#define HALL_TIMER_SELECTION              ETIMER_Module_0
#define HALL_TIMER_REMAPPING              NO_REMAP_TIM2  /* NO_REMAP, FULL_REMAP, 
                                                            PARTIAL_REMAP */
#define HALL_S1_CH                        ETIMER_CH0
#define HALL_S2_CH                        ETIMER_CH2
#define HALL_S3_CH                        ETIMER_CH1
#define HALL_CTU_TRIGGER                  ETIMER_CH3

#define H1_GPIO_PORT                      GPIOA
#define H2_GPIO_PORT                      GPIOA
#define H3_GPIO_PORT                      GPIOA

#define H1_GPIO_PIN                       GPIO_Pin_0
#define H2_GPIO_PIN                       GPIO_Pin_2
#define H3_GPIO_PIN                       GPIO_Pin_1

/* Encoder timer section */
#define ENC_TIMER_SELECTION               (ETIMER_Module_1)

#define ENC_QUAD_CHANNEL                  (ETIMER_CH2)
#define ENC_CH_A_INPUT                    (ETIMER_CH3)
#define ENC_CH_B_INPUT                    (ETIMER_CH4)
#define ENC_POSITION_CH                   (ETIMER_CH5)
#define ENC_INDEX_CH_INPUT                (ETIMER_CH1)
#define ENC_INDEX_COUNTER_CH              (ETIMER_CH0)
#define ENC_OVERFLOW_CH                   (ENC_INDEX_COUNTER_CH)

#define ENC_INDEX_EN                      (FALSE)
#define ENC_PWM_SYNC_EN                   (TRUE)

#define ENC_A_GPIO_PORT                   GPIOA
#define ENC_B_GPIO_PORT                   GPIOA
#define ENC_A_GPIO_PIN                    GPIO_Pin_0
#define ENC_B_GPIO_PIN                    GPIO_Pin_1

/* Digital Outputs */
#define R_BRAKE_GPIO_PORT                 GPIOD
#define R_BRAKE_GPIO_PIN                  GPIO_Pin_5
#define OV_CURR_BYPASS_GPIO_PORT          GPIOD        
#define OV_CURR_BYPASS_GPIO_PIN           GPIO_Pin_5
#define INRUSH_CURRLIMIT_GPIO_PORT        GPIOD
#define INRUSH_CURRLIMIT_GPIO_PIN         GPIO_Pin_4

/* Resolver timer section */
#define RES_ETIMER_MODULE                 (RES_ETIMER_Module_0)    /* HW Settings */
#define RES_ETIMER_CHANNEL                (ETIMER_CH0)     /* Resolver excitation eTimer channel */
#define RES_ADC_MODULE_SIN                (ADC_UNIT_0)
#define RES_ADC_CHANNEL_SIN               (ADC_AN_1)
#define RES_ADC_MODULE_COS                (ADC_UNIT_1)
#define RES_ADC_CHANNEL_COS               (ADC_AN_1)
#define RES_DMA_CHANNEL                   (DMA_CHANNEL_FOR_RESOLVER)

/************************ ANALOG I/O DEFINITION SECTION  *********************/
/** Currents reading  **/
/* ICS */
#define ADC_0_PERIPH                    (ADC_UNIT_0)
#define PHASE_IA_CURR_CHANNEL           (ADC_AN_0)
#define ADC_1_PERIPH                    (ADC_UNIT_1)
#define PHASE_IB_CURR_CHANNEL           (ADC_AN_0)
#define ICS_DMA_CHANNEL                 (DMA_CHANNEL_FOR_CURRENT_SENSING)

/* Maximum modulation index */
#define MAX_MODULATION_100_PER_CENT

/* ADC User conversion */
#define ICS_DMA_USER_CHANNEL  (DMA_CHANNEL_FOR_ADC_CONVERSIONS)

#ifdef ADC_USER_ENABLE
#define ADC_USER_CONV_0       (0U)
#define ADC_MOD_USER_CONV_0   (0x0U)  /* ADC_UNIT_0 */
#define ADC_CH_USER_CONV_0    (ADC_AN_2)

#define ADC_USER_CONV_MAX      (1U)

/* parameter used by MC_RequestRegularConv */
#define ADC_USER_MODULE        (ADC_UNIT_0)
#define ADC_USER_CH            (ADC_AN_2)

#if (ADC_USER_CONV_MAX > 1U)
#define ADC_MULTIPLE_CONV_ENABLED
#endif /* #if (ADC_USER_CONV_MAX > 1U) */

#endif /* #ifdef ADC_USER_ENABLE */


/* Only for three shunt resistors  */
#define PHASE_U_CURR_ADC                ADC1_2
#define PHASE_U_CURR_CHANNEL            ADC_Channel_10
#define PHASE_U_GPIO_PORT               GPIOC
#define PHASE_U_GPIO_PIN                GPIO_Pin_0
#define PHASE_V_CURR_ADC                ADC1_2
#define PHASE_V_CURR_CHANNEL            ADC_Channel_11
#define PHASE_V_GPIO_PORT               GPIOC
#define PHASE_V_GPIO_PIN                GPIO_Pin_1
/* Only for three shunts case */
#define PHASE_W_CURR_ADC                ADC1_2
#define PHASE_W_CURR_CHANNEL            ADC_Channel_12
#define PHASE_W_GPIO_PORT               GPIOC
#define PHASE_W_GPIO_PIN                GPIO_Pin_2       
/* Only for 1 shunt resistor case */
#define ADC_PERIPH                      ADC1
#define PHASE_CURRENTS_CHANNEL          ADC_Channel_9
#define PHASE_CURRENTS_GPIO_PORT        GPIOB
#define PHASE_CURRENTS_GPIO_PIN         GPIO_Pin_1   

/* Common */
#define ADC_AHBPERIPH                   RCC_AHBPeriph_ADC12
#define ADC_CLOCK_WB_FREQ               12
#define ADC_CLOCK_WB_DIV                0
#define CURR_SAMPLING_TIME              7  /*!< Sampling time duration  
                                                           in ADC clock cycles (1 for  
                                                           1.5, 7 for 7.5, ...) */

/** Bus and temperature readings **/
#define REGCONVADC                      ADC1

#define VBUS_ADC_MODULE                 (ADC_UNIT_0) /*!< Adc module used for VBus and Temperature sensing */
#define VBUS_ADC_CHANNEL                (ADC_AN_3) /*!< Adc channel used for VBus and Temperature sensing */
#define VBUS_DMA_CHANNEL                (DMA_CHANNEL_FOR_VBUS_TEMPERATURE) /*!< Dma channel used for VBus and Temperature sensing */
#define VBUS_ADC_SAMPLING_TIME          (0U)
#define VBUS_ADC_MOD                    (0x0U)


#define TEMP_FDBK_ADC                   ADC1_2
#define TEMP_FDBK_CHANNEL               ADC_Channel_10
#define TEMP_FDBK_GPIO_PORT             GPIOC
#define TEMP_FDBK_GPIO_PIN              GPIO_Pin_0
#define TEMP_ADC_SAMPLING_TIME          13

/* Serial communication */
#define USART_SELECTION                 USE_USART1
#define USART_REMAPPING                 NO_REMAP_USART1
#define USART_TX_GPIO_PORT              GPIOA
#define USART_TX_GPIO_PIN               GPIO_Pin_9
#define USART_RX_GPIO_PORT              GPIOA
#define USART_RX_GPIO_PIN               GPIO_Pin_10
#define USART_SPEED                     115200

/* OPAMP Settings */
#define USE_INTERNAL_OPAMP                     DISABLE

#define OPAMP1_SELECTION                       OPAMP_Selection_OPAMP1
#define OPAMP1_INVERTINGINPUT_MODE             INT_MODE
#define OPAMP1_INVERTINGINPUT                  OPAMP1_InvertingInput_PGA
#define OPAMP1_INVERTINGINPUT_GPIO_PORT        GPIOA
#define OPAMP1_INVERTINGINPUT_GPIO_PIN         GPIO_Pin_3
#define OPAMP1_NONINVERTINGINPUT_PHA           OPAMP1_NonInvertingInput_PA1
#define OPAMP1_NONINVERTINGINPUT_PHA_GPIO_PORT GPIOA
#define OPAMP1_NONINVERTINGINPUT_PHA_GPIO_PIN  GPIO_Pin_1
#define OPAMP1_NONINVERTINGINPUT_PHB           OPAMP1_NonInvertingInput_PA7
#define OPAMP1_NONINVERTINGINPUT_PHB_GPIO_PORT GPIOA
#define OPAMP1_NONINVERTINGINPUT_PHB_GPIO_PIN  GPIO_Pin_7
#define OPAMP1_OUT_GPIO_PORT                   GPIOA
#define OPAMP1_OUT_GPIO_PIN                    GPIO_Pin_2

#define OPAMP2_SELECTION                       OPAMP_Selection_OPAMP2
#define OPAMP2_INVERTINGINPUT_MODE             INT_MODE
#define OPAMP2_INVERTINGINPUT                  OPAMP2_InvertingInput_PGA
#define OPAMP2_INVERTINGINPUT_GPIO_PORT        GPIOC
#define OPAMP2_INVERTINGINPUT_GPIO_PIN         GPIO_Pin_5
#define OPAMP2_NONINVERTINGINPUT_PHA           OPAMP2_NonInvertingInput_PD14
#define OPAMP2_NONINVERTINGINPUT_PHA_GPIO_PORT GPIOD
#define OPAMP2_NONINVERTINGINPUT_PHA_GPIO_PIN  GPIO_Pin_14
#define OPAMP2_NONINVERTINGINPUT_PHB           OPAMP2_NonInvertingInput_PA7
#define OPAMP2_NONINVERTINGINPUT_PHB_GPIO_PORT GPIOA
#define OPAMP2_NONINVERTINGINPUT_PHB_GPIO_PIN  GPIO_Pin_7
#define OPAMP2_NONINVERTINGINPUT_PHC           OPAMP2_NonInvertingInput_PD14
#define OPAMP2_NONINVERTINGINPUT_PHC_GPIO_PORT GPIOD
#define OPAMP2_NONINVERTINGINPUT_PHC_GPIO_PIN  GPIO_Pin_14
#define OPAMP2_OUT_GPIO_PORT                   GPIOA
#define OPAMP2_OUT_GPIO_PIN                    GPIO_Pin_6

/* Only for 1 shunt resistor case */
#define OPAMP_SELECTION                        OPAMP_Selection_OPAMP1
#define OPAMP_INVERTINGINPUT_MODE              INT_MODE
#define OPAMP_INVERTINGINPUT                   OPAMP1_InvertingInput_PGA
#define OPAMP_INVERTINGINPUT_GPIO_PORT         GPIOA
#define OPAMP_INVERTINGINPUT_GPIO_PIN          GPIO_Pin_3
#define OPAMP_NONINVERTINGINPUT                OPAMP1_NonInvertingInput_PA7
#define OPAMP_NONINVERTINGINPUT_GPIO_PORT      GPIOA
#define OPAMP_NONINVERTINGINPUT_GPIO_PIN       GPIO_Pin_7
#define OPAMP_OUT_GPIO_PORT                    GPIOA
#define OPAMP_OUT_GPIO_PIN                     GPIO_Pin_2

/* OPAMP common settings*/
#define OPAMP_PGAGAIN                          OPAMP_OPAMP_PGAGain_2
#define OPAMP_PGACONNECT                       OPAMP_PGAConnect_No

/* COMP Settings */
#define INTERNAL_OVERCURRENTPROTECTION    DISABLE
#define OCPREF                            23830

#define INTERNAL_OVERVOLTAGEPROTECTION    DISABLE
#define OVPREF                            23830

/* Only for 1 shunt resistor case */
#define OCP_SELECTION                     COMP_Selection_COMP2
#define OCP_INVERTINGINPUT_MODE           INT_MODE
#define OCP_INVERTINGINPUT                COMPX_InvertingInput_VREF
#define OCP_INVERTINGINPUT_GPIO_PORT      GPIOA
#define OCP_INVERTINGINPUT_GPIO_PIN       GPIO_Pin_4
#define OCP_NONINVERTINGINPUT             COMP2_NonInvertingInput_PA7
#define OCP_NONINVERTINGINPUT_GPIO_PORT   GPIOA
#define OCP_NONINVERTINGINPUT_GPIO_PIN    GPIO_Pin_7
#define OCP_OUTPUT_MODE                   INT_MODE
#define OCP_OUTPUT                        COMP_Output_TIM1BKIN2
#define OCP_OUTPUT_GPIO_PORT              GPIOA
#define OCP_OUTPUT_GPIO_PIN               GPIO_Pin_2
#define OCP_OUTPUT_GPIO_AF                GPIO_AF_8
#define OCP_OUTPUTPOL                     COMP_OutputPol_NonInverted

#define OCPA_SELECTION                    COMP_Selection_COMP1
#define OCPA_INVERTINGINPUT_MODE          INT_MODE
#define OCPA_INVERTINGINPUT               COMPX_InvertingInput_VREF
#define OCPA_INVERTINGINPUT_GPIO_PORT     GPIOA
#define OCPA_INVERTINGINPUT_GPIO_PIN      GPIO_Pin_4
#define OCPA_NONINVERTINGINPUT            COMP1_NonInvertingInput_PA1
#define OCPA_NONINVERTINGINPUT_GPIO_PORT  GPIOA
#define OCPA_NONINVERTINGINPUT_GPIO_PIN   GPIO_Pin_1
#define OCPA_OUTPUT_MODE                  INT_MODE
#define OCPA_OUTPUT                       COMP_Output_TIM1BKIN2
#define OCPA_OUTPUT_GPIO_PORT             GPIOA
#define OCPA_OUTPUT_GPIO_PIN              GPIO_Pin_0
#define OCPA_OUTPUT_GPIO_AF               GPIO_AF_8
#define OCPA_OUTPUTPOL                    COMP_OutputPol_NonInverted

#define OCPB_SELECTION                    COMP_Selection_COMP2
#define OCPB_INVERTINGINPUT_MODE          INT_MODE
#define OCPB_INVERTINGINPUT               COMPX_InvertingInput_VREF
#define OCPB_INVERTINGINPUT_GPIO_PORT     GPIOA
#define OCPB_INVERTINGINPUT_GPIO_PIN      GPIO_Pin_4
#define OCPB_NONINVERTINGINPUT            COMP2_NonInvertingInput_PA7
#define OCPB_NONINVERTINGINPUT_GPIO_PORT  GPIOA
#define OCPB_NONINVERTINGINPUT_GPIO_PIN   GPIO_Pin_7
#define OCPB_OUTPUT_MODE                  INT_MODE
#define OCPB_OUTPUT                       COMP_Output_TIM1BKIN2
#define OCPB_OUTPUT_GPIO_PORT             GPIOA
#define OCPB_OUTPUT_GPIO_PIN              GPIO_Pin_2
#define OCPB_OUTPUT_GPIO_AF               GPIO_AF_8
#define OCPB_OUTPUTPOL                    COMP_OutputPol_NonInverted

#define OCPC_SELECTION                    COMP_Selection_COMP3
#define OCPC_INVERTINGINPUT_MODE          INT_MODE
#define OCPC_INVERTINGINPUT               COMPX_InvertingInput_VREF
#define OCPC_INVERTINGINPUT_GPIO_PORT     GPIOA
#define OCPC_INVERTINGINPUT_GPIO_PIN      GPIO_Pin_4
#define OCPC_NONINVERTINGINPUT            COMP3_NonInvertingInput_PD14
#define OCPC_NONINVERTINGINPUT_GPIO_PORT  GPIOD
#define OCPC_NONINVERTINGINPUT_GPIO_PIN   GPIO_Pin_14
#define OCPC_OUTPUT_MODE                  INT_MODE
#define OCPC_OUTPUT                       COMP_Output_TIM1BKIN2
#define OCPC_OUTPUT_GPIO_PORT             GPIOC
#define OCPC_OUTPUT_GPIO_PIN              GPIO_Pin_8
#define OCPC_OUTPUT_GPIO_AF               GPIO_AF_7
#define OCPC_OUTPUTPOL                    COMP_OutputPol_NonInverted
                                                              
#define OVP_SELECTION                     COMP_Selection_COMP7
#define OVP_INVERTINGINPUT_MODE           INT_MODE
#define OVP_INVERTINGINPUT                COMPX_InvertingInput_VREF
#define OVP_INVERTINGINPUT_GPIO_PORT      GPIOA
#define OVP_INVERTINGINPUT_GPIO_PIN       GPIO_Pin_4
#define OVP_NONINVERTINGINPUT             COMP7_NonInvertingInput_PA0
#define OVP_NONINVERTINGINPUT_GPIO_PORT   GPIOA
#define OVP_NONINVERTINGINPUT_GPIO_PIN    GPIO_Pin_0
#define OVP_OUTPUT_MODE                   INT_MODE
#define OVP_OUTPUT                        COMP_Output_TIM1BKIN
#define OVP_OUTPUT_GPIO_PORT              GPIOC
#define OVP_OUTPUT_GPIO_PIN               GPIO_Pin_2
#define OVP_OUTPUT_GPIO_AF                GPIO_AF_3
#define OVP_OUTPUTPOL                     COMP_OutputPol_NonInverted

#define HIGH_SIDE_BRAKE_STATE             TURN_OFF /*!< TURN_OFF, TURN_ON */
#define LOW_SIDE_BRAKE_STATE              TURN_OFF /*!< TURN_OFF, TURN_ON */

#define BKIN1_FILTER                      0
#define BKIN2_FILTER                      0

#define OCP_FILTER                        COMP_Mode_HighSpeed
#define OVP_FILTER                        COMP_Mode_HighSpeed

/* Debug Setting */
#define DAC_FUNCTIONALITY                DISABLE
#define DEBUG_DAC_CH1                    ENABLE
#define DEBUG_DAC_CH2                    ENABLE
#define DEFAULT_DAC_CHANNEL_1            MC_PROTOCOL_REG_I_A
#define DEFAULT_DAC_CHANNEL_2            MC_PROTOCOL_REG_I_B
#define DEFAULT_DAC_MOTOR                0

#define DAC_TIMER_SELECTION              TIM3
#define DAC_TIMER_REMAPPING              NO_REMAP
#define DAC_TIMER_CH1_GPIO_PORT          GPIOB
#define DAC_TIMER_CH1_GPIO_PIN           GPIO_Pin_0
#define DAC_TIMER_CH2_GPIO_PORT          GPIOB
#define DAC_TIMER_CH2_GPIO_PIN           GPIO_Pin_1

#define SW_OV_CURRENT_PROT_ENABLING      DISABLE /*!< Over-current detection 
                                                         enabling */

#define START_STOP_GPIO_PORT             GPIOA
#define START_STOP_GPIO_PIN              GPIO_Pin_0
#define START_STOP_POLARITY              DIN_ACTIVE_LOW

#endif /*__CONTROL_STAGE_PARAMETERS_H*/
