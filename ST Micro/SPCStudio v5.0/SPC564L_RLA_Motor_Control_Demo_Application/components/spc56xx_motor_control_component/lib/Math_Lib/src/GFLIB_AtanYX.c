/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    GFLIB_AtanYX.c
*   @version BETA 0.9.1
*   @brief   Function performs calculation the TangentYX. 
*          
*   @details Function performs calculation the TangentYX.  
*
*/
#ifdef __cplusplus
extern "C" {
#endif
    
/******************************************************************************
 * Includes
 ******************************************************************************/
#include "SWLIBS_Typedefs.h"

#include "GFLIB_Atan.h"
#include "GFLIB_AtanYX.h"

/******************************************************************************
 * External declarations
 ******************************************************************************/

/******************************************************************************
 * Defines and macros            (scope: module-local)
 ******************************************************************************/
    
/******************************************************************************
 * Typedefs and structures       (scope: module-local)
 ******************************************************************************/


/******************************************************************************
 * Global variable definitions   (scope: module-exported)
 ******************************************************************************/

/******************************************************************************
 * Global variable definitions   (scope: module-local)
 ******************************************************************************/

/******************************************************************************
 * Function prototypes           (scope: module-local)
 ******************************************************************************/

/*******************************************************************************
 * Function implementations      (scope: module-local)
 ******************************************************************************/

/******************************************************************************
 * Function implementations      (scope: module-exported)
 ******************************************************************************/

/**************************************************************************//*!
@brief      The function calculates the angle between the positive x-axis
            and the direction of a vector given by the (x, y) coordinates.

@param[in]  s32InY  The ordinate of the input vector (y coordinate).
@param[in]  s32InX  The abscissa of the input vector (x coordinate).

@return     The function returns the angle between the positive x-axis of
            a plane and the direction of the vector given by the x and y
            coordinates provided as parameters.
        
@details    The function returns the angle between the positive x-axis of
            a plane and the direction of the vector given by the x and y
            coordinates provided as parameters.  The first parameter, \c
            s32InY, is the ordinate (the y coordinate) and the second one, \c
            s32InX, is the abscissa (the x coordinate).

            Both the input parameters are assumed to be in the fractional range
            of \f$[-1, 1)\f$.  The computed angle is limited by the
            fractional range of \f$[-1, 1)\f$, which corresponds to the real
            range of \f$[-\pi, \pi)\f$.  The counter-clockwise direction is
            assumed to be positive and thus a positive angle will be computed
            if the provided ordinate (\c s32InY) is positive. Similarly a
            negative angle will be computed for the negative ordinate.

            The calculations are performed in a few steps.
            
            In the first step, the angle is positioned within a correct
            half-quarter of the circumference of a circle by dividing the angle
            into two parts: the integral multiple of 45 deg (half-quarter) and
            the remaining offset within the 45 deg range.  Simple geometric
            properties of the cartesian coordinate system are used to calculate
            the coordinates of the vector with the calculated angle offset.

            In the second step, the vector ordinate is divided by the vector
            abscissa (y/x) to obtain the tangent value of the angle offset.
            The angle offset is computed by applying the ordinary arc
            tangent function.

            The sum of the integral multiple of half-quarters and the angle
            offset within a single half-quarter form the angle to be computed.

            The function will return 0 if both input arguments are 0.
                                   
            In comparison to the #GFLIB_Atan function, the #GFLIB_AtanYX
            function correctly places the calculated angle in the whole
            fractional range of \f$\left[-1, 1\right)\f$, which corresponds to
            the real angle range of \f$[-\pi, \pi)\f$.
            
@note       The function calls the #GFLIB_Atan function. The computed value
            is within the range of \f$[-1, 1)\f$.

@par Reentrancy:
            The function is reentrant.

@par Code Example:
\code
#include "gflib.h"

tFrac32 s32InY;
tFrac32 s32InX;
tFrac32 s32Ang;

void main(void)
{
    s32InY = FRAC32(0.5);
    s32InX = FRAC32(0.5);
    
    // Angle 45 deg = PI/4 rad = 0.25 = 0x20000000
    // output should be close to 0x20001000
    s32Ang = GFLIB_AtanYX(s32InY, s32InX);

    return;
}
\endcode

@par Performance:
            \anchor tab1_GFLIB_ControllerPIp
            <table border="1" CELLPADDING="5" align = "center">
            <caption>#GFLIB_AtanYX function performance</caption>
            <tr>
              <th>Code size [bytes] GHS/CW</th> <td>408/384</td>
            </tr>
            <tr>
              <th>Data size [bytes] GHS/CW</th> <td>0/0</td>
            </tr>
            <tr>
              <th>Execution clock cycles max [clk] GHS/CW</th> <td>216/214</td>
            </tr>
            <tr>
              <th>Execution clock cycles min [clk] GHS/CW</th> <td>23/31</td>
            </tr>
            </table>

******************************************************************************/

math_tS32 GFLIB_AtanYXANSIC(math_tS32 s32InY, math_tS32 s32InX)
{
    tFrac32 s32Ang1;
    tFrac32 s32Ang2;
    tFrac32 s32O1Tan;
    tFrac32 s32Y2;
    math_tS32 s32Norm;

    /* For (0, 0) vector return 0 */
    if( (s32InY | s32InX) == 0)
    {
        return 0;
    }

    /* Firstly,  position the angle within a quarter */
    s32Ang1 = 0;
    if( s32InY < 0 )
    {
        if( s32InX < 0 )
        {
            s32Ang1 = FRAC32(-1.0);
            s32InY = -s32InY;
            s32InX = -s32InX;
        }
        else
        {
            s32Ang1 = FRAC32(-0.5);
            s32Y2 = s32InY;
            s32InY = s32InX;
            s32InX = -s32Y2;
        }
    }
    else
    {
        if( s32InX < 0 )
        {
            s32Ang1 = FRAC32(0.5);
            s32Y2 = s32InY;
            s32InY = -s32InX;
            s32InX = s32Y2;
        }
    } 

    /* Secondly, compute the angle offset within half-quarter
     * This transformation can be also viewed as a single CORDIC algorithm
     * iteration. */

    /* NOTE 1: 
     *   The unsigned casting is necessary for some boundary conditions.  For
     *   example if initially s32InY = 0 and s32InX = 0x80000000 (= -1.0) then
     *   here s32InY will be 0x80000000 (= 1.0).
     * 
     * NOTE 2:
     *   The first condition may also include the equal sign (>=)
     *   however then for the case s32InY = 0x80000000, x32InX = 0x80000000
     *   the division would get s32InY = 0, s32InX = 0, which is 
     *   division by 0.  Because division by 0 may be treated differently
     *   on different platform, the case is treated by a separate
     *   condition of s32InY and s32InX equal.
     *
     * NOTE 3:
     *   Before the division the values are normalized.  This is necessary
     *   to get the required accuracy. */

    if (((math_tU32) s32InY) > ((math_tU32) s32InX))
    {
        s32Ang1 += FRAC32(0.25);
        s32Y2 = s32InY;
        s32InY -= s32InX;
        s32InX += s32Y2;

        /* s32InX needs to be treated as unsigned number */
        s32Norm = (s32InX < 0) ? 0:F32Norm(s32InX);

        /* Normalization, note that s32InY < s32InX (unsigned) */
        s32InY = s32InY << s32Norm;
        s32InX = s32InX << s32Norm;

        /* logical shift right */
        s32InX = (math_tS32) (((math_tU32) s32InX)>>16);
        s32O1Tan = (tFrac32)(((math_tU32) (s32InY))/((math_tU32) s32InX));
    }
    else if (((math_tU32) s32InY) == ((math_tU32) s32InX))
    {
        s32Ang1 += FRAC32(0.25);
        s32O1Tan = 0;
    }
    else
    {
        /* s32InX needs to be treated as unsigned number */
        s32Norm = (s32InX < 0) ? 0:F32Norm(s32InX);

        /* Normalization, note that s32InY < s32InX (unsigned) */
        s32InY = s32InY << s32Norm;
        s32InX = s32InX << s32Norm;

        /* logical shift right */
        s32InX = (math_tS32) (((math_tU32) s32InX)>>16);

        s32O1Tan = (tFrac32)(((math_tU32) (s32InY))/((math_tU32) s32InX));
    }

    /* Calculate tangent within 45 deg and use Atan function to 
     * get the angle offset within 45 deg.
     */

    /* 
     * NOTE:
     *   Saturation is necessary to protect from the overflow over
     *   0x7fffffff.
     */
    s32O1Tan = s32O1Tan<<14;
    s32O1Tan = F32AddSat(s32O1Tan, s32O1Tan);
    s32Ang2 = GFLIB_Atan(s32O1Tan);

    /* The final angle is the sum of the angle located within 45 deg
     * and the angle offset.
     *
     * NOTE:
     *   Saturation is not necessary due to circular nature of the angle.
     */
    s32Ang1 = F32Add(s32Ang1, s32Ang2);

    return s32Ang1;
}

#ifdef __cplusplus
}
#endif
