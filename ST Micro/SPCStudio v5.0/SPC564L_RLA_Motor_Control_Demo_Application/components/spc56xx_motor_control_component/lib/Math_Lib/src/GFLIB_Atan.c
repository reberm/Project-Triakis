/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    GFLIB_Atan.c
*   @version BETA 0.9.1
*   @brief   Function performs calculation the Tangent. 
*          
*   @details Function performs calculation the Tangent. 
*
*/

/** @addtogroup SPC5_PMSM_MC_Mathlib
  * @{
  */

/** @addtogroup GFLIB_ATAN
  * @{
*/

#ifdef __cplusplus
extern "C" {
#endif
    
/******************************************************************************
 * Includes
 ******************************************************************************/
#include "SWLIBS_Typedefs.h"

#include "GFLIB_Atan.h"

/******************************************************************************
 * External declarations
 ******************************************************************************/

/******************************************************************************
 * Defines and macros            (scope: module-local)
 ******************************************************************************/

/******************************************************************************
 * Typedefs and structures       (scope: module-local)
 ******************************************************************************/


/******************************************************************************
 * Global variable definitions   (scope: module-exported)
 ******************************************************************************/

/******************************************************************************
 * Global variable definitions   (scope: module-local)
 ******************************************************************************/
const GFLIB_ATAN_TAYLOR_T gflibAtanCoef = {
    {
        {
            {-53248,   -165888,   42557440,    42668032}    /* Coef.<0;1/8) */
        },
        {
            {-45056,   -464896,   41271296,    126697472}   /* Coef.<1/8;2/8) */
        },
        {
            {-30720,   -690176,   38922240,    207040512}    /* Coef.<2/8;3/8) */
        },
        {
            {-14336,   -821248,   35858432,    281909248}    /* Coef.<3/8;4/8) */
        },
        {
            {-2048,    -866304,   32454656,    350251008}    /* Coef.<4/8;5/8) */
        },
        {
            {8192,    -845824,   29009920,    411703296}    /* Coef.<5/8;6/8) */
        },
        {
            {12288,    -786432,   25735168,    466407424}    /* Coef.<6/8;7/8) */
        },
        {
            {14336,    -708608,   22738944,    514828288}    /* Coef.<7/8;1) */
        }
    }
};

/******************************************************************************
 * Function prototypes           (scope: module-local)
 ******************************************************************************/

/******************************************************************************
 * Function implementations      (scope: module-local)
 ******************************************************************************/

/******************************************************************************
 * Function implementations      (scope: module-exported)
 ******************************************************************************/

/**************************************************************************//*!
@brief      Arctangent function - Taylor polynomial approximation.

@param[in]  *pParam     Pointer to array of Taylor coefficients. Using the
                        function alias #GFLIB_Atan, default coefficients are
                        used.
@param[in]  s32In       Input argument is a 32bit number between 
                        \f$\left[-1,1\right)\f$.


@return     The function returns the atan of the input argument as fixed point
            32-bit number that contains the angle in radians between 
            \f$\left[-\frac{\pi}{4},\frac{\pi}{4}\right)\f$, normalized between 
            \f$\left[-0.25,0.25 \right)\f$.

@details    The #GFLIB_AtanANSIC function, denoting ANSI-C compatible
            source code implementation, can be called via function alias
            #GFLIB_Atan.

            \par
            The #GFLIB_Atan function provides a computational method for
            calculation of a standard trigonometric \e arctangent function
            \f$\arctan(x)\f$, using the piece-wise polynomial approximation.
            Function \f$\arctan(x)\f$ takes a ratio and returns the angle of two
            sides of a right triangle. The ratio is the length of the side
            opposite the angle divided by the length of the side adjacent to
            the angle. The graph of \f$\arctan(x)\f$ is shown on 
            Fig. \ref fig1_GFLIB_Atan.

            \anchor fig1_GFLIB_Atan
            \image latex GFLIB_Atan_Figure1.eps "Course of the functionGFLIB_Atan" width=10cm
            \image html GFLIB_Atan_Figure1.jpg "Course of the functionGFLIB_Atan"

            #GFLIB_Atan function is implemented with considering fixed
            point fractional arithmetic. As can be further seen from 
            Fig. \ref fig1_GFLIB_Atan, arctangent values are identical for input
            range   \f$\left[-1,0\right)\f$ and \f$\left[0,1\right)\f$.

            Moreover it can be observed from Fig. \ref fig1_GFLIB_Atan,
            that course of the \f$\arctan(x)\f$ function
            output for ratio in interval 1. is identical, but with opposite sign,
            to output for ratio in interval 2. Therefore the approximation of
            \e arctangent function for whole defined range of input ratio can be
            simplified to approximation for ratio in range \f$\left[0,1\right)\f$,
            and then depending on the input ratio the result will be negated.
            In order to increase accuracy of approximation without need for a
            high order polynomial, the interval \f$\left[0,1\right)\f$
            is further divided into eight equally spaced sub intervals, and
            polynomial approximation is done for each interval respectively.
            Such division results in eight sets of
            polynomial coefficients. Moreover it allows using polynomial only of 4th order
            to achieve accuracy of less than 0.5LSB (on upper 16 bits of 32 bit
            results) across full range of input ratio.

            \par
            #GFLIB_Atan function uses fixed point fractional arithmetic so to
            cast the fractional value of the output angle
            \f$\left[-0.25,0.25 \right)\f$
            into correct range \f$\left[-\frac{\pi}{4},\frac{\pi}{4} \right)\f$,
            the fixed point output angle can be multiplied by \f$\pi\f$ for 
            angle in radians. Then the fixed point fractional
            implementation of the approximation polynomial, used for calculation
            of each sub sector, is defined as follows:
            \anchor eq1_GFLIB_Atan
            \f[
                \mbox{s32Dump} = a_1 \cdot s32In^3 + a_2 \cdot s32In^2 + a_3 \cdot s32In + a_4
            \f]

            \anchor eq2_GFLIB_Atan
            \f[
                \arctan(s32In) =
                    \left\{\begin{array}{lclcl}
                       \mbox{s32Dump}  & \mbox{if} & 0 \leq \mbox{s32In} < 1 \\
                       \\
                       \mbox{-s32Dump} & \mbox{if} & -1 \leq \mbox{s32In} < 0
                    \end{array}\right.
            \f]

            Division of \f$\left[0,1\right)\f$ interval into eight
            sub-intervals with polynomial coefficients calculated for each
            sub-interval, are noted in table \ref tab1_GFLIB_Atan. Polynomial
            coefficients were obtained using Matlab fitting function,
            where polynomial of 4th order was used for fitting of each respective
            sub-interval.

            \anchor tab1_GFLIB_Atan
            <table border="1" CELLPADDING="5" align = "center">
            <caption>Integer Polynomial coefficients for each interval:</caption>
            <tr>
              <th>Interval</th>
              <th>\f$a_1\f$</th>
              <th>\f$a_2\f$</th>
              <th>\f$a_3\f$</th>
              <th>\f$a_4\f$</th>
            </tr>
            <tr align="center">
              <td style="font-weight:bold"> \f$ \left< 0 , \frac{1}{8} \right) \f$ </td>
              <td>-53248 </td> <td>-165888 </td> <td>42557440</td> <td>42668032</td>
            </tr>
            <tr align="center">
              <td style="font-weight:bold">\f$ \left< \frac{1}{8}  , \frac{2}{8} \right) \f$</td>
              <td>-45056</td><td>-464896</td><td>41271296</td><td>126697472</td>
            </tr>
            <tr align="center">
              <td style="font-weight:bold">\f$ \left< \frac{2}{8}  , \frac{3}{8} \right) \f$</td>
              <td>-30720 <td>-690176<td>38922240<td>207040512
            </tr>
            <tr align="center">
              <td style="font-weight:bold">\f$ \left< \frac{3}{8}  , \frac{4}{8} \right) \f$</td>
              <td>-14336<td>-821248<td>35858432<td>281909248
            </tr>
            <tr align="center">
              <td style="font-weight:bold">\f$ \left< \frac{4}{8}  , \frac{5}{8} \right) \f$</td>
              <td>-2048<td>-866304<td>32454656<td>350251008
            </tr>
            <tr align="center">
              <td style="font-weight:bold">\f$ \left< \frac{5}{8}  , \frac{6}{8} \right) \f$</td>
              <td>8192<td>-845824<td>29009920<td>411703296
            </tr>
            <tr align="center">
              <td style="font-weight:bold">\f$ \left< \frac{6}{8}  , \frac{7}{8} \right) \f$</td>
              <td>12288<td>-786432<td>25735168<td>466407424
            </tr>
            <tr align="center">
              <td style="font-weight:bold">\f$ \left< \frac{7}{8}  , 1 \right) \f$</td>
              <td>14336<td>-708608<td>22738944<td>514828288
            </tr>
            </table>

            \anchor fig2_GFLIB_Atan
            \image latex GFLIB_Atan_Figure2.eps "atan(x) vs. GFLIB_Atan(s32In)" width=14cm
            \image html GFLIB_Atan_Figure2.jpg "atan(x) vs. GFLIB_Atan(s32In)"

            \par
            Fig.\ref fig2_GFLIB_Atan depicts a floating
            point \e arctangent function generated from Matlab and approximated value of
            \e arctangent function obtained from #GFLIB_Atan, plus their difference.
            The course of calculation accuracy as a function of input value can be
            observed from this figure.
            The achieved accuracy with considering the 4th order piece-wise polynomial
            approximation and described fixed point scaling, is less 0.5LSB on upper
            16-bit of the 32bit result.

@note       Output angle is normalized into range \f$\left[-0.25,0.25\right)\f$.
            The polynomial coefficients are stored in locally defined structure,
            call of which is masked by function alias #GFLIB_Atan.
            The polynomial coefficients can be calculated by the user and in
            such case full #GFLIB_AtanANSIC function call with pointer to
            the newly defined coefficients shall be used instead of the function
            alias.

@par Reentrancy:
            The function is reentrant.

@par Code Example:
\code
#include "gflib.h"

tFrac32 s32Input;
tFrac32 s32Angle;

void main(void)
{
    // input ratio = 0x7FFFFFFF => 1
    s32Input  = FRAC32(1);

    // output angle should be 0x1FFFBD7F = 0.249999 => pi/4
    s32Angle = GFLIB_Atan(s32Input);
}
\endcode

@par Performance:
            \anchor tab2_GFLIB_Atan
            <table border="1" CELLPADDING="5" align = "center">
            <caption>#GFLIB_Atan function performance</caption>
            <tr>
              <th>Code size [bytes] GHS/CW</th> <td>108/92</td>
            </tr>
            <tr>
              <th>Data size [bytes] GHS/CW</th> <td>128/128</td>
            </tr>
            <tr>
              <th>Execution clock cycles max [clk] GHS/CW</th> <td>57/52</td>
            </tr>
            <tr>
              <th>Execution clock cycles min [clk] GHS/CW</th> <td>51/46</td>
            </tr>
            </table>

******************************************************************************/

tFrac32 GFLIB_AtanANSIC(tFrac32 s32In, const GFLIB_ATAN_TAYLOR_T *const pParam)
{
    tFrac32 s32Dump;
    tFrac32 s32Sign;
    math_tS32    nSector;

    /* The Tan function is same on the ranges (-pi;-pi/2) and (0;pi/2) */
    
    /* The sign is saved in the s32Sign. */
    /* Function -(ABS(s32In)) */
    /* Saturation is necessary to protect from overflow at 0x80000000. */
    s32Sign = s32In;
    s32In = F32AbsSat(s32In);
        
    /* Polynom sector choose */
    nSector =  s32In >> 28;
    
    /* Input value is scaled on the range FRAC <0,0.5) */
    s32In = s32In - (nSector << 28);    
    s32In = s32In << 3;
    
    /* Input value is scaled on the range FRAC <-0.5,0.5) */
    s32In = s32In - INT32_MAX/2;

    /* Input value is scaled on the range FRAC <-1,1) and value prepar for */
    /* multiple 16 x 16 = 32 */
    s32In = s32In >> 15;
    
    /* Polynom */
    /* (a1 * x) + a2 */
    s32Dump = ((pParam->GFLIB_ATAN_SECTOR[nSector].s32a[0] >> 13) * (s32In>>2)) + pParam->GFLIB_ATAN_SECTOR[nSector].s32a[1];
    /* ((a1 * x) + a2) * x + a3 */
    s32Dump = ((s32Dump >> 13) * (s32In>>2)) + pParam->GFLIB_ATAN_SECTOR[nSector].s32a[2];
    /* (((a1 * x) + a2) * x + a3) * x + a4 */
    s32Dump = ((s32Dump >> 13) *(s32In>>2)) + pParam->GFLIB_ATAN_SECTOR[nSector].s32a[3];
    
    /* Sign is added to the result value */
    s32Dump = (s32Sign > 0)?s32Dump:-s32Dump;

    return (s32Dump);
}

#ifdef __cplusplus
}
#endif


/**
  * @}
  */
  
/**
  * @}
  */

