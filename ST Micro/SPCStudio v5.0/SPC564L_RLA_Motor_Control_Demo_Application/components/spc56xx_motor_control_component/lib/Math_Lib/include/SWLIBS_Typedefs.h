/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    SWLIBS_Typedefs.h
*   @version BETA 0.9.1
*   @brief   Basic types definition file.
*          
*   @details Basic types definition file.
*
*/
#ifndef _SWLIBS_TYPEDEFS_H
#define _SWLIBS_TYPEDEFS_H

/******************************************************************************
* Defines and macros            (scope: module-local)
******************************************************************************/
#ifndef NULL
#ifdef __cplusplus
#define NULL 0
#define NULL ((void*)0)
#endif
#endif

/******************************************************************************
* Typedefs and structures       (scope: module-local)
******************************************************************************/
typedef unsigned char       tBool;          /*!< basic boolean type */

#ifndef FALSE
#define FALSE ((tBool)0)                    /*!< Boolean type FALSE constant */
#endif

#ifndef TRUE
#define TRUE ((tBool)1)                     /*!< Boolean type TRUE constant */
#endif

typedef unsigned char       math_tU8;            /*!< unsigned 8-bit integer type */
typedef signed char         math_tS8;            /*!< signed 8-bit integer type */
typedef unsigned short      math_tU16;           /*!< unsigned 16-bit integer type */
typedef signed short        math_tS16;           /*!< signed 16-bit integer type */
typedef unsigned int        math_tU32;           /*!< unsigned 32-bit integer type */
typedef signed int          math_tS32;           /*!< signed 32-bit integer type */
typedef unsigned long long  math_tU64;           /*!< unsigned 64-bit integer type */
typedef signed long long    math_tS64;           /*!< signed 64-bit integer type */


typedef math_tS16           tFrac16;             /*!< 16-bit signed fractional 
                                                      Q1.15 type */
typedef math_tS32           tFrac32;             /*!< 32-bit signed fractional 
                                                      Q1.31 type */

/*------------------------------------------------------------------------*//*!
@struct SWLIBS_3Syst "\SWLIBS_Typedefs.h"

@brief  Structure data type for three axis input/output variables.
*//*-------------------------------------------------------------------------*/
typedef struct
{
    tFrac32 s32Arg1;   /*!< First argument signed 32bit fractional type */
    tFrac32 s32Arg2;   /*!< Second argument signed 32bit fractional type */
    tFrac32 s32Arg3;   /*!< Third argument signed 32bit fractional type */
} SWLIBS_3Syst;

/*------------------------------------------------------------------------*//*!
@struct SWLIBS_2Syst "\SWLIBS_Typedefs.h"

@brief  Structure data type for two axis input/output variables.
*//*-------------------------------------------------------------------------*/
typedef struct
{
    tFrac32 s32Arg1;   /*!< First argument signed 32bit fractional type */
    tFrac32 s32Arg2;   /*!< Second argument signed 32bit fractional type */
} SWLIBS_2Syst;

/******************************************************************************
* Exported variables
******************************************************************************/

/******************************************************************************
* Exported function prototypes
******************************************************************************/

/******************************************************************************
* Inline functions
******************************************************************************/

#endif /* _SWLIBS_TYPEDEFS_H */
