/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    SWLIBS_Defines.h
*   @version BETA 0.9.1
*   @brief   Defines and common Macros definition file.
*          
*   @details Defines and common Macros definition file.
*
*/
#ifndef _SWLIBS_DEFINES_H
#define _SWLIBS_DEFINES_H

#include "SWLIBS_Typedefs.h"

/******************************************************************************
* Defines and macros            (scope: module-local)
******************************************************************************/
#define USE_FRAC32_ARITHMETIC

/*! Constant representing maximal negative value of signed fixed point fractional
 * 16-bit number e.q. -1.0 */
#define SFRACT_MIN          (-1.0)

/*! Constant representing maximal positive value of signed fixed point fractional
 * 16-bit number e.q. 0.999969482421875 */
#define SFRACT_MAX          (0.999969482421875)

/*! Constant representing maximal negative value of signed fixed point fractional
 * 32-bit number e.q. -1.0 */
#define FRACT_MIN           (-1.0)

/*! Constant representing maximal positive value of signed fixed point fractional
 * 32-bit number e.q. 0.9999999995343387126922607421875 */
#define FRACT_MAX           (0.9999999995343387126922607421875)

/*! Constant representing maximal positive value of signed fixed point integer
 * 16-bit number e.q. \f$ 2^{16-1}-1 = \f$ 0x7fff. */
#ifndef INT16_MAX
#define INT16_MAX           ((math_tS16) 0x7fff)
#endif

/*! Constant representing maximal negative value of signed fixed point integer
 * 16-bit number e.q. \f$ -2^{16-1} = \f$ 0x8000. */
#ifndef INT16_MIN
#define INT16_MIN           ((math_tS16) 0x8000)
#endif

/*! Constant representing maximal positive value of signed fixed point integer
 * 32-bit number e.q. \f$ 2^{32-1}-1 = \f$ 0x7fff ffff. */
#ifndef INT32_MAX
#define INT32_MAX           ((math_tS32) 0x7fffffff)
#endif

/*! Constant representing maximal negative value of signed fixed point integer
 * 32-bit number e.q. \f$ -2^{32-1} = \f$ 0x8000 0000. */
#ifndef INT32_MIN
#define INT32_MIN           ((math_tS32) 0x80000000)
#endif

/*! Macro converting signed fractional [-1,1) number into a fixed point 16-bit
 * number in format Q1.15.*/
#define FRAC16(x)           ((tFrac16) ((x) < (SFRACT_MAX) ? ((x) >= SFRACT_MIN ? (x)*0x8000 : 0x8000) : 0x7fff))

/*! Macro converting signed fractional [-1,1) number into a fixed point 32-bit
 * number in format Q1.31.*/
#define FRAC32(x)           ((tFrac32) ((x) < (FRACT_MAX) ? ((x) >= FRACT_MIN ? (x)*0x80000000 : 0x80000000) : 0x7fffffff))

/*! Type casting - signed fractional 16-bit value casted to signed integer 16-bit */
#define F16TOINT16(x)       ((math_tS16) (x))

/*! Type casting - lower 16bits of signed fractional 32-bit value casted
 * to signed integer 16-bit */
#define F32TOINT16(x)       ((math_tS16) (x))

/*! Type casting - lower 16bits of signed fractional 64-bit value casted
 * to signed integer 16-bit */
#define F64TOINT16(x)       ((math_tS16) (x))

/*! Type casting - signed fractional 16-bit value casted to signed integer
 * 32-bit, value placed at lower 16 bits of 32 bit result */
#define F16TOINT32(x)       ((math_tS32) (x))

/*! Type casting - signed fractional 32-bit value casted to signed integer
 * 32-bit */
#define F32TOINT32(x)       ((math_tS32) (x))

/*! Type casting - lower 32bits of signed fractional 64-bit value casted
 * to signed integer 32-bit */
#define F64TOINT32(x)       ((math_tS32) (x))

/*! Type casting - signed fractional 16-bit value casted to signed integer
 * 64-bit, value placed at lower 16 bits of 64 bit result*/
#define F16TOINT64(x)       ((math_tS64) (x))

/*! Type casting - signed fractional 32-bit value casted to signed integer
 * 64-bit, value placed at lower 32 bits of 64 bit result*/
#define F32TOINT64(x)       ((math_tS64) (x))

/*! Type casting - signed fractional 64-bit value casted to signed integer
 * 64-bit*/
#define F64TOINT64(x)       ((math_tS64) (x))

/*! Type casting - signed integer 16-bit value casted to signed fractional
 * 16-bit*/
#define INT16TOF16(x)       ((tFrac16) (x))

/*! Type casting - signed integer 16-bit value casted to signed fractional
 * 32-bit, value placed at lower 16 bits of 32 bit result*/
#define INT16TOF32(x)       ((tFrac32) (x))

/*! Type casting - lower 16 bits of signed integer 32-bit value casted
 * to signed fractional 16-bit*/
#define INT32TOF16(x)       ((tFrac16) (x))

/*! Type casting - signed integer 32-bit value casted to signed fractional
 * 32-bit*/
#define INT32TOF32(x)       ((tFrac32) (x))

/*! Type casting - lower 16 bits of signed integer 64-bit value casted
 * to signed fractional 16-bit*/
#define INT64TOF16(x)       ((tFrac16) (x))

/*! Type casting - lower 32 bits of signed integer 64-bit value casted
 * to signed fractional 32-bit*/
#define INT64TOF32(x)       ((tFrac32) (x))

/*! One over sqrt(3) with 16 bit result, result rounded for better
 * precision,i.e. \f$ round(1/sqrt(3)*2^{15}) \f$ */
#define F16_1_DIVBY_SQRT3   ((tFrac16) 0x49E7)

/*! One over sqrt(3) with 32 bit result, result rounded for better
 *  precision,i.e. \f$ round(1/sqrt(3)*2^{31}) \f$ */
#define F32_1_DIVBY_SQRT3   ((tFrac32) 0x49E69D16)

/*! Sqrt(3) divided by two with 16 bit result, result rounded for better
 * precision,i.e. \f$ round(sqrt(3)/2*2^{15}) \f$ */
#define F16_SQRT3_DIVBY_2   ((tFrac16) 0x6EDA)

/*! Sqrt(3) divided by two with 32 bit result, result rounded for better
 * precision,i.e. \f$ round(sqrt(3)/2*2^{31}) \f$ */
#define F32_SQRT3_DIVBY_2   ((tFrac32) 0x6ED9EBA1)

/******************************************************************************
* Typedefs and structures       (scope: module-local)
******************************************************************************/

/******************************************************************************
* Exported variables
******************************************************************************/

/******************************************************************************
* Exported function prototypes
******************************************************************************/

/******************************************************************************
* Inline functions
******************************************************************************/

#endif /* _SWLIBS_DEFINES_H */

