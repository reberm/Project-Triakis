/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    GFLIB_AtanYX.h
*   @version BETA 0.9.1
*   @brief   Header file for #GFLIB_AtanYX function.
*          
*   @details Header file for #GFLIB_AtanYX function.
*
*/
#ifndef GFLIB_ATANYX_H_
#define GFLIB_ATANYX_H_

#ifndef  _MATLAB_BAM_CREATE    
#include "SWLIBS_Typedefs.h"
#include "SWLIBS_Defines.h"
#include "SWLIBS_Inlines.h"
#endif
/******************************************************************************
* Defines and macros            (scope: module-local)
******************************************************************************/
/*!
  \def GFLIB_AtanYX
  Function alias for the #GFLIB_AtanYXANSIC function.
*/
#define GFLIB_AtanYX(y,x) GFLIB_AtanYXANSIC((y), (x))

/*!
  \def GFLIB_AtanXY
  Function alias for the #GFLIB_AtanYXANSIC function.
*/
#define GFLIB_AtanXY(x, y) GFLIB_AtanYXANSIC((y), (x))

/******************************************************************************
* Typedefs and structures       (scope: module-local)
******************************************************************************/

/******************************************************************************
* Exported Variables
******************************************************************************/

/******************************************************************************
* Exported function prototypes
******************************************************************************/
/*! 
* @ingroup GFLIB_GROUP 
*/
extern math_tS32 GFLIB_AtanYXANSIC(math_tS32 s32InY, math_tS32 s32InX);

/******************************************************************************
* Inline functions
******************************************************************************/ 

#endif /*GFLIB_ATANYX_H_*/
