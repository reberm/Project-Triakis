/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    GFLIB_Atan.h
*   @version BETA 0.9.1
*   @brief   Header file for #GFLIB_Atan function.
*          
*   @details Header file for #GFLIB_Atan function.
*
*/
#ifndef GFLIB_ATAN_H_
#define GFLIB_ATAN_H_

#ifndef  _MATLAB_BAM_CREATE    
#include "SWLIBS_Typedefs.h"
#include "SWLIBS_Defines.h"
#include "SWLIBS_Inlines.h"
#endif

/** @addtogroup SPC5_PMSM_MC_Mathlib
  * @{
  */
  
/** @addtogroup GFLIB_ATAN
  * @{
  */

/** @defgroup ATAN_class_exported_types ATAN class exported types
* @{
*/


/******************************************************************************
* Defines and macros            (scope: module-local)
******************************************************************************/
/*!
  \def GFLIB_Atan
  Function alias.
*/
#define GFLIB_Atan(x) GFLIB_AtanANSIC((x), &gflibAtanCoef)

/******************************************************************************
* Typedefs and structures       (scope: module-local)
******************************************************************************/

/*------------------------------------------------------------------------*//*!
@struct GFLIB_ATAN_TAYLOR_COEF_T "\GFLIB_Atan.h"

@brief      Structure containing four polynomial coefficients for one sub-interval.

@details    Output of \f$\arctan\left(s32In\right)\f$ for
            interval \f$\left[0,1\right)\f$ of input ratio is divided
            into eight sub-sectors. Polynomial approximation is done using 4th
            order polynomial, for each sub-sector respectively. Four coefficients
            for a single sub-interval are stored in this (#GFLIB_ATAN_TAYLOR_COEF_T)
            structure.
*//*-------------------------------------------------------------------------*/
typedef struct
{
    const tFrac32  s32a[4];  /*!< Array of five 32-bit elements for storing
                            coefficients of piece-wise polynomial. */
}GFLIB_ATAN_TAYLOR_COEF_T;

/*------------------------------------------------------------------------*//*!
@struct GFLIB_ATAN_TAYLOR_T "\GFLIB_Atan.h"

@brief      Structure containing eight sub-structures with polynomial
            coefficients to cover all sub-intervals.

@details    Output of \f$\arctan\left(s32In\right)\f$ for
            interval \f$\left[0,1\right)\f$ of input ratio is divided
            into eight sub-sectors. Polynomial approximation is done using 4th
            order polynomial, for each sub-sector respectively. Eight arrays,
            each including four polynomial coefficients for each sub-interval,
            are stored in this (#GFLIB_ATAN_TAYLOR_COEF_T) structure.

            \par
            By calling function alias #GFLIB_Atan, default values of
            coefficients are used. Polynomial coefficients can be modified by
            the user and in such a case full function call shall be used,i.e.
            #GFLIB_AtanANSIC
*//*-------------------------------------------------------------------------*/

typedef struct
{
    const GFLIB_ATAN_TAYLOR_COEF_T  GFLIB_ATAN_SECTOR[8]; /*!< Array of eight
                                elements for storing eight sub-arrays (each
                                sub-array contains four 32-bit coefficients)
                                for all sub-intervals.*/
} GFLIB_ATAN_TAYLOR_T;

/**
  * @}
  */
  

/******************************************************************************
* Exported Variables
******************************************************************************/

extern const GFLIB_ATAN_TAYLOR_T gflibAtanCoef;

/******************************************************************************
* Exported function prototypes
******************************************************************************/
/** @defgroup ATAN_class_exported_methods ATAN class exported methods
  * @{
  */


/*! 
* @ingroup GFLIB_GROUP 
*/ 
extern tFrac32 GFLIB_AtanANSIC(tFrac32 s32In, const GFLIB_ATAN_TAYLOR_T *const pParam);

/**
  * @}
  */
  
/**
  * @}
  */

/**
  * @}
  */

/******************************************************************************
* Inline functions
******************************************************************************/ 

#endif /*GFLIB_ATAN_H_*/
