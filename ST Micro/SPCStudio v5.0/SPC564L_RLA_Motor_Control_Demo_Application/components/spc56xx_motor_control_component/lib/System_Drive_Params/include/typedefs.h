/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    typedefs.h
*   @version BETA 0.9.1
*   @brief   General typedef
*          
*   @details General typedef.
*
*/

#ifndef _TYPEDEFS_H_
#define _TYPEDEFS_H_

/*typedef enum {FALSE,TRUE} bool;*/
#ifdef __MWERKS__    /* Metrowerk CodeWarrior */
#error "IM USING Metrowerk!!!!!!!!!!!!!!!!!!!!!!!!!"
    #include <stdint.h>
    #include <stdbool.h>

    /* Standard typedefs used by header files, based on ISO C standard */
    typedef volatile int8_t vint8_t;
    typedef volatile uint8_t vuint8_t;

    typedef volatile int16_t vint16_t;
    typedef volatile uint16_t vuint16_t;

    typedef volatile int32_t vint32_t;
    typedef volatile uint32_t vuint32_t;

#else
#ifdef __ghs__    /* GreenHills */
    #include <stdint.h>
    #include <stdbool.h>
    /* Standard typedefs used by header files, based on ISO C standard */
    typedef volatile int8_t vint8_t;
    typedef volatile uint8_t vuint8_t;

    typedef volatile int16_t vint16_t;
    typedef volatile uint16_t vuint16_t;

    typedef volatile int32_t vint32_t;
    typedef volatile uint32_t vuint32_t;

    /* MATH lib typedefs */
    typedef uint32_t  u32;
    typedef uint16_t u16;
    typedef uint8_t  u8;


#else
#ifdef __GNUC__    /* HighTec */

    #include <stdint.h>
    #include <stdbool.h>

    /* Standard typedefs used by header files, based on ISO C standard */
    typedef volatile int8_t vint8_t;
    typedef volatile uint8_t vuint8_t;

    typedef volatile int16_t vint16_t;
    typedef volatile uint16_t vuint16_t;

    typedef volatile int32_t vint32_t;
    typedef volatile uint32_t vuint32_t;

    /* MATH lib typedefs */
    typedef uint32_t  u32;
    typedef uint16_t u16;
    typedef uint8_t  u8;


#else
    #error "IM USING NOTHING!!!!!!!!!!!!!!!!!!!!!!!!!"
    /* This is needed for compilers that don't have a stdint.h file */

    typedef signed char int8_t;
    typedef unsigned char uint8_t;
    typedef volatile signed char vint8_t;
    typedef volatile unsigned char vuint8_t;

    typedef signed short int16_t;
    typedef unsigned short uint16_t;
    typedef volatile signed short vint16_t;
    typedef volatile unsigned short vuint16_t;

    typedef signed int int32_t;
    typedef unsigned int uint32_t;
    typedef volatile signed int vint32_t;
    typedef volatile unsigned int vuint32_t;

    /* MATH lib typedefs */
    typedef uint32_t  u32;
    typedef uint16_t u16;
    typedef uint8_t  u8;


#endif
#endif
#endif

    #define STATIC static

    typedef uint16_t    tU16;
    typedef uint8_t     tU8;
    typedef unsigned long long tU64;

    /* typedef volatile tU32  tVU32; */

#endif
