/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    Spc5xxx_MC_it.h
*   @version BETA 0.9.1
*   @brief   This file contains ISR function prototypes
*          
*   @details This file contains ISR function prototypes
*
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __SPC5XX_MC_IT_H
#define __SPC5XX_MC_IT_H

void SPD_eTIMER1_TC1IR_IRQHandler(void);
void SPD_eTIMER_InputCapture_HALL_S1_IRQHandler(void);
void SPD_eTIMER_InputCapture_HALL_S2_IRQHandler(void);
void SPD_eTIMER_InputCapture_HALL_S3_IRQHandler(void);
void CTU_FIF0_1_DMAComplete_IRQHandler(void);
void SysTasks_Handler(void);
void L99ASC03_WdgTrigger(void);
void ADC_STM_User_Handler(void);
void CTU_FIF0_2_DMAComplete_IRQHandler(void);

#if (defined RES_DMA_DEBUG)
void SPD_Resolver_DMA_IRQHandler(void);
#endif

#endif /* __SPC5XX_MC_IT_H*/
