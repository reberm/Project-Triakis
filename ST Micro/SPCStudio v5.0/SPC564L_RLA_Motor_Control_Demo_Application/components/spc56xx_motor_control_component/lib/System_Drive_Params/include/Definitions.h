/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    Control stage parameters.h
*   @version BETA 0.9.1
*   @brief   This file contains motor parameters needed by PMSM MC FW library v3.0
*          
*   @details This file contains motor parameters needed by PMSM MC FW library v3.0.
*
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __DEFINITIONS_H
#define __DEFINITIONS_H

/************************* DEBUG APPLICATION *************************/
/* Enable debug on DEBUG_PIN_x to measure tasks duration */
#define MCTK_DEBUG_TASK 

/* Enable debug variables for current RPM and angle (for aux and main sensors) */
#define MCTK_DEBUG_RPM_ANGLE

/* Enable debug array for ANGLE (for aux and main sensors) */
/*#define MCTK_DEBUG_ANGLE_SENSOR*/
/*#define MCTK_DEBUG_ANGLE_SENSOR_BUFFER_SIZE 4000*/

/* Enable debug array for RPM (for aux and main sensors) */
/* Use the following variable to switch to main or aux rpm values: */
/* mctk_debug_rpm_array_main_aux == 0 --> MAIN  */
/* mctk_debug_rpm_array_main_aux == 1 --> AUX   */
/*#define MCTK_DEBUG_RPM_ARRAY */
/*#define MCTK_DEBUG_RPM_SENSOR_BUFFER_SIZE 1000 */

/* Enable debug to diff main sensor vs aux sensor (DebugTestEncHallDiff) */
/*#define MCTK_DEBUG_ANGLE_DIFF*/

/* Enable debug in current sensig */
#define MCTK_DEBUG_CURRENT
#define MCTK_DEBUG_CURRENT_SIZE 2500

/* Resolver DMA debug */
#define RES_DMA_DEBUG FALSE
/********************************************************************/

#define DEGREES_120 0u
#define DEGREES_60 1u

#define ENABLE   1
#define DISABLE  0
#define TURN_ON  1
#define TURN_OFF 0
#define INRUSH_ACTIVE   1
#define INRUSH_INACTIVE 0

#define TURN_OFF_PWM      0
#define TURN_ON_R_BRAKE   1  
#define TURN_ON_LOW_SIDES 2

#define VFQFPN36  1
#define VFQFPN48  2
#define LQFP48    3
#define LQFP64    4
#define LQFP100   5
#define LQFP144   6
#define WLCSP64   7
#define LFBGA100  8
#define LFBGA144  9
#define BGA100    10
#define BGA64     11
#define TFBGA64   12

#define HIGHEST_FREQ 2
#define LOWEST_FREQ 1

#define HALL_TIM2 2
#define HALL_TIM3 3
#define HALL_TIM4 4
#define HALL_TIM5 5

#define ENC_TIM2 2
#define ENC_TIM3 3
#define ENC_TIM4 4
#define ENC_TIM5 5

/** 
  * @brief  eTimer module 0
  */
#define ETIMER_Module_0 ((uint8)(0))
/** 
  * @brief  eTimer module 1
  */
#define ETIMER_Module_1 ((uint8)(1))

#define ETIMER_CH0 ((uint8)(0))
#define ETIMER_CH1 ((uint8)(1))
#define ETIMER_CH2 ((uint8)(2))
#define ETIMER_CH3 ((uint8)(3))
#define ETIMER_CH4 ((uint8)(4))
#define ETIMER_CH5 ((uint8)(5))

/** 
  * @brief  FlexPWM channel polarity HIGH
  *
  */
#define FLEXPWM_Polarity_High  0x0000U 
/** 
  * @brief  FlexPWM channel polarity LOW
  *
  */
#define FLEXPWM_Polarity_Low   0x0400U
/** 
  * @brief  FlexPWM channel N polarity HIGH
  *
  */
#define FLEXPWM_NPolarity_High 0x0200U
/** 
  * @brief  FlexPWM channel N polarity LOW
  *
  */
#define FLEXPWM_NPolarity_Low  0x0000U 

/** 
  * @brief  FlexPWM idle state channel (HIGH)
  *
  */
#define FLEXPWM_IDLE_STATE_HIGH 1
/** 
  * @brief  FlexPWM idle state channel (LOW)
  *
  */
#define FLEXPWM_IDLE_STATE_LOW  0
/** 
  * @brief  FlexPWM idle state channel N (HIGH)
  *
  */
#define FLEXPWM_NIDLE_STATE_HIGH 1
/** 
  * @brief  FlexPWM idle state channel N (LOW)
  *
  */
#define FLEXPWM_NIDLE_STATE_LOW  0

/** 
  * @brief  FlexPWM FAULT polarity LOW
  *
  */
#define FAULT_POLARITY_LOW   (uint16_t ) 0x0100
/** 
  * @brief  FlexPWM FAULT polarity HIGH
  *
  */
#define FAULT_POLARITY_HIGH  (uint16_t ) 0x1100

/** 
  * @brief  FlexPWM FAULT pin 0
  *
  */
#define FAULT_PIN0 0
/** 
  * @brief  FlexPWM FAULT pin 1
  *
  */
#define FAULT_PIN1 1
/** 
  * @brief  FlexPWM FAULT pin 2
  *
  */
#define FAULT_PIN2 2
/** 
  * @brief  FlexPWM FAULT pin 3
  *
  */
#define FAULT_PIN3 3

/** 
* @brief           ADC Hardware unit 0
  *
  */
#define ADC_UNIT_0                          0
/** 
* @brief           ADC Hardware unit 1
  *
  */
#define ADC_UNIT_1                          1

/** 
* @brief           ADC Hardware channel 0
  *
  */
#define ADC_AN_0                            0
/** 
* @brief           ADC Hardware channel 1
  *
  */
#define ADC_AN_1                            1
/** 
* @brief           ADC Hardware channel 2
  *
  */
#define ADC_AN_2                            2
/** 
* @brief           ADC Hardware channel 3
  *
  */
#define ADC_AN_3                            3
/** 
* @brief           ADC Hardware channel 4
  *
  */
#define ADC_AN_4                            4
/** 
* @brief           ADC Hardware channel 5
  *
  */
#define ADC_AN_5                            5
/** 
* @brief           ADC Hardware channel 6
  *
  */
#define ADC_AN_6                            6
/** 
* @brief           ADC Hardware channel 7
  *
  */
#define ADC_AN_7                            7
/** 
* @brief           ADC Hardware channel 8
  *
  */
#define ADC_AN_8                            8
/** 
* @brief           ADC Hardware channel 9
  *
  */
#define ADC_AN_9                            9
/** 
* @brief           ADC Hardware channel 10
  *
  */
#define ADC_AN_10                            10
/** 
* @brief           ADC Hardware channel 11
  *
  */
#define ADC_AN_11                            11
/** 
* @brief           ADC Hardware channel 12
  *
  */
#define ADC_AN_12                            12
/** 
* @brief           ADC Hardware channel 13
  *
  */
#define ADC_AN_13                            13
/** 
* @brief           ADC Hardware channel 14
  *
  */
#define ADC_AN_14                            14
/** 
* @brief           ADC Hardware channel 15
  *
  */
#define ADC_AN_15                            15

#endif /*__DEFINITIONS_H*/
