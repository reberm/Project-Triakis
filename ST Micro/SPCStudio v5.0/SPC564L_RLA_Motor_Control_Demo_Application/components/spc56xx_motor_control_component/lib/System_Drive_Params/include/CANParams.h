/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    CANParams.h
*   @version BETA 0.9.1
*   @brief   This file contains CAN constant parameters definition
*          
*   @details This file contains CAN constant parameters definition.
*
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __CAN_PARAMS_H
#define __CAN_PARAMS_H
#include "Parameters conversion.h"

#include "CAN_PhysicalLayerCommunication_Class.h"
#include "UIIRQHandlerClass.h"

#if 0
const USART_InitTypeDef USARTInitHW_str = 
{
  USART_SPEED,                    /* USART_BaudRate */
  USART_WordLength_8b,            /* USART_WordLength */
  USART_StopBits_1,               /* USART_StopBits */
  USART_Parity_No,                /* USART_Parity */
  USART_Mode_Rx | USART_Mode_Tx,  /* USART_Mode */
  USART_HardwareFlowControl_None  /* USART_HardwareFlowControl */
};

#endif


CANParams_t CANParams_str =
{
  &CAND1,                  /* CAN1 */
#if defined _SPC56ELxx_
  SYSCLK_FREQ_120MHz,       /* CAN_CLK */
#else
  SYSCLK_FREQ_64MHz,       /* CAN_CLK */
#endif
  UI_IRQ_CAN,              /* IRQ Number */
  CAN_CONFIG               /* CAN_InitStructure: can_config_to250kbps,
                                                 can_config_to500kbps */
};

#endif /* __CAN_PARAMS_H */
