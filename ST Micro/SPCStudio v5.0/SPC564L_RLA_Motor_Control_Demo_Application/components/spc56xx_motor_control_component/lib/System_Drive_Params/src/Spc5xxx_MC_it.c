/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    Spc5xxx_MC_it.c
*   @version BETA 0.9.1
*   @brief   Main Interrupt Service Routines
*          
*   @details This file provides template for all exceptions handler and 
*            peripherals interrupt service routine, related to Motor Control.
*
*/

#include "SpeednPosFdbkClass.h"
#include "Control stage parameters.h"
#ifdef ADC_USER_ENABLE
#include "PWMnCurrFdbkClass.h"
#include "ICS_PWMnCurrFdbkClass.h"
#endif
#if defined _SPC56ELxx_
#include "xpc56el.h"
#else
#include "xpc560p.h"
#endif
#include "pal.h"
#include "pal_lld.h"
#include "MCIRQHandlerClass.h"
#include "Parameters conversion.h"
#include "Drive parameters.h"
#include "typedefs.h"
#include "Dma_LLD.h"
#include "Reg_eSys_FlexPwm.h"
#include "Reg_eSys_eTimer.h"
#include "Timebase.h"
#include "MCTasks.h"
#include "MC_Lib_Setup.h"
#include "Reg_eSys_Stm.h"
#include "Reg_eSys_CTU2.h"

#if (defined HALL_SENSORS) || (defined AUX_HALL_SENSORS) 
  #include "HALL_SpeednPosFdbkClass.h"
  #include "HALL_SpeednPosFdbkPrivate.h"
#endif

/*===============================================================================================
*                          LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
===============================================================================================*/

/*===============================================================================================
*                                       CONSTANTS
===============================================================================================*/
#ifdef ADC_USER_ENABLE
extern const ICS_ADC_User_ch ICS_ADC_Curr[];
#endif
/*===============================================================================================
*                                       LOCAL VARIABLES
===============================================================================================*/

/*===============================================================================================
*                                       GLOBAL CONSTANTS
===============================================================================================*/
#if defined _SPC56ELxx_
CONST(uint32, GPT_CONST) STM_BASE_ADDR[] = {
#ifdef STM_0_BASEADDR
    STM_0_BASEADDR
#endif
#ifdef STM_1_BASEADDR
   ,STM_1_BASEADDR
#endif
#ifdef STM_2_BASEADDR
   ,STM_2_BASEADDR
#endif
};
#endif
/*===============================================================================================
*                                       GLOBAL VARIABLES
===============================================================================================*/
#ifdef ADC_MULTIPLE_CONV_ENABLED
extern uint32 RegularCTUFifoBuffer[];
extern uint16_t ADC_User_multiple_buffer[ADC_USER_CONV_MAX];
#endif
/*===============================================================================================
*                                   LOCAL FUNCTION PROTOTYPES
===============================================================================================*/

/*===============================================================================================
*                                             FUNCTIONS
===============================================================================================*/

/******************************************************************************/
/*                 SPC5xxx Peripherals Interrupt Handlers                     */
/******************************************************************************/

#if (defined ENCODER) || (defined AUX_ENCODER) 
/**
* @brief      This function handles eTimerx global interrupt request for M1 Speed Sensor.
*
*/
void SPD_eTIMER1_TC1IR_IRQHandler(void)
{
   /* check Input Capture 1 flag for eTimer1 channel 1 (encoder index signal) */
   if((REG_READ16(ETIMER_STS(ENC_TIMER_SELECTION, ENC_INDEX_CH_INPUT)) & STS_ICF1_MASK) == STS_ICF1_MASK)
   {
      /* Clear Input Capture Flag 1 */
      REG_WRITE16(ETIMER_STS(ENC_TIMER_SELECTION, ENC_INDEX_CH_INPUT), STS_ICF1_MASK);
      Exec_IRQ_Handler(MC_IRQ_SPEEDNPOSFDBK_1,0);
   }
}
#else
void SPD_eTIMER1_TC1IR_IRQHandler(void)
{
}
#endif

#if (defined HALL_SENSORS) || (defined AUX_HALL_SENSORS) 
/**
  * @brief  This function handles eTimerx for HALL Sensor input S1.
  * @param  None
  * @retval None
  */
void SPD_eTIMER_InputCapture_HALL_S1_IRQHandler(void)
{
   /* check Input Capture 1 flag for eTimer channel (hall sensor signal 1 (ICF1)) */
   if((REG_READ16(ETIMER_STS(HALL_TIMER_SELECTION, HALL_S1_CH)) & STS_ICF1_MASK) == STS_ICF1_MASK)
   {
      /* Clear Input Capture Flag 1 */
      REG_WRITE16(ETIMER_STS(HALL_TIMER_SELECTION, HALL_S1_CH), STS_ICF1_MASK);
      Exec_IRQ_Handler(MC_IRQ_SPEEDNPOSFDBK_HS,HS1_IRQ_RIS);
   }
   /* check Input Capture 1 flag for eTimer channel (hall sensor signal 1 (ICF2) */
   if((REG_READ16(ETIMER_STS(HALL_TIMER_SELECTION, HALL_S1_CH)) & STS_ICF2_MASK) == STS_ICF2_MASK)
   {
      /* Clear Input Capture Flag 2 */
      REG_WRITE16(ETIMER_STS(HALL_TIMER_SELECTION, HALL_S1_CH), STS_ICF2_MASK);
      Exec_IRQ_Handler(MC_IRQ_SPEEDNPOSFDBK_HS,HS1_IRQ_FAL);
   }  
}

/**
  * @brief  This function handles eTimerx for HALL Sensor input S2.
  * @param  None
  * @retval None
  */
void SPD_eTIMER_InputCapture_HALL_S2_IRQHandler(void)
{
   /* check Input Capture 1 flag for eTimer channel (hall sensor signal 2 (ICF1)) */
   if((REG_READ16(ETIMER_STS(HALL_TIMER_SELECTION, HALL_S2_CH)) & STS_ICF1_MASK) == STS_ICF1_MASK)
   {
      /* Clear Input Capture Flag 1 */
      REG_WRITE16(ETIMER_STS(HALL_TIMER_SELECTION, HALL_S2_CH), STS_ICF1_MASK);
      Exec_IRQ_Handler(MC_IRQ_SPEEDNPOSFDBK_HS,HS2_IRQ_RIS);
   }
   /* check Input Capture 1 flag for eTimer channel (hall sensor signal 2 (ICF2) */
   if((REG_READ16(ETIMER_STS(HALL_TIMER_SELECTION, HALL_S2_CH)) & STS_ICF2_MASK) == STS_ICF2_MASK)
   {
      /* Clear Input Capture Flag 2 */
      REG_WRITE16(ETIMER_STS(HALL_TIMER_SELECTION, HALL_S2_CH), STS_ICF2_MASK);
      Exec_IRQ_Handler(MC_IRQ_SPEEDNPOSFDBK_HS,HS2_IRQ_FAL);
   }   
}

/**
  * @brief  This function handles eTimerx for HALL Sensor input S3.
  * @param  None
  * @retval None
  */
void SPD_eTIMER_InputCapture_HALL_S3_IRQHandler(void)
{
   /* check Input Capture 1 flag for eTimer channel (hall sensor signal 2 (ICF1)) */
   if((REG_READ16(ETIMER_STS(HALL_TIMER_SELECTION, HALL_S3_CH)) & STS_ICF1_MASK) == STS_ICF1_MASK)
   {
      /* Clear Input Capture Flag 1 */
      REG_WRITE16(ETIMER_STS(HALL_TIMER_SELECTION, HALL_S3_CH), STS_ICF1_MASK);
      Exec_IRQ_Handler(MC_IRQ_SPEEDNPOSFDBK_HS,HS3_IRQ_RIS);
   }
   /* check Input Capture 1 flag for eTimer channel (hall sensor signal 2 (ICF2) */
   if((REG_READ16(ETIMER_STS(HALL_TIMER_SELECTION, HALL_S3_CH)) & STS_ICF2_MASK) == STS_ICF2_MASK)
   {
      /* Clear Input Capture Flag 2 */
      REG_WRITE16(ETIMER_STS(HALL_TIMER_SELECTION, HALL_S3_CH), STS_ICF2_MASK);
      Exec_IRQ_Handler(MC_IRQ_SPEEDNPOSFDBK_HS,HS3_IRQ_FAL);
   }  
}
#else
void SPD_eTIMER_InputCapture_HALL_S1_IRQHandler(void)
{
}
void SPD_eTIMER_InputCapture_HALL_S2_IRQHandler(void)
{
}
void SPD_eTIMER_InputCapture_HALL_S3_IRQHandler(void)
{
}
#endif

/**
* @brief    This function handles CTU FIFO 1 DMA complete (interrupt handler).
*
*/
void CTU_FIF0_1_DMAComplete_IRQHandler(void)
{
  /* clear DMA flag */
  CLEAR_DMA_INTC(DMA_CHANNEL_FOR_CURRENT_SENSING);
#if defined(MCTK_DEBUG_TASK)
  pal_lld_togglepad(PORT_A, DEBUG_PIN_0);
#endif
  TSK_HighFrequencyTask(); 
}

/**
  * @brief  This function handles SysTick Handler.
  * @param  None
  * @retval None
  */
void SysTasks_Handler(void)
{
  uint32 counter;
  
  REG_WRITE32( STM_CIR(0) , 0x00000001 );
  
#if defined(MCTK_DEBUG_TASK)
  pal_lld_togglepad(PORT_A, DEBUG_PIN_1);
#endif

#if defined _SPC56ELxx_
  REG_WRITE32( STM_CCR(0) , 0x00000000 );
  counter = REG_READ32(STM_CNT(0));
#ifdef CPU_CLK_64_MHZ   
  REG_WRITE32( STM_CMP(0), counter + ((SYSCLK_FREQ_64MHz) / SYS_TICK_FREQUENCY) );
#elif defined CPU_CLK_120_MHZ 
  REG_WRITE32( STM_CMP(0) , counter + ((SYSCLK_FREQ_120MHz) / SYS_TICK_FREQUENCY) ); 
#endif 
  REG_WRITE32( STM_CCR(0) , 0x00000001 );
#else  
  REG_WRITE32( STM_CCR0 , 0x00000000 );
  counter = REG_READ32(STM_CNT);
  REG_WRITE32( STM_CMP0 , counter + ((SYSCLK_FREQ_64MHz) / SYS_TICK_FREQUENCY) );
  REG_WRITE32( STM_CCR0 , 0x00000001 );  
#endif  

  TB_Scheduler();
}

#if (defined RES_DMA_DEBUG)
/**
  * @brief  This function handles Resolver DMA Handler.
  * @param  None
  * @retval None
  */
void SPD_Resolver_DMA_IRQHandler(void)
{
    /* clear DMA flag */
    CLEAR_DMA_INTC(DMA_CHANNEL_FOR_RESOLVER);
}
#endif


/**
* @brief    This function handles CTU FIFO 2 DMA complete (interrupt handler).
*
*/
#ifdef ADC_MULTIPLE_CONV_ENABLED
void CTU_FIF0_2_DMAComplete_IRQHandler(void)
{
  static uint8_t ADC_User_index_buffer = 0;  
  
  /* clear DMA flag */
  CLEAR_DMA_INTC(DMA_CHANNEL_FOR_ADC_CONVERSIONS);
  
  if (((REG_READ16(CTUV2_CTUCR))&CTUV2_CTUCR_GRE)==CTUV2_CTUCR_GRE)
  {
     /* All double-buffered registers cannot be written
      * to while the GRE bit remains set to 1 */

     /* return error  */
     ADC_User_multiple_buffer[ADC_User_index_buffer] = 0xFFFFU;
  }
  else
  {
    /* Update the ADC buffer */
    ADC_User_multiple_buffer[ADC_User_index_buffer] =  (uint16_t)(((RegularCTUFifoBuffer[0]) & (0x0000FFFFUL)));
    ADC_User_index_buffer = ADC_User_index_buffer + 1U;
    if(ADC_User_index_buffer >= ADC_USER_CONV_MAX)
    {
        ADC_User_index_buffer = 0U;
    }

    /* Update ADC command list */
    REG_WRITE16(CTUV2_CLRx(9), CTUV2_CLR_CIR_DISABLE | CTUV2_CLR_LC_NOT_LAST | CTUV2_CLR_CMS_SINGLE |
        CTUV2_CLR_FIFO(2) | ICS_ADC_Curr[ADC_User_index_buffer].Adc_Unit |
        ICS_ADC_Curr[ADC_User_index_buffer].hIChannel);

    /* Update the double buffered registers */
    REG_WRITE16(CTUV2_CTUCR, CTUV2_CTUCR_GRE);

  }
}
#else
void CTU_FIF0_2_DMAComplete_IRQHandler(void)
{
}
#endif



