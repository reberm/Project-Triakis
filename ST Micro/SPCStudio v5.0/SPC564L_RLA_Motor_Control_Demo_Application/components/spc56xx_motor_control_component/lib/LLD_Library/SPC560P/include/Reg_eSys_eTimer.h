/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    Reg_eSys_eTimer.h
*   @version BETA 0.9.1
*   @brief   This file contains the eTIMER module register description
*          
*   @details eTIMER module register description.
*
*/

#ifndef REG_ESYS_ETIMER_H
#define REG_ESYS_ETIMER_H

#ifdef __cplusplus
extern "C"{
#endif

/*
* @page misra_violations MISRA-C:2004 violations
*
* @section Reg_eSys_ETIMER_H_REF_1
* Violates MISRA 2004 Required Rule 1.4, The compiler/linker shall be checked to ensure 31
* character significance and case sensitivity are supported for external identifiers.
* This is not a violation since all the compilers used interpret the identifiers correctly.
*
* @section Reg_eSys_ETIMER_H_REF_2
* Violates MISRA 2004 Advisory Rule 19.7, Function-like macro defined
* This violation is due to function like macros defined for register operations.
* Function like macros are used to reduce code complexity.
*
* @section Reg_eSys_ETIMER_H_REF_3
* Violates MISRA 2004 Required Rule 19.15, Precautions shall be taken in order to prevent the
* contents of a header file being included twice
* This is not a violation since all header files are protected against multiple inclusions
*
*/

/*==================================================================================================
*                                        INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/
/*
* @violates @ref Reg_eSys_ETIMER_H_REF_3 precautions to prevent the contents of a header file being
* included twice.
*/
#include "modules.h"
/*
* @violates @ref Reg_eSys_ETIMER_H_REF_3 precautions to prevent the contents of a header file being
* included twice.
*/
#include "Reg_eSys.h"
/*
* @violates @ref Reg_eSys_ETIMER_H_REF_3 precautions to prevent the contents of a header file being
* included twice.
*/
#include "Reg_Macros.h"

/*==================================================================================================
*                              SOURCE FILE VERSION INFORMATION
==================================================================================================*/
#define REG_ESYS_ETIMER_H_VENDOR_ID                       27
#define REG_ESYS_ETIMER_H_AR_RELEASE_MAJOR_VERSION        4
#define REG_ESYS_ETIMER_H_AR_RELEASE_MINOR_VERSION        0
#define REG_ESYS_ETIMER_H_AR_RELEASE_REVISION_VERSION     3
#define REG_ESYS_ETIMER_H_SW_MAJOR_VERSION                1
#define REG_ESYS_ETIMER_H_SW_MINOR_VERSION                0
#define REG_ESYS_ETIMER_H_SW_PATCH_VERSION                0

/*==================================================================================================
*                                     FILE VERSION CHECKS
==================================================================================================*/

/*==================================================================================================
*                                          CONSTANTS
==================================================================================================*/

/*==================================================================================================
*                                GLOBAL DEFINES & DECLARATIONS
==================================================================================================*/
#if (USE_GPT_MODULE==STD_ON)
#define ETIMER_CONST GPT_CONST

#define GPT_START_SEC_CONST_32
/*
* @violates @ref Reg_eSys_ETIMER_H_REF_3 precautions to prevent the contents of a header file being
* included twice.
*/
#include "MemMap.h"

#endif

#if ((USE_GPT_MODULE==STD_OFF) && (USE_PWM_MODULE==STD_ON))
#define ETIMER_CONST PWM_CONST

#define PWM_START_SEC_CONST_32
/*
* @violates @ref Reg_eSys_ETIMER_H_REF_3 precautions to prevent the contents of a header file being
* included twice.
*/
#include "MemMap.h"

#endif

#if ((USE_GPT_MODULE==STD_OFF) && (USE_PWM_MODULE==STD_OFF) && (USE_ICU_MODULE==STD_ON))
#define ETIMER_CONST ICU_CONST

#define ICU_START_SEC_CONST_32
/*
* @violates @ref Reg_eSys_ETIMER_H_REF_3 precautions to prevent the contents of a header file being
* included twice.
*/
#include "MemMap.h"

#endif

/**
* @brief ETIMER base address array declaration
*/
extern CONST(uint32, ETIMER_CONST) ETIMER_BASE_ADDR[];

#if (USE_GPT_MODULE==STD_ON)

#define GPT_STOP_SEC_CONST_32
/*
* @violates @ref Reg_eSys_ETIMER_H_REF_3 precautions to prevent the contents of a header file being
* included twice.
*/
#include "MemMap.h"

#endif

#if ((USE_GPT_MODULE==STD_OFF) && (USE_PWM_MODULE==STD_ON))

#define PWM_STOP_SEC_CONST_32
/*
* @violates @ref Reg_eSys_ETIMER_H_REF_3 precautions to prevent the contents of a header file being
* included twice.
*/
#include "MemMap.h"

#endif

#if ((USE_GPT_MODULE==STD_OFF) && (USE_PWM_MODULE==STD_OFF) && (USE_ICU_MODULE==STD_ON))

#define ICU_STOP_SEC_CONST_32
/*
* @violates @ref Reg_eSys_ETIMER_H_REF_3 precautions to prevent the contents of a header file being
* included twice.
*/
#include "MemMap.h"

#endif

/*==================================================================================================
*                                      DEFINES AND MACROS
==================================================================================================*/
/**
* @brief eTimer Module Registers - Channel specific registers.
*/
/*
* @violates @ref Reg_eSys_ETIMER_H_REF_2 Function-like macro
* defined.
*/
#define ETIMER_COMP1(mIdx, chIdx)      (ETIMER_BASE_ADDR[mIdx] + (uint32)((uint32)(chIdx)<<(5UL)))
/*
* @violates @ref Reg_eSys_ETIMER_H_REF_2 Function-like macro
* defined.
*/
#define ETIMER_COMP2(mIdx, chIdx)      (ETIMER_BASE_ADDR[mIdx] +  \
                                                          (uint32)((uint32)(chIdx)<<(5UL)) + 0x02UL)
/*
* @violates @ref Reg_eSys_ETIMER_H_REF_2 Function-like macro
* defined.
*/
#define ETIMER_CAPT1(mIdx, chIdx)      (ETIMER_BASE_ADDR[mIdx] +  \
                                                          (uint32)((uint32)(chIdx)<<(5UL)) + 0x04UL)
/*
* @violates @ref Reg_eSys_ETIMER_H_REF_2 Function-like macro
* defined.
*/
#define ETIMER_CAPT2(mIdx, chIdx)      (ETIMER_BASE_ADDR[mIdx] +  \
                                                          (uint32)((uint32)(chIdx)<<(5UL)) + 0x06UL)
/*
* @violates @ref Reg_eSys_ETIMER_H_REF_2 Function-like macro
* defined.
*/
#define ETIMER_LOAD(mIdx, chIdx)       (ETIMER_BASE_ADDR[mIdx] +  \
                                                          (uint32)((uint32)(chIdx)<<(5UL)) + 0x08UL)
/*
* @violates @ref Reg_eSys_ETIMER_H_REF_2 Function-like macro
* defined.
*/
#define ETIMER_HOLD(mIdx, chIdx)       (ETIMER_BASE_ADDR[mIdx] +  \
                                                          (uint32)((uint32)(chIdx)<<(5UL)) + 0x0AUL)
/*
* @violates @ref Reg_eSys_ETIMER_H_REF_2 Function-like macro
* defined.
*/
#define ETIMER_CNTR(mIdx, chIdx)       (ETIMER_BASE_ADDR[mIdx] +  \
                                                          (uint32)((uint32)(chIdx)<<(5UL)) + 0x0CUL)
/*
* @violates @ref Reg_eSys_ETIMER_H_REF_2 Function-like macro
* defined.
*/
#define ETIMER_CTRL1(mIdx, chIdx)      (ETIMER_BASE_ADDR[mIdx] +  \
                                                          (uint32)((uint32)(chIdx)<<(5UL)) + 0x0EUL)
/*
* @violates @ref Reg_eSys_ETIMER_H_REF_2 Function-like macro
* defined.
*/
#define ETIMER_CTRL2(mIdx, chIdx)      (ETIMER_BASE_ADDR[mIdx] +  \
                                                          (uint32)((uint32)(chIdx)<<(5UL)) + 0x10UL)
/*
* @violates @ref Reg_eSys_ETIMER_H_REF_2 Function-like macro
* defined.
*/
#define ETIMER_CTRL3(mIdx, chIdx)      (ETIMER_BASE_ADDR[mIdx] +  \
                                                          (uint32)((uint32)(chIdx)<<(5UL)) + 0x12UL)
/*
* @violates @ref Reg_eSys_ETIMER_H_REF_2 Function-like macro
* defined.
*/
#define ETIMER_STS(mIdx, chIdx)        (ETIMER_BASE_ADDR[mIdx] +  \
                                                          (uint32)((uint32)(chIdx)<<(5UL)) + 0x14UL)
/*
* @violates @ref Reg_eSys_ETIMER_H_REF_2 Function-like macro
* defined.
*/
#define ETIMER_INTDMA(mIdx, chIdx)     (ETIMER_BASE_ADDR[mIdx] +  \
                                                          (uint32)((uint32)(chIdx)<<(5UL)) + 0x16UL)
/*
* @violates @ref Reg_eSys_ETIMER_H_REF_2 Function-like macro
* defined.
*/
#define ETIMER_CMPLD1(mIdx, chIdx)     (ETIMER_BASE_ADDR[mIdx] +  \
                                                          (uint32)((uint32)(chIdx)<<(5UL)) + 0x18UL)
/*
* @violates @ref Reg_eSys_ETIMER_H_REF_2 Function-like macro
* defined.
*/
#define ETIMER_CMPLD2(mIdx, chIdx)     (ETIMER_BASE_ADDR[mIdx] +  \
                                                          (uint32)((uint32)(chIdx)<<(5UL)) + 0x1AUL)
/*
* @violates @ref Reg_eSys_ETIMER_H_REF_2 Function-like macro
* defined.
*/
#define ETIMER_CCCTRL(mIdx, chIdx)     (ETIMER_BASE_ADDR[mIdx] +  \
                                                          (uint32)((uint32)(chIdx)<<(5UL)) + 0x1CUL)
/*
* @violates @ref Reg_eSys_ETIMER_H_REF_2 Function-like macro
* defined.
*/
#define ETIMER_FILT(mIdx, chIdx)       (ETIMER_BASE_ADDR[mIdx] +  \
                                                          (uint32)((uint32)(chIdx)<<(5UL)) + 0x1EUL)

/*
* @brief eTimer Module Registers - Watchdog registers.
*/

/*
* @violates @ref Reg_eSys_ETIMER_H_REF_2 Function-like macro
* defined.
*/
#define ETIMER_WDTOL(mIdx)             (ETIMER_BASE_ADDR[mIdx] + (uint32)0x0100UL)
/*
* @violates @ref Reg_eSys_ETIMER_H_REF_2 Function-like macro
* defined.
*/
#define ETIMER_WDTOH(mIdx)             (ETIMER_BASE_ADDR[mIdx] + (uint32)0x0102UL)

/**
* @brief eTimer Module Registers - Configuration registers.
*/

/*
* @violates @ref Reg_eSys_ETIMER_H_REF_2 Function-like macro
* defined.
*/
#define ETIMER_ENBL(mIdx)              (ETIMER_BASE_ADDR[mIdx] + (uint32)0x010CUL)
/*
* @violates @ref Reg_eSys_ETIMER_H_REF_2 Function-like macro
* defined.
*/
#define ETIMER_DREQ0(mIdx)             (ETIMER_BASE_ADDR[mIdx] + (uint32)0x0110UL)
/*
* @violates @ref Reg_eSys_ETIMER_H_REF_2 Function-like macro
* defined.
*/
#define ETIMER_DREQ1(mIdx)             (ETIMER_BASE_ADDR[mIdx] + (uint32)0x0112UL)

/*
* @violates @ref Reg_eSys_ETIMER_H_REF_2 Function-like macro
* defined.
*/
#define ETIMER_DREQ(mIdx, mDrqIdx)     (ETIMER_BASE_ADDR[mIdx] + \
                                                (uint32)0x0110UL + (uint32)(mDrqIdx<<1UL))

/*
* @violates @ref Reg_eSys_ETIMER_H_REF_2 Function-like macro
* defined.
*/
#define DMA_CHANNEL_CAPT1_REQUEST(chIdx)    (uint16)(0x8000UL + (chIdx<<2UL))


/** MASK and SHIFT values for each register field */

/**
* @brief CTRL1 - Control Register 1 - bitfield mask and shift defines.
*/
#define CTRL1_CNTMODE_MASK         (uint16)(BIT15|BIT14|BIT13)
#define CTRL1_CNTMODE_SHIFT        (uint16)(13U)

#define CTRL1_PRISRC_MASK          (uint16)(BIT12|BIT11|BIT10|BIT9|BIT8)
#define CTRL1_PRISRC_SHIFT         (uint16)(8U)

#define CTRL1_ONCE_MASK            (uint16)(BIT7)
#define CTRL1_ONCE_SHIFT           (uint16)(7U)

#define CTRL1_LENGTH_MASK          (uint16)(BIT6)
#define CTRL1_LENGTH_SHIFT         (uint16)(6U)

#define CTRL1_DIR_MASK             (uint16)(BIT5)
#define CTRL1_DIR_SHIFT            (uint16)(5U)

#define CTRL1_SECSRC_MASK          (uint16)(BIT4|BIT3|BIT2|BIT1|BIT0)
#define CTRL1_SECSRC_SHIFT         (uint16)(0U)

/**
* @brief CTRL2 - Control Register 2 - bitfield mask and shift defines.
*/
#define CTRL2_OEN_MASK             (uint16)(BIT15)
#define CTRL2_OEN_SHIFT            (uint16)(15U)

#define CTRL2_RDNT_MASK            (uint16)(BIT14)
#define CTRL2_RDNT_SHIFT           (uint16)(14U)

#define CTRL2_INPUT_MASK           (uint16)(BIT13)
#define CTRL2_INPUT_SHIFT          (uint16)(13U)

#define CTRL2_VAL_MASK             (uint16)(BIT12)
#define CTRL2_VAL_SHIFT            (uint16)(12U)

#define CTRL2_FORCE_MASK           (uint16)(BIT11)
#define CTRL2_FORCE_SHIFT          (uint16)(11U)

#define CTRL2_COFRC_MASK           (uint16)(BIT10)
#define CTRL2_COFRC_SHIFT          (uint16)(10U)

#define CTRL2_COINIT_MASK          (uint16)(BIT9|BIT8)
#define CTRL2_COINIT_SHIFT         (uint16)(8U)

#define CTRL2_SIPS_MASK            (uint16)(BIT7)
#define CTRL2_SIPS_SHIFT           (uint16)(7U)

#define CTRL2_PIPS_MASK            (uint16)(BIT6)
#define CTRL2_PIPS_SHIFT           (uint16)(6U)

#define CTRL2_OPS_MASK             (uint16)(BIT5)
#define CTRL2_OPS_SHIFT            (uint16)(5U)

#define CTRL2_MSTR_MASK            (uint16)(BIT4)
#define CTRL2_MSTR_SHIFT           (uint16)(4U)

#define CTRL2_OUTMODE_MASK         (uint16)(BIT3|BIT2|BIT1|BIT0)
#define CTRL2_OUTMODE_SHIFT        (uint16)(0U)

/**
* @brief CTRL3 - Control Register 3 - bitfield mask and shift defines.
*/
#define CTRL3_STPEN_MASK           (uint16)(BIT15)
#define CTRL3_STPEN_SHIFT          (uint16)(15U)

#define CTRL3_ROC_MASK             (uint16)(BIT14|BIT13)
#define CTRL3_ROC_SHIFT            (uint16)(13U)

#define CTRL3_C2FCNT_MASK          (uint16)(BIT7|BIT6|BIT5)
#define CTRL3_C2FCNT_SHIFT         (uint16)(5U)

#define CTRL3_C1FCNT_MASK          (uint16)(BIT4|BIT3|BIT2)
#define CTRL3_C1FCNT_SHIFT         (uint16)(2U)

#define CTRL3_DBGEN_MASK           (uint16)(BIT1|BIT0)
#define CTRL3_DBGEN_SHIFT          (uint16)(0U)

/**
* @brief STS - Status register - bitfield mask and shift defines.
*/
#define STS_WDF_MASK               (uint16)(BIT9)
#define STS_WDF_SHIFT              (uint16)(9U)

#define STS_RCF_MASK               (uint16)(BIT8)
#define STS_RCF_SHIFT              (uint16)(8U)

#define STS_ICF2_MASK              (uint16)(BIT7)
#define STS_ICF2_SHIFT             (uint16)(7U)

#define STS_ICF1_MASK              (uint16)(BIT6)
#define STS_ICF1_SHIFT             (uint16)(6U)

#define STS_IEHF_MASK              (uint16)(BIT5)
#define STS_IEHF_SHIFT             (uint16)(5U)

#define STS_IELF_MASK              (uint16)(BIT4)
#define STS_IELF_SHIFT             (uint16)(4U)

#define STS_TOF_MASK               (uint16)(BIT3)
#define STS_TOF_SHIFT              (uint16)(3U)

#define STS_TCF2_MASK              (uint16)(BIT2)
#define STS_TCF2_SHIFT             (uint16)(2U)

#define STS_TCF1_MASK              (uint16)(BIT1)
#define STS_TCF1_SHIFT             (uint16)(1U)

#define STS_TCF_MASK               (uint16)(BIT0)
#define STS_TCF_SHIFT              (uint16)(0U)

/**
* @brief INTDMA  - Interrupt and DMA Enable Register - bitfield mask and shift defines.
*/
#define INTDMA_ICF2DE_MASK         (uint16)(BIT15)
#define INTDMA_ICF2DE_SHIFT        (uint16)(15U)

#define INTDMA_ICF1DE_MASK         (uint16)(BIT14)
#define INTDMA_ICF1DE_SHIFT        (uint16)(14U)

#define INTDMA_CMPLD2DE_MASK       (uint16)(BIT13)
#define INTDMA_CMPLD2DE_SHIFT      (uint16)(13U)

#define INTDMA_CMPLD1DE_MASK       (uint16)(BIT12)
#define INTDMA_CMPLD1DE_SHIFT      (uint16)(12U)

#define INTDMA_WDFIE_MASK          (uint16)(BIT9)
#define INTDMA_WDFIE_SHIFT         (uint16)(9U)

#define INTDMA_RCFIE_MASK          (uint16)(BIT8)
#define INTDMA_RCFIE_SHIFT         (uint16)(8U)

#define INTDMA_ICF2IE_MASK         (uint16)(BIT7)
#define INTDMA_ICF2IE_SHIFT        (uint16)(7U)

#define INTDMA_ICF1IE_MASK         (uint16)(BIT6)
#define INTDMA_ICF1IE_SHIFT        (uint16)(6U)

#define INTDMA_IEHFIE_MASK         (uint16)(BIT5)
#define INTDMA_IEHFIE_SHIFT        (uint16)(5U)

#define INTDMA_IELFIE_MASK         (uint16)(BIT4)
#define INTDMA_IELFIE_SHIFT        (uint16)(4U)

#define INTDMA_TOFIE_MASK          (uint16)(BIT3)
#define INTDMA_TOFIE_SHIFT         (uint16)(3U)

#define INTDMA_TCF2IE_MASK         (uint16)(BIT2)
#define INTDMA_TCF2IE_SHIFT        (uint16)(2U)

#define INTDMA_TCF1IE_MASK         (uint16)(BIT1)
#define INTDMA_TCF1IE_SHIFT        (uint16)(1U)

#define INTDMA_TCFIE_MASK          (uint16)(BIT0)
#define INTDMA_TCFIE_SHIFT         (uint16)(0U)

/**
* @brief CCCTRL - Compare and Capture Control Register - bitfield mask and shift defines.
*/
#define CCCTRL_CLC2_MASK           (uint16)(BIT15|BIT14|BIT13)
#define CCCTRL_CLC2_SHIFT          (uint16)(13U)

#define CCCTRL_CLC1_MASK           (uint16)(BIT12|BIT11|BIT10)
#define CCCTRL_CLC1_SHIFT          (uint16)(10U)

#define CCCTRL_CMPMODE_MASK        (uint16)(BIT9|BIT8)
#define CCCTRL_CMPMODE_SHIFT       (uint16)(8U)

#define CCCTRL_CPT2MODE_MASK       (uint16)(BIT7|BIT6)
#define CCCTRL_CPT2MODE_SHIFT      (uint16)(6U)

#define CCCTRL_CPT1MODE_MASK       (uint16)(BIT5|BIT4)
#define CCCTRL_CPT1MODE_SHIFT      (uint16)(4U)

#define CCCTRL_CPWM_MASK           (uint16)(BIT3|BIT2)
#define CCCTRL_CPWM_SHIFT          (uint16)(2U)

#define CCCTRL_ONESHOT_MASK        (uint16)(BIT1)
#define CCCTRL_ONESHOT_SHIFT       (uint16)(1U)

#define CCCTRL_ARM_MASK            (uint16)(BIT0)
#define CCCTRL_ARM_SHIFT           (uint16)(0U)

/**
* @brief ENBL - Channel Enable Register - bitfield mask and shift defines.
*/
#define ENBL_ENBL_MASK             (uint16)(BIT5|BIT4|BIT3|BIT2|BIT1|BIT0)
#define ENBL_ENBL_SHIFT            (uint16)(0U)

/**
* @brief DREQ0/1 - Dma Request Select Registers - bitfield mask and shift defines.
*/
#define DREQ0_DREQ0_MASK           (uint16)(BIT4|BIT3|BIT2|BIT1|BIT0)
#define DREQ0_DREQ0_SHIFT          (uint16)(0U)

#define DREQ1_DREQ1_MASK           (uint16)(BIT4|BIT3|BIT2|BIT1|BIT0)
#define DREQ1_DREQ1_SHIFT          (uint16)(0U)


/** Bitfield defines. */
/**
* @brief CTRL1 - bitfield defines.
*/
#define CTRL1_CNTMODE_NOP          (uint16)(0U)
#define CTRL1_CNTMODE_CREPS        (uint16)(1U)
#define CTRL1_CNTMODE_CRFEPS       (uint16)(2U)
#define CTRL1_CNTMODE_CREPS_SIH    (uint16)(3U)
#define CTRL1_CNTMODE_QCOUNT       (uint16)(4U)
#define CTRL1_CNTMODE_CREPS_SDIR   (uint16)(5U)
#define CTRL1_CNTMODE_ESECOND_TRIG (uint16)(6U)
#define CTRL1_CNTMODE_CASCADE      (uint16)(7U)

#define CTRL1_PRISRC_CNT0_IN       (uint16)(0U)
#define CTRL1_PRISRC_CNT1_IN       (uint16)(1U)
#define CTRL1_PRISRC_CNT2_IN       (uint16)(2U)
#define CTRL1_PRISRC_CNT3_IN       (uint16)(3U)
#define CTRL1_PRISRC_CNT4_IN       (uint16)(4U)
#define CTRL1_PRISRC_CNT5_IN       (uint16)(5U)
#define CTRL1_PRISRC_CNT6_IN       (uint16)(6U)
#define CTRL1_PRISRC_CNT7_IN       (uint16)(7U)
#define CTRL1_PRISRC_AUX0_IN       (uint16)(8U)
#define CTRL1_PRISRC_AUX1_IN       (uint16)(9U)
#define CTRL1_PRISRC_AUX2_IN       (uint16)(10U)
#define CTRL1_PRISRC_AUX3_IN       (uint16)(11U)
#define CTRL1_PRISRC_AUX4_IN       (uint16)(12U)
#define CTRL1_PRISRC_AUX5_IN       (uint16)(13U)
#define CTRL1_PRISRC_AUX6_IN       (uint16)(14U)
#define CTRL1_PRISRC_AUX7_IN       (uint16)(15U)
#define CTRL1_PRISRC_CNT0_OUT      (uint16)(16U)
#define CTRL1_PRISRC_CNT1_OUT      (uint16)(17U)
#define CTRL1_PRISRC_CNT2_OUT      (uint16)(18U)
#define CTRL1_PRISRC_CNT3_OUT      (uint16)(19U)
#define CTRL1_PRISRC_CNT4_OUT      (uint16)(20U)
#define CTRL1_PRISRC_CNT5_OUT      (uint16)(21U)
#define CTRL1_PRISRC_CNT6_OUT      (uint16)(22U)
#define CTRL1_PRISRC_CNT7_OUT      (uint16)(23U)
#define CTRL1_PRISRC_IP_DIV1       (uint16)(24U)
#define CTRL1_PRISRC_IP_DIV2       (uint16)(25U)
#define CTRL1_PRISRC_IP_DIV4       (uint16)(26U)
#define CTRL1_PRISRC_IP_DIV8       (uint16)(27U)
#define CTRL1_PRISRC_IP_DIV16      (uint16)(28U)
#define CTRL1_PRISRC_IP_DIV32      (uint16)(29U)
#define CTRL1_PRISRC_IP_DIV64      (uint16)(30U)
#define CTRL1_PRISRC_IP_DIV128     (uint16)(31U)

#define CTRL1_ONCE_CONTINUOUS      (uint16)(0U)
#define CTRL1_ONCE_ONE_SHOT        (uint16)(1U)

#define CTRL1_LENGTH_ROLLOVER      (uint16)(0U)
#define CTRL1_LENGTH_COMPARE       (uint16)(1U)

#define CTRL1_DIR_UP               (uint16)(0U)
#define CTRL1_DIR_DOWN             (uint16)(1U)

#define CTRL1_SECSRC_CNT0_IN       (uint16)(0U)
#define CTRL1_SECSRC_CNT1_IN       (uint16)(1U)
#define CTRL1_SECSRC_CNT2_IN       (uint16)(2U)
#define CTRL1_SECSRC_CNT3_IN       (uint16)(3U)
#define CTRL1_SECSRC_CNT4_IN       (uint16)(4U)
#define CTRL1_SECSRC_CNT5_IN       (uint16)(5U)
#define CTRL1_SECSRC_CNT6_IN       (uint16)(6U)
#define CTRL1_SECSRC_CNT7_IN       (uint16)(7U)
#define CTRL1_SECSRC_AUX0_IN       (uint16)(8U)
#define CTRL1_SECSRC_AUX1_IN       (uint16)(9U)
#define CTRL1_SECSRC_AUX2_IN       (uint16)(10U)
#define CTRL1_SECSRC_AUX3_IN       (uint16)(11U)
#define CTRL1_SECSRC_AUX4_IN       (uint16)(12U)
#define CTRL1_SECSRC_AUX5_IN       (uint16)(13U)
#define CTRL1_SECSRC_AUX6_IN       (uint16)(14U)
#define CTRL1_SECSRC_AUX7_IN       (uint16)(15U)
#define CTRL1_SECSRC_CNT0_OUT      (uint16)(16U)
#define CTRL1_SECSRC_CNT1_OUT      (uint16)(17U)
#define CTRL1_SECSRC_CNT2_OUT      (uint16)(18U)
#define CTRL1_SECSRC_CNT3_OUT      (uint16)(19U)
#define CTRL1_SECSRC_CNT4_OUT      (uint16)(20U)
#define CTRL1_SECSRC_CNT5_OUT      (uint16)(21U)
#define CTRL1_SECSRC_CNT6_OUT      (uint16)(22U)
#define CTRL1_SECSRC_CNT7_OUT      (uint16)(23U)

/**
* @brief CTRL2 - bitfield defines.
*/
#define CTRL2_OEN_INPUT            (uint16)(0U)
#define CTRL2_OEN_OUTPUT           (uint16)(1U)

#define CTRL2_RDNT_DISABLE         (uint16)(0U)
#define CTRL2_RDNT_ENABLE          (uint16)(1U)



/* CTRL2_INPUT bit is Read Only - bitfield define not needed */
#define CTRL2_VAL_0                (uint16)(0U)
#define CTRL2_VAL_1                (uint16)(1U)

#define CTRL2_FORCE_0              (uint16)(0U)
#define CTRL2_FORCE_1              (uint16)(1U)

#define CTRL2_COFRC_DISABLE        (uint16)(0U)
#define CTRL2_COFRC_ENABLE         (uint16)(1U)

#define CTRL2_COINIT_DISABLE       (uint16)(0U)
#define CTRL2_COINIT_LOAD          (uint16)(1U)
#define CTRL2_COINIT_CMPLD         (uint16)(2U)

#define CTRL2_SIPS_NONINVERTED     (uint16)(0U)
#define CTRL2_SIPS_INVERTED        (uint16)(1U)

#define CTRL2_PIPS_NONINVERTED     (uint16)(0U)
#define CTRL2_PIPS_INVERTED        (uint16)(1U)

#define CTRL2_OPS_NONINVERTED      (uint16)(0U)
#define CTRL2_OPS_INVERTED         (uint16)(1U)

#define CTRL2_MSTR_DISABLE         (uint16)(0U)
#define CTRL2_MSTR_ENABLE          (uint16)(1U)

#define CTRL2_OUTMODE_SOFTWARE                             (uint16)(0U)
#define CTRL2_OUTMODE_CLEAR_CMP1_CMP2                      (uint16)(1U)
#define CTRL2_OUTMODE_SET_CMP1_CMP2                        (uint16)(2U)
#define CTRL2_OUTMODE_TOGGLE_CMP1_CMP2                     (uint16)(3U)
#define CTRL2_OUTMODE_TOGGLE_ALTERNATE                     (uint16)(4U)
#define CTRL2_OUTMODE_SET_CMP1_CLEAR_SEC_INPUT             (uint16)(5U)
#define CTRL2_OUTMODE_SET_CMP2_CLEAR_SEC_INPUT             (uint16)(6U)
#define CTRL2_OUTMODE_SET_CMP_CLEAR_ROLLOVER               (uint16)(7U)
#define CTRL2_OUTMODE_SET_CMP1_CLEAR_CMP2                  (uint16)(8U)
#define CTRL2_OUTMODE_ASSERTED_ACTIVE_CLEARED_STOPPED      (uint16)(9U)
#define CTRL2_OUTMODE_ASSERTED_UP_CLEARED_DOWN             (uint16)(10U)
#define CTRL2_OUTMODE_ENABLE_GATED_CLOCK                   (uint16)(15U)

/**
* @brief CTRL3 - bitfield defines.
*/
#define CTRL3_STPEN_UNAFFECTED_STOP   (uint16)(0U)
#define CTRL3_STPEN_DISABLED_STOP     (uint16)(1U)

#define CTRL3_ROC_DISABLE             (uint16)(0U)
#define CTRL3_ROC_CAPTURE1            (uint16)(1U)
#define CTRL3_ROC_CAPTURE2            (uint16)(2U)
#define CTRL3_ROC_CAPTURE1_2          (uint16)(3U)

/* C2FCNT - CAPT2 FIFO Word Count  - bitfield defines not needed  */
/* C1FCNT - CAPT1 FIFO Word Count  - bitfield defines not needed  */
#define CTRL3_DBGEN_OFF               (uint16)(0U)
#define CTRL3_DBGEN_HALT              (uint16)(1U)
#define CTRL3_DBGEN_FORCE0_OPS        (uint16)(2U)
#define CTRL3_DBGEN_HALT_FORCE0_OPS   (uint16)(3U)

/* STS - Status Register - bitfield defines not needed */
/**
* @brief INTDMA  - Interrupt and DMA Enable Register - bitfield defines.
*/
#define INTDMA_ICF2DE_DISABLE         (uint16)(0U)
#define INTDMA_ICF2DE_ENABLE          (uint16)(1U)

#define INTDMA_ICF1DE_DISABLE         (uint16)(0U)
#define INTDMA_ICF1DE_ENABLE          (uint16)(1U)

#define INTDMA_CMPLD2DE_DISABLE       (uint16)(0U)
#define INTDMA_CMPLD2DE_ENABLE        (uint16)(1U)

#define INTDMA_CMPLD1DE_DISABLE       (uint16)(0U)
#define INTDMA_CMPLD1DE_ENABLE        (uint16)(1U)

#define INTDMA_WDFIE_DISABLE          (uint16)(0U)
#define INTDMA_WDFIE_ENABLE           (uint16)(1U)

#define INTDMA_RCFIE_DISABLE          (uint16)(0U)
#define INTDMA_RCFIE_ENABLE           (uint16)(1U)

#define INTDMA_ICF2IE_DISABLE         (uint16)(0U)
#define INTDMA_ICF2IE_ENABLE          (uint16)(1U)

#define INTDMA_ICF1IE_DISABLE         (uint16)(0U)
#define INTDMA_ICF1IE_ENABLE          (uint16)(1U)

#define INTDMA_IEHFIE_DISABLE         (uint16)(0U)
#define INTDMA_IEHFIE_ENABLE          (uint16)(1U)

#define INTDMA_IELFIE_DISABLE         (uint16)(0U)
#define INTDMA_IELFIE_ENABLE          (uint16)(1U)

#define INTDMA_TOFIE_DISABLE          (uint16)(0U)
#define INTDMA_TOFIE_ENABLE           (uint16)(1U)

#define INTDMA_TCF2IE_DISABLE         (uint16)(0U)
#define INTDMA_TCF2IE_ENABLE          (uint16)(1U)

#define INTDMA_TCF1IE_DISABLE         (uint16)(0U)
#define INTDMA_TCF1IE_ENABLE          (uint16)(1U)

#define INTDMA_TCFIE_DISABLE          (uint16)(0U)
#define INTDMA_TCFIE_ENABLE           (uint16)(1U)

/**
* @brief CCCTRL - Compare and Capture Control Register - bitfield defines.
*/
#define CCCTRL_CLC2_NEVER_PRELOAD                   (uint16)(0U)
#define CCCTRL_CLC2_LOAD_COMP2_W_CMPLD1_ON_COMP1    (uint16)(2U)
/*
*@violates @ref Reg_eSys_ETIMER_H_REF_1 Identifier clash
*/
#define CCCTRL_CLC2_LOAD_COMP2_W_CMPLD1_ON_COMP2    (uint16)(3U)
#define CCCTRL_CLC2_LOAD_COMP2_W_CMPLD2_ON_COMP1    (uint16)(4U)
/*
*@violates @ref Reg_eSys_ETIMER_H_REF_1 Identifier clash
*/
#define CCCTRL_CLC2_LOAD_COMP2_W_CMPLD2_ON_COMP2    (uint16)(5U)
#define CCCTRL_CLC2_LOAD_CNTR_W_CMPLD2_ON_COMP1     (uint16)(6U)
/*
*@violates @ref Reg_eSys_ETIMER_H_REF_1 Identifier clash
*/
#define CCCTRL_CLC2_LOAD_CNTR_W_CMPLD2_ON_COMP2     (uint16)(7U)

#define CCCTRL_CLC1_NEVER_PRELOAD                   (uint16)(0U)
#define CCCTRL_CLC1_LOAD_COMP1_W_CMPLD1_ON_COMP1    (uint16)(2U)
/*
*@violates @ref Reg_eSys_ETIMER_H_REF_1 Identifier clash
*/
#define CCCTRL_CLC1_LOAD_COMP1_W_CMPLD1_ON_COMP2    (uint16)(3U)
#define CCCTRL_CLC1_LOAD_COMP1_W_CMPLD2_ON_COMP1    (uint16)(4U)
/*
*@violates @ref Reg_eSys_ETIMER_H_REF_1 Identifier clash
*/
#define CCCTRL_CLC1_LOAD_COMP1_W_CMPLD2_ON_COMP2    (uint16)(5U)
#define CCCTRL_CLC1_LOAD_CNTR_W_CMPLD1_ON_COMP1     (uint16)(6U)
/*
*@violates @ref Reg_eSys_ETIMER_H_REF_1 Identifier clash
*/
#define CCCTRL_CLC1_LOAD_CNTR_W_CMPLD1_ON_COMP2     (uint16)(7U)

#define CCCTRL_CMPMODE_CMP1_UP_CMP2_UP              (uint16)(0U)
#define CCCTRL_CMPMODE_CMP1_DOWN_CMP2_UP            (uint16)(1U)
#define CCCTRL_CMPMODE_CMP1_UP_CMP2_DOWN            (uint16)(2U)
#define CCCTRL_CMPMODE_CMP1_DOWN_CMP2_DOWN          (uint16)(3U)

#define CCCTRL_CPT2MODE_DISABLED                    (uint16)(0U)
#define CCCTRL_CPT2MODE_FALLING                     (uint16)(1U)
#define CCCTRL_CPT2MODE_RISING                      (uint16)(2U)
#define CCCTRL_CPT2MODE_ANY                         (uint16)(3U)

#define CCCTRL_CPT1MODE_DISABLED                    (uint16)(0U)
#define CCCTRL_CPT1MODE_FALLING                     (uint16)(1U)
#define CCCTRL_CPT1MODE_RISING                      (uint16)(2U)
#define CCCTRL_CPT1MODE_ANY                         (uint16)(3U)

#define CCCTRL_CFWM_0                               (uint16)(0U)
#define CCCTRL_CFWM_1                               (uint16)(1U)
#define CCCTRL_CFWM_2                               (uint16)(2U)
#define CCCTRL_CFWM_3                               (uint16)(3U)

#define CCCTRL_ONESHOT_DISABLED                     (uint16)(0U)
#define CCCTRL_ONESHOT_ENABLED                      (uint16)(1U)

#define CCCTRL_ARM_DISABLED                         (uint16)(0U)
#define CCCTRL_ARM_ENABLED                          (uint16)(1U)

/**
* @brief ENBL - Channel Enable Register - bitfield defines.
*/
#define ENBL_ENBL_CH0_DISABLE                       (uint16)(0U)
#define ENBL_ENBL_CH0_ENABLED                       (uint16)(1U)
#define ENBL_ENBL_CH1_DISABLE                       (uint16)(0U)
#define ENBL_ENBL_CH1_ENABLE                        (uint16)(2U)
#define ENBL_ENBL_CH2_DISABLE                       (uint16)(0U)
#define ENBL_ENBL_CH2_ENABLE                        (uint16)(4U)
#define ENBL_ENBL_CH3_DISABLE                       (uint16)(0U)
#define ENBL_ENBL_CH3_ENABLE                        (uint16)(8U)
#define ENBL_ENBL_CH4_DISABLE                       (uint16)(0U)
#define ENBL_ENBL_CH4_ENABLE                        (uint16)(16U)
#define ENBL_ENBL_CH5_DISABLE                       (uint16)(0U)
#define ENBL_ENBL_CH5_ENABLE                        (uint16)(32U)

/**
* @brief DMA0 Request Select Register - bitfield defines.
*/
#define DREQ0_DREQ0_CH0_CAPT1_READ                  (uint16)(0U)
#define DREQ0_DREQ0_CH0_CAPT2_READ                  (uint16)(1U)
#define DREQ0_DREQ0_CH0_CMPLD1_WRITE                (uint16)(2U)
#define DREQ0_DREQ0_CH0_CMPLD2_WRITE                (uint16)(3U)
#define DREQ0_DREQ0_CH1_CAPT1_READ                  (uint16)(4U)
#define DREQ0_DREQ0_CH1_CAPT2_READ                  (uint16)(5U)
#define DREQ0_DREQ0_CH1_CMPLD1_WRITE                (uint16)(6U)
#define DREQ0_DREQ0_CH1_CMPLD2_WRITE                (uint16)(7U)
#define DREQ0_DREQ0_CH2_CAPT1_READ                  (uint16)(8U)
#define DREQ0_DREQ0_CH2_CAPT2_READ                  (uint16)(9U)
#define DREQ0_DREQ0_CH2_CMPLD1_WRITE                (uint16)(10U)
#define DREQ0_DREQ0_CH2_CMPLD2_WRITE                (uint16)(11U)
#define DREQ0_DREQ0_CH3_CAPT1_READ                  (uint16)(12U)
#define DREQ0_DREQ0_CH3_CAPT2_READ                  (uint16)(13U)
#define DREQ0_DREQ0_CH3_CMPLD1_WRITE                (uint16)(14U)
#define DREQ0_DREQ0_CH3_CMPLD2_WRITE                (uint16)(15U)
#define DREQ0_DREQ0_CH4_CAPT1_READ                  (uint16)(16U)
#define DREQ0_DREQ0_CH4_CAPT2_READ                  (uint16)(17U)
#define DREQ0_DREQ0_CH4_CMPLD1_WRITE                (uint16)(18U)
#define DREQ0_DREQ0_CH4_CMPLD2_WRITE                (uint16)(19U)
#define DREQ0_DREQ0_CH5_CAPT1_READ                  (uint16)(20U)
#define DREQ0_DREQ0_CH5_CAPT2_READ                  (uint16)(21U)
#define DREQ0_DREQ0_CH5_CMPLD1_WRITE                (uint16)(22U)
#define DREQ0_DREQ0_CH5_CMPLD2_WRITE                (uint16)(23U)
#define DREQ0_DREQ0_CH6_CAPT1_READ                  (uint16)(24U)
#define DREQ0_DREQ0_CH6_CAPT2_READ                  (uint16)(25U)
#define DREQ0_DREQ0_CH6_CMPLD1_WRITE                (uint16)(26U)
#define DREQ0_DREQ0_CH6_CMPLD2_WRITE                (uint16)(27U)
#define DREQ0_DREQ0_CH7_CAPT1_READ                  (uint16)(28U)
#define DREQ0_DREQ0_CH7_CAPT2_READ                  (uint16)(29U)
#define DREQ0_DREQ0_CH7_CMPLD1_WRITE                (uint16)(30U)
#define DREQ0_DREQ0_CH7_CMPLD2_WRITE                (uint16)(31U)

/**
* @brief DMA1 Request Select Register - bitfield defines.
*/
#define DREQ1_DREQ1_CH0_CAPT1_READ                  (uint16)(0U)
#define DREQ1_DREQ1_CH0_CAPT2_READ                  (uint16)(1U)
#define DREQ1_DREQ1_CH0_CMPLD1_WRITE                (uint16)(2U)
#define DREQ1_DREQ1_CH0_CMPLD2_WRITE                (uint16)(3U)
#define DREQ1_DREQ1_CH1_CAPT1_READ                  (uint16)(4U)
#define DREQ1_DREQ1_CH1_CAPT2_READ                  (uint16)(5U)
#define DREQ1_DREQ1_CH1_CMPLD1_WRITE                (uint16)(6U)
#define DREQ1_DREQ1_CH1_CMPLD2_WRITE                (uint16)(7U)
#define DREQ1_DREQ1_CH2_CAPT1_READ                  (uint16)(8U)
#define DREQ1_DREQ1_CH2_CAPT2_READ                  (uint16)(9U)
#define DREQ1_DREQ1_CH2_CMPLD1_WRITE                (uint16)(10U)
#define DREQ1_DREQ1_CH2_CMPLD2_WRITE                (uint16)(11U)
#define DREQ1_DREQ1_CH3_CAPT1_READ                  (uint16)(12U)
#define DREQ1_DREQ1_CH3_CAPT2_READ                  (uint16)(13U)
#define DREQ1_DREQ1_CH3_CMPLD1_WRITE                (uint16)(14U)
#define DREQ1_DREQ1_CH3_CMPLD2_WRITE                (uint16)(15U)
#define DREQ1_DREQ1_CH4_CAPT1_READ                  (uint16)(16U)
#define DREQ1_DREQ1_CH4_CAPT2_READ                  (uint16)(17U)
#define DREQ1_DREQ1_CH4_CMPLD1_WRITE                (uint16)(18U)
#define DREQ1_DREQ1_CH4_CMPLD2_WRITE                (uint16)(19U)
#define DREQ1_DREQ1_CH5_CAPT1_READ                  (uint16)(20U)
#define DREQ1_DREQ1_CH5_CAPT2_READ                  (uint16)(21U)
#define DREQ1_DREQ1_CH5_CMPLD1_WRITE                (uint16)(22U)
#define DREQ1_DREQ1_CH5_CMPLD2_WRITE                (uint16)(23U)
#define DREQ1_DREQ1_CH6_CAPT1_READ                  (uint16)(24U)
#define DREQ1_DREQ1_CH6_CAPT2_READ                  (uint16)(25U)
#define DREQ1_DREQ1_CH6_CMPLD1_WRITE                (uint16)(26U)
#define DREQ1_DREQ1_CH6_CMPLD2_WRITE                (uint16)(27U)
#define DREQ1_DREQ1_CH7_CAPT1_READ                  (uint16)(28U)
#define DREQ1_DREQ1_CH7_CAPT2_READ                  (uint16)(29U)
#define DREQ1_DREQ1_CH7_CMPLD1_WRITE                (uint16)(30U)
#define DREQ1_DREQ1_CH7_CMPLD2_WRITE                (uint16)(31U)

/**
* @brief   Highest possible value for eTIMER channels.
*/
#define ETIMER_CNTR_MAX_VALUE                       (uint16)(0xFFFFUL)

/**
* @brief DMA Request inactive value
*/
#define ETIMER_DREQ_INACTIVE                        (uint16)(0x001FUL)


/*==================================================================================================
*                                            ENUMS
==================================================================================================*/

/*==================================================================================================
*                                STRUCTURES AND OTHER TYPEDEFS
==================================================================================================*/

/*==================================================================================================
*                                GLOBAL VARIABLE DECLARATIONS
==================================================================================================*/

/*==================================================================================================
*                                    FUNCTION PROTOTYPES
==================================================================================================*/

#ifdef __cplusplus
}
#endif

#endif /* REG_ESYS_ETIMER_H */
