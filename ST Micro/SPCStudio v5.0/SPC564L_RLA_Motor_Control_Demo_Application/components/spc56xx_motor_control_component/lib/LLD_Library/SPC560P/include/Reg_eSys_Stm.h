/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    Reg_eSys_Stm.h
*   @version BETA 0.9.1
*   @brief   This file contains the STM driver header file
*          
*   @details STM main register description.
*
*/

#ifndef REG_ESYS_STM_H
#define REG_ESYS_STM_H

#ifdef __cplusplus
extern "C"{
#endif

/*
* @page misra_violations MISRA-C:2004 violations
*
* @section REG_ESYS_STM_H_REF_1
* Violates MISRA 2004 Advisory Rule 19.7, Use of function like macro.
* This violation is due to function like macros defined for register operations.
* Function like macros are used to reduce code complexity.
*
* @section REG_ESYS_STM_H_REF_2
* Violates MISRA 2004 Required Rule 19.15, Repeated include file
* This comes from the order of includes in the .c file and from include dependencies. As a safe
* approach, any file must include all its dependencies. Header files are already protected against
* double inclusions.
*
*/

/*==================================================================================================
*                                         INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/
/* @violates @ref REG_ESYS_STM_H_REF_2 Violates MISRA 2004 Required Rule 19.15, Repeated include file MemMap.h */
#include "Reg_eSys.h"
/* @violates @ref REG_ESYS_STM_H_REF_2 Violates MISRA 2004 Required Rule 19.15, Repeated include file MemMap.h */
#include "Reg_Macros.h"

/*==================================================================================================
*                               SOURCE FILE VERSION INFORMATION
==================================================================================================*/
/**
* @brief Parameters that shall be published within the Gpt driver header file and also in the
*       module's description file
* @requirements BSW00374, BSW00379, BSW00318
*/
#define REG_ESYS_STM_VENDOR_ID                       M4_SRC_AR_MODULE_VENDOR_ID
#define REG_ESYS_STM_AR_RELEASE_MAJOR_VERSION        M4_SRC_AR_SPEC_VERSION_MAJOR
#define REG_ESYS_STM_AR_RELEASE_MINOR_VERSION        M4_SRC_AR_SPEC_VERSION_MINOR
#define REG_ESYS_STM_AR_RELEASE_REVISION_VERSION     M4_SRC_AR_SPEC_VERSION_PATCH
#define REG_ESYS_STM_SW_MAJOR_VERSION                M4_SRC_SW_VERSION_MAJOR
#define REG_ESYS_STM_SW_MINOR_VERSION                M4_SRC_SW_VERSION_MINOR
#define REG_ESYS_STM_SW_PATCH_VERSION                M4_SRC_SW_VERSION_PATCH

/*==================================================================================================
*                                      FILE VERSION CHECKS
==================================================================================================*/


/*==================================================================================================
*                                           CONSTANTS
==================================================================================================*/

/*==================================================================================================
*                                       DEFINES AND MACROS
==================================================================================================*/
/**
* @brief System Timer Module (STM) Registers address
*/
#define STM_CR                  (STM_BASEADDR + (uint32)0x0UL)
#define STM_CNT                 (STM_BASEADDR + (uint32)0x4UL)

#define STM_CCR0                (STM_BASEADDR + (uint32)0x10UL)
#define STM_CIR0                (STM_BASEADDR + (uint32)0x14UL)

/* @violates @ref REG_ESYS_STM_H_REF_1 Violates MISRA 2004 Advisory Rule 19.7, Use of function like macro */
#define STM_CIR(channel)        (STM_BASEADDR + (uint32)0x14UL + ((uint32)0x10UL*(uint32)channel))
#define STM_CMP0                (STM_BASEADDR + (uint32)0x18UL)

#define STM_CCR1                (STM_BASEADDR + (uint32)0x20UL)
#define STM_CIR1                (STM_BASEADDR + (uint32)0x24UL)
#define STM_CMP1                (STM_BASEADDR + (uint32)0x28UL)

#define STM_CCR2                (STM_BASEADDR + (uint32)0x30UL)
#define STM_CIR2                (STM_BASEADDR + (uint32)0x34UL)
#define STM_CMP2                (STM_BASEADDR + (uint32)0x38UL)

#define STM_CCR3                (STM_BASEADDR + (uint32)0x40UL)
#define STM_CIR3                (STM_BASEADDR + (uint32)0x44UL)
#define STM_CMP3                (STM_BASEADDR + (uint32)0x48UL)

/**
* @brief Mask Enabling and Disabling function
*/
#define STM_TEN_DISABLED (uint32)0x00000000UL
#define STM_TEN_ENABLED  (uint32)0x00000001UL
#define STM_CCR_DISABLE  (uint32)0x00000000UL
#define STM_CCR_ENABLE   (uint32)0x00000001UL
#define STM_CIR_CLEAR    (uint32)0x00000001UL
#define STM_FRZ_ENABLE   (uint32)0x00000002UL

#define STM_OVERFLOW_PAD         ((uint8)1U)
/* Mask for prescaler setting */
#define STM_PRESCALER_MASK (uint32)0x0000FF00UL

/**
* @brief Highest possible value for STM channels
*/
#define STM_CNT_MAX_VALUE                      (uint32)0xFFFFFFFFuL

/**
* @brief Macros for System Timer Module (STM) Registers
*/
/* @violates @ref REG_ESYS_STM_H_REF_1 Violates MISRA 2004 Advisory Rule 19.7, Use of function like macro */
#define STM_TIMER_COUNTER_DISABLE()        (REG_BIT_CLEAR32(STM_CR,STM_TEN_ENABLED))  /**< @brief Macro to Disable STM counter */
/* @violates @ref REG_ESYS_STM_H_REF_1 Violates MISRA 2004 Advisory Rule 19.7, Use of function like macro */
#define STM_TIMER_COUNTER_ENABLE()         (REG_BIT_SET32(STM_CR,STM_TEN_ENABLED))  /**< @brief Macro to enable STM counter */
/* @violates @ref REG_ESYS_STM_H_REF_1 Violates MISRA 2004 Advisory Rule 19.7, Use of function like macro */
#define STM_CH_DISABLE(channel)            (REG_WRITE32((STM_CCR0+((uint32)0x10UL*(uint32)channel)),STM_CCR_DISABLE))  /**< @brief Macro to disable STM channel */
/* @violates @ref REG_ESYS_STM_H_REF_1 Violates MISRA 2004 Advisory Rule 19.7, Use of function like macro */
#define STM_CH_ENABLE(channel)             (REG_WRITE32((STM_CCR0+((uint32)0x10UL*(uint32)channel)),STM_CCR_ENABLE))  /**< @brief Macro to enable STM channel */
/* @violates @ref REG_ESYS_STM_H_REF_1 Violates MISRA 2004 Advisory Rule 19.7, Use of function like macro */
#define STM_CH_READ_ISR_REQ(channel)       (REG_READ32(STM_CIR0+((uint32)0x10UL*(uint32)channel)))  /**< @brief Macro to read the interrupt status */
/* @violates @ref REG_ESYS_STM_H_REF_1 Violates MISRA 2004 Advisory Rule 19.7, Use of function like macro */
#define STM_CH_CLEAR_ISR_REQ(channel)      (REG_WRITE32((STM_CIR0+((uint32)0x10UL*(uint32)channel)),STM_CIR_CLEAR))  /**< @brief Macro to clear the interrupt flag  */
/* @violates @ref REG_ESYS_STM_H_REF_1 Violates MISRA 2004 Advisory Rule 19.7, Use of function like macro */
#define STM_GET_COUNTER()                  (REG_READ32(STM_CNT))  /**< @brief Macro to get the counter value */
/* @violates @ref REG_ESYS_STM_H_REF_1 Violates MISRA 2004 Advisory Rule 19.7, Use of function like macro */
#define STM_SET_PRESCALER(value)           (REG_RMW32(STM_CR,STM_PRESCALER_MASK,(uint32)((uint32)value<<(uint32)8U)))  /**< @brief Macro to set the prescaler value  */
/* @violates @ref REG_ESYS_STM_H_REF_1 Violates MISRA 2004 Advisory Rule 19.7, Use of function like macro */
#define STM_SET_COMPARE(channel,value)     (REG_WRITE32((STM_CMP0+((uint32)0x10UL*(uint32)channel)),(value)))  /**< @brief Macro to set the compare value */
/* @violates @ref REG_ESYS_STM_H_REF_1 Violates MISRA 2004 Advisory Rule 19.7, Use of function like macro */
#define STM_GET_COMPARE(channel)           (REG_READ32((STM_CMP0+((uint32)0x10UL*(uint32)channel))))  /**< @brief Macro to get the compare value */
/* @violates @ref REG_ESYS_STM_H_REF_1 Violates MISRA 2004 Advisory Rule 19.7, Use of function like macro */
#define STM_SET_FRZ()                      (REG_BIT_SET32(STM_CR,STM_FRZ_ENABLE))  /**< @brief Macro to enable the Freeze */
/**< @brief Macro to disbale the Freeze */
/* @violates @ref REG_ESYS_STM_H_REF_1 Violates MISRA 2004 Advisory Rule 19.7, Use of function like macro */
#define STM_CLR_FRZ()                      (REG_BIT_CLEAR32(STM_CR,STM_FRZ_ENABLE))  

/*==================================================================================================
*                                             ENUMS
==================================================================================================*/

/*==================================================================================================
*                                 STRUCTURES AND OTHER TYPEDEFS
==================================================================================================*/

/*==================================================================================================
*                                 GLOBAL VARIABLE DECLARATIONS
==================================================================================================*/

/*==================================================================================================
*                                     FUNCTION PROTOTYPES
==================================================================================================*/

#ifdef __cplusplus
}
#endif

#endif /*REG_ESYS_STM_H*/
