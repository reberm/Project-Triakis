/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    Reg_eSys.h
*   @version BETA 0.9.1
*   @brief   This file contains the AdBIP Memory and DMA mapping
*          
*   @details Memory mapping of the IP modules and DMA channels present on the XPC560XP hardware platform.
*
*/

/*
* @page misra_violations MISRA-C:2004 violations
*
* @section Reg_eSys_h_REF_1
* Violates MISRA 2004 Required Rule 19.15, Repeated include file
* This comes from the order of includes in the .c file and from include dependencies. As a safe
* approach, any file must include all its dependencies. Header files are already protected against
* double inclusions.
*
*/

#ifndef REG_ESYS_H
#define REG_ESYS_H

#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
*                                         INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/
/*
* @file        Reg_eSys.h
* @brief       Include platform types
*/
/*
* @violates @ref Reg_eSys_h_REF_1 Repeated include file.
*/
#include "Platform_Types.h"
#include "Soc_Ips.h"

/*==================================================================================================
*                               SOURCE FILE VERSION INFORMATION
==================================================================================================*/

/*==================================================================================================
*                                      FILE VERSION CHECKS
==================================================================================================*/

/*==================================================================================================
*                                          CONSTANTS
==================================================================================================*/

/*==================================================================================================
*                                       DEFINES AND MACROS
==================================================================================================*/
/**
* @brief Internal Flash Array 0
* @details XPC560XP System Memory Map
*/
#define INTERNAL_FLASH_BASEADDR             ((uint32)0x00000000UL)
/**
* @brief Internal Flash Array 1
* @details XPC560XP System Memory Map
*/
#define INTERNAL_FLASH1_BASEADDR            ((uint32)0x00800000UL)
/**
* @brief Internal RAM
* @details XPC560XP System Memory Map
*/
#define INTERNAL_RAM_BASEADDR              ((uint32)0x40000000UL)
/**
* @brief Second Internal RAM memory
* @details XPC560XP System Memory Map
*/
#define INTERNAL_RAM1_BASEADDR             ((uint32)0x50000000UL)

/**
* @brief CodeFLASH0_A
* @details XPC560XP System Memory Map
*/
#define FLASHMEM0_CF0_A_BASEADDR            ((uint32)0xC3F88000UL)
/**
* @brief DataFLASH0_A
* @details XPC560XP System Memory Map
*/
#define FLASHMEM0_DF0_A_BASEADDR            ((uint32)0xC3F8C000UL)
/**
* @brief System Integration Unit Lite (SIUL)
* @details XPC560XP System Memory Map
*/
#define SIUL_BASEADDR                       ((uint32)0xC3F90000UL)
/**
* @brief WakeUp Unit
* @details XPC560XP System Memory Map
*/
#define WKPU_BASEADDR                       ((uint32)0xC3F94000UL)

/**
* @brief System Status and Configuration Module (SSCM)
* @details XPC560XP System Memory Map
*/
#define SSCM_BASEADDR                       ((uint32)0xC3FD8000UL)
/**
* @brief Mode Entry Module (MC_ME)
* @details XPC560XP System Memory Map
*/
#define MC_ME_BASEADDR                      ((uint32)0xC3FDC000UL)
/**
* @brief Clock Generation Module (MC_CGM)
* @details XPC560XP System Memory Map
*/
#define MC_CGM_BASEADDR                     ((uint32)0xC3FE0000UL)
/**
* @brief Reset Generation Module (MC_RGM)
* @details XPC560XP System Memory Map
*/
#define MC_RGM_BASEADDR                     ((uint32)0xC3FE4000UL)
/**
* @brief Power Control Unit (MC_PCU)
* @details XPC560XP System Memory Map
*/
#define MC_PCU_BASEADDR                     ((uint32)0xC3FE8000UL)

/**
* @brief Periodic Interrupt Timer (PIT)
* @details XPC560XP System Memory Map
*/
#define PIT_BASEADDR                        ((uint32)0xC3FF0000UL)

/**
* @brief Analog to digital converter 0 (ADC 0)
* @details XPC560XP System Memory Map
*/
#define ADC0_BASEADDR                       ((uint32)0xFFE00000UL)
/**
* @brief Analog to digital converter 1 (ADC 1)
* @details XPC560XP System Memory Map
*/
#define ADC1_BASEADDR                       ((uint32)0xFFE04000UL)

/**
* @brief Cross triggering unit (CTU)
* @details XPC560XP System Memory Map
*/
#define CTU_BASEADDR                        ((uint32)0xFFE0C000UL)

/**
* @brief eTimer 0
* @details XPC560XP System Memory Map
*/
#define ETIMER_0_BASEADDR                   ((uint32)0xFFE18000UL)
/**
* @brief eTimer 1
* @details XPC560XP System Memory Map
*/
#define ETIMER_1_BASEADDR                   ((uint32)0xFFE1C000UL)

/**
* @brief FlexPWM 0
* @details XPC560XP System Memory Map
*/
#define FLEXPWM_0_BASEADDR                  ((uint32)0xFFE24000UL)

/**
* @brief LINFlex 0
* @details XPC560XP System Memory Map
*/
#define LINFLEX0_BASEADDR                   ((uint32)0xFFE40000UL)
/**
* @brief LINFlex 1
* @details XPC560XP System Memory Map
*/
#define LINFLEX1_BASEADDR                   ((uint32)0xFFE44000UL)

/**
* @brief Cyclic redundancy check unit (CRC)
* @details XPC560XP System Memory Map
*/
#define CRC_BASEADDR                        ((uint32)0xFFE68000UL)


/**
* @brief Platform FLASH Controller
* @details XPC560XP System Memory Map
*/
#define FLASHMEM0_PFC0_BASEADDR             ((uint32)0xFFE88000UL)

/**
* @brief Software Watchdog (SWT_0)
* @details XPC560XP System Memory Map
*/
#define SWT_BASEADDR                        ((uint32)0xFFF38000UL)
/**
* @brief System Timer Module (STM_0)
* @details XPC560XP System Memory Map
*/
#define STM_BASEADDR                        ((uint32)0xFFF3C000UL)
/**
* @brief Error Correction Status Module (ECSM)
* @details XPC560XP System Memory Map
*/
#define ECSM_BASEADDR                       ((uint32)0xFFF40000UL)
/**
* @brief Error Correction Status Module (ECSM_1)
* @details XPC560XP System Memory Map
*/
#define ECSM_1_BASEADDR                     ((uint32)0x8FF40000UL)
/**
* @brief Direct Memory Access Controller (DMA)
* @details XPC560XP System Memory Map
*/
#define DMA_BASEADDR                        ((uint32)0xFFF44000UL)
/**
* @brief Interrupt Controller (INTC)
* @details XPC560XP System Memory Map
*/
#define INTC_BASEADDR                       ((uint32)0xFFF48000UL)

/**
* @brief DSPI 0
* @details XPC560XP System Memory Map
*/
#define DSPI0_BASEADDR                      ((uint32)0xFFF90000UL)
/**
* @brief DSPI 1
* @details XPC560XP System Memory Map
*/
#define DSPI1_BASEADDR                      ((uint32)0xFFF94000UL)
/**
* @brief DSPI 2
* @details XPC560XP System Memory Map
*/
#define DSPI2_BASEADDR                      ((uint32)0xFFF98000UL)
/**
* @brief DSPI 3
* @details XPC560XP System Memory Map
*/
#define DSPI3_BASEADDR                      ((uint32)0xFFF9C000UL)

/**
* @brief DSPI 4
* @details XPC560XP System Memory Map
*/
#define DSPI4_BASEADDR                      ((uint32)0x8FFA0000UL)

/**
* @brief FlexCan 0 (CAN0)
* @details XPC560XP System Memory Map
*/
#define FLEXCAN0_BASEADDR                   ((uint32)0xFFFC0000UL)

/**
* @brief FlexCan 2 (CAN2)
* @details XPC560XP System Memory Map
*/
#define FLEXCAN2_BASEADDR                   ((uint32)0x8FFC4000UL)

/**
* @brief DMA Channel Multiplexer
* @details XPC560XP System Memory Map
*/
#define DMAMUX_BASEADDR                     ((uint32)0xFFFDC000UL)
/**
* @brief FlexRay controller (FlexRay)
* @details XPC560XP System Memory Map
*/
#define FLEXRAY_BASEADDR                    ((uint32)0xFFFE0000UL)

/**
* @brief Safety Port FlexCan 1 (CAN1)
* @details XPC560XP System Memory Map
*/
#define FLEXCAN1_BASEADDR                   ((uint32)0xFFFE8000UL)

/**
* @brief Boot Assist Module (BAM)
* @details XPC560XP System Memory Map
*/
#define BAM_BASEADDR                        ((uint32)0xFFFFC000UL)

/**************************************************************************************************/

/**
* @brief INTC OFFSET address for exit interrupt
*/
#define INTC_EOIR_OFFSET                     ((uint32)0x00000018UL) 

/*==================================================================================================
*                                             ENUMS
==================================================================================================*/

/*==================================================================================================
*                                 STRUCTURES AND OTHER TYPEDEFS
==================================================================================================*/

/*==================================================================================================
*                                 GLOBAL VARIABLE DECLARATIONS
==================================================================================================*/

/*==================================================================================================
*                                     FUNCTION PROTOTYPES
==================================================================================================*/

#ifdef __cplusplus
}
#endif

#endif /* #ifndef REG_ESYS_H*/
