/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    Soc_Ips.h
*   @version BETA 0.9.1
*   @brief   This file contains the IP modules versions used on PA hardware platform and
*            IP specific definest
*          
*   @details This file contains the IP modules versions used on PA hardware platform and
*            IP specific definest.
*
*/

/*
* @page misra_violations MISRA-C:2004 violations
*
* @section Soc_Ips_h_REF_1
* Violates MISRA 2004 Required Rule 19.15, Repeated include file
* This comes from the order of includes in the .c file and from include dependencies. As a safe
* approach, any file must include all its dependencies. Header files are already protected against
* double inclusions.
*
*/

#ifndef SOC_IPS_H
#define SOC_IPS_H

#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
*                                         INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/
/*
* @file        Soc_Ips.h
* @brief Include platform types
*/
/*
* @violates @ref Soc_Ips_h_REF_1 Repeated include file.
*/
#include "Platform_Types.h"

/*==================================================================================================
*                               SOURCE FILE VERSION INFORMATION
==================================================================================================*/

/*==================================================================================================
*                                      FILE VERSION CHECKS
==================================================================================================*/

/*==================================================================================================
*                                           CONSTANTS
==================================================================================================*/

/**
* @brief LINFLEX IP Version: LINFLEX2_IPS v0.16.a/v0.17.a
* @details XPC560XP IP Versions
*/
#define IPV_LINFLEX              0x00001000UL


/**
* @brief Fault collection and control unit (FCCU)
* @details XPC560XP System Memory Map
*/
#define FCCU_BASEADDR                       ((uint32)0xFFE6C000UL) 
/** 
* @brief           Defines the ADCDIG IP version for Pictus 256K/512K.
*/
#define IPV_ADCDIG  0x10601UL   /* 1.6.a */

/** 
* @brief           Defines the CTU IP version for Pictus 256K/512K.
*/
#define IPV_CTU     0x505UL     /* 0.5.e */

/** 
* @brief           Defines the Number of CTU2 hardare units
*/
#define CTU_MAX_HW_UNITS  1U   
/**
* @brief SIUL IP Version: siul.01.00.01.15 on Pictus 512k
* @details XPC560XP IP Versions
*/
#define IPV_SIUL                 0x01000115UL
/**
* @brief FLASH IP Version: STA1_FLH4_544K_C90FG@v2.9.a on Pictus 512k
* @details XPC560XP IP Versions
*/
#define IPV_FLASH                0x05120029UL
/**
* @brief ECSM IP Version: 01.00.01.02 on Pictus 512k
* @details XPC560XP IP Versions
*/
#define IPV_ECSM                 0x01000102UL
/**
* @brief Workaround implemented for Errata e2958/err002958
* @details MC_RGM: Clearing a flag in RGM_DES or RGM_FES register may be
*         prevented by a reset.
*/
#define  ERR_IPV_MC_0004         STD_ON
/**
* @brief Workaround implemented for Errata e3060/err003060
* @details MC_RGM: SAFE mode exit may be possible even though condition causing
*        the SAFE mode request has not been cleared.
*/
#define  ERR_IPV_MC_0009         STD_ON
/**
* @brief Workaround implemented for Errata e3269/err003269
* @details MC_ME: Peripheral clocks may get incorrectly disabled or enabled after
*         entering debug mode.
*/
#define  ERR_IPV_MC_0014         STD_ON
/**
* @brief Workaround implemented for Errata e3584/err003584
* @details Configures the MCU to avoid the machine check interrupt that could
*         lead to a checkstop reset (if flash is powered down in low power mode)
*/
#define  ERR_IPV_MC_0017         STD_ON
/**
* @brief Workaround implemented for Errata e2977/err002977
* @details MC_RGM: Long Reset Sequence Occurs on Short Functional Reset Event
*/
#define  ERR_IPV_MC_0019         STD_ON
/**
* @brief Workaround implemented for Errata e2813/err002813
* @details MC_RGM: Reset source flag not set correctly after previous clear
*/
#define  ERR_IPV_MC_0026         STD_ON

/**
* @brief Enable/disable feature for second RAM sector - enabled only for Pictus 1M
* @details XPC560XP Platform Defines/Configurations
*/
#define SECOND_RAM_SECTOR        STD_OFF

/**
* @brief Number of DMA controllers available
* @details XPC560XP Platform Defines/Configurations
*/
#define DMA_NB_CONTROLLERS       (0x01U)
/**
* @brief Number of DMA channels available
* @details XPC560XP Platform Defines/Configurations
*/
#define DMA_NB_CHANNELS          (0x10U)
/**
* @brief Number of DMA_MUX channels
* @details XPC560XP Platform Defines/Configurations
*/
#define DMAMUX_NB_CHANNELS       (DMA_NB_CHANNELS)
/**
* @brief Mask for  DMA_MUX channels
* @details XPC560XP Platform Defines/Configurations
*/
#define DMA_CTRL_MAX_CHANNELS_MASK   0x0FU
/**
* @brief Maximum Number of DMA_MUX channels
* @details XPC560XP Platform Defines/Configurations
*/
#define DMA_CTRL_MAX_CHANNELS  0x10U

/**
* @brief Defines which AutoSAR drivers use the common eDMA IP driver
*/
#define LIN_USE_DMA_LLD          (STD_OFF)
#define ADC_USE_DMA_LLD          (STD_ON)
#define SPI_USE_DMA_LLD          (STD_ON)
#define MCU_USE_DMA_LLD          (STD_ON)
#define ICU_USE_DMA_LLD          (STD_ON)

/*==================================================================================================
                                       DEFINES AND MACROS
==================================================================================================*/

/*==================================================================================================
                                             ENUMS
==================================================================================================*/

/*==================================================================================================
                                 STRUCTURES AND OTHER TYPEDEFS
==================================================================================================*/

/*==================================================================================================
                                 GLOBAL VARIABLE DECLARATIONS
==================================================================================================*/

/*==================================================================================================
                                     FUNCTION PROTOTYPES
==================================================================================================*/

#ifdef __cplusplus
}
#endif

#endif /* #ifndef SOC_IPS_H*/
