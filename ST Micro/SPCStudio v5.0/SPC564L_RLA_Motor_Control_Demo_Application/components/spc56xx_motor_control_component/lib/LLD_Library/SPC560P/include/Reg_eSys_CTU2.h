/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    Reg_eSys_CTU2.h
*   @version BETA 0.9.1
*   @brief   This file contains the CTU2 module register and macro definitions
*          
*   @details CTU2 module registers, and macrodefinitions used to manipulate the module registers.
*
*/

#ifndef REG_ESYS_CTU2_H
#define REG_ESYS_CTU2_H

#ifdef __cplusplus
extern "C"{
#endif

/*
* @page misra_violations MISRA-C:2004 violations
*
* @section Reg_eSys_CTU2_h_REF_1
* Violates MISRA 2004 Required Rule 19.15, Repeated include file
* There are different kinds of execution code sections.
*
* @section Reg_eSys_CTU2_h_REF_2
* Violates MISRA 2004 Required Rule 1.4,
* The compiler/linker shall be checked to ensure that 31 character signifiance and case
* sensitivity are supported for external identifiers.
* This violation is due to the requirement that requests to have a file version check.
*
* @section Reg_eSys_CTU2_h_REF_3
* Violates MISRA 2004 Advisory Rule 19.7, Function-like macro defined
* This violation is due to function like macros defined for register operations.
* Function like macros are used to reduce code complexity.
*
* @section Reg_eSys_CTU2_h_REF_4
* Violates MISRA 2004 Required Rule 10.1,
* Cast from integer type to different type or a wider integer type
* This is used for the Speed optimization of the memory access.
*
* @section Reg_eSys_CTU2_h_REF_5
* Violates MISRA 2004 Required Rule 5.1, Identifiers shall not more than 31 characters
* Violation is needed because of Autosar restrictions for naming File version check macros.
*
*/

/*==================================================================================================
*                                        INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/
/* @violates @ref Reg_eSys_CTU2_h_REF_1 Repeated include files */
#include "Std_Types.h"
/* @violates @ref Reg_eSys_CTU2_h_REF_1 Repeated include files */
#include "Reg_eSys.h"
/* @violates @ref Reg_eSys_CTU2_h_REF_1 Repeated include files */
/*#include "Adc_CTU_ipversion.h"*/
/* @violates @ref Reg_eSys_CTU2_h_REF_1 Repeated include files */
/*#include "Adc_CTU2_Cfg.h"*/
/*#include "Adc_CTU2_PBcfg.h"*/

/*==================================================================================================
*                              SOURCE FILE VERSION INFORMATION
==================================================================================================*/
/*
* @file           Reg_eSys_CTU2.h
*/
#define CTU2_REG_VENDOR_ID                       27
/* @violates @ref Reg_eSys_CTU2_h_REF_5 Identifiers shall not more than 31 characters */
#define CTU2_REG_AR_RELEASE_MAJOR_VERSION        4
/* @violates @ref Reg_eSys_CTU2_h_REF_5 Identifiers shall not more than 31 characters */
#define CTU2_REG_AR_RELEASE_MINOR_VERSION        0
/*
* @violates @ref Reg_eSys_CTU2_h_REF_2 The compiler/linker shall be checked to ensure that 31 character
* signifiance and case sensitivity are supported for external identifiers
*/
/* @violates @ref Reg_eSys_CTU2_h_REF_5 Identifiers shall not more than 31 characters */
#define CTU2_REG_AR_RELEASE_REVISION_VERSION     3
#define CTU2_REG_SW_MAJOR_VERSION                1
#define CTU2_REG_SW_MINOR_VERSION                0
#define CTU2_REG_SW_PATCH_VERSION                1

/*==================================================================================================
*                                     FILE VERSION CHECKS
==================================================================================================*/
#ifndef DISABLE_MCAL_INTERMODULE_ASR_CHECK
 /* Check if Std_Types.h file and CTU2 register header file are of the same Autosar version */
 #if ((CTU2_REG_AR_RELEASE_MAJOR_VERSION != STD_TYPES_AR_RELEASE_MAJOR_VERSION) || \
      (CTU2_REG_AR_RELEASE_MINOR_VERSION != STD_TYPES_AR_RELEASE_MINOR_VERSION))
 #error "AutoSar Version Numbers of Reg_eSys_CTU2.h and Std_Types.h are different"
 #endif
#endif

/*==================================================================================================
*                                          CONSTANTS
==================================================================================================*/
/**
* @brief         These definitions are used to select the derivatives.
*/
#define IPV_CTUV2_00_00_05_03 (0x503UL) /**< @brief Leopard, Leopard cut2/cut3 and Komodo 2M - v0.5.c */
#define IPV_CTUV2_00_00_05_05 (0x505UL) /**< @brief Pictus 512k - v0.5.c; Pictus 256k - v0.5.e */
#define IPV_CTUV2_00_00_05_06 (0x506UL) /**< @brief Pictus 1M - v0.5.f */
#define IPV_CTUV2_00_00_10_00 (0x00001000UL) /**< @brief Panther */

/**
* @brief         Number of bytes in a DMA word.
*/
#define CTUV2_DMA_TRANSFER_WORD_SIZE 4

#if (IPV_CTU == IPV_CTUV2_00_00_05_03) || (IPV_CTU == IPV_CTUV2_00_00_10_00)
/* @violates @ref Reg_eSys_CTU2_h_REF_4 cast from integer type to a other type or a wider integer type */
  #if (CTU_MAX_HW_UNITS == 1)
    #define CTUV2_FIFO_DMA_MUX_CHANNEL_BASE 8U
  #else
    #if (IPV_CTU == IPV_CTUV2_00_00_10_00)
/**
* @brief         Base channel in the DMA_MUX source slot mapping table.
*/
        #define CTUV2_0_FIFO_DMA_MUX_CHANNEL_BASE (uint8)6U
        #define CTUV2_1_FIFO_DMA_MUX_CHANNEL_BASE (uint8)6U
     #else
/**
* @brief         Base channel in the DMA_MUX source slot mapping table.
*/
        #define CTUV2_0_FIFO_DMA_MUX_CHANNEL_BASE (uint8)8U
        #define CTUV2_1_FIFO_DMA_MUX_CHANNEL_BASE (uint8)29U
     #endif
  #endif /* CTU_MAX_HW_UNITS == 1 */
#endif /* (IPV_CTU == IPV_CTUV2_00_00_05_03) */

#if (IPV_CTU == IPV_CTUV2_00_00_05_05) || (IPV_CTU == IPV_CTUV2_00_00_05_06)
  #define CTUV2_FIFO_DMA_MUX_CHANNEL_BASE 10U
#endif

/* @violates @ref Reg_eSys_CTU2_h_REF_4 cast from integer type to a other type or a wider integer type */
#if (CTU_MAX_HW_UNITS == 1)
/**
* @brief         CTUv2 channels in the DMA_MUX source slot mapping table.
*/
  #define CTUV2_FIFO0_DMA_MUX_CHANNEL (CTUV2_FIFO_DMA_MUX_CHANNEL_BASE)
  #define CTUV2_FIFO1_DMA_MUX_CHANNEL (CTUV2_FIFO0_DMA_MUX_CHANNEL + 1)
  #define CTUV2_FIFO2_DMA_MUX_CHANNEL (CTUV2_FIFO0_DMA_MUX_CHANNEL + 2)
  #define CTUV2_FIFO3_DMA_MUX_CHANNEL (CTUV2_FIFO0_DMA_MUX_CHANNEL + 3)
#else
/**
* @brief         CTUv2 channels in the DMA_MUX source slot mapping table.
*/
  #define CTUV2_0_FIFO0_DMA_MUX_CHANNEL   (CTUV2_0_FIFO_DMA_MUX_CHANNEL_BASE)
  #define CTUV2_0_FIFO1_DMA_MUX_CHANNEL   (CTUV2_0_FIFO_DMA_MUX_CHANNEL_BASE + 1)
  #define CTUV2_0_FIFO2_DMA_MUX_CHANNEL   (CTUV2_0_FIFO_DMA_MUX_CHANNEL_BASE + 2)
  #define CTUV2_0_FIFO3_DMA_MUX_CHANNEL   (CTUV2_0_FIFO_DMA_MUX_CHANNEL_BASE + 3)
  #define CTUV2_1_FIFO0_DMA_MUX_CHANNEL   (CTUV2_1_FIFO_DMA_MUX_CHANNEL_BASE)
  #define CTUV2_1_FIFO1_DMA_MUX_CHANNEL   (CTUV2_1_FIFO_DMA_MUX_CHANNEL_BASE + 1)
  #define CTUV2_1_FIFO2_DMA_MUX_CHANNEL   (CTUV2_1_FIFO_DMA_MUX_CHANNEL_BASE + 2)
  #define CTUV2_1_FIFO3_DMA_MUX_CHANNEL   (CTUV2_1_FIFO_DMA_MUX_CHANNEL_BASE + 3)
#endif /* CTU_MAX_HW_UNITS == 1 */

/**
* @brief          All CTUV2 outputs active
*/
#define CTUV2_ALL_OUTPUTS_ACTIVE (0x3F3F3F3FUL)
/**
* @brief          All CTUV2 ADC outputs active
*/
#define CTUV2_ADC_OUTPUTS_ACTIVE (0x01010101UL)

/*==================================================================================================
*                                      DEFINES AND MACROS
==================================================================================================*/
/* @violates @ref Reg_eSys_CTU2_h_REF_4 cast from integer type to a other type or a wider integer type */
#if (CTU_MAX_HW_UNITS > 1)
/**
* @brief          Trigger Generator Sub-unit Registers.
*/
/**
* @brief Input Selection Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_TGISR(CTU_BASEADDR) (CTU_BASEADDR + (uint32)0x0UL)
/**
* @brief Control Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_TGSCR(CTU_BASEADDR) (CTU_BASEADDR + (uint32)0x4UL)
/**
* @brief Trigger 0 Compare Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_T0CR(CTU_BASEADDR) (CTU_BASEADDR + (uint32)0x6UL)
/**
* @brief Trigger 1 Compare Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_T1CR(CTU_BASEADDR) (CTU_BASEADDR + (uint32)0x8UL)
/**
* @brief Trigger 2 Compare Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_T2CR(CTU_BASEADDR) (CTU_BASEADDR + (uint32)0xAUL)
/**
* @brief Trigger 3 Compare Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_T3CR(CTU_BASEADDR) (CTU_BASEADDR + (uint32)0xCUL)
/**
* @brief Trigger 4 Compare Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_T4CR(CTU_BASEADDR) (CTU_BASEADDR + (uint32)0xEUL)
/**
* @brief Trigger 5 Compare Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_T5CR(CTU_BASEADDR) (CTU_BASEADDR + (uint32)0x10UL)
/**
* @brief Trigger 6 Compare Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_T6CR(CTU_BASEADDR) (CTU_BASEADDR + (uint32)0x12UL)
/**
* @brief Trigger 7 Compare Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_T7CR(CTU_BASEADDR) (CTU_BASEADDR + (uint32)0x14UL)
/**
* @brief Trigger x Compare Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_TxCR(CTU_BASEADDR, x) (CTUV2_T0CR(CTU_BASEADDR) + (uint32)((uint32)(x)<<1U))
/**
* @brief Counter Compare Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_TGSCCR(CTU_BASEADDR) (CTU_BASEADDR + (uint32)0x16UL)
/**
* @brief Counter Reload Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_TGSCRR(CTU_BASEADDR) (CTU_BASEADDR + (uint32)0x18UL)
#else
/**
* @brief          Trigger Generator Sub-unit Registers.
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
/**
* @brief Input Selection Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_TGISR (CTU_BASEADDR + (uint32)0x0UL)
/**
* @brief Control Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_TGSCR (CTU_BASEADDR + (uint32)0x4UL)
/**
* @brief Trigger 0 Compare Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_T0CR (CTU_BASEADDR + (uint32)0x6UL)
/**
* @brief Trigger 1 Compare Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_T1CR (CTU_BASEADDR + (uint32)0x8UL)
/**
* @brief Trigger 2 Compare Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_T2CR (CTU_BASEADDR + (uint32)0xAUL)
/**
* @brief Trigger 3 Compare Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
  #define CTUV2_T3CR (CTU_BASEADDR + (uint32)0xCUL)
/**
* @brief Trigger 4 Compare Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_T4CR (CTU_BASEADDR + (uint32)0xEUL)
/**
* @brief Trigger 5 Compare Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_T5CR (CTU_BASEADDR + (uint32)0x10UL)
/**
* @brief Trigger 6 Compare Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_T6CR (CTU_BASEADDR + (uint32)0x12UL)
/**
* @brief Trigger 7 Compare Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_T7CR (CTU_BASEADDR + (uint32)0x14UL)
/**
* @brief Trigger x Compare Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_TxCR(x) (CTUV2_T0CR + (uint32)((uint32)((x)<<1U)))
/**
@brief Counter Compare Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_TGSCCR (CTU_BASEADDR + (uint32)0x16UL)
/**
* @brief Counter Reload Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_TGSCRR (CTU_BASEADDR + (uint32)0x18UL)
#endif /* CTU_MAX_HW_UNITS > 1 */

/* @violates @ref Reg_eSys_CTU2_h_REF_4 cast from integer type to a other type or a wider integer type */
#if (CTU_MAX_HW_UNITS > 1)
/**
* @brief          Scheduler Sub-unit Registers.
*/

/**
* @brief Command List Control Register 1
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_CLCR1(CTU_BASEADDR) (CTU_BASEADDR + (uint32)0x1CUL)
/**
* @brief Command List Control Register 2
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_CLCR2(CTU_BASEADDR) (CTU_BASEADDR + (uint32)0x20UL)
/**
* @brief Trigger Handler Control Register 1
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_THCR1(CTU_BASEADDR) (CTU_BASEADDR + (uint32)0x24UL)
/**
* @brief Trigger Handler Control Register 2
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_THCR2(CTU_BASEADDR) (CTU_BASEADDR + (uint32)0x28UL)
/**
* @brief Commands List Register 0 
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_CLR0(CTU_BASEADDR) (CTU_BASEADDR + (uint32)0x2CUL)
/**
* @brief Commands List Register 23
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_CLR23(CTU_BASEADDR) (CTU_BASEADDR + (uint32)0x5AUL)
/**
* @brief Commands List Register x
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_CLRx(CTU_BASEADDR, x) (CTUV2_CLR0(CTU_BASEADDR) + ((uint32)(x)<<(uint32)1))
#else
/**
* @brief          Scheduler Sub-unit Registers.
*/

/**
* @brief Command List Control Register 1
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_CLCR1 (CTU_BASEADDR + (uint32)0x1CUL)
/**
* @brief Command List Control Register 2
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_CLCR2 (CTU_BASEADDR + (uint32)0x20UL)
/**
* @brief Trigger Handler Control Register 1
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_THCR1 (CTU_BASEADDR + (uint32)0x24UL)
/**
* @brief Trigger Handler Control Register 2
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_THCR2 (CTU_BASEADDR + (uint32)0x28UL)
/**
* @brief Commands List Register 0
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_CLR0 (CTU_BASEADDR + (uint32)0x2CUL)
/**
* @brief Commands List Register 23
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_CLR23 (CTU_BASEADDR + (uint32)0x5AUL)
/**
* @brief Commands List Register x
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_CLRx(x) (CTUV2_CLR0 + ((uint32)(x)<<(uint32)1))
#endif /* CTU_MAX_HW_UNITS > 1 */

/* @violates @ref Reg_eSys_CTU2_h_REF_4 cast from integer type to a other type or a wider integer type */
#if (CTU_MAX_HW_UNITS > 1)
/**
* @brief          CrossTrigger Unit (CTU) unit Registers.
*/

/**
* @brief Error Flag Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_CTUEFR(CTU_BASEADDR) (CTU_BASEADDR + (uint32)0xC0UL)
/**
* @brief Interrupt Flag Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_CTUIFR(CTU_BASEADDR) (CTU_BASEADDR + (uint32)0xC2UL)
/**
* @brief Interrupt Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_CTUIR(CTU_BASEADDR) (CTU_BASEADDR + (uint32)0xC4UL)
/**
* @brief Control ON-Time Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_COTR(CTU_BASEADDR) (CTU_BASEADDR + (uint32)0xC6UL)
/**
* @brief Control Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_CTUCR(CTU_BASEADDR) (CTU_BASEADDR + (uint32)0xC8UL)
/**
* @brief Digital Filter Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_CTUDF(CTU_BASEADDR) (CTU_BASEADDR + (uint32)0xCAUL)
/**
* @brief Power Control Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_CTUPCR(CTU_BASEADDR) (CTU_BASEADDR + (uint32)0xCCUL)
/**
* @brief Cross Triggering Unit Expected Value A Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTU_EXPECTED_A(CTU_BASEADDR) (CTU_BASEADDR + (uint32)0xCCUL)
/**
* @brief Cross Triggering Unit Expected Value B Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTU_EXPECTED_B(CTU_BASEADDR) (CTU_BASEADDR + (uint32)0xCEUL)
/**
* @brief Cross Triggering Unit Count Range Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTU_CNT_RANGE(CTU_BASEADDR) (CTU_BASEADDR + (uint32)0xD0UL)
#else
/**
* @brief          CrossTrigger Unit (CTU) unit Registers.
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/

/**
* @brief Error Flag Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_CTUEFR (CTU_BASEADDR + (uint32)0xC0UL)
/**
* @brief Interrupt Flag Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_CTUIFR (CTU_BASEADDR + (uint32)0xC2UL)
/**
* @brief Interrupt Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_CTUIR (CTU_BASEADDR + (uint32)0xC4UL)
/**
* @brief Control ON-Time Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_COTR (CTU_BASEADDR + (uint32)0xC6UL)
/**
* @brief Control Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_CTUCR (CTU_BASEADDR + (uint32)0xC8UL)
/**
* @brief Digital Filter Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_CTUDF (CTU_BASEADDR + (uint32)0xCAUL)
/**
* @brief Power Control Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_CTUPCR (CTU_BASEADDR + (uint32)0xCCUL)
#endif /* CTU_MAX_HW_UNITS > 1 */

/* @violates @ref Reg_eSys_CTU2_h_REF_4 cast from integer type to a other type or a wider integer type */
#if (CTU_MAX_HW_UNITS > 1)
/**
* @brief          Fifo's Registers.
*/

/**
* @brief Fifo DMA Control Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_FDCR(CTU_BASEADDR) (CTU_BASEADDR + (uint32)0x6C)
/**
* @brief Fifo Control Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_FCR(CTU_BASEADDR) (CTU_BASEADDR + (uint32)0x70)
/**
* @brief Fifo Threshold Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_FTH(CTU_BASEADDR) (CTU_BASEADDR + (uint32)0x74)
/**
* @brief Fifo Status Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_FST(CTU_BASEADDR) (CTU_BASEADDR + (uint32)0x7C)
/**
* @brief Fifo Right Aligned Data 0 Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_FR0(CTU_BASEADDR) (CTU_BASEADDR + (uint32)0x80)
/**
* @brief Fifo Right Aligned Data 1 Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_FR1(CTU_BASEADDR) (CTU_BASEADDR + (uint32)0x84)
/**
* @brief Fifo Right Aligned Data 2 Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_FR2(CTU_BASEADDR) (CTU_BASEADDR + (uint32)0x88)
/**
* @brief Fifo Right Aligned Data 3 Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_FR3(CTU_BASEADDR) (CTU_BASEADDR + (uint32)0x8C)
/**
* @brief Fifo Left Aligned Data 0 Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_FL0(CTU_BASEADDR) (CTU_BASEADDR + (uint32)0xA0)
/**
* @brief Fifo Left Aligned Data 1 Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_FL1(CTU_BASEADDR) (CTU_BASEADDR + (uint32)0xA4)
/**
* @brief Fifo Left Aligned Data 2 Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_FL2(CTU_BASEADDR) (CTU_BASEADDR + (uint32)0xA8)
/**
* @brief Fifo Left Aligned Data 3 Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_FL3(CTU_BASEADDR) (CTU_BASEADDR + (uint32)0xAC)

/**
* @brief Fifo Data Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/  
  #define CTUV2_FIFO_ADDR(CTU_BASEADDR, x) (CTUV2_FL0(CTU_BASEADDR) + (uint32)((uint32)(x)<<2) )


#else
/**
* @brief          Fifo's Registers.
*/

/**
* @brief Fifo DMA Control Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_FDCR (CTU_BASEADDR + (uint32)0x6C)
/**
* @brief Fifo Control Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_FCR (CTU_BASEADDR + (uint32)0x70)
/**
* @brief Fifo Threshold Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_FTH (CTU_BASEADDR + (uint32)0x74)
/**
* @brief Fifo Status Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_FST (CTU_BASEADDR + (uint32)0x7C)
/**
* @brief Fifo Right Aligned Data 0 Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_FR0 (CTU_BASEADDR + (uint32)0x80)
/**
* @brief Fifo Right Aligned Data 1 Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_FR1 (CTU_BASEADDR + (uint32)0x84)
/**
* @brief Fifo Right Aligned Data 2 Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_FR2 (CTU_BASEADDR + (uint32)0x88)
/**
* @brief Fifo Right Aligned Data 3 Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_FR3 (CTU_BASEADDR + (uint32)0x8C)
/**
* @brief Fifo Left Aligned Data 0 Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_FL0 (CTU_BASEADDR + (uint32)0xA0)
/**
* @brief Fifo Left Aligned Data 1 Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_FL1 (CTU_BASEADDR + (uint32)0xA4)
/**
* @brief Fifo Left Aligned Data 2 Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_FL2 (CTU_BASEADDR + (uint32)0xA8)
/**
* @brief Fifo Left Aligned Data 3 Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_FL3 (CTU_BASEADDR + (uint32)0xAC)
 
/**
* @brief Fifo Data Register
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/ 
  #define CTUV2_FIFO_ADDR(x) (CTUV2_FL0 + (uint32)((uint32)(x)<<2) )


#endif /* CTU_MAX_HW_UNITS > 1 */

/**
* @brief          TGISR Fields.
*/
#define CTUV2_TGISR_DISABLED 0x0U
#define CTUV2_TGISR_RISING_EDGE 0x1U
#define CTUV2_TGISR_FALLING_EDGE 0x2U
#define CTUV2_TGISR_BOTH_EDGES 0x3U

/* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined */
#define CTUV2_TGISER_DISABLED_INPUT(x)     ((uint32)((uint32)CTUV2_TGISR_DISABLED << (uint32)(2*(x))))
/* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined */
#define CTUV2_TGISER_RISING_EDGE_INPUT(x)  ((uint32)((uint32)CTUV2_TGISR_RISING_EDGE << (uint32)(2*(x))))
/* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined */
#define CTUV2_TGISER_FALLING_EDGE_INPUT(x) ((uint32)(CTUV2_TGISR_FALLING_EDGE << (uint32)(2*(x))))
/* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined */
#define CTUV2_TGISER_BOTH_EDGE_INPUT(x)    ((uint32)(CTUV2_TGISR_BOTH_EDGES << (uint32)(2*(x))))

/**
* @brief          TGSCR Fields.
*/
#define CTUV2_TGSCR_ET_TM_ENABLED 0x100U
#define CTUV2_TGSCR_ET_TM_DISABLED 0x0U
#define CTUV2_TGSCR_PRES_1 (0x0<<6)
#define CTUV2_TGSCR_PRES_2 (0x1<<6)
#define CTUV2_TGSCR_PRES_3 (0x2<<6)
#define CTUV2_TGSCR_PRES_4 (0x3<<6)
/* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined */
#define CTUV2_TGSCR_MRS_SM(x) ((x)<<1)
/* @violates @ref Reg_eSys_CTU2_h_REF_5 Identifiers shall not more than 31 characters */
#define CTUV2_TGSCR_TGS_M_TRIGGERED_MODE (0x0U)
/* @violates @ref Reg_eSys_CTU2_h_REF_5 Identifiers shall not more than 31 characters */
#define CTUV2_TGSCR_TGS_M_SEQUENTIAL_MODE (0x1U)

/**
* @brief          CLCR Fields.
*/
/* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined */
#define CTUV2_CLCR1_T0_INDEX(x) (x)
/* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined */
#define CTUV2_CLCR1_T1_INDEX(x) ((x)<<8)
/* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined */
#define CTUV2_CLCR1_T2_INDEX(x) ((x)<<16)
/* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined */
#define CTUV2_CLCR1_T3_INDEX(x) ((x)<<24)
/* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined */
#define CTUV2_CLCR2_T4_INDEX(x) (x)
/* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined */
#define CTUV2_CLCR2_T5_INDEX(x) ((x)<<8)
/* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined */
#define CTUV2_CLCR2_T6_INDEX(x) ((x)<<16)
/* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined */
#define CTUV2_CLCR2_T7_INDEX(x) ((x)<<24)

/**
* @brief          x=trigger number, y=composed field value from the above defines.
*/
/* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined */
#define CTUV2_CLCR1_TRIGGER(x, y) ((uint32)(((uint32)(y)) << (((uint32)(x))*8) ))
/* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined */
#define CTUV2_CLCR2_TRIGGER(x, y) ((uint32)(((uint32)(y)) << ((((uint32)(x))*8) -32) ))

#if (IPV_CTU == IPV_CTUV2_00_00_05_03) || (IPV_CTU == IPV_CTUV2_00_00_10_00)
/**
* @brief          THCR Fields.
*/
  #define CTUV2_TRIGGERx_E 0x40U
  #define CTUV2_TRIGGERx_ETE 0x20U
  #define CTUV2_TRIGGERx_T4E 0x10U
  #define CTUV2_TRIGGERx_T3E 0x8U
  #define CTUV2_TRIGGERx_T2E 0x4U
  #define CTUV2_TRIGGERx_T1E 0x2U
  #define CTUV2_TRIGGERx_ADCE 0x1U
#else
/**
* @brief          THCR Fields.
*/
  #define CTUV2_TRIGGERx_E 0x10U
  #define CTUV2_TRIGGERx_ETE 0x8U
  #define CTUV2_TRIGGERx_T1E 0x4U
  #define CTUV2_TRIGGERx_T0E 0x2U
  #define CTUV2_TRIGGERx_ADCE 0x1U
#endif

#if (IPV_CTU == IPV_CTUV2_00_00_05_03) /* LEOPARD */
/**
* @brief          Trigger n External Trigger and Trigger n Timer m output enable.
*/
/* @violates @ref Reg_eSys_CTU2_h_REF_5 Identifiers shall not more than 31 characters */
  #define CTUV2_TRIGGER_EXT_TIMER_OUTPUT_MASK (0x3EU)

/**
* @brief          Trigger n enable bit.
*/
/* @violates @ref Reg_eSys_CTU2_h_REF_5 Identifiers shall not more than 31 characters */
 #define CTUV2_TRIGGER_ENABLE_OUTPUT_MASK (0x40U)  
 
 /**
* @brief          Trigger Cmd list bit.
*/
/* @violates @ref Reg_eSys_CTU2_h_REF_5 Identifiers shall not more than 31 characters */
 #define CTUV2_TRIGGER_CMD_LIST_OUTPUT_MASK (0x1FU)
 
#endif /* #if (IPV_CTU == IPV_CTUV2_00_00_05_03) */
  
#if (IPV_CTU == IPV_CTUV2_00_00_05_05) || (IPV_CTU == IPV_CTUV2_00_00_05_06) /* PICTUS family */
/**
* @brief          Trigger n External Trigger and Trigger n Timer m output enable.
*/
/* @violates @ref Reg_eSys_CTU2_h_REF_5 Identifiers shall not more than 31 characters */
  #define CTUV2_TRIGGER_EXT_TIMER_OUTPUT_MASK (0xEU)
  /**
* @brief           Trigger n enable bit .
*/
/* @violates @ref Reg_eSys_CTU2_h_REF_5 Identifiers shall not more than 31 characters */
  #define CTUV2_TRIGGER_ENABLE_OUTPUT_MASK (0x10U)
  
  /**
* @brief          Trigger Cmd list bit.
*/
/* @violates @ref Reg_eSys_CTU2_h_REF_5 Identifiers shall not more than 31 characters */
 #define CTUV2_TRIGGER_CMD_LIST_OUTPUT_MASK (0x1FU)
#endif /* #if (IPV_CTU == IPV_CTUV2_00_00_05_05) || (IPV_CTU == IPV_CTUV2_00_00_05_06) */


/**
* @brief          x=trigger number, y=composed field value from the above defines.
*/
/* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined */
#define CTUV2_THCR1_TRIGGER(x, y) ((uint32)(((uint32)(y))<< (((uint32)(x))*8)))
/* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined */
#define CTUV2_THCR2_TRIGGER(x, y) ((uint32)(((uint32)(y)) << ((((uint32)(x))*8) -32) ))

/**
* @brief          CLR Fields.
*/
#define CTUV2_CLR_CIR_ENABLE (0x8000U)
#define CTUV2_CLR_CIR_DISABLE (0x0U)
#define CTUV2_CLR_LC_LAST (0x4000U)
#define CTUV2_CLR_LC_NOT_LAST (0x0U)
#define CTUV2_CLR_FC_FIRST (0x4000U)
#define CTUV2_CLR_FC_NOT_FIRST (0x0U)
#define CTUV2_CLR_CMS_DUAL (0x2000U)
#define CTUV2_CLR_CMS_SINGLE (0x0U)
/* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined */
#define CTUV2_CLR_FIFO(x) ((uint16)((uint16)(x)<<(uint16)10))
#define CTUV2_SU_ADC_B (0x0020U)
#define CTUV2_SU_ADC_A (0x0U)
#define CTUV2_SU_ADC_1 (CTUV2_SU_ADC_B)
#define CTUV2_SU_ADC_0 (CTUV2_SU_ADC_A)
#define CTUV2_SU_ADC_3 (CTUV2_SU_ADC_B)
#define CTUV2_SU_ADC_2 (CTUV2_SU_ADC_A)
/* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined */
#define CTUV2_SU_CH(x) ((uint16)(x))
/* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined */
#define CTUV2_SU_CH_A(x) (CTUV2_SU_CH(x))
/* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined */
#define CTUV2_SU_CH_B(x) ((uint16)((uint16)(x)<<(uint16)5))

#if (IPV_CTU == IPV_CTUV2_00_00_05_03) || (IPV_CTU == IPV_CTUV2_00_00_10_00)
/**
* @brief          CTUEFR Fields.
*/
  #define CTUV2_CTUEFR_ET_OE (0x0800U)
  #define CTUV2_CTUEFR_ERR_CMP (0x0400U)
  /* @violates @ref Reg_eSys_CTU2_h_REF_4 cast from integer type to a other type or a wider integer type */
  #if (CTU_MAX_HW_UNITS == 1)
    #define CTUV2_CTUEFR_T4_OE (0x0200U)
  #endif /* CTU_MAX_HW_UNITS == 1 */
  /* For Komodo 2M the T0_OE, T1_OE and T2_OE here are called: T1_OE, T2_OE and T3_OE*/

  #define CTUV2_CTUEFR_T3_OE (0x0100U)
  #define CTUV2_CTUEFR_T2_OE (0x0080U)
  #define CTUV2_CTUEFR_T1_OE (0x0040U)
  #define CTUV2_CTUEFR_ADC_OE (0x0020U)
  #define CTUV2_CTUEFR_TGS_OSM (0x0010U)
  #define CTUV2_CTUEFR_MRS_O (0x0008U)
  #define CTUV2_CTUEFR_ICE (0x0004U)
  #define CTUV2_CTUEFR_SM_TO (0x0002U)
  #define CTUV2_CTUEFR_MRS_RE (0x0001U)
  /* @violates @ref Reg_eSys_CTU2_h_REF_4 cast from integer type to a other type or a wider integer type */
  #if (CTU_MAX_HW_UNITS > 1)
    #define CTUV2_CTUEFR_CS (0x1000U)
  #endif /* CTU_MAX_HW_UNITS > 1 */
#else
/**
* @brief          CTUEFR Fields.
*/
  #define CTUV2_CTUEFR_CS (0x1000U)
  #define CTUV2_CTUEFR_ET_OE (0x0100U)
  #define CTUV2_CTUEFR_T1_OE (0x0080U)
  #define CTUV2_CTUEFR_T0_OE (0x0040U)
  #define CTUV2_CTUEFR_ADC_OE (0x0020U)
  #define CTUV2_CTUEFR_TGS_OSM (0x0010U)
  #define CTUV2_CTUEFR_MRS_O (0x0008U)
  #define CTUV2_CTUEFR_ICE (0x0004U)
  #define CTUV2_CTUEFR_SM_TO (0x0002U)
  #define CTUV2_CTUEFR_MRS_RE (0x0001U)
#endif

/* @violates @ref Reg_eSys_CTU2_h_REF_4 cast from integer type to a other type or a wider integer type */
#if (CTU_MAX_HW_UNITS > 1)
/**
* @brief          CTUIFR Fields.
*/
  #define CTUV2_CTUIFR_ERR_B (0x0800U)
  #define CTUV2_CTUIFR_ERR_A (0x0400U)
#endif /* CTU_MAX_HW_UNITS > 1 */
#define CTUV2_CTUIFR_ADC_I (0x0200U)
#define CTUV2_CTUIFR_T7_I (0x0100U)
#define CTUV2_CTUIFR_T6_I (0x0080U)
#define CTUV2_CTUIFR_T5_I (0x0040U)
#define CTUV2_CTUIFR_T4_I (0x0020U)
#define CTUV2_CTUIFR_T3_I (0x0010U)
#define CTUV2_CTUIFR_T2_I (0x0008U)
#define CTUV2_CTUIFR_T1_I (0x0004U)
#define CTUV2_CTUIFR_T0_I (0x0002U)
#define CTUV2_CTUIFR_MRS_I (0x0001U)

/**
* @brief          CTUIR Fields.
*/
#define CTUV2_CTUIR_T7_IE (0x8000U)
#define CTUV2_CTUIR_T6_IE (0x4000U)
#define CTUV2_CTUIR_T5_IE (0x2000U)
#define CTUV2_CTUIR_T4_IE (0x1000U)
#define CTUV2_CTUIR_T3_IE (0x0800U)
#define CTUV2_CTUIR_T2_IE (0x0400U)
#define CTUV2_CTUIR_T1_IE (0x0200U)
#define CTUV2_CTUIR_T0_IE (0x0100U)
/* @violates @ref Reg_eSys_CTU2_h_REF_4 cast from integer type to a other type or a wider integer type */
#if (CTU_MAX_HW_UNITS > 1)
  #define CTUV2_CTUIR_CNT_B_E (0x0020U)
  #define CTUV2_CTUIR_CNT_A_E (0x0010U)
  #define CTUV2_CTUIR_DMAE (0x0008U)
#endif /* CTU_MAX_HW_UNITS > 1 */
#define CTUV2_CTUIR_MRS_DMAE (0x0004U)
#define CTUV2_CTUIR_MRS_IE (0x0002U)
#define CTUV2_CTUIR_IEE (0x0001U)

/**
* @brief          CTUCR Fields.
*/
#define CTUV2_CTUCR_T7_SG (0x8000U)
#define CTUV2_CTUCR_T6_SG (0x4000U)
#define CTUV2_CTUCR_T5_SG (0x2000U)
#define CTUV2_CTUCR_T4_SG (0x1000U)
#define CTUV2_CTUCR_T3_SG (0x0800U)
#define CTUV2_CTUCR_T2_SG (0x0400U)
#define CTUV2_CTUCR_T1_SG (0x0200U)
#define CTUV2_CTUCR_T0_SG (0x0100U)
#define CTUV2_CTUCR_CRU_ADC_R (0x0080U)
#define CTUV2_CTUCR_CTU_ODIS (0x0040U)
#define CTUV2_CTUCR_FE (0x0020U)
#define CTUV2_CTUCR_CGRE (0x0010U)
#define CTUV2_CTUCR_FGRE (0x0008U)
#define CTUV2_CTUCR_MRS_SG (0x0004U)
#define CTUV2_CTUCR_GRE (0x0002U)
#define CTUV2_CTUCR_TGSISR_RE (0x0001U)

/**
* @brief          CTUPCR Fields.
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_CTUPCR_MDIS_ENABLED (0x0000U)
#define CTUV2_CTUPCR_MDIS_DISABLED (0x0001U)

/**
* @brief          FDCR Fields.
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_FDCR_DMA_ENABLE(x) (1<<(x))

/**
* @brief          FCR Fields.
*/
#define CTUV2_FCR_OR_EN3 (0x8000U)
#define CTUV2_FCR_OF_EN3 (0x4000U)
#define CTUV2_FCR_EMPTY_EN3 (0x2000U)
#define CTUV2_FCR_FULL_EN3 (0x1000U)
#define CTUV2_FCR_OR_EN2 (0x0800U)
#define CTUV2_FCR_OF_EN2 (0x0400U)
#define CTUV2_FCR_EMPTY_EN2 (0x0200U)
#define CTUV2_FCR_FULL_EN2 (0x0100U)
#define CTUV2_FCR_OR_EN1 (0x0080U)
#define CTUV2_FCR_OF_EN1 (0x0040U)
#define CTUV2_FCR_EMPTY_EN1 (0x0020U)
#define CTUV2_FCR_FULL_EN1 (0x0010U)
#define CTUV2_FCR_OR_EN0 (0x0008U)
#define CTUV2_FCR_OF_EN0 (0x0004U)
#define CTUV2_FCR_EMPTY_EN0 (0x0002U)
#define CTUV2_FCR_FULL_EN0 (0x0001U)

/**
* @brief          FTH Fields.
* details         x=fifo number, y=threshold value
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_FTH_THRESHOLDS(x, y) ((y)<<(8*(x)))

/**
* @brief          FST Fields.
*/
#define CTUV2_FST_OR3 (0x8000U)
#define CTUV2_FST_OF3 (0x4000U)
#define CTUV2_FST_EMP3 (0x2000U)
#define CTUV2_FST_FULL3 (0x1000U)
#define CTUV2_FST_OR2 (0x0800U)
#define CTUV2_FST_OF2 (0x0400U)
#define CTUV2_FST_EMP2 (0x0200U)
#define CTUV2_FST_FULL2 (0x0100U)
#define CTUV2_FST_OR1 (0x0080U)
#define CTUV2_FST_OF1 (0x0040U)
#define CTUV2_FST_EMP1 (0x0020U)
#define CTUV2_FST_FULL1 (0x0010U)
#define CTUV2_FST_OR0 (0x0008U)
#define CTUV2_FST_OF0 (0x0004U)
#define CTUV2_FST_EMP0 (0x0002U)
#define CTUV2_FST_FULL0 (0x0001U)

/**
* @brief          FRx Fields - fifo right aligned data.
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_FRx_N_CH(x) ((x)<<16)
/* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined */
#define CTUV2_FRx_DATA(x) (x)

/**
* @brief          FLx Fields - fifo signed left aligned data.
*
* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined
*/
#define CTUV2_FLx_N_CH(x) ((x)<<16)
#if (IPV_CTU == IPV_CTUV2_00_00_05_03) || (IPV_CTU == IPV_CTUV2_00_00_10_00)
/* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined */
  #define CTUV2_FLx_DATA(x) ((x)<<3)
#else
/* @violates @ref Reg_eSys_CTU2_h_REF_3 Function-like macro defined */
  #define CTUV2_FLx_DATA(x) ((x)<<5)
#endif /* (IPV_CTU == IPV_CTUV2_00_00_05_03) */


/*==================================================================================================
*                                             ENUMS
==================================================================================================*/

/*==================================================================================================
*                                STRUCTURES AND OTHER TYPEDEFS
==================================================================================================*/

/*==================================================================================================
*                                GLOBAL VARIABLE DECLARATIONS
==================================================================================================*/

/*==================================================================================================
*                                    FUNCTION PROTOTYPES
==================================================================================================*/

#ifdef __cplusplus
}
#endif

#endif /* REG_ESYS_CTU2_H */
