/*
  ******************************************************************************
  * @attention
*
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
*
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
*
  *        http://goo.gl/28pHKW
*
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
* @file        Reg_eSys.h
*   @version BETA 0.9.1
*   @brief   This file contains the AdBIP Memory and DMA mapping
*          
*   @details Memory mapping of the IP modules and DMA channels present on the XPC560XP hardware platform.
*
*/

/*
* @page misra_violations MISRA-C:2004 violations
* 
* @section Reg_eSys_h_REF_1
* Violates MISRA 2004 Required Rule 19.15, Repeated include file 
* This comes from the order of includes in the .c file and from include dependencies. As a safe 
* approach, any file must include all its dependencies. Header files are already protected against 
* double inclusions.
*
* @section Reg_eSys_h_REF_2
* Violates MISRA 2004 Required Rule 20.2, The names of standard library macros, objects and
* functions shall not be reused. Symbol required to be defined when one compiler is used.
* Requested by AutoSAR (Req. COMPILER010).
*/

#ifndef REG_ESYS_H
#define REG_ESYS_H

#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
*                                         INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/
/** 
* @file        Reg_eSys.h
* @brief Include platform types
* @violates @ref Reg_eSys_h_REF_1 MISRA 2004 Required Rule 19.15, Repeated include file
*/
#include "Platform_Types.h"

/** 
* @file        Reg_eSys.h
* @brief Include IP module versions
*/
#include "Soc_Ips.h"

/*==================================================================================================
*                               SOURCE FILE VERSION INFORMATION
==================================================================================================*/

/*==================================================================================================
*                                      FILE VERSION CHECKS
==================================================================================================*/

/*==================================================================================================
*                                          CONSTANTS
==================================================================================================*/

/*==================================================================================================
*                                       DEFINES AND MACROS
==================================================================================================*/
/**
* @brief  Internal Flash  
* @details XPC56XXL System Memory Map
* @implements DBASE10001
*/
#define INTERNAL_FLASH_BASEADDR             ((uint32)0x00000000UL)  
/**
* @brief  Internal RAM  
* @details XPC56XXL System Memory Map
* @implements DBASE10001
*/
#define INTERNAL_RAM_BASEADDR               ((uint32)0x40000000UL)  

/**
* @brief  AIPS_1  
* @details XPC56XXL System Memory Map
* @implements DBASE10001
*/
#define AIPS_1_BASEADDR                     ((uint32)0x8FF00000UL)  
/**
* @brief  XBAR_1  
* @details XPC56XXL System Memory Map
* @implements DBASE10001
*/
#define XBAR_1_BASEADDR                     ((uint32)0x8FF04000UL)  
/**
* @brief  Memory protection unit (MPU_1)  
* @details XPC56XXL System Memory Map
* @implements DBASE10001
*/
#define MPU_1_BASEADDR                      ((uint32)0x8FF10000UL)  
/**
* @brief  Semaphores (SEMA4_1)  
* @details XPC56XXL System Memory Map
* @implements DBASE10001
*/
#define SEMA4_1_BASEADDR                    ((uint32)0x8FF24000UL)  
/**
* @brief  Software watchdog timer (SWT_1)  
* @details XPC56XXL System Memory Map
* @implements DBASE10001
*/
#define SWT_1_BASEADDR                      ((uint32)0x8FF38000UL)  
/**
* @brief  System timer module (STM_1)  
* @details XPC56XXL System Memory Map
* @implements DBASE10001
*/
#define STM_1_BASEADDR                      ((uint32)0x8FF3C000UL)  
/**
* @brief  Error correction status module (ECSM_1)  
* @details XPC56XXL System Memory Map
* @implements DBASE10001
*/
/*
* @violates @ref Reg_eSys_h_REF_2 The names of standard library
* macros, objects and functions shall not be reused.
*/
#define ECSM_1_BASEADDR                     ((uint32)0x8FF40000UL)  
/**
* @brief  Enhanced direct memory access controller (eDMA_1)  
* @details XPC56XXL System Memory Map
* @implements DBASE10001
*/
#define DMA_1_BASEADDR                      ((uint32)0x8FF44000UL)  
/**
* @brief  Interrupt controller (INTC_1)  
* @details XPC56XXL System Memory Map
* @implements DBASE10001
*/
#define INTC_1_BASEADDR                     ((uint32)0x8FF48000UL)  

/**
* @brief  FLS_ARRAY_0  
* @details XPC56XXL System Memory Map
* @implements DBASE10001
*/
#define FLASHMEM0_ARRAY_0_BASEADDR          ((uint32)0xC3F88000UL)  
    
/**
* @brief  System Integration Unit Lite (SIUL)  
* @details XPC56XXL System Memory Map
* @implements DBASE10001
*/
#define SIUL_BASEADDR                       ((uint32)0xC3F90000UL)  
/**
* @brief  WakeUp Unit  
* @details XPC56XXL System Memory Map
* @implements DBASE10001
*/ 
#define WKPU_BASEADDR                       ((uint32)0xC3F94000UL)  
/**
* @brief  System Status and Configuration Module (SSCM)  
* @details XPC56XXL System Memory Map
* @implements DBASE10001
*/
#define SSCM_BASEADDR                       ((uint32)0xC3FD8000UL)  
/**
* @brief  Mode Entry Module (MC_ME)  
* @details XPC56XXL System Memory Map
* @implements DBASE10001
*/
#define MC_ME_BASEADDR                      ((uint32)0xC3FDC000UL)  
/**
* @brief  Clock Generation Module (MC_CGM)  
* @details XPC56XXL System Memory Map
* @implements DBASE10001
*/
#define MC_CGM_BASEADDR                     ((uint32)0xC3FE0000UL)  
/**
* @brief  Reset Generation Module (MC_RGM)  
* @details XPC56XXL System Memory Map
* @implements DBASE10001
*/
#define MC_RGM_BASEADDR                     ((uint32)0xC3FE4000UL)  
/**
* @brief  Power Control Unit (MC_PCU)  
* @details XPC56XXL System Memory Map
* @implements DBASE10001
*/
#define MC_PCU_BASEADDR                     ((uint32)0xC3FE8000UL)  
/**
* @brief  Periodic Interrupt Timer (PIT)  
* @details XPC56XXL System Memory Map
* @implements DBASE10001
*/
#define PIT_BASEADDR                        ((uint32)0xC3FF0000UL)  
/**
* @brief  Self-test control unit (STCU)  
* @details XPC56XXL System Memory Map
* @implements DBASE10001
*/
#define STCU_BASEADDR                       ((uint32)0xC3FF4000UL)  
            
/**
* @brief  Analog to digital converter 0 (ADC 0)  
* @details XPC56XXL System Memory Map
* @implements DBASE10001
*/
#define ADC0_BASEADDR                       ((uint32)0xFFE00000UL)  
/**
* @brief  Analog to digital converter 1 (ADC 1)  
* @details XPC56XXL System Memory Map
* @implements DBASE10001
*/
#define ADC1_BASEADDR                       ((uint32)0xFFE04000UL)  
/**
* @brief  Cross triggering unit (CTU)  
* @details XPC56XXL System Memory Map
* @implements DBASE10001
*/
#define CTU_BASEADDR                        ((uint32)0xFFE0C000UL)  
/**
* @brief  eTimer 0  
* @details XPC56XXL System Memory Map
* @implements DBASE10001
*/
/*
* @violates @ref Reg_eSys_h_REF_2 The names of standard library
* macros, objects and functions shall not be reused.
*/
#define ETIMER_0_BASEADDR                   ((uint32)0xFFE18000UL)  
/**
* @brief  eTimer 1  
* @details XPC56XXL System Memory Map
* @implements DBASE10001
*/
/*
* @violates @ref Reg_eSys_h_REF_2 The names of standard library
* macros, objects and functions shall not be reused.
*/
#define ETIMER_1_BASEADDR                   ((uint32)0xFFE1C000UL)  
/**
* @brief  eTimer 2  
* @details XPC56XXL System Memory Map
* @implements DBASE10001
*/
/*
* @violates @ref Reg_eSys_h_REF_2 The names of standard library
* macros, objects and functions shall not be reused.
*/
#define ETIMER_2_BASEADDR                   ((uint32)0xFFE20000UL)  
/**
* @brief  FlexPWM 0  
* @details XPC56XXL System Memory Map
* @implements DBASE10001
*/
#define FLEXPWM_0_BASEADDR                  ((uint32)0xFFE24000UL)  
/**
* @brief  FlexPWM 1  
* @details XPC56XXL System Memory Map
* @implements DBASE10001
*/
#define FLEXPWM_1_BASEADDR                  ((uint32)0xFFE28000UL)  
/**
* @brief  LINFlex 0  
* @details XPC56XXL System Memory Map
* @implements DBASE10001
*/
#define LINFLEX0_BASEADDR                   ((uint32)0xFFE40000UL)  
/**
* @brief  LINFlex 1  
* @details XPC56XXL System Memory Map
* @implements DBASE10001
*/
#define LINFLEX1_BASEADDR                   ((uint32)0xFFE44000UL)  
/**
* @brief  Cyclic redundancy check unit (CRC)  
* @details XPC56XXL System Memory Map
* @implements DBASE10001
*/
#define CRC_BASEADDR                        ((uint32)0xFFE68000UL)  
/**
* @brief  Fault collection and control unit (FCCU)  
* @details XPC56XXL System Memory Map
* @implements DBASE10001
*/
#define FCCU_BASEADDR                       ((uint32)0xFFE6C000UL)  
/**
* @brief  Sine wave generator (SWG)  
* @details XPC56XXL System Memory Map
* @implements DBASE10001
*/
#define SWG_BASEADDR                        ((uint32)0xFFE78000UL)  
/**
* @brief  Platform FLASH Controller  
* @details XPC56XXL System Memory Map
* @implements DBASE10001
*/
#define FLASHMEM0_PFC0_BASEADDR             ((uint32)0xFFE88000UL)
/**
* @brief  AIPS_0, AIPS_1  
* @details XPC56XXL System Memory Map
* @implements DBASE10001
*/
#define AIPS_BASEADDR                       ((uint32)0xFFF00000UL)  
/**
* @brief  XBAR_0, XBAR_1  
* @details XPC56XXL System Memory Map
* @implements DBASE10001
*/
#define XBAR_BASEADDR                       ((uint32)0xFFF04000UL)  
/**
* @brief  MPU  
* @details XPC56XXL System Memory Map
* @implements DBASE10001
*/
#define MPU_BASEADDR                        ((uint32)0xFFF10000UL)  
/**
* @brief  Semaphores (SEMA4_0)  
* @details XPC56XXL System Memory Map
* @implements DBASE10001
*/
#define SEMA4_BASEADDR                      ((uint32)0xFFF24000UL)  
/**
* @brief  SWT_0, SWT_1  
* @details XPC56XXL System Memory Map
* @implements DBASE10001
*/
#define SWT0_BASEADDR                        ((uint32)0xFFF38000UL)  
/**
* @brief  STM_0, STM_1  
* @details XPC56XXL System Memory Map
* @implements DBASE10001
*/
#define STM_0_BASEADDR                        ((uint32)0xFFF3C000UL)  
/**
* @brief  ECSM_0, ECSM_1  
* @details XPC56XXL System Memory Map
* @implements DBASE10001
*/
/*
* @violates @ref Reg_eSys_h_REF_2 The names of standard library
* macros, objects and functions shall not be reused.
*/
#define ECSM_BASEADDR                       ((uint32)0xFFF40000UL)  
/**
* @brief  DMA_0, DMA_1  
* @details XPC56XXL System Memory Map
* @implements DBASE10001
*/
#define DMA_BASEADDR                        ((uint32)0xFFF44000UL)  
/**
* @brief  INTC_0, INTC_1  
* @details XPC56XXL System Memory Map
* @implements DBASE10001
*/
#define INTC_BASEADDR                       ((uint32)0xFFF48000UL)  
/**
* @brief  DSPI 0  
* @details XPC56XXL System Memory Map
* @implements DBASE10001
*/
#define DSPI0_BASEADDR                      ((uint32)0xFFF90000UL)  
/**
* @brief  DSPI 1  
* @details XPC56XXL System Memory Map
* @implements DBASE10001
*/
#define DSPI1_BASEADDR                      ((uint32)0xFFF94000UL)  
/**
* @brief  DSPI 2  
* @details XPC56XXL System Memory Map
* @implements DBASE10001
*/
#define DSPI2_BASEADDR                      ((uint32)0xFFF98000UL)  
/**
* @brief  FlexCan 0 (CAN0)  
* @details XPC56XXL System Memory Map
* @implements DBASE10001
*/
#define FLEXCAN0_BASEADDR                   ((uint32)0xFFFC0000UL)  
/**
* @brief  FlexCan 1 (CAN1)  
* @details XPC56XXL System Memory Map
* @implements DBASE10001
*/
#define FLEXCAN1_BASEADDR                   ((uint32)0xFFFC4000UL)  
/**
* @brief  DMA Channel Multiplexer  
* @details XPC56XXL System Memory Map
* @implements DBASE10001
*/
#define DMAMUX_BASEADDR                     ((uint32)0xFFFDC000UL)  
/**
* @brief  FlexRay controller (FlexRay)  
* @details XPC56XXL System Memory Map
* @implements DBASE10001
*/
#define FLEXRAY_BASEADDR                    ((uint32)0xFFFE0000UL)  
/**
* @brief  Boot Assist Module (BAM)  
* @details XPC56XXL System Memory Map
* @implements DBASE10001
*/
#define BAM_BASEADDR                        ((uint32)0xFFFFC000UL)      

/**
* @brief INTC OFFSET address for exit interrupt
*/
#define INTC_EOIR_OFFSET                    ((uint32)0x00000018UL)

/*==================================================================================================
*                                             ENUMS
==================================================================================================*/

/*==================================================================================================
*                                 STRUCTURES AND OTHER TYPEDEFS
==================================================================================================*/

/*==================================================================================================
*                                 GLOBAL VARIABLE DECLARATIONS
==================================================================================================*/

/*==================================================================================================
*                                     FUNCTION PROTOTYPES
==================================================================================================*/

#ifdef __cplusplus
}
#endif

#endif /* #ifndef REG_ESYS_H*/
