/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    Reg_eSys_eTimer.c
*   @version BETA 0.9.1
*   @brief   This file contains the Pwm - ETIMER driver source file
*          
*   @details ETIMER global variables.
*
*/
    
#ifdef __cplusplus
extern "C"{
#endif

/*
* @page misra_violations MISRA-C:2004 violations
*
* @section REG_ESYS_eTIMER_C_REF_1
* Violates MISRA 2004 Required Rule 19.15, Precautions shall be taken in order to
* prevent the contents of a header file being included twice
* This is not a violation since all header files are protected against multiple inclusions
*
* @section REG_ESYS_eTIMER_C_REF_2
* Violates MISRA 2004 Advisory Rule 19.1, only preprocessor statements and comments
* before "#include". This violation  is not  fixed since  the inclusion  of MemMap.h
* is as  per Autosar  requirement MEMMAP003.
*
* @section REG_ESYS_eTIMER_C_REF_3
* Violates MISRA 2004 Required Rule 5.1, Identifiers (internal and external) shall not rely on the significance
* of more than 31 characters.
* The long identifiers are maintained for better readability.
*
* @section REG_ESYS_eTIMER_C_REF_4
* Violates MISRA 2004 Required Rule 19.4, C macros shall only expand to a braced initialiser, a constant, a
* parenthesised expression, a type qualifier, a storage class specifier, or a do-while-zero construct
*
* @section REG_ESYS_eTIMER_C_REF_5
* Violates MISRA 2004 Required Rule 20.2, The names of standard library macros, objects and functions shall not be reused
* This violation is due to the use of "_" which is required as per Autosar
*/

/*==================================================================================================
*                                        INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/
/*
* @file           Reg_eSys_eTimer.c
*/
/* @implements     DICU03701 */
#include "Std_Types.h"
#include "modules.h"
#include "Reg_eSys.h"
#include "Reg_eSys_eTimer.h"


/*==================================================================================================
*                              SOURCE FILE VERSION INFORMATION
==================================================================================================*/
/* @implements     DICU03511, DICU03512, DICU03513, DICU03514, DICU03515, DICU03516, DICU03517  */
/*
* @violates @ref REG_ESYS_eTIMER_C_REF_5 The names of standard library macros, objects and functions shall not be reused.
* This violation is due to the use of "_" which is required as per Autosar.
*/
#define ETIMER_COMMON_MODULE_ID                           100
#define REG_ESYS_ETIMER_C_VENDOR_ID                       43
/* @violates @ref REG_ESYS_eTIMER_C_REF_3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define REG_ESYS_ETIMER_C_AR_RELEASE_MAJOR_VERSION        4
/* @violates @ref REG_ESYS_eTIMER_C_REF_3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define REG_ESYS_ETIMER_C_AR_RELEASE_MINOR_VERSION        0
/* @violates @ref REG_ESYS_eTIMER_C_REF_3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define REG_ESYS_ETIMER_C_AR_RELEASE_REVISION_VERSION     3
/* @violates @ref REG_ESYS_eTIMER_C_REF_3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define REG_ESYS_ETIMER_C_SW_MAJOR_VERSION                1
/* @violates @ref REG_ESYS_eTIMER_C_REF_3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define REG_ESYS_ETIMER_C_SW_MINOR_VERSION                0
/* @violates @ref REG_ESYS_eTIMER_C_REF_3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define REG_ESYS_ETIMER_C_SW_PATCH_VERSION                1

/*==================================================================================================
*                                     FILE VERSION CHECKS
==================================================================================================*/
#ifndef DISABLE_MCAL_INTERMODULE_ASR_CHECK
    /* Check if source file and Std_Types.h file are of the same Autosar version */
    #if ((REG_ESYS_ETIMER_C_AR_RELEASE_MAJOR_VERSION != STD_TYPES_AR_RELEASE_MAJOR_VERSION) || \
         (REG_ESYS_ETIMER_C_AR_RELEASE_MINOR_VERSION != STD_TYPES_AR_RELEASE_MINOR_VERSION))
        #error "AutoSar Version Numbers of Reg_eSys_eTimer.c and Std_Types.h are different"
    #endif

    /* Check if source file and header file are of the same Autosar version */
    #if ((REG_ESYS_ETIMER_C_AR_RELEASE_MAJOR_VERSION != MODULES_AR_RELEASE_MAJOR_VERSION_H) || \
         (REG_ESYS_ETIMER_C_AR_RELEASE_MINOR_VERSION != MODULES_AR_RELEASE_MINOR_VERSION_H))
        #error "AutoSar Version Numbers of Reg_eSys_eTimer.c and modules.h are different"
    #endif
#endif

/* Check if source file and Reg_eSys_eTimer.h are of the same vendor */
#if (REG_ESYS_ETIMER_C_VENDOR_ID != REG_ESYS_ETIMER_H_VENDOR_ID)
    #error "Reg_eSys_eTimer.c and Reg_eSys_eTimer.h have different vendor ids"
#endif
/* Check if source file and header file are of the same Autosar version */
#if ((REG_ESYS_ETIMER_C_AR_RELEASE_MAJOR_VERSION != REG_ESYS_ETIMER_H_AR_RELEASE_MAJOR_VERSION) || \
     (REG_ESYS_ETIMER_C_AR_RELEASE_MINOR_VERSION != REG_ESYS_ETIMER_H_AR_RELEASE_MINOR_VERSION) || \
     (REG_ESYS_ETIMER_C_AR_RELEASE_REVISION_VERSION != \
                                                     REG_ESYS_ETIMER_H_AR_RELEASE_REVISION_VERSION))
    #error "AutoSar Version Numbers of Reg_eSys_eTimer.c and Reg_eSys_eTimer.h are different"
#endif
/* Check if source file and header file are of the same Software version */
#if ((REG_ESYS_ETIMER_C_SW_MAJOR_VERSION != REG_ESYS_ETIMER_H_SW_MAJOR_VERSION)  || \
     (REG_ESYS_ETIMER_C_SW_MINOR_VERSION != REG_ESYS_ETIMER_H_SW_MINOR_VERSION)  || \
     (REG_ESYS_ETIMER_C_SW_PATCH_VERSION != REG_ESYS_ETIMER_H_SW_PATCH_VERSION))
    #error "Software Version Numbers of Reg_eSys_eTimer.c and Reg_eSys_eTimer.h are different"
#endif

/*==================================================================================================
*                          LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
==================================================================================================*/


/*==================================================================================================
*                                       LOCAL MACROS
==================================================================================================*/


/*==================================================================================================
*                                      LOCAL CONSTANTS
==================================================================================================*/


/*==================================================================================================
*                                      LOCAL VARIABLES
==================================================================================================*/


/*==================================================================================================
*                                      GLOBAL CONSTANTS
==================================================================================================*/


/*==================================================================================================
*                                      GLOBAL VARIABLES
==================================================================================================*/
/**
* @brief base address array for ETIMER
*/
CONST(uint32, ETIMER_CONST) ETIMER_BASE_ADDR[] = {
#ifdef ETIMER_0_BASEADDR
    ETIMER_0_BASEADDR
#endif
#ifdef ETIMER_1_BASEADDR
    ,ETIMER_1_BASEADDR
#endif
#ifdef ETIMER_2_BASEADDR
    ,ETIMER_2_BASEADDR
#endif
};

/*==================================================================================================
*                                  LOCAL FUNCTION PROTOTYPES
==================================================================================================*/

/*==================================================================================================
*                                      LOCAL FUNCTIONS
==================================================================================================*/

/*==================================================================================================
*                                      GLOBAL FUNCTIONS
==================================================================================================*/

#ifdef __cplusplus
}
#endif
