/*
  ******************************************************************************
  * @attention
*
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
*
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
* @file        Soc_Ips.h
*   @version BETA 0.9.1
*   @brief   This file contains the IP modules versions used on PA hardware platform and
*            IP specific definest
*          
*   @details This file contains the IP modules versions used on PA hardware platform and
*            IP specific definest.
*
*/

/*
* @page misra_violations MISRA-C:2004 violations
* 
* @section Soc_Ips_h_REF_1
* Violates MISRA 2004 Required Rule 19.15, Repeated include file 
* This comes from the order of includes in the .c file and from include dependencies. As a safe 
* approach, any file must include all its dependencies. Header files are already protected against 
* double inclusions.
*
* @section Soc_Ips_h_REF_2
* Violates MISRA 2004 Required Rule 20.2, The names of standard library macros, objects and
* functions shall not be reused. Symbol required to be defined when one compiler is used.
* Requested by AutoSAR (Req. COMPILER010).
*
*/

#ifndef SOC_IPS_H
#define SOC_IPS_H

#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
*                                         INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/
/** 
* @file        Soc_Ips.h
* @brief Include platform types
* @violates @ref Soc_Ips_h_REF_1 MISRA 2004 Required Rule 19.15, Repeated include file
*/
#include "Platform_Types.h"

/*==================================================================================================
*                               SOURCE FILE VERSION INFORMATION
==================================================================================================*/

/*==================================================================================================
*                                      FILE VERSION CHECKS
==================================================================================================*/

/*==================================================================================================
*                                           CONSTANTS
==================================================================================================*/

/**                                   
* @brief ECSM IP (Platform) Version: acp_leopard_tb.01.00.02.00
* @details XPC56XXL IP Versions
* @implements DBASE08001
*/
#define IPV_ECSM                (0x01000200UL)


/** 
* @brief FLASH IP Version: IPV_FLASH_C90FL3_1024_6_0
* @details XPC56XXL IP Versions 
* @implements DBASE08001
*/
#define IPV_FLASH               (0x1000119UL)
/**
* @brief LINFLEX IP Version: LINFLEX3_IPS v3.00.m 
* @details XPC56XXL IP Version
* @implements DBASE08001
*/
#define IPV_LINFLEX             (0x0003006DUL)
/**
* @brief SIUL IP Version: siul.01.00.01.17
* @details XPC56XXL IP Versions 
* @implements DBASE08001
*/
#define IPV_SIUL                 (0x01000117UL)
/**
* @brief STCU IP Version: STA1_STCU1_IPS@v.1.0.a
* @details XPC56XXL IP Versions
* @implements DBASE08001
*/
#define IPV_STCU                 (0x00010061UL)
/**
* @brief WKUP IP Version: WKUP_01_00_10_01
* @details XPC56XXL IP Versions 
* @implements DBASE08001
*/
#define IPV_WKUP                 (0x01001001UL)
/** 
* @brief           Defines the ADCDIG IP version for Leopard cut2/cut3.
*/
#define IPV_ADCDIG  (0x31301UL)   /* 3.13.a */
/** 
* @brief           Defines the CTU2 IP version for Leopard cut2/cut3
*/
#define IPV_CTU     (0x503UL)     /* 0.5.c */
/** 
* @brief           Defines the Number of CTU2 hardare units in Leopard.
*/
#define CTU_MAX_HW_UNITS  1U

/** 
* @brief Number of DMA controllers available 
* @details XPC56XXL Platform Defines/Configurations
*/
#define DMA_NB_CONTROLLERS           (0x01U)
/**
* @brief Number of DMA channels available 
* @details XPC56XXL Platform Defines/Configurations
*/
#define DMA_NB_CHANNELS              (0x10U)
/**
* @brief Number of DMA_MUX channels
* @details XPC56XXL Platform Defines/Configurations
*/
#define DMAMUX_NB_CHANNELS           (DMA_NB_CHANNELS)
/**
* @brief Mask used to select the DMA controller for a given channel
*/
#define DMA_CTRL_MAX_CHANNELS_MASK       (0xFU)
/**
* @brief Define to identify max number of channels per DMA controller 
*/ 
#define DMA_CTRL_MAX_CHANNELS            (0x10U)
/**
* @brief Number of STM modules
* @details XPC56XXL Platform Defines/Configurations
*/
#define STM_NB_MODULES           (0x02U)
/** 
@{
* @brief Defines which AutoSAR drivers use the common eDMA IP driver 
*/
#define LIN_USE_DMA_LLD         (STD_OFF)
#define ADC_USE_DMA_LLD         (STD_ON)
#define SPI_USE_DMA_LLD         (STD_ON)
#define MCU_USE_DMA_LLD         (STD_ON)
#define ICU_USE_DMA_LLD         (STD_ON)
#define PWM_USE_DMA_LLD         (STD_ON)
/**@}*/

/*==================================================================================================
*                              Software Erratas for Hardware Erratas
==================================================================================================*/

/**
* @brief Workaround implemented for Errata e4186
* @details ADCDIG: triggering an ABORT or ABORTCHAIN before the conversion starts
*/
/*
* @violates @ref Soc_Ips_h_REF_2 This is not a standard library macro
*/
#define  ERR_IPV_ADCDIG_0014 (STD_ON)

/**
* @brief Workaround implemented for Errata e4334
* @details MC_RGM: Device stays in reset state on external reset assertion.
*/
/*
* @violates @ref Soc_Ips_h_REF_2 This is not a standard library macro
*/
#define  ERR_IPV_MC_0030  (STD_ON)

/**
* @brief platform feature dependent flag to handle  e2656 errata  which is documented in the Reference Manual
*/
#define CAN_USE_MCR_ABORT_ENABLE (STD_ON)

 
/*==================================================================================================
*                                       DEFINES AND MACROS
==================================================================================================*/

/*==================================================================================================
*                                             ENUMS
==================================================================================================*/

/*==================================================================================================
*                                 STRUCTURES AND OTHER TYPEDEFS
==================================================================================================*/

/*==================================================================================================
*                                 GLOBAL VARIABLE DECLARATIONS
==================================================================================================*/

/*==================================================================================================
*                                     FUNCTION PROTOTYPES
==================================================================================================*/

#ifdef __cplusplus
}
#endif

#endif /* #ifndef SOC_IPS_H*/
