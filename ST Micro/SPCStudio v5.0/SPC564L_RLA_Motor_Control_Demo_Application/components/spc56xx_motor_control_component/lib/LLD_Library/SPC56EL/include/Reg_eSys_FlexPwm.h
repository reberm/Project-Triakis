/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    Reg_eSys_FlexPwm.h
*   @version BETA 0.9.1
*   @brief   This file contains the FlexPWM register and bitfield defines
*
*   @details FlexPWM register and bitfield defines, used by driver that access the FlexPWM resources.
*
*/

#ifndef REG_eSys_FlexPwm_H
#define REG_eSys_FlexPwm_H

#ifdef __cplusplus
extern "C" {
#endif

/*
* @page misra_violations MISRA-C:2004 violations
*
* @section Reg_eSys_RexPwm_H_REF1
* Violates MISRA 2004 Required Rule 1.4, The compiler/linker shall be checked to ensure 31 character
* significance and case sensitivity are supported for external identifiers.
* This is not a violation since all the compilers used interpret the identifiers correctly.
*
* @section Reg_eSys_RexPwm_H_REF2
* Violates MISRA 2004 Required Rule 19.15, Repeated include file MemMap.h
* There are different kinds of execution code sections.
*
* @section Reg_eSys_RexPwm_H_REF3
* Violates MISRA 2004 Required Rule 5.1, Identifiers (internal and external) shall not rely on the significance
* of more than 31 characters.
* The long identifiers are maintained for better readability.
*
* @section Reg_eSys_RexPwm_H_REF4
* Violates MISRA 2004 Required Rule 8.12, When an array is declared with external linkage, its size shall be 
* stated explicitly or defined implicitly by initialisation.
*
* @section Reg_eSys_RexPwm_H_REF5
* Violates MISRA 2004 Advisory Rule 19.7, Function-like macro defined
* This violation is due to function like macros defined for register operations.
* Function like macros are used to reduce code complexity.
*
* @section Reg_eSys_RexPwm_H_REF6
* Violates MISRA 2004 Required Rule 19.10, In the definition of a function-like macro each instance of a
* parameter shall be enclosed in parentheses unless it is used as the
* operand of #or##.
*/
/*===============================================================================================
*                                         INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
===============================================================================================*/

/**
* @file           Reg_eSys_FlexPwm.h
*
*/

/*
* @violates @ref Reg_eSys_RexPwm_H_REF2 Violates MISRA 2004 Required Rule 19.15, Repeated include file MemMap.h
*/
#include "Reg_eSys.h"

/*===============================================================================================
*                               SOURCE FILE VERSION INFORMATION
===============================================================================================*/
/**
* @{
* @file Reg_eSys_FlexPwm.h
* @implements
*/
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define PWM_REG_ESYS_FLEXPWM_H_VENDOR_ID                    43
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define PWM_REG_ESYS_FLEXPWM_H_MODULE_ID                    121
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define PWM_REG_ESYS_FLEXPWM_H_AR_RELEASE_MAJOR_VERSION     4
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
/*
* @violates @ref Reg_eSys_RexPwm_H_REF1 Identifier clash.
*/
#define PWM_REG_ESYS_FLEXPWM_H_AR_RELEASE_MINOR_VERSION     0
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
/*
* @violates @ref Reg_eSys_RexPwm_H_REF1 Identifier clash.
*/
#define PWM_REG_ESYS_FLEXPWM_H_AR_RELEASE_REVISION_VERSION  3
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define PWM_REG_ESYS_FLEXPWM_H_SW_MAJOR_VERSION             1
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define PWM_REG_ESYS_FLEXPWM_H_SW_MINOR_VERSION             0
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define PWM_REG_ESYS_FLEXPWM_H_SW_PATCH_VERSION             1
/**@}*/
/*==================================================================================================
*                                GLOBAL DEFINES & DECLARATIONS
==================================================================================================*/


/**
* @brief FLEXPWM0 base address array declaration
*/
/* define module-specific constants */
#define FLEXPWM0_BASEADDR_SUB0      (FLEXPWM_0_BASEADDR)
#define FLEXPWM0_BASEADDR_SUB1      ((uint32)FLEXPWM0_BASEADDR_SUB0+(uint32)0x50)
#define FLEXPWM0_BASEADDR_SUB2      ((uint32)FLEXPWM0_BASEADDR_SUB1+(uint32)0x50)
#define FLEXPWM0_BASEADDR_SUB3      ((uint32)FLEXPWM0_BASEADDR_SUB2+(uint32)0x50)
#define FLEXPWM0_BASEADDR_CTRL      ((uint32)FLEXPWM0_BASEADDR_SUB0+(uint32)0x140)

/* FlexPwm1 defines*/
#define FLEXPWM1_BASEADDR_SUB0      (FLEXPWM_1_BASEADDR)
#define FLEXPWM1_BASEADDR_SUB1      ((uint32)FLEXPWM1_BASEADDR_SUB0+(uint32)0x50)
#define FLEXPWM1_BASEADDR_SUB2      ((uint32)FLEXPWM1_BASEADDR_SUB1+(uint32)0x50)
#define FLEXPWM1_BASEADDR_SUB3      ((uint32)FLEXPWM1_BASEADDR_SUB2+(uint32)0x50)
#define FLEXPWM1_BASEADDR_CTRL      ((uint32)FLEXPWM1_BASEADDR_SUB0+(uint32)0x140)


/* ************************************************** */
/* FlexPWM registers offset                           */
/* ************************************************** */
/**
@{
@brief FlexPWM Submodule registers offset
@remarks
*/
#define FLEXPWM_CNT         ((uint32)0x00)
#define FLEXPWM_INIT        ((uint32)0x02)
#define FLEXPWM_CTRL2       ((uint32)0x04)
#define FLEXPWM_CTRL1       ((uint32)0x06)
#define FLEXPWM_VAL0        ((uint32)0x08)
#define FLEXPWM_VAL1        ((uint32)0x0A)
#define FLEXPWM_VAL2        ((uint32)0x0C)
#define FLEXPWM_VAL3        ((uint32)0x0E)
#define FLEXPWM_VAL4        ((uint32)0x10)
#define FLEXPWM_VAL5        ((uint32)0x12)
#define FLEXPWM_OCTRL       ((uint32)0x18)
#define FLEXPWM_STS         ((uint32)0x1A)
#define FLEXPWM_INTEN       ((uint32)0x1C)
#define FLEXPWM_DMAEN       ((uint32)0x1E)
#define FLEXPWM_TCTRL       ((uint32)0x20)
#define FLEXPWM_DISMAP      ((uint32)0x22)
#define FLEXPWM_DTCNT0      ((uint32)0x24)
#define FLEXPWM_DTCNT1      ((uint32)0x26)
#define FLEXPWM_CAPTCTRLX   ((uint32)0x30)
#define FLEXPWM_CAPTCOMPX   ((uint32)0x32)
#define FLEXPWM_CVAL0       ((uint32)0x34)
#define FLEXPWM_CVAL0C      ((uint32)0x36)
#define FLEXPWM_CVAL1       ((uint32)0x38)
#define FLEXPWM_CVAL1C      ((uint32)0x3A)
/**@}*/

/**
@{
@brief FlexPWM Control registers offset from [base] + 0x140
@remarks
*/
#define FLEXPWM_OUTEN       ((uint32)0x00)
#define FLEXPWM_MASK        ((uint32)0x02)
#define FLEXPWM_SWCOUT      ((uint32)0x04)
#define FLEXPWM_DTSRCSEL    ((uint32)0x06)
#define FLEXPWM_MCTRL       ((uint32)0x08)
/**@}*/

/**
@{
@brief Fault channel registers offset from [base] + 0x140
@remarks
*/
#define FLEXPWM_FCTRL       ((uint32)0x0C)
#define FLEXPWM_FSTS        ((uint32)0x0E)
#define FLEXPWM_FFILT       ((uint32)0x10)
/**@}*/

/**
@{
@brief CTRL bits control
@remarks
*/
/* DBLEN double switching enable (bit0) */
#define FLEXPWM_CTRL_DBLEN_DIS          ((uint16)0x0)
#define FLEXPWM_CTRL_DBLEN_EN           ((uint16)0x1)
/* LDMOD bit (bit2)*/
#define FLEXPWM_CTRL_LDMOD              ((uint16)0x4)
/* PRSR prescaler (bit4, bit5, bit6) */
#define FLEXPWM_CTRL_PRSC_PRESC_1       ((uint16)0)
#define FLEXPWM_CTRL_PRSC_PRESC_2       ((uint16)((uint16)0x1 << (uint16)4))
#define FLEXPWM_CTRL_PRSC_PRESC_4       ((uint16)((uint16)0x2 << (uint16)4))
#define FLEXPWM_CTRL_PRSC_PRESC_8       ((uint16)((uint16)0x3 << (uint16)4))
#define FLEXPWM_CTRL_PRSC_PRESC_16      ((uint16)((uint16)0x4 << (uint16)4))
#define FLEXPWM_CTRL_PRSC_PRESC_32      ((uint16)((uint16)0x5 << (uint16)4))
#define FLEXPWM_CTRL_PRSC_PRESC_64      ((uint16)((uint16)0x6 << (uint16)4))
#define FLEXPWM_CTRL_PRSC_PRESC_128     ((uint16)((uint16)0x7 << (uint16)4))
/* FULL full cycle reload (bit10) */
#define FLEXPWM_CTRL_FULL_DIS           ((uint16)0)
#define FLEXPWM_CTRL_FULL_EN            ((uint16)((uint16)0x1 << (uint16)10))
/* HALF half cycle reload (bit11) */
#define FLEXPWM_CTRL_HALF_DIS           ((uint16)0)
#define FLEXPWM_CTRL_HALF_EN            ((uint16)((uint16)0x1 << (uint16)11))
/* LDFQ load frequency */
#define FLEXPWM_CTRL_LDFQ_EACH1         ((uint16)0)
#define FLEXPWM_CTRL_LDFQ_EACH2         ((uint16)((uint16)0x1 << (uint16)12))
#define FLEXPWM_CTRL_LDFQ_EACH3         ((uint16)((uint16)0x2 << (uint16)12))
#define FLEXPWM_CTRL_LDFQ_EACH4         ((uint16)((uint16)0x3 << (uint16)12))
#define FLEXPWM_CTRL_LDFQ_EACH5         ((uint16)((uint16)0x4 << (uint16)12))
#define FLEXPWM_CTRL_LDFQ_EACH6         ((uint16)((uint16)0x5 << (uint16)12))
#define FLEXPWM_CTRL_LDFQ_EACH7         ((uint16)((uint16)0x6 << (uint16)12))
#define FLEXPWM_CTRL_LDFQ_EACH8         ((uint16)((uint16)0x7 << (uint16)12))
#define FLEXPWM_CTRL_LDFQ_EACH9         ((uint16)((uint16)0x8 << (uint16)12))
#define FLEXPWM_CTRL_LDFQ_EACH10        ((uint16)((uint16)0x9 << (uint16)12))
#define FLEXPWM_CTRL_LDFQ_EACH11        ((uint16)((uint16)0xA << (uint16)12))
#define FLEXPWM_CTRL_LDFQ_EACH12        ((uint16)((uint16)0xB << (uint16)12))
#define FLEXPWM_CTRL_LDFQ_EACH13        ((uint16)((uint16)0xC << (uint16)12))
#define FLEXPWM_CTRL_LDFQ_EACH14        ((uint16)((uint16)0xD << (uint16)12))
#define FLEXPWM_CTRL_LDFQ_EACH15        ((uint16)((uint16)0xE << (uint16)12))
#define FLEXPWM_CTRL_LDFQ_EACH16        ((uint16)((uint16)0xF << (uint16)12))
/**@}*/

/**
@{
@brief CTRL2 bit control
@remarks
*/
/*  CLK_SEL Clock source select (bit1, bit0) */
#define FLEXPWM_CTRL2_CLK_SEL_IPBUS        ((uint16)0x0U)
#define FLEXPWM_CTRL2_CLK_SEL_EXT_CLK      ((uint16)0x1U)
#define FLEXPWM_CTRL2_CLK_SEL_AUX_CLK      ((uint16)0x2U)
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define FLEXPWM_CTRL2_RELOAD_SEL_LOCAL_RELOAD   ((uint16)0x0U)
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define FLEXPWM_CTRL2_RELOAD_SEL_MASTER_RELOAD  ((uint16)0x1U << (uint16)2)

#define FLEXPWM_CTRL2_FORCE_SEL_FORCE         ((uint16)0x0U)
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define FLEXPWM_CTRL2_FORCE_SEL_MASTER_FORCE  ((uint16)((uint16)0x1U << (uint16)3))
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define FLEXPWM_CTRL2_FORCE_SEL_LOCAL_RELOAD  ((uint16)((uint16)0x2U << (uint16)3))
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
/* @violates @ref Reg_eSys_RexPwm_H_REF1 Violates MISRA 2004 Required Rule 1.4, Identifier clash */
#define FLEXPWM_CTRL2_FORCE_SEL_MASTER_RELOAD ((uint16)((uint16)0x3U << (uint16)3))
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define FLEXPWM_CTRL2_FORCE_SEL_LOCAL_SYNC    ((uint16)((uint16)0x4U << (uint16)3))
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
/* @violates @ref Reg_eSys_RexPwm_H_REF1 Violates MISRA 2004 Required Rule 1.4, Identifier clash */
#define FLEXPWM_CTRL2_FORCE_SEL_MASTER_SYNC   ((uint16)((uint16)0x5U << (uint16)3))
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define FLEXPWM_CTRL2_FORCE_SEL_EXT_FORCE     ((uint16)((uint16)0x6U << (uint16)3))
/*
* @violates @ref Reg_eSys_RexPwm_H_REF1 Identifier clash.
*/
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define FLEXPWM_CTRL2_FORCE_SEL_MASTER_RELOAD ((uint16)((uint16)0x3U << (uint16)3))
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
/* @violates @ref Reg_eSys_RexPwm_H_REF1 Violates MISRA 2004 Required Rule 1.4, Identifier clash */
#define FLEXPWM_CTRL2_FORCE_SEL_LOCAL_SYNC    ((uint16)((uint16)0x4U << (uint16)3))
/*
* @violates @ref Reg_eSys_RexPwm_H_REF1 Identifier clash.
*/
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define FLEXPWM_CTRL2_FORCE_SEL_MASTER_SYNC   ((uint16)((uint16)0x5U << (uint16)3))
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define FLEXPWM_CTRL2_FORCE_SEL_EXT_FORCE     ((uint16)((uint16)0x6U << (uint16)3))
#define FLEXPWM_CTRL2_FORCE_SEL               ((uint16)((uint16)0x7U << (uint16)3))

#define FLEXPWM_CTRL2_FORCE_DIS               ((uint16)0x0U)
#define FLEXPWM_CTRL2_FORCE_EN                ((uint16)((uint16)0x1U << (uint16)6))
#define FLEXPWM_CTRL2_FORCE                   ((uint16)((uint16)0x1U << (uint16)6))

/* FRCEN Force Initialization Enable  (bit 7) */
#define FLEXPWM_CTRL2_FRCEN_DIS              ((uint16)0x0U)
#define FLEXPWM_CTRL2_FRCEN_EN               ((uint16)((uint16)0x1U << (uint16)7))
/* INIT_SEL Initialization Control Select  (bit8-9) */
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define FLEXPWM_CTRL2_INIT_SEL_LOCAL_SYNC    ((uint16)0x0U)
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define FLEXPWM_CTRL2_INIT_SEL_MASTER_RELOAD ((uint16)((uint16)0x1U << (uint16)8))
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define FLEXPWM_CTRL2_INIT_SEL_MASTER_SYNC   ((uint16)((uint16)0x2U << (uint16)8))
#define FLEXPWM_CTRL2_INIT_SEL_EXT_SYNC      ((uint16)((uint16)0x3U << (uint16)8))
/* PWMX Initial Value (bit10) */
#define FLEXPWM_CTRL2_PWMX_INIT_LOW          ((uint16)0x0)
#define FLEXPWM_CTRL2_PWMX_INIT_HIGH         ((uint16)((uint16)0x1 << (uint16)10 ))
/* PWM45 Initial Value (bit11) */
#define FLEXPWM_CTRL2_PWM45_INIT_LOW         ((uint16)0x0)
#define FLEXPWM_CTRL2_PWM45_INIT_HIGH        ((uint16)((uint16)0x1 << (uint16)11 ))
/* PWM23 Initial Value (bit12) */
#define FLEXPWM_CTRL2_PWM23_INIT_LOW         ((uint16)0x0)
#define FLEXPWM_CTRL2_PWM23_INIT_HIGH        ((uint16)((uint16)0x1 << (uint16)12 ))
/* INDEP Independent or Complementary Pair Operation (bit13) */
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define FLEXPWM_CTRL2_INDEP_COMPLEMENTARY    ((uint16)0x0)
#define FLEXPWM_CTRL2_INDEP_INDEPENDENT      ((uint16)((uint16)0x1 << (uint16)13U))
/* WAITEN WAIT enable (bit14) */
#define FLEXPWM_CTRL2_WAITEN_DIS             ((uint16)0x0)
#define FLEXPWM_CTRL2_WAITEN_EN              ((uint16)((uint16)0x1 << (uint16)14U))
/* DBGEN Debug Enable (bit15) */
#define FLEXPWM_CTRL2_DBGEN_DIS              ((uint16)0x0)
#define FLEXPWM_CTRL2_DBGEN_EN               ((uint16)((uint16)0x1 << (uint16)15U))
/* ************** End of CTRL2 register *************************************/
/**@}*/

/**
@{
@brief DMAEN bit control
@remarks
*/
#define FLEXPWM_DMAEN_CX0DE        ((uint16)((uint16)0x1))
#define FLEXPWM_DMAEN_CX1DE        ((uint16)((uint16)0x1 << (uint16)1U))
#define FLEXPWM_DMAEN_CAPTDE       ((uint16)((uint16)0x1 << (uint16)6U))
#define FLEXPWM_DMAEN_FAND         ((uint16)((uint16)0x1 << (uint16)8U))
#define FLEXPWM_DMAEN_VALDE        ((uint16)((uint16)0x1 << (uint16)9U))

/**
@{
@brief OCTRL bit control
@remarks
*/
/* PWMA_IN bit */
#define FLEXPWM_OCTRL_PWMA_IN_LOW      ((uint16)0x0)
#define FLEXPWM_OCTRL_PWMA_IN_HIGH     ((uint16)((uint16)0x1 << (uint16)15U))
#define FLEXPWM_OCTRL_PWMA_IN          ((uint16)((uint16)0x1 << (uint16)15U))
/* PWMB_IN bit */
#define FLEXPWM_OCTRL_PWMB_IN_LOW      ((uint16)0x0)
#define FLEXPWM_OCTRL_PWMB_IN_HIGH     ((uint16)((uint16)0x1 << (uint16)14U))
#define FLEXPWM_OCTRL_PWMB_IN          ((uint16)((uint16)0x1 << (uint16)14U))
/* PWMX_IN bit */
#define FLEXPWM_OCTRL_PWMX_IN_LOW      ((uint16)0x0)
#define FLEXPWM_OCTRL_PWMX_IN_HIGH     ((uint16)((uint16)0x1 << (uint16)13U))
#define FLEXPWM_OCTRL_PWMX_IN          ((uint16)((uint16)0x1 << (uint16)13U))
/* POLA bit */
#define FLEXPWM_OCTRL_POLA_NOINVERT    ((uint16)0x0)
#define FLEXPWM_OCTRL_POLA_INVERT      ((uint16)((uint16)0x1 << (uint16)10U))
#define FLEXPWM_OCTRL_POLA             ((uint16)((uint16)0x1 << (uint16)10U))
/* POLB bit */
#define FLEXPWM_OCTRL_POLB_NOINVERT    ((uint16)0x0)
#define FLEXPWM_OCTRL_POLB_INVERT      ((uint16)((uint16)0x1 << (uint16)9U))
#define FLEXPWM_OCTRL_POLB             ((uint16)((uint16)0x1 << (uint16)9U))
/* POLX bit */
#define FLEXPWM_OCTRL_POLX_NOINVERT    ((uint16)0x0)
#define FLEXPWM_OCTRL_POLX_INVERT      ((uint16)((uint16)0x1 << (uint16)8U))
#define FLEXPWM_OCTRL_POLX             ((uint16)((uint16)0x1 << (uint16)8U))
/* PWMAFS bit */
#define FLEXPWM_OCTRL_PWMAFS_LOW       ((uint16)0x0)
#define FLEXPWM_OCTRL_PWMAFS_HIGH      ((uint16)((uint16)0x1 << (uint16)4))
#define FLEXPWM_OCTRL_PWMAFS_TRISTATE  ((uint16)((uint16)0x2 << (uint16)4))
/* PWMBFS bit */
#define FLEXPWM_OCTRL_PWMBFS_LOW       ((uint16)0x0)
#define FLEXPWM_OCTRL_PWMBFS_HIGH      ((uint16)((uint16)0x1 << (uint16)2))
#define FLEXPWM_OCTRL_PWMBFS_TRISTATE  ((uint16)((uint16)0x2 << (uint16)2))
/* PWMXFS bit */
#define FLEXPWM_OCTRL_PWMXFS_LOW       ((uint16)0x0)
#define FLEXPWM_OCTRL_PWMXFS_HIGH      ((uint16)0x1)
#define FLEXPWM_OCTRL_PWMXFS_TRISTATE  ((uint16)0x2)
/**@}*/

/**
@{
@brief OUTEN bit control
@remarks
*/
/* PWMX_EN PWMX Output enable (bit0) */
#define FLEXPWM_OUTEN_SUBMOD_0_PWMX_DIS     ((uint16)0x0)
#define FLEXPWM_OUTEN_SUBMOD_0_PWMX_EN      ((uint16)0x1)
/* PWMX_EN PWMX Output enable (bit1) */
#define FLEXPWM_OUTEN_SUBMOD_1_PWMX_DIS     ((uint16)0x0)
#define FLEXPWM_OUTEN_SUBMOD_1_PWMX_EN      ((uint16)((uint16)0x1 << (uint16)1U))
/* PWMX_EN PWMX Output enable (bit2) */
#define FLEXPWM_OUTEN_SUBMOD_2_PWMX_DIS     ((uint16)0x0)
#define FLEXPWM_OUTEN_SUBMOD_2_PWMX_EN      ((uint16)((uint16)0x1 << (uint16)2U))
/* PWMX_EN PWMX Output enable (bit3) */
#define FLEXPWM_OUTEN_SUBMOD_3_PWMX_DIS     ((uint16)0x0)
#define FLEXPWM_OUTEN_SUBMOD_3_PWMX_EN      ((uint16)((uint16)0x1 << (uint16)3U))

/* PWMB_EN PWMB Output enable (bit4) */
#define FLEXPWM_OUTEN_SUBMOD_0_PWMB_DIS     ((uint16)0x0)
#define FLEXPWM_OUTEN_SUBMOD_0_PWMB_EN      ((uint16)((uint16)0x1 << (uint16)4))
/* PWMB_EN PWMB Output enable (bit5)*/
#define FLEXPWM_OUTEN_SUBMOD_1_PWMB_DIS     ((uint16)0x0)
#define FLEXPWM_OUTEN_SUBMOD_1_PWMB_EN      ((uint16)((uint16)0x1 << (uint16)5U))
/* PWMB_EN PWMB Output enable (bit6) */
#define FLEXPWM_OUTEN_SUBMOD_2_PWMB_DIS     ((uint16)0x0)
#define FLEXPWM_OUTEN_SUBMOD_2_PWMB_EN      ((uint16)((uint16)0x1 << (uint16)6U))
/* PWMB_EN PWMB Output enable (bit7) */
#define FLEXPWM_OUTEN_SUBMOD_3_PWMB_DIS     ((uint16)0x0)
#define FLEXPWM_OUTEN_SUBMOD_3_PWMB_EN      ((uint16)((uint16)0x1 << (uint16)7U))

/* PWMA_EN PWMA Output enable (bit8) */
#define FLEXPWM_OUTEN_SUBMOD_0_PWMA_DIS     ((uint16)0x0)
#define FLEXPWM_OUTEN_SUBMOD_0_PWMA_EN      ((uint16)((uint16)0x1 << (uint16)8))
/* PWMA_EN PWMA Output enable (bit9)*/
#define FLEXPWM_OUTEN_SUBMOD_1_PWMA_DIS     ((uint16)0x0)
#define FLEXPWM_OUTEN_SUBMOD_1_PWMA_EN      ((uint16)((uint16)0x1 << (uint16)9U))
/* PWMA_EN PWMA Output enable (bit10) */
#define FLEXPWM_OUTEN_SUBMOD_2_PWMA_DIS     ((uint16)0x0)
#define FLEXPWM_OUTEN_SUBMOD_2_PWMA_EN      ((uint16)((uint16)0x1 << (uint16)10U))
/* PWMA_EN PWMA Output enable (bit11) */
#define FLEXPWM_OUTEN_SUBMOD_3_PWMA_DIS     ((uint16)0x0)
#define FLEXPWM_OUTEN_SUBMOD_3_PWMA_EN      ((uint16)((uint16)0x1 << (uint16)11U))
/* ************************************************** */
/**@}*/

/**
@{
@brief INTEN bit control
@remarks
*/
#define FLEXPWM_INTEN_CMPIE             ((uint16)0x3F)
#define FLEXPWM_INTEN_REIE_EN           ((uint16)((uint16)0x1 << (uint16)13))
#define FLEXPWM_INTEN_RIE_EN            ((uint16)((uint16)0x1 << (uint16)12))
#define FLEXPWM_INTEN_CMPIE5_EN         ((uint16)((uint16)0x1 << (uint16)5))
#define FLEXPWM_INTEN_CMPIE4_EN         ((uint16)((uint16)0x1 << (uint16)4))
#define FLEXPWM_INTEN_CMPIE3_EN         ((uint16)((uint16)0x1 << (uint16)3))
#define FLEXPWM_INTEN_CMPIE2_EN         ((uint16)((uint16)0x1 << (uint16)2))
#define FLEXPWM_INTEN_CMPIE1_EN         ((uint16)((uint16)0x1 << (uint16)1))
#define FLEXPWM_INTEN_CMPIE0_EN         ((uint16)((uint16)0x1 << (uint16)0))

#define FLEXPWM_INTEN_REIE_DIS          ((uint16)0x0)
#define FLEXPWM_INTEN_RIE_DIS           ((uint16)0x0)
#define FLEXPWM_INTEN_CMPIE5_DIS        ((uint16)0x0)
#define FLEXPWM_INTEN_CMPIE4_DIS        ((uint16)0x0)
#define FLEXPWM_INTEN_CMPIE3_DIS        ((uint16)0x0)
#define FLEXPWM_INTEN_CMPIE2_DIS        ((uint16)0x0)
#define FLEXPWM_INTEN_CMPIE1_DIS        ((uint16)0x0)
#define FLEXPWM_INTEN_CMPIE0_DIS        ((uint16)0x0)
/**@}*/

/**
@{
@brief STS bit control
@remarks
*/
#define FLEXPWM_STS_CMPF      ((uint16)0x3F)
#define FLEXPWM_STS_CMPF_0    ((uint16)0x1)
#define FLEXPWM_STS_CMPF_1    ((uint16)((uint16)0x1 << (uint16)1U))
#define FLEXPWM_STS_CMPF_2    ((uint16)((uint16)0x1 << (uint16)2U))
#define FLEXPWM_STS_CMPF_3    ((uint16)((uint16)0x1 << (uint16)3U))
#define FLEXPWM_STS_CMPF_4    ((uint16)((uint16)0x1 << (uint16)4U))
#define FLEXPWM_STS_CMPF_5    ((uint16)((uint16)0x1 << (uint16)5U))
#define FLEXPWM_STS_RF        ((uint16)((uint16)0x1 << (uint16)12U))
#define FLEXPWM_STS_REF       ((uint16)((uint16)0x1 << (uint16)13U))
#define FLEXPWM_STS_RUF       ((uint16)((uint16)0x1 << (uint16)14U))
/**@}*/

/**
@{
@brief TCTRL bit control
@remarks
*/
#define FLEXPWM_TCTRL_MASK         ((uint16)0x003F)
#define FLEXPWM_TCTRL_VAL0         ((uint16)0x1)
#define FLEXPWM_TCTRL_VAL1         ((uint16)((uint16)0x1 << (uint16)0x01))
#define FLEXPWM_TCTRL_VAL2         ((uint16)((uint16)0x1 << (uint16)0x02))
#define FLEXPWM_TCTRL_VAL3         ((uint16)((uint16)0x1 << (uint16)0x03))
#define FLEXPWM_TCTRL_VAL4         ((uint16)((uint16)0x1 << (uint16)0x04))
#define FLEXPWM_TCTRL_VAL5         ((uint16)((uint16)0x1 << (uint16)0x05))
/**@}*/

/**
@{
@brief DISMAP bit control
@remarks
*/
#define FLEXPWM_DISMAP_DISA_FAULT0_MASK    ((uint16)0x1)
#define FLEXPWM_DISMAP_DISA_FAULT1_MASK    ((uint16)0x2)
#define FLEXPWM_DISMAP_DISA_FAULT2_MASK    ((uint16)0x4)
#define FLEXPWM_DISMAP_DISA_FAULT3_MASK    ((uint16)0x8)

#define FLEXPWM_DISMAP_DISB_FAULT0_MASK    ((uint16)((uint16)0x1 << (uint16)4))
#define FLEXPWM_DISMAP_DISB_FAULT1_MASK    ((uint16)((uint16)0x2 << (uint16)4))
#define FLEXPWM_DISMAP_DISB_FAULT2_MASK    ((uint16)((uint16)0x4 << (uint16)4))
#define FLEXPWM_DISMAP_DISB_FAULT3_MASK    ((uint16)((uint16)0x8 << (uint16)4))

#define FLEXPWM_DISMAP_DISX_FAULT0_MASK    ((uint16)((uint16)0x1 << (uint16)8))
#define FLEXPWM_DISMAP_DISX_FAULT1_MASK    ((uint16)((uint16)0x2 << (uint16)8))
#define FLEXPWM_DISMAP_DISX_FAULT2_MASK    ((uint16)((uint16)0x4 << (uint16)8))
#define FLEXPWM_DISMAP_DISX_FAULT3_MASK    ((uint16)((uint16)0x8 << (uint16)8))
/**@}*/

/**
@{
@brief MASK bit control
@remarks
*/
#define FLEXPWM_MASK_SUBMOD_0_MASKX_EN    ((uint16)0x1)
#define FLEXPWM_MASK_SUBMOD_1_MASKX_EN    ((uint16)0x2)
#define FLEXPWM_MASK_SUBMOD_2_MASKX_EN    ((uint16)0x4)
#define FLEXPWM_MASK_SUBMOD_3_MASKX_EN    ((uint16)0x8)
#define FLEXPWM_MASK_SUBMOD_0_MASKX_DIS   ((uint16)0x0)
#define FLEXPWM_MASK_SUBMOD_1_MASKX_DIS   ((uint16)0x0)
#define FLEXPWM_MASK_SUBMOD_2_MASKX_DIS   ((uint16)0x0)
#define FLEXPWM_MASK_SUBMOD_3_MASKX_DIS   ((uint16)0x0)

#define FLEXPWM_MASK_SUBMOD_0_MASKB_EN    ((uint16)((uint16)0x1 << (uint16)4))
#define FLEXPWM_MASK_SUBMOD_1_MASKB_EN    ((uint16)((uint16)0x2 << (uint16)4))
#define FLEXPWM_MASK_SUBMOD_2_MASKB_EN    ((uint16)((uint16)0x4 << (uint16)4))
#define FLEXPWM_MASK_SUBMOD_3_MASKB_EN    ((uint16)((uint16)0x8 << (uint16)4))
#define FLEXPWM_MASK_SUBMOD_0_MASKB_DIS   ((uint16)0x0)
#define FLEXPWM_MASK_SUBMOD_1_MASKB_DIS   ((uint16)0x0)
#define FLEXPWM_MASK_SUBMOD_2_MASKB_DIS   ((uint16)0x0)
#define FLEXPWM_MASK_SUBMOD_3_MASKB_DIS   ((uint16)0x0)

#define FLEXPWM_MASK_SUBMOD_0_MASKA_EN    ((uint16)((uint16)0x1 << (uint16)8))
#define FLEXPWM_MASK_SUBMOD_1_MASKA_EN    ((uint16)((uint16)0x2 << (uint16)8))
#define FLEXPWM_MASK_SUBMOD_2_MASKA_EN    ((uint16)((uint16)0x4 << (uint16)8))
#define FLEXPWM_MASK_SUBMOD_3_MASKA_EN    ((uint16)((uint16)0x8 << (uint16)8))
#define FLEXPWM_MASK_SUBMOD_0_MASKA_DIS   ((uint16)0x0)
#define FLEXPWM_MASK_SUBMOD_1_MASKA_DIS   ((uint16)0x0)
#define FLEXPWM_MASK_SUBMOD_2_MASKA_DIS   ((uint16)0x0)
#define FLEXPWM_MASK_SUBMOD_3_MASKA_DIS   ((uint16)0x0)
/**@}*/

/**
@{
@brief SWCOUT bit control
@remarks
*/
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define FLEXPWM_SWCOUT_SUBMOD_0_OUT45_HIGH  ((uint16)0x01)
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define FLEXPWM_SWCOUT_SUBMOD_0_OUT23_HIGH  ((uint16)0x02)
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define FLEXPWM_SWCOUT_SUBMOD_1_OUT45_HIGH  ((uint16)((uint16)0x1 << (uint16)2))
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define FLEXPWM_SWCOUT_SUBMOD_1_OUT23_HIGH  ((uint16)((uint16)0x2 << (uint16)2))
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define FLEXPWM_SWCOUT_SUBMOD_2_OUT45_HIGH  ((uint16)((uint16)0x1 << (uint16)4))
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define FLEXPWM_SWCOUT_SUBMOD_2_OUT23_HIGH  ((uint16)((uint16)0x2 << (uint16)4))
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define FLEXPWM_SWCOUT_SUBMOD_3_OUT45_HIGH  ((uint16)((uint16)0x1 << (uint16)6))
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define FLEXPWM_SWCOUT_SUBMOD_3_OUT23_HIGH  ((uint16)((uint16)0x2 << (uint16)6))
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define FLEXPWM_SWCOUT_SUBMOD_0_OUT45_LOW   ((uint16)0x00)
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define FLEXPWM_SWCOUT_SUBMOD_0_OUT23_LOW   ((uint16)0x00)
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define FLEXPWM_SWCOUT_SUBMOD_1_OUT45_LOW   ((uint16)0x00)
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define FLEXPWM_SWCOUT_SUBMOD_1_OUT23_LOW   ((uint16)0x00)
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define FLEXPWM_SWCOUT_SUBMOD_2_OUT45_LOW   ((uint16)0x00)
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define FLEXPWM_SWCOUT_SUBMOD_2_OUT23_LOW   ((uint16)0x00)
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define FLEXPWM_SWCOUT_SUBMOD_3_OUT45_LOW   ((uint16)0x00)
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define FLEXPWM_SWCOUT_SUBMOD_3_OUT23_LOW   ((uint16)0x00)

/**
@{
@brief DTSRCSEL bitfield mask and shift defines.
@remarks
*/
#define FLEXPWM_DTSRCSEL_SEL_MASK                ((uint16)(BIT1|BIT0))

#define FLEXPWM_DTSRCSEL_SEL_NORMAL              ((uint16)0x0)
#define FLEXPWM_DTSRCSEL_SEL_INVERTED            ((uint16)0x1)
#define FLEXPWM_DTSRCSEL_SEL_OUT                 ((uint16)0x2)
#define FLEXPWM_DTSRCSEL_SEL_EXTA                ((uint16)0x3)
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define FLEXPWM_DTSRCSEL_SUBMOD_0_SEL45_MASK     ((FLEXPWM_DTSRCSEL_SEL_MASK) << (uint16)0)
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define FLEXPWM_DTSRCSEL_SUBMOD_0_SEL23_MASK     ((FLEXPWM_DTSRCSEL_SEL_MASK) << (uint16)2)
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
/* @violates @ref Reg_eSys_RexPwm_H_REF1 Violates MISRA 2004 Required Rule 1.4, Identifier clash */
#define FLEXPWM_DTSRCSEL_SUBMOD_0_SEL45_SHIFT    ((uint16)(0U))
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
/* @violates @ref Reg_eSys_RexPwm_H_REF1 Violates MISRA 2004 Required Rule 1.4, Identifier clash */
#define FLEXPWM_DTSRCSEL_SUBMOD_0_SEL23_SHIFT    ((uint16)(2U))
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define FLEXPWM_DTSRCSEL_SUBMOD_1_SEL45_MASK     ((FLEXPWM_DTSRCSEL_SEL_MASK) << (uint16)4)
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define FLEXPWM_DTSRCSEL_SUBMOD_1_SEL23_MASK     ((FLEXPWM_DTSRCSEL_SEL_MASK) << (uint16)6)
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
/* @violates @ref Reg_eSys_RexPwm_H_REF1 Violates MISRA 2004 Required Rule 1.4, Identifier clash */
#define FLEXPWM_DTSRCSEL_SUBMOD_1_SEL45_SHIFT    ((uint16)(4U))
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
/* @violates @ref Reg_eSys_RexPwm_H_REF1 Violates MISRA 2004 Required Rule 1.4, Identifier clash */
#define FLEXPWM_DTSRCSEL_SUBMOD_1_SEL23_SHIFT    ((uint16)(6U))
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define FLEXPWM_DTSRCSEL_SUBMOD_2_SEL45_MASK     ((FLEXPWM_DTSRCSEL_SEL_MASK) << (uint16)8)
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define FLEXPWM_DTSRCSEL_SUBMOD_2_SEL23_MASK     ((FLEXPWM_DTSRCSEL_SEL_MASK) << (uint16)10)
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
/* @violates @ref Reg_eSys_RexPwm_H_REF1 Violates MISRA 2004 Required Rule 1.4, Identifier clash */
#define FLEXPWM_DTSRCSEL_SUBMOD_2_SEL45_SHIFT    ((uint16)(8U))
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
/* @violates @ref Reg_eSys_RexPwm_H_REF1 Violates MISRA 2004 Required Rule 1.4, Identifier clash */
#define FLEXPWM_DTSRCSEL_SUBMOD_2_SEL23_SHIFT    ((uint16)(10U))
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define FLEXPWM_DTSRCSEL_SUBMOD_3_SEL45_MASK     ((FLEXPWM_DTSRCSEL_SEL_MASK) << (uint16)12)
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define FLEXPWM_DTSRCSEL_SUBMOD_3_SEL23_MASK     ((FLEXPWM_DTSRCSEL_SEL_MASK) << (uint16)14)
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
/* @violates @ref Reg_eSys_RexPwm_H_REF1 Violates MISRA 2004 Required Rule 1.4, Identifier clash */
#define FLEXPWM_DTSRCSEL_SUBMOD_3_SEL45_SHIFT    ((uint16)(12U))
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
/* @violates @ref Reg_eSys_RexPwm_H_REF1 Violates MISRA 2004 Required Rule 1.4, Identifier clash */
#define FLEXPWM_DTSRCSEL_SUBMOD_3_SEL23_SHIFT    ((uint16)(14U))
/**@}*/

/**
@{
@brief MCTRL bit control
@remarks
*/
#define FLEXPWM_MCTRL_LDOK_MASK             ((uint16)0x000F)
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define FLEXPWM_MCTRL_SUBMOD_0_LDOK_MASK    ((uint16)0x1)
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define FLEXPWM_MCTRL_SUBMOD_1_LDOK_MASK    ((uint16)0x2)
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define FLEXPWM_MCTRL_SUBMOD_2_LDOK_MASK    ((uint16)0x4)
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define FLEXPWM_MCTRL_SUBMOD_3_LDOK_MASK    ((uint16)0x8)
#define FLEXPWM_MCTRL_CLDOK_MASK            ((uint16)0x00F0)
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define FLEXPWM_MCTRL_SUBMOD_0_CLDOK_MASK   ((uint16)0x10)
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define FLEXPWM_MCTRL_SUBMOD_1_CLDOK_MASK   ((uint16)0x20)
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define FLEXPWM_MCTRL_SUBMOD_2_CLDOK_MASK   ((uint16)0x40)
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define FLEXPWM_MCTRL_SUBMOD_3_CLDOK_MASK   ((uint16)0x80)
#define FLEXPWM_MCTRL_RUN_MASK              ((uint16)0x0F00)
#define FLEXPWM_MCTRL_SUBMOD_0_RUN_MASK     ((uint16)((uint16)0x1 << (uint16)8))
#define FLEXPWM_MCTRL_SUBMOD_1_RUN_MASK     ((uint16)((uint16)0x2 << (uint16)8))
#define FLEXPWM_MCTRL_SUBMOD_2_RUN_MASK     ((uint16)((uint16)0x4 << (uint16)8))
#define FLEXPWM_MCTRL_SUBMOD_3_RUN_MASK     ((uint16)((uint16)0x8 << (uint16)8))
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define FLEXPWM_MCTRL_SUBMOD_0_IPOL_MASK    ((uint16)((uint16)0x1 << (uint16)12))
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define FLEXPWM_MCTRL_SUBMOD_1_IPOL_MASK    ((uint16)((uint16)0x2 << (uint16)12))
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define FLEXPWM_MCTRL_SUBMOD_2_IPOL_MASK    ((uint16)((uint16)0x4 << (uint16)12))
/* @violates @ref Reg_eSys_RexPwm_H_REF3 Violates MISRA 2004 Required Rule 5.1, The long identifiers are maintained for better readability. */
#define FLEXPWM_MCTRL_SUBMOD_3_IPOL_MASK    ((uint16)((uint16)0x8 << (uint16)12))
/**@}*/

/**
@{
@brief FCTRL bit control
@remarks
*/
#define FLEXPWM_FCTRL_FAULT_0_FLVL_HIGH  ((uint16)((uint16)0x1 << (uint16)12))
#define FLEXPWM_FCTRL_FAULT_1_FLVL_HIGH  ((uint16)((uint16)0x2 << (uint16)12))
#define FLEXPWM_FCTRL_FAULT_2_FLVL_HIGH  ((uint16)((uint16)0x4 << (uint16)12))
#define FLEXPWM_FCTRL_FAULT_3_FLVL_HIGH  ((uint16)((uint16)0x8 << (uint16)12))
#define FLEXPWM_FCTRL_FAULT_0_FLVL_LOW   ((uint16)0x0)
#define FLEXPWM_FCTRL_FAULT_1_FLVL_LOW   ((uint16)0x0)
#define FLEXPWM_FCTRL_FAULT_2_FLVL_LOW   ((uint16)0x0)
#define FLEXPWM_FCTRL_FAULT_3_FLVL_LOW   ((uint16)0x0)

#define FLEXPWM_FCTRL_FAULT_0_FAUTO_EN   ((uint16)((uint16)0x1 << (uint16)8))
#define FLEXPWM_FCTRL_FAULT_1_FAUTO_EN   ((uint16)((uint16)0x2 << (uint16)8))
#define FLEXPWM_FCTRL_FAULT_2_FAUTO_EN   ((uint16)((uint16)0x4 << (uint16)8))
#define FLEXPWM_FCTRL_FAULT_3_FAUTO_EN   ((uint16)((uint16)0x8 << (uint16)8))
#define FLEXPWM_FCTRL_FAULT_0_FAUTO_DIS  ((uint16)0x0)
#define FLEXPWM_FCTRL_FAULT_1_FAUTO_DIS  ((uint16)0x0)
#define FLEXPWM_FCTRL_FAULT_2_FAUTO_DIS  ((uint16)0x0)
#define FLEXPWM_FCTRL_FAULT_3_FAUTO_DIS  ((uint16)0x0)

#define FLEXPWM_FCTRL_FAULT_0_FSAFE_EN   ((uint16)((uint16)0x1 << (uint16)4))
#define FLEXPWM_FCTRL_FAULT_1_FSAFE_EN   ((uint16)((uint16)0x2 << (uint16)4))
#define FLEXPWM_FCTRL_FAULT_2_FSAFE_EN   ((uint16)((uint16)0x4 << (uint16)4))
#define FLEXPWM_FCTRL_FAULT_3_FSAFE_EN   ((uint16)((uint16)0x8 << (uint16)4))
#define FLEXPWM_FCTRL_FAULT_0_FSAFE_DIS  ((uint16)0x0)
#define FLEXPWM_FCTRL_FAULT_1_FSAFE_DIS  ((uint16)0x0)
#define FLEXPWM_FCTRL_FAULT_2_FSAFE_DIS  ((uint16)0x0)
#define FLEXPWM_FCTRL_FAULT_3_FSAFE_DIS  ((uint16)0x0)

#define FLEXPWM_FCTRL_FAULT_0_FIE_EN     ((uint16)0x1)
#define FLEXPWM_FCTRL_FAULT_1_FIE_EN     ((uint16)0x2)
#define FLEXPWM_FCTRL_FAULT_2_FIE_EN     ((uint16)0x4)
#define FLEXPWM_FCTRL_FAULT_3_FIE_EN     ((uint16)0x8)
#define FLEXPWM_FCTRL_FAULT_0_FIE_DIS    ((uint16)0x0)
#define FLEXPWM_FCTRL_FAULT_1_FIE_DIS    ((uint16)0x0)
#define FLEXPWM_FCTRL_FAULT_2_FIE_DIS    ((uint16)0x0)
#define FLEXPWM_FCTRL_FAULT_3_FIE_DIS    ((uint16)0x0)
/**@}*/

/**
@{
@brief FSTS bit control
@remarks
*/
#define FLEXPWM_FSTS_FAULT_TEST_EN       ((uint16)((uint16)0x1 << (uint16)12))
#define FLEXPWM_FSTS_FAULT_TEST_DIS      ((uint16)0x0)
#define FLEXPWM_FSTS_FAULT_0_FFPIN       ((uint16)((uint16)0x1 << (uint16)4U))
#define FLEXPWM_FSTS_FAULT_1_FFPIN       ((uint16)((uint16)0x1 << (uint16)5U))
#define FLEXPWM_FSTS_FAULT_2_FFPIN       ((uint16)((uint16)0x1 << (uint16)6U))
#define FLEXPWM_FSTS_FAULT_3_FFPIN       ((uint16)((uint16)0x1 << (uint16)7U))
#define FLEXPWM_FSTS_FAULT_0_FFULL_EN    ((uint16)((uint16)0x1 << (uint16)8U))
#define FLEXPWM_FSTS_FAULT_1_FFULL_EN    ((uint16)((uint16)0x1 << (uint16)9U))
#define FLEXPWM_FSTS_FAULT_2_FFULL_EN    ((uint16)((uint16)0x1 << (uint16)10U))
#define FLEXPWM_FSTS_FAULT_3_FFULL_EN    ((uint16)((uint16)0x1 << (uint16)11U))
#define FLEXPWM_FSTS_FAULT_0_FFULL_DIS   ((uint16)0x0)
#define FLEXPWM_FSTS_FAULT_1_FFULL_DIS   ((uint16)0x0)
#define FLEXPWM_FSTS_FAULT_2_FFULL_DIS   ((uint16)0x0)
#define FLEXPWM_FSTS_FAULT_3_FFULL_DIS   ((uint16)0x0)
#define FLEXPWM_FSTS_FFLAG               ((uint16)0x0F)
#define FLEXPWM_FSTS_FAULT_0_FFLAG       ((uint16)((uint16)0x1 << (uint16)0U))
#define FLEXPWM_FSTS_FAULT_1_FFLAG       ((uint16)((uint16)0x1 << (uint16)1U))
#define FLEXPWM_FSTS_FAULT_2_FFLAG       ((uint16)((uint16)0x1 << (uint16)2U))
#define FLEXPWM_FSTS_FAULT_3_FFLAG       ((uint16)((uint16)0x1 << (uint16)3U))
/**@}*/

/**
@{
@brief FFILT bit control
@remarks
*/
#define FLEXPWM_FFILT_FILT_PER_MASK       ((uint16)(BIT7|BIT6|BIT5|BIT4|BIT3|BIT2|BIT1|BIT0))
#define FLEXPWM_FFILT_FILT_CNT_MASK       ((uint16)(BIT10|BIT9|BIT8))
#define FLEXPWM_FFILT_GSTR_EN             ((uint16)((uint16)0x1 << (uint16)15))
#define FLEXPWM_FFILT_GSTR_DIS            ((uint16)(0U))
/**@}*/

#ifdef __cplusplus
}
#endif



#endif /* REG_eSys_FlexPwm_H */
