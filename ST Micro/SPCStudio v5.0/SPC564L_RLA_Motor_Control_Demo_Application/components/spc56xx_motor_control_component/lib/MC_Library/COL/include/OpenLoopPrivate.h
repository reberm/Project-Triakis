/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    OpenLoopPrivate.h
*   @version BETA 0.9.1
*   @brief   This file contains private definition of OpenLoop class
*          
*   @details This file contains private definition of OpenLoop class  
*
*/
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __OPENLOOPPRIVATE_H
#define __OPENLOOPPRIVATE_H

/** @addtogroup SPC5_PMSM_MC_Library
  * @{
  */

/** @addtogroup OpenLoop
  * @{
  */

/** @defgroup OpenLoop_class_private_types OpenLoop class private types
* @{
*/

/** 
  * @brief  OpenLoop class members definition
  */
typedef struct
{
  int16_t hVoltage; /*!< Open loop voltage to be applied */
  CSPD oVSS;        /*!< Related VSS object used */
  bool VFMode;      /*!< TRUE to enable V/F mode otherwise FALSE */
} Vars_t,*pVars_t;

/** 
  * @brief  Redefinition of parameter structure
  */
typedef OpenLoopParams_t Params_t, *pParams_t;

/** 
  * @brief  Private OpenLoop class definition 
  */
typedef struct
{
    Vars_t Vars_str;        /*!< Class members container */
    pParams_t pParams_str;  /*!< Class parameters container */
} _COL_t, *_COL;
/**
  * @}
  */
  
/**
  * @}
  */

/**
  * @}
  */

#endif /*__OPENLOOPPRIVATE_H*/
