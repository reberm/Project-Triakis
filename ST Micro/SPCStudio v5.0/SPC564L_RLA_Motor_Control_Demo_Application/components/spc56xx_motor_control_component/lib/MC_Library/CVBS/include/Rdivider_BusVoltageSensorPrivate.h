/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    Rdivider_BusVoltageSensorPrivate.h
*   @version BETA 0.9.1
*   @brief   This file contains private definition of Rdivider class.
*          
*   @details This file contains private definition of Rdivider class.
*
*/
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __RDIVIDER_BUSVOLTAGESENSORPRIVATE_H
#define __RDIVIDER_BUSVOLTAGESENSORPRIVATE_H

/** @addtogroup SPC5_PMSM_MC_Library
  * @{
  */
  
/** @addtogroup BusVoltageSensor_Rdivider
  * @{
  */

/** @defgroup Rdivider_private_types Rdivider private types
* @{
*/

/** 
  * @brief  Rdivider class members definition 
  */
typedef struct
{
  CPWMC oPWMnCurrentSensor;     /*! CPWMC object to be used for regular 
                                    conversions*/
}DVars_t,*pDVars_t;

/** 
  * @brief  Redefinition of parameter structure
  */
typedef RdividerParams_t DParams_t, *pDParams_t; 

/** 
  * @brief Private Rdivider class definition 
  */
typedef struct
{
    DVars_t DVars_str;          /*!< Derived class members container */
    pDParams_t pDParams_str;    /*!< Derived class parameters container */
}_CRVBS_VBS_t, *_CRVBS_VBS;
/**
  * @}
  */
  
/**
  * @}
  */

/**
  * @}
  */

#endif /*__RDIVIDER_BUSVOLTAGESENSORPRIVATE_H*/
