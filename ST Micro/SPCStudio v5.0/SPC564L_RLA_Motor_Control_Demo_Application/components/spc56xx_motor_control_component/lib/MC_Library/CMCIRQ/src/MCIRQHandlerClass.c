/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    MCIRQHandlerClass.c
*   @version BETA 0.9.1
*   @brief   This file contains implementation of MC IRQ Handler class.
*          
*   @details This file contains implementation of MC IRQ Handler class.
*
*/
/*===============================================================================================
*                                         INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
===============================================================================================*/
#include "MCIRQHandlerClass.h"
#include "MCIRQHandlerPrivate.h"

/*===============================================================================================
*                                       CONSTANTS
===============================================================================================*/
/* Definition of MC IRQ Table */
#define MAX_MC_IRQ_NUM 4

/*===============================================================================================
*                                       LOCAL VARIABLES
===============================================================================================*/

/*===============================================================================================
*                                       GLOBAL CONSTANTS
===============================================================================================*/

/*===============================================================================================
*                                       GLOBAL VARIABLES
===============================================================================================*/
_CMCIRQ MC_IRQTable[MAX_MC_IRQ_NUM];

/*===============================================================================================
*                                       LOCAL FUNCTIONS
===============================================================================================*/

/*===============================================================================================
*                                       GLOBAL FUNCTIONS
===============================================================================================*/

/**
* @brief        Insert the object oIRQ into the MC vector table
*
* @param[in]    oIRQ MCIRQ object to be added to the MC vector list
* @param[in]    bIRQAddr MC vector table position
*
* @return       None
*
* @api
*
*/
void Set_IRQ_Handler(uint8_t bIRQAddr, _CMCIRQ oIRQ)
{
  MC_IRQTable[bIRQAddr] = oIRQ;
}

/**
* @brief        Start the execution of the MC_IRQ at bIRQAddr position inside MC 
*               vector table
*
* @param[in]    bIRQAddr MC IRQ position inside MC vector table
* @param[in]    flag parameter passed to the MC interrupt, for instance to identify 
*               the event request the MC ISR execution
*
* @return       pvoid pointer to Drive object related to the interrupt
*
* @api
*
*/
void* Exec_IRQ_Handler(uint8_t bIRQAddr, uint8_t flag)
{  
  return(MC_IRQTable[bIRQAddr]->pIRQ_Handler((void*)(MC_IRQTable)[bIRQAddr],flag));
}
