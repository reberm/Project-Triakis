/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    MCIRQHandlerClass.h
*   @version BETA 0.9.1
*   @brief   This file contains the interface of MC IRQ Handler class.
*          
*   @details This file contains the interface of MC IRQ Handler class.
*
*/
#ifndef __MC_IRQHANDLERCLASS_H
#define __MC_IRQHANDLERCLASS_H
/*===============================================================================================
*                                         INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
===============================================================================================*/
#include "MC_type.h"

/** @addtogroup SPC5_PMSM_MC_Library
  * @{
  */
  
/** @addtogroup MCIRQ_Handler
  * @{
  */

/** @defgroup MC_IRQHandler_exported_definitions MCIRQ_Handler module exported definitions
  * @{
  */

/*===============================================================================================
*                                       CONSTANTS
===============================================================================================*/
/** 
  * @brief  MC IRQ interrupts addresses inside MC vector table 
  */
#define MC_IRQ_SPEEDNPOSFDBK_1  0u  /*!< Reserved for SpeednPosFdbk first instance.*/
#define MC_IRQ_SPEEDNPOSFDBK_HS 1u  /*!< Reserved for SpeednPosFdbk_HS first instance.*/

/**
* @}
*/

/*===============================================================================================
*                                       LOCAL VARIABLES
===============================================================================================*/

/*===============================================================================================
*                                       GLOBAL CONSTANTS
===============================================================================================*/

/*===============================================================================================
*                                       GLOBAL VARIABLES
===============================================================================================*/

/*===============================================================================================
*                                      FUNCTION PROTOTYPES
===============================================================================================*/
/** @defgroup MC_IRQHandler_exported_functions MCIRQ Handler module exported functions
  * @{
  */

void* Exec_IRQ_Handler(uint8_t bIRQAddr, uint8_t flag);

/**
* @}
*/

/**
* @}
*/

/**
* @}
*/
#endif /* __MC_IRQHANDLERCLASS_H */
