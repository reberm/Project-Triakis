/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    MCIRQHandlerPrivate.h
*   @version BETA 0.9.1
*   @brief   This file contains the private definition of MC IRQ Handler class.
*          
*   @details This file contains the private definition of MC IRQ Handler class.
*
*/
#ifndef __MC_IRQHANDLERPRIVATE_H
#define __MC_IRQHANDLERPRIVATE_H
/*===============================================================================================
*                                         INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
===============================================================================================*/
#include "MC_type.h"

/*===============================================================================================
*                                       CONSTANTS
===============================================================================================*/

/*===============================================================================================
*                          LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
===============================================================================================*/
/** 
  * @brief  MCIRQ private module definition 
  */
typedef struct
{
  void* (*pIRQ_Handler)(void *this, unsigned char flag);
}_CMCIRQ_t,*_CMCIRQ;

/*===============================================================================================
*                                      FUNCTION PROTOTYPES
===============================================================================================*/
void Set_IRQ_Handler(uint8_t bIRQAddr,_CMCIRQ oIRQ);

#endif /* __MC_IRQHANDLERPRIVATE_H */
