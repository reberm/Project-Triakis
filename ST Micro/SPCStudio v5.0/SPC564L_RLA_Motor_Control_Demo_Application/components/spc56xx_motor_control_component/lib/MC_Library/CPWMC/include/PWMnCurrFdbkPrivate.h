/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    PWMnCurrFdbkPrivate.h
*   @version BETA 0.9.1
*   @brief   This file contains private definition of PWMnCurrFdbk class
*          
*   @details PWM generation and Current sensing interface.
*
*/
#ifndef __PWMNCURRFDBKPRIVATE_H
#define __PWMNCURRFDBKPRIVATE_H

/*===============================================================================================
*                                         INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
===============================================================================================*/
#include "PWMnCurrFdbkClass.h"

/*===============================================================================================
*                                       CONSTANTS
===============================================================================================*/

/** 
* @brief  Space vector sectors constant definitions
*/
#define SECTOR_1    0u
#define SECTOR_2    1u
#define SECTOR_3    2u
#define SECTOR_4    3u
#define SECTOR_5    4u
#define SECTOR_6    5u

/*===============================================================================================
*                          LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
===============================================================================================*/

/** 
* @brief  PWMnCurrFdbk class members definition
*/
typedef struct
{
  uint16_t  hT_Sqrt3;   /*!< Contains a constant utilized by SVPWM algorithm */
  uint16_t  hSector;    /*!< Contains the space vector sector number */
  uint16_t  hCntPhA;
  uint16_t  hCntPhB;
  uint16_t  hCntPhC;
  uint16_t  SWerror;
  bool bTurnOnLowSidesAction; /*!< TRUE if TurnOnLowSides action is active,
                                   FALSE otherwise. */
  uint16_t  hOffCalibrWaitTimeCounter; /*!< Counter to wait fixed time before
                                            motor current measurement offset 
                                            calibration. */
  uint8_t   bMotor;     /*!< Motor reference number */
}Vars_t,*pVars_t;

/** 
* @brief  Redefinition of parameter structure
*/
typedef PWMnCurrFdbkParams_t Params_t, *pParams_t;

/**
* @brief Virtual methods container
*/
typedef struct
{
  void *(*pIRQ_Handler)(void *this, unsigned char flag);
  void (*pPWMC_Init)(CPWMC this);
  void (*pPWMC_GetPhaseCurrents)(CPWMC this, Curr_Components* pStator_Currents); 
  void (*pPWMC_SwitchOffPWM)(CPWMC this); 
  void (*pPWMC_SwitchOnPWM )(CPWMC this); 
  void (*pPWMC_CurrentReadingCalibr)(CPWMC this); 
  void (*pPWMC_TurnOnLowSides)(CPWMC this); 
  void (*pPWMC_SetSamplingTime)(ADConv_t ADConv_struct); 
  uint16_t (*pPWMC_SetADCSampPointSect1)(CPWMC this);
  uint16_t (*pPWMC_SetADCSampPointSect2)(CPWMC this);
  uint16_t (*pPWMC_SetADCSampPointSect3)(CPWMC this);
  uint16_t (*pPWMC_SetADCSampPointSect4)(CPWMC this);
  uint16_t (*pPWMC_SetADCSampPointSect5)(CPWMC this);
  uint16_t (*pPWMC_SetADCSampPointSect6)(CPWMC this);
  uint16_t (*pPWMC_ExecRegularConv)(CPWMC this, uint8_t bChannel, uint8_t ADC_Unit);
  uint16_t (*pPWMC_GetMultipleRegularConv)(CPWMC this, uint8_t Adc_Channel);  
  uint16_t (*pPWMC_IsOverCurrentOccurred)(CPWMC this);
  void (*pPWMC_OCPSetReferenceVoltage)(CPWMC this,uint16_t hDACVref);
}Methods_t,*pMethods_t;

/** 
* @brief  Private PWMnCurrFdbk class definition 
*/
typedef struct
{
  Methods_t Methods_str;    /*!< Virtual methods container */
  Vars_t Vars_str;      /*!< Class members container */
  pParams_t pParams_str;    /*!< Class parameters container */
  void *DerivedClass;       /*!< Pointer to derived class */
  
}_CPWMC_t, *_CPWMC;

/*===============================================================================================
*                                      FUNCTION PROTOTYPES
===============================================================================================*/
CPWMC PWMC_NewObject(pPWMnCurrFdbkParams_t pPWMnCurrFdbkParams);

#endif /*__PWMNCURRFDBKPRIVATE_H*/
