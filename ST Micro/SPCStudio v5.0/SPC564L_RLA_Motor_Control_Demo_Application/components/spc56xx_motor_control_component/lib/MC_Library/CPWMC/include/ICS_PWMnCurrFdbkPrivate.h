/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    ICS_PWMnCurrFdbkPrivate.h
*   @version BETA 0.9.1
*   @brief   This file contains private definition of ICS class
*          
*   @details PWM generation and Current sensing using FlexPWM, ADC, DMA and CTU module.
*
*/

/*===============================================================================================
*                                         INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
===============================================================================================*/
#ifndef __ICS_PWMNCURRFDBKPRIVATE_H
#define __ICS_PWMNCURRFDBKPRIVATE_H

/*===============================================================================================
*                                       CONSTANTS
===============================================================================================*/
#define FLEX_PWMA 0x02
#define FLEX_PWMB 0x01

#define ICS_CTU_PERIOD(LDFQ) (LDFQ >> 12)

/** 
  * @brief  ICS class members definition 
  */
typedef struct
{
  uint16_t hPhaseAOffset;   /*!< Offset of Phase A current sensing network  */
  uint16_t hPhaseBOffset;   /*!< Offset of Phase B current sensing network  */
  uint16_t Half_PWMPeriod;          /* Half PWM Period in timer clock counts */
  uint16_t hRegConv;          /*!< Variable used to store regular conversions
                              result*/  
}DVars_t,*pDVars_t;

/** 
  * @brief  Redefinition of parameter structure
  */
typedef ICS_Params_t DParams_t, *pDParams_t; 

/** 
  * @brief Private ICS class definition 
  */
typedef struct
{
    DVars_t DVars_str;          /*!< Derived class members container */
    pDParams_t pDParams_str;    /*!< Derived class parameters container */
}_DCICS_PWMC_t, *_DCICS_PWMC;

#endif /*__ICS_PWMNCURRFDBKPRIVATE_H*/
