/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    ICS_PWMnCurrFdbkClass.h
*   @version BETA 0.9.1
*   @brief   This file contains interface of ICS class
*          
*   @details PWM generation and Current sensing using FlexPWM, ADC, DMA and CTU module.
*
*/

/*===============================================================================================
*                                         INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
===============================================================================================*/
#ifndef __ICS_PWMNCURRFDBKCLASS_H
#define __ICS_PWMNCURRFDBKCLASS_H

/*===============================================================================================
*                                       CONSTANTS
===============================================================================================*/

/*===============================================================================================
*                          LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
===============================================================================================*/
/** @addtogroup SPC5_PMSM_MC_Library
  * @{
  */
  
/** @addtogroup PWMnCurrFdbk_ICS
  * @{
  */
  
/** @defgroup ICS_class_exported_types ICS_class exported types
* @{
*/

/** 
  * @brief  Public ICS class definition
  *
  */
typedef struct CICS_PWMC_t *CICS_PWMC;

/** 
  * @brief  ICS class parameters definition
  *
  */
typedef const struct
{
  /* Current reading A/D Conversions initialization ------------------------- */
  /** @brief  ADC channel configuration used for conversion of current Ia.    */
  ADConv_t hIaChannel;
  /** @brief  ADC channel configuration used for conversion of current Ib.    */
  ADConv_t hIbChannel;
  /** @brief  Power Down Exit Delay for ADC Units                             */
  uint8_t AdcPwrDownDelay;
  /** @brief  Dma channel for ADC conversion                                  */
  uint8_t DmaChannel;
  /** @brief  Dma channel for VBus ADC conversion                             */
  uint8_t DmaVBusChannel;
  /** @brief  Dma channel for User ADC conversion                             */
  uint8_t DmaUserChannel;
  /* PWM generation parameters -----------------------------------------------*/
  /** @brief PWM clock frequency divide ratio:                                
      PWM clock frequency divide ratio:   FLEXPWM_CTRL_PRSC_PRESC_1,
                                          FLEXPWM_CTRL_PRSC_PRESC_2,
                                          FLEXPWM_CTRL_PRSC_PRESC_4,
                                          FLEXPWM_CTRL_PRSC_PRESC_8,
                                          FLEXPWM_CTRL_PRSC_PRESC_16
                                          FLEXPWM_CTRL_PRSC_PRESC_32
                                          FLEXPWM_CTRL_PRSC_PRESC_64
                                          FLEXPWM_CTRL_PRSC_PRESC_128         */
  uint8_t FlexPWM_Clock_Divider;
  /** @brief FlexPWM used for PWM generation.                                 */
  uint8_t  FlexPWMModule;
  /** @brief Dead time in number of IPBUS clock cycles regardless of the
      setting of FlexPWM_Clock_Divider. */
  uint16_t hDeadTime;
  /** @brief PWM load frequency: FLEXPWM_CTRL_LDFQ_EACH1- FLEXPWM_CTRL_LDFQ_EACH16  */
  uint16_t  PWMLoadFreq;
  /* PWM Driving signals initialization --------------------------------------*/  
  /** @brief Channel 1 (high side) output polarity,it must be
      FLEXPWM_Polarity_High or FLEXPWM_Polarity_Low */
  uint16_t hCh1Polarity;
  /** @brief Channel 1 (high side) state (low or high) when FlexPWM is in     
      Idle state. It must be FLEXPWM_IDLE_STATE_HIGH or FLEXPWM_IDLE_STATE_LOW */  
  uint16_t hCh1IdleState;
  /** @brief Channel 2 (high side) output polarity,it must be
      FLEXPWM_Polarity_High or FLEXPWM_Polarity_Low  */
  uint16_t hCh2Polarity;
  /** @brief Channel 2 (high side) state (low or high) when FlexPWM is in     
      Idle state. It must be FLEXPWM_IDLE_STATE_HIGH or FLEXPWM_IDLE_STATE_LOW */  
  uint16_t hCh2IdleState;
  /** @brief Channel 3 (high side) output polarity,it must be
      FLEXPWM_Polarity_High or FLEXPWM_Polarity_Low.  */
  uint16_t hCh3Polarity;
  /** @brief Channel 3 (high side) state (low or high) when FlexPWM is in     
      Idle state. It must be FLEXPWM_IDLE_STATE_HIGH or FLEXPWM_IDLE_STATE_LOW */  
  uint16_t hCh3IdleState;
  /** @brief Low side or enabling signals generation method definition. */
  LowSideOutputsFunction_t LowSideOutputs;
  /** @brief Channel 1N (low side) output polarity,it must be
      FLEXPWM_NPolarity_High or FLEXPWM_NPolarity_Low. */
  uint16_t hCh1NPolarity;
  /** @brief Channel 1N (low side) state (low or high) when FlexPWM is in     
      Idle state. It must be FLEXPWM_IDLE_STATE_HIGH or FLEXPWM_IDLE_STATE_LOW */  
  uint16_t hCh1NIdleState;
  /** @brief Channel 2N (low side) output polarity,it must be
      FLEXPWM_NPolarity_High or FLEXPWM_NPolarity_Low. */
  uint16_t hCh2NPolarity;
  /** @brief Channel 2N (low side) state (low or high) when FlexPWM is in     
      Idle state. It must be FLEXPWM_IDLE_STATE_HIGH or FLEXPWM_IDLE_STATE_LOW. */  
  uint16_t hCh2NIdleState;
  /** @brief Channel 3N (low side) output polarity,it must be
      FLEXPWM_NPolarity_High or FLEXPWM_NPolarity_Low. */
  uint16_t hCh3NPolarity;
  /** @brief Channel 3N (low side) state (low or high) when FlexPWM is in     
      Idle state. It must be FLEXPWM_IDLE_STATE_HIGH or FLEXPWM_IDLE_STATE_LOW */  
  uint16_t hCh3NIdleState;
  /* PWM Driving signals initialization ----------------------------------------*/  
  /** @brief It enable/disable the management of an emergency input instantaneously 
      stopping PWM generation. It must be either equal to ENABLE or DISABLE. */
  FunctionalState EmergencyStop;
  /** @brief  Emergency Stop PIN. It must be: FAULT_PIN0-FAULT_PIN3 */
  uint8_t FaultPIN;
  /** @brief  Emergency Stop Input polarity, it must be: FAULT_POLARITY_HIGH or 
      FAULT_OLARITY_LOW. */
  uint16_t FaultPolarity;
}ICS_Params_t, *pICS_Params_t;

/** 
  * @brief  ICS ADC User Conversion
  */
typedef const struct
{
      uint8_t Adc_Unit;                     /*!< ADC HW Unit. It must be equal to:
                                                 ADC_UNIT_0 or ADC_UNIT_1 */
      uint8_t hIChannel;                   /*!< ADC channel used for conversion of
                                                 current.  It must be in the range:
                                                 ADC_AN_0 .. ADC_AN_14  */
}ICS_ADC_User_ch;

/**
  * @}
  */
  
/*===============================================================================================
*                                      FUNCTION PROTOTYPES
===============================================================================================*/
/** @defgroup ICS_class_exported_methods ICS class exported 
  *           methods
  * @{
  */
CICS_PWMC ICS_NewObject(pPWMnCurrFdbkParams_t pPWMnCurrFdbkParams, 
                                    pICS_Params_t pICS_Params);

/**
  * @}
  */
  
/**
  * @}
  */

/**
  * @}
  */
#endif /*__ICS_PWMNCURRFDBKCLASS_H*/
