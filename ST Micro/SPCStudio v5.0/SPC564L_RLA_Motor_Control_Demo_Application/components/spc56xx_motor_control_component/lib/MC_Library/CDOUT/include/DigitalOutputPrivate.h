/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    DigitalOutputPrivate.h
*   @version BETA 0.9.1
*   @brief   This file contains private definition of DigitalOutput class
*          
*   @details This file contains private definition of DigitalOutput class  
*
*/
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __DIGITALOUTPUTPRIVATE_H
#define __DIGITALOUTPUTPRIVATE_H

/** @addtogroup SPC5_PMSM_MC_Library
  * @{
  */

/** @addtogroup DigitalOutput
  * @{
  */

/** @defgroup DigitalOutput_class_private_types DigitalOutput class private types
* @{
*/

/** 
  * @brief  DigitalOutput class members definition
  */
typedef struct
{
  DOutputState_t OutputState;
}Vars_t,*pVars_t;

/** 
  * @brief  Redefinition of parameter structure
  */
typedef DigitalOutputParams_t Params_t, *pParams_t;

/** 
  * @brief  Private DigitalOutput class definition 
  */
typedef struct
{
    Vars_t Vars_str;        /*!< Class members container */
    pParams_t pParams_str;  /*!< Class parameters container */
}_CDOUT_t, *_CDOUT;
/**
  * @}
  */
  
/**
  * @}
  */

/**
  * @}
  */

#endif /*__DIGITALOUTPUTPRIVATE_H*/
