/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    HALL_SpeednPosFdbkPrivate.h
*   @version BETA 0.9.1
*   @brief   This file contains private definition of HALL class.
*          
*   @details Hall sensor implementation using eTimer.
*
*/
/*===============================================================================================
*                                         INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
===============================================================================================*/
#ifndef __HALL_SPEEDNPOSFDBKPRIVATE_H
#define __HALL_SPEEDNPOSFDBKPRIVATE_H

#include "PMSM motor parameters.h"
#include "Drive parameters.h"
/*===============================================================================================
*                                       CONSTANTS
===============================================================================================*/

/*===============================================================================================
*                          LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
===============================================================================================*/
/** 
  * @brief  HALL class members definition 
  */
typedef struct
{
  uint16 SensorPeriod[POLE_PAIR_NUM*6U]; /*!< Holding the last captures */

  uint16 hNewCapture_sample;      /* Last capture value (edge counter) */

  boolean bBufferValid;           /* Flag to indicate that at least one capture event occurred */

  uint16 h_delta_same_hs;         /* Time between two consecutive edges of the same hall sensor signals on 360 electrical degree */

  uint16 h_delta_different_hs;    /* Time between two consecutive edges of the differen hall sensor signals on 60 electrical degree */

  uint16 h_Prev_capture;          /* Prev capture value of hNewCapture_sample */

  sint16 hMeasuredElAngle;        /*!< This is the electrical angle  measured at each 
                                     Hall sensor signal edge. It is considered the 
                                     best measurement of electrical rotor angle.*/

  sint16 hPrev_MeasuredElAngle;   /*!< This is the previous electrical angle  measured at each 
                                        Hall sensor signal edge. */

  uint16  h_u_angle;               /*!< Angle captured in unsigned format [0..65535] */

  uint32 speed_buffer[HALL_AVERAGING_FIFO_DEPTH];     /*!< Buffer of last measured angle in long format from Hall Sensor */

  uint16 h_polar_pairs;            /*!< Index used to fill SensorPeriod buffer in order to manage motor with pole pair > 1*/

  boolean SensorIsReliable;             /*!< Flag to indicate a wrong configuration 
                                         of the Hall sensor signanls.*/
  
  sint8 bSpeed;          /*!< Instantaneous direction of rotor between two 
                               captures*/
 
  sint16 hAvrElSpeedDpp; /*!< It is the averaged rotor electrical speed express
                               in s16degree per current control period.*/
                          
  uint8 bHallState;     /*!< Current HALL state configuration */

}DVars_t,*pDVars_t;

/** 
  * @brief  Redefinition of parameter structure
  */
typedef HALLParams_t DParams_t, *pDParams_t; 

/** 
  * @brief Private HALL class definition 
  */
typedef struct
{
    DVars_t DVars_str;          /*!< Derived class members container */
    pDParams_t pDParams_str;    /*!< Derived class parameters container */
}_DCHALL_SPD_t, *_DCHALL_SPD;

/*===============================================================================================
*                                       LOCAL VARIABLES
===============================================================================================*/

/*===============================================================================================
*                                       GLOBAL CONSTANTS
===============================================================================================*/

/*===============================================================================================
*                                       GLOBAL VARIABLES
===============================================================================================*/

/*===============================================================================================
*                                   LOCAL FUNCTION PROTOTYPES
===============================================================================================*/

#endif /*__HALL_SPEEDNPOSFDBKPRIVATE_H*/

