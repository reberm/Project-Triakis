/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    STO_SpeednPosFdbkPrivate.h
*   @version BETA 0.9.1
*   @brief   This file contains private definition of STO class.
*          
*   @details This file contains private definition of STO class.
*
*/
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __STO_SPEEDNPOSFDBKPRIVATE_H
#define __STO_SPEEDNPOSFDBKPRIVATE_H

/** 
  * @brief  STO class members definition 
  */
typedef struct
{
  int16_t  hC1;                 /*!< Variable containing state observer constant
                                     C1 to speed-up computations */
  int16_t  hC2;                /*!< Variable containing state observer constant
                                     C2, it can be computed as F1 * K1/ State 
                                     observer execution rate [Hz] being K1 one
                                     of the two observer gains   */
  int16_t  hC3;                 /*!< Variable containing state observer constant
                                     C3 */
  int16_t  hC4;                  /*!< State Observer constant C4, it can 
                                      be computed as K2 * max measurable 
                                      current (A) / (Max application speed 
                                      [rpm] * Motor B-emf constant 
                                      [Vllrms/krpm] * sqrt(2) * F2 * State 
                                      observer execution rate [Hz]) being 
                                       K2 one of the two observer gains  */
  int16_t  hC5;                 /*!< Variable containing state observer constant 
                                     C5 */
  int16_t  hC6;                 /*!< State observer constant C6, computed with a 
                                    specific procedure starting from the other 
                                    constants */ 
  int16_t  hF1;                 /*!< Variable containing state observer scaling
                                     factor F1 */
  int16_t  hF2;                 /*!< Variable containing state observer scaling
                                     factor F2 */  
  int16_t  hF3;                  /*!< State observer scaling factor F3 */
  uint16_t hF3POW2;              /*!< State observer scaling factor F3 expressed 
                                                                         as power of 2.
                                     E.g. if gain divisor is 512 the value 
                                     must be 9 because 2^9 = 512 */
  CPI     oPIRegulator;         /*!< Pointer to PI regulator object, used for 
                                     PLL implementation */
  int32_t wIalfa_est;           /*!< Estimated Ialfa current in int32 format */
  int32_t wIbeta_est;           /*!< Estimated Ialfa current in int32 format */
  int32_t wBemf_alfa_est;       /*!< Estimated B-emf alfa in int32_t format */
  int32_t wBemf_beta_est;       /*!< Estimated B-emf beta in int32_t format */
  int16_t hBemf_alfa_est;       /*!< Estimated B-emf alfa in int16_t format */
  int16_t hBemf_beta_est;       /*!< Estimated B-emf beta in int16_t format */
  int16_t hSpeed_Buffer[64];    /*!< Estimated speed FIFO, it contains latest 
                                     bSpeedBufferSize speed measurements*/
  uint8_t bSpeed_Buffer_Index;  /*!< Position of latest speed estimation in 
                                     estimated speed FIFO */  
  bool bIsSpeedReliable;        /*!< Latest private speed reliability information,
                                     updated by SPD_CalcAvrgMecSpeed01Hz, it is 
                                     TRUE if the speed measurement variance is 
                                     lower then threshold corresponding to 
                                     hVariancePercentage */
  uint8_t bConsistencyCounter;  /*!< Counter of passed tests for start-up 
                                     validation */
  uint8_t  bReliabilityCounter; /*!< Counter for reliability check internal to 
                                     derived class */
  bool bIsAlgorithmConverged;   /*!< Boolean variable containing observer 
                                     convergence information */
  bool bIsBemfConsistent;       /*!< Sensor reliability information, updated by
                                     SPD_CalcAvrgMecSpeed01Hz, it is TRUE if the
                                     observed back-emfs are consistent with
                                     expectation*/
  int32_t wObs_Bemf_Level;      /*!< Observed back-emf Level*/
  int32_t wEst_Bemf_Level;      /*!< Estimated back-emf Level*/
  bool bEnableDualCheck;        /*!< Consistency check enabler*/
  int32_t wDppBufferSum;        /*!< summation of speed buffer elements [dpp]*/
  int16_t hSpeedBufferOldestEl; /*!< Oldest element of the speed buffer*/
}DVars_t,*pDVars_t;                 

/** 
  * @brief  Redefinition of parameter structure
  */
typedef STOParams_t DParams_t, *pDParams_t; 

/** 
  * @brief Private STO class definition 
  */
typedef struct
{
    DVars_t DVars_str;          /*!< Derived class members container */
    pDParams_t pDParams_str;    /*!< Derived class parameters container */
}_DCSTO_SPD_t, *_DCSTO_SPD;


#endif /*__STO_SPEEDNPOSFDBKPRIVATE_H*/
