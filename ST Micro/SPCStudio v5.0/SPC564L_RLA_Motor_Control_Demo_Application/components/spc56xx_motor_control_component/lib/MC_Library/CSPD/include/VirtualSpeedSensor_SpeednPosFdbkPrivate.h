/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    VirtualSpeedSensor_SpeednPosFdbkPrivate.h
*   @version BETA 0.9.1
*   @brief   This file contains private definition of VirtualSpeedSensor class
*          
*   @details This file contains private definition of VirtualSpeedSensor class
*
*/
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __VIRTUALSPEEDSENSOR_SPEEDNPOSFDBKPRIVATE_H
#define __VIRTUALSPEEDSENSOR_SPEEDNPOSFDBKPRIVATE_H

/** 
  * @brief  VirtualSpeedSensor class members definition 
  */
typedef struct
{
  int32_t wElAccDppP32;   /*!< Delta electrical speed expressed in dpp per speed 
                               sampling period to be appied each time is called 
                               SPD_CalcAvrgMecSpeed01Hz multiplied by scaling
                               factor of 65536.*/
  int32_t wElSpeedDpp32;  /*!< Electrical speed expressed in dpp multiplied by
                               scaling factor 65536.*/
  uint16_t hRemainingStep;/*!< Number of steps remaining to reach the final
                               speed.*/
  int16_t hFinalMecSpeed01Hz;/*!< Backup of hFinalMecSpeed01Hz to be applied in 
                               the last step.*/
  bool bTransitionStarted;    /*!< Retaining information about Transition status.*/
  bool bTransitionEnded;      /*!< Retaining information about ransition status.*/
  int16_t hTransitionRemainingSteps;  /*!< Number of steps remaining to end
                               transition from CVSS_SPD to other CSPD*/
  int16_t hElAngleAccu;        /*!< Electrical angle accumulator*/
  bool bTransitionLocked;      /*!< Transition acceleration started*/
}DVars_t,*pDVars_t;

/** 
  * @brief  Redefinition of parameter structure
  */
typedef VirtualSpeedSensorParams_t DParams_t, *pDParams_t; 

/** 
  * @brief Private VirtualSpeedSensor class definition 
  */
typedef struct
{
    DVars_t DVars_str;          /*!< Derived class members container */
    pDParams_t pDParams_str;    /*!< Derived class parameters container */
}_DCVSS_SPD_t, *_DCVSS_SPD;


#endif /*__VIRTUALSPEEDSENSOR_SPEEDNPOSFDBKPRIVATE_H*/
