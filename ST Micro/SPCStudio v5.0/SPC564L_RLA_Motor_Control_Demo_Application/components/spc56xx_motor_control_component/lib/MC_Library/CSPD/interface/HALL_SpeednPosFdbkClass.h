/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    HALL_SpeednPosFdbkClass.h
*   @version BETA 0.9.1
*   @brief   This file contains interface of HALL class.
*          
*   @details Hall sensor implementation using eTimer.
*
*/
#ifndef __HALL_SPEEDNPOSFDBKCLASS_H
#define __HALL_SPEEDNPOSFDBKCLASS_H
/*===============================================================================================
*                                         INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
===============================================================================================*/

/*===============================================================================================
*                                       CONSTANTS
===============================================================================================*/

/* HALL SENSORS PLACEMENT */
#define DEGREES_120 0u
#define DEGREES_60 1u

/**
  * @brief  IRQ flags assignment
  */
#define HS1_IRQ_RIS ((uint8_t)(0))       /* Hall signal 1 Rising interrupt */
#define HS1_IRQ_FAL ((uint8_t)(1))       /* Hall signal 1 Falling interrupt */
#define HS2_IRQ_RIS ((uint8_t)(2))       /* Hall signal 1 Rising interrupt */
#define HS2_IRQ_FAL ((uint8_t)(3))       /* Hall signal 1 Falling interrupt */
#define HS3_IRQ_RIS ((uint8_t)(4))       /* Hall signal 1 Rising interrupt */
#define HS3_IRQ_FAL ((uint8_t)(5))       /* Hall signal 1 Falling interrupt */

/*===============================================================================================
*                          LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
===============================================================================================*/

/** @addtogroup SPC5_PMSM_MC_Library
  * @{
  */
  
/** @addtogroup SpeednPosFdbk_HALL
  * @{
  */

/** @defgroup HALL_class_exported_types HALL class exported types
* @{
*/

/** 
  * @brief  Public HALL class definition
  */
typedef struct CHALL_SPD_t *CHALL_SPD;

/** 
  * @brief  HALL class parameters definition
  */
typedef const struct
{
  /* SW Settings */
  /** @brief MC IRQ number used for eTimer capture events.*/
  uint8_t IRQnb;
  /** @brief here the mechanical position of the sensors with 
      reference to an electrical cycle. Allowed values are: DEGREES_120 
      or DEGREES_60. N.B. DEGREES_60 not implemented. */
  uint8_t bSensorPlacement; 
  /** @brief Define here in s16degree the electrical phase shift 
      between the low to high transition of signal H1 and
      the maximum of the Bemf induced on phase A.*/  
  int16_t hPhaseShift;
  /** @brief Frequency (Hz) at which motor speed is to be computed. 
      It must be equal to the frequency at which function 
      SPD_CalcAvrgMecSpeed01Hz is called.*/
  uint16_t hSpeedSamplingFreqHz; 
  /** @brief Mechanical speed (01Hz) threshold used to select speed measurement among two
      consecutive edges of the different or the same hall sensor signals.*/
  uint16_t hSpeedThreshold;
  /* HW Settings */
  /** @brief eTimer clock frequency used for Hall sensor class*/
  uint32_t eTimerClockFreq;  
 /** @brief Input filter applied to HALL sensor capture channels (for channel 1 
      and channel 2 and Channel 3). This value configure the Input Filter register 
      (FILT) of eTimer channel */
  uint16_t hInpCaptFilter_ch1_2_3;
  /** @brief  Timer used for HALL sensor management.*/
  uint8_t eTimerModule;
  /** @brief  HALL SENSOR 1 - INPUT 1 eTimer channel */
  uint8_t S1_eTimer_ch;
  /** @brief  HALL SENSOR 2 - INPUT 2 eTimer channel */
  uint8_t S2_eTimer_ch;
  /** @brief  HALL SENSOR 3 - INPUT 3 eTimer channel */
  uint8_t S3_eTimer_ch;
  /** @brief  eTimer channel CTU trigger */
  uint8_t ctu_trig_eTimer_ch;
} HALLParams_t, *pHALLParams_t;

/**
  * @}
  */


/*===============================================================================================
*                                      FUNCTION PROTOTYPES
===============================================================================================*/

/** @defgroup HALL_class_exported_methods HALL class exported methods
  * @{
  */

CHALL_SPD HALL_NewObject(pSpeednPosFdbkParams_t pSpeednPosFdbkParams, pHALLParams_t pHALLParams);

/**
  * @}
  */
  
/**
  * @}
  */

/**
  * @}
  */

#endif /*__HALL_SPEEDNPOSFDBKCLASS_H*/
