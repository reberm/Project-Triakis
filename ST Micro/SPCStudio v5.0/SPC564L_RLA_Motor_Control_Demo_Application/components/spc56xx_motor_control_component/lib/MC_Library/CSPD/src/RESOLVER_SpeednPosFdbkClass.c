/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    RESOLVER_SpeednPosFdbkClass.c
*   @version BETA 0.9.1
*   @brief   This file contains private implementation of RESOLEVR class.
*          
*   @details Resolver implementation.
*
*/

/** @addtogroup SPC5_PMSM_MC_Library
  * @{
  */

/** @addtogroup SpeednPosFdbk_RESOLVER
  * @{
*/

/**
*   @defgroup RESOLVER RESOLVER Sensor Description
*   @brief   RESOLVER sensor implementation
*   @details A resolver is an absolute mechanical position sensor used, for example, in Electric Power
*            Steering (EPS). Resolver hardware can be viewed as two inductive position sensors, which,
*            upon a supplied sinusoidal-shaped signal on input, generate two sinusoidal signals on output.
*            The sinusoidal input reference is amplitude modulated with the sine and cosine of the
*            mechanical angle, respectively. These two output signals have to be decoded to obtain the
*            angular position.
*            \image html resolver_figure1.png "Resolver block diagram"
*            \image html resolver_figure2.png "Resolver feedback signals"
*            This task is performed by means of Resolver (CRES_SPD) class, and shall be carried out at
*            the first motor startup.\n\n\n
*            The CRES_SPD derived class use the eTimer, ADC and CTU hardware peripherals.
*            - The Resolver has been implemented inside a system where the PWM duty cycles are
*              updates every two PWM periods. The MRS signal generated from the FlexPWM module
*              is internally routed to the Cross Triggering Unit (CTU) module. The trigger events T1CR
*              and T2CR are used to generate a square wave signal (exciting signal for resolver),
*              synchronized with MRS. 
*            - The eTimer module is used for generation of an exciting signal of resolver. The purpose
*              is to generate a square wave signal with frequency 10kHz (configurable) which is then
*              processed by a hardware low pass filter. Because of the low pass filter, the resulting
*              harmonic signal is phase shifted. 
*            - The CTU Scheduler subUnit (SU) generates the following trigger events:
*              -# eTimer channel 1 output:T1CR generates eTimer1 event output, which toggles its output
*                 to generate rising edge of resolver exciting signal. T1CR is phase shifted to account for
*                 the delay casued by resolver HW circuitry.
*              -# eTimer channel 2 output:T2CR generates eTimer1 event output, which toggles its output
*                 to the generate falling edge of the resolver exciting signal.
*              -# ADC command output: T3CR generates ADC command event for ADC: when a T3CR
*                 trigger events occurs, it generates the ADC start of conversion request to sample both
*                 resolver signals at point of their maximal amplitude variation.
*            - For correct calculation, the Resolver measurements of channel A (SIN) and channel B
*             (COS) must be executed at the same time. To do this, the CTU ADC command register
*             is configured in DUAL CONVERSION.
*            - This sampling/conversion is triggered by the CTU trigger T3CR, which is delayed in time
*              to sample both resolver signals at the point of their maximal amplitude variation, see below
*              figure.\n
*
*            \image html resolver_figure3.png "Resolver trigger"
*            CTU triggers T1CR and T2CR generate the resolver exciting signal phase shifted to fulfill
*            the following requirements:\n
*            - The point where amplitude of both resulting resolver signals varies the most must be as
*              close as possible before phase currents measurement to ensure position and current
*              samples correspond to each other.
*            - Keep 50% duty-cycle ratio of the resolver exciting signal.\n\n
*
*            The angular position can be obtain using the following two methods:\n
*            - Lookup table 
*            - Arctan
*/ 

/**
  * @}
  */
  
/**
  * @}
  */

/*
* @page misra_violations MISRA-C:2004 violations
*
* @section Resolver_SpeednPosFdbkClass_REF_1
* Violates MISRA 2004 Required Rule 11.1, Cast from unsigned int to pointer.
* This rule can not be avoided because it appears when addressing memory mapped registers or other hardware specific feature
*
* @section Resolver_SpeednPosFdbkClass_REF_2
* Violates MISRA 2004 Advisory Rule 16.7,
* A pointer parameter in a function prototype should be declared as
* pointer to constif the pointer is not used to modify the addressed
* object. This rule can not be avoided because this pointer is used also to modify the addressed object.
*
* @section Resolver_SpeednPosFdbkClass_REF_3
* Violates MISRA 2004 Required Rule 1.2, Place reliance on undefined or unspecified behaviour.
* Violation is needed to implement Data Hiding. It is used to preventing the unintended  changes on private 
* parameters and functions.
*
* @section Resolver_SpeednPosFdbkClass_REF_4
* Violates MISRA 2004 Advisory Rule 11.4, Cast from pointer to pointer. 
* Violation is needed to implement Data Hiding. It is used to preventing the unintended  changes on private 
* parameters and functions.
*
* @section Resolver_SpeednPosFdbkClass_REF_5
* Violates MISRA 2004 Required Rule 10.1,
* Cast from integer type to different type or a wider integer type
* This is used for the Speed optimization of the memory access. 
*
* @section Resolver_SpeednPosFdbkClass_REF_6
* Violates MISRA 2004 Required Rule 10.3 , The value of a complex expression of integer type may only be cast
* to a type that is narrower and of the same signedness as the underlying type of the expression
* This violation is due to the value of a complex expression of integer type was cast to a type that is
* not narrower.
*
* @section Resolver_SpeednPosFdbkClass_REF_7
* Violates MISRA 2004 Required Rule 10.5, if the  bitwise operator ~ and << are applied to an operand of
* underlying type unsigned char or unsigned short the result shall be immediately cast to the undrlying type
* of the operand
*
*/

/*===============================================================================================
*                                         INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
===============================================================================================*/
#include "Reg_eSys_eTimer.h"
#include "Reg_eSys_ADCDig.h"
#include "Reg_eSys_CTU2.h"
#include "SpeednPosFdbkClass.h"
#include "SpeednPosFdbkPrivate.h"
#include "RESOLVER_SpeednPosFdbkClass.h"
#include "RESOLVER_SpeednPosFdbkPrivate.h"
#include "Drive parameters.h"
#include "Dmamux_LLD.h"
#include "Dma_LLD.h"
#include "MCLibraryConf.h"
#include "MC_type.h"
#include "GFLIB_AtanYX.h"

/*===============================================================================================
*                          LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
===============================================================================================*/

/*===============================================================================================
*                                       CONSTANTS
===============================================================================================*/
#define DCLASS_PARAM (((_DCRES_SPD)(((_CSPD) this)->DerivedClass))->pDParams_str)
#define DCLASS_VARS  (&(((_DCRES_SPD)(((_CSPD) this)->DerivedClass))->DVars_str))
#define  CLASS_VARS  (&(((_CSPD)this)->Vars_str))
#define  CLASS_PARAM (((_CSPD)this)->pParams_str)

#define RES_PICTUS_TRIG_2_ETIMER_0 (0x00120000UL)
#define RES_PICTUS_TRIG_1_ETIMER_0 (0x00001200UL)

#define RES_PICTUS_TRIG_2_ETIMER_1 (0x00140000UL)
#define RES_PICTUS_TRIG_1_ETIMER_1 (0x00001400UL)

#define RES_PICTUS_TRIG_3_ADC (0x11000000UL)

#if defined ADC_USER_ENABLE
#define RES_TRIGGER_ADC_T3_INDEX (0x05000000UL)
#else
#define RES_TRIGGER_ADC_T3_INDEX (0x04000000UL)
#endif

#ifdef MC_CLASS_DYNAMIC
    #include "stdlib.h" /* Used for dynamic allocation */
#endif
/*===============================================================================================
*                                       LOCAL VARIABLES
===============================================================================================*/

/*===============================================================================================
*                                       GLOBAL CONSTANTS
===============================================================================================*/
sint32 Res_current_speed;

/**
* @brief Resolver CTU buffers
*/
uint32 ResCTUFifoBuffer[2];

/* Only for debug purpose */
#define RES_ANGLE_DEBUG

/*===============================================================================================
*                                       GLOBAL VARIABLES
===============================================================================================*/
#if defined(RES_ANGLE_DEBUG)
uint16 ResTmp=0U;

#if defined(RES_ANGLE_CALC_LOOKUP_TABLE)
uint16 ResBufTestSin[1000];
uint16 ResBufTestCos[1000];
#else
sint32 ResBufTestSin[1000];
sint32 ResBufTestCos[1000];
#endif  
uint32 Res_angle_buffer_count_debug=0U;
sint16 Res_angle_buffer_debug[2000];
#endif

/*===============================================================================================
*                                   LOCAL FUNCTION PROTOTYPES
===============================================================================================*/
static void Res_Init(CSPD this);
static void Res_Clear(CSPD this);
static sint16 Res_CalcAngle(CSPD this, const void *pInputVars_str);
static bool Res_CalcAvrgMecSpeed01Hz(CSPD this, sint16 *pMecSpeed01Hz);
static void Res_SetMecAngle(CSPD this, sint16 hMecAngle);
static void Res_eTimer_Exciting_Signal(uint8 eTimer_ModuleID, uint8 channel);
static void Res_CTU_Init(CSPD this, uint8 eTimer_ModuleID);

/*===============================================================================================
*                                       LOCAL FUNCTIONS
===============================================================================================*/

/*===============================================================================================
*                                       GLOBAL FUNCTIONS
===============================================================================================*/

/**
* @brief        Creates an object of the class RESOLVER
*
* @param[in]    pSpeednPosFdbkParams pointer to an SpeednPosFdbk parameters structure
* @param[in]    pRESOLVERParams pointer to an RESOLVER parameters structure
*
* @return       CENC_SPD new instance of RESOLVER object
*
* @api
*
*/
CRES_SPD Res_NewObject(pSpeednPosFdbkParams_t pSpeednPosFdbkParams, pRESOLVERParams_t pRESOLVERParams)
{
    _CSPD oSpeednPosFdbk;
    _DCRES_SPD oRESOLVER;

#ifndef MC_CLASS_DYNAMIC
    static _DCRES_SPD_t Res_SPDpool[MAX_RES_SPD_NUM];
    static uint8 Res_SPD_Allocated = 0u;
#endif

    /* @violates @ref Resolver_SpeednPosFdbkClass_REF_3 place reliance on undefined or unspecified behavior. */
    /* @violates @ref Resolver_SpeednPosFdbkClass_REF_4 Cast from pointer to pointer. */
    oSpeednPosFdbk = (_CSPD)SPD_NewObject(pSpeednPosFdbkParams);

    #ifdef MC_CLASS_DYNAMIC
        oRESOLVER = (_DCRES_SPD)calloc(1u,sizeof(_DCRES_SPD_t));
    #else
        if (Res_SPD_Allocated  < MAX_RES_SPD_NUM)
        {           
            oRESOLVER = &Res_SPDpool[Res_SPD_Allocated];
            Res_SPD_Allocated = Res_SPD_Allocated  + 1U;
        }
        else
        {
            oRESOLVER = (_DCRES_SPD)MC_NULL;
        }
    #endif
  
    oRESOLVER->pDParams_str = pRESOLVERParams;
    oSpeednPosFdbk->DerivedClass = (void*)oRESOLVER;

    oRESOLVER->DVars_str.pole_pair_count_max = POLE_PAIR_NUM*2 - 1;

    oRESOLVER->DVars_str.pole_pair_ranges[0].adc_min_val = oRESOLVER->pDParams_str->hResMinADCChannelA;
    oRESOLVER->DVars_str.pole_pair_ranges[0].adc_max_val = oRESOLVER->pDParams_str->hResMaxADCChannelA;
    oRESOLVER->DVars_str.pole_pair_ranges[1].adc_min_val = oRESOLVER->pDParams_str->hResMinADCChannelB;
    oRESOLVER->DVars_str.pole_pair_ranges[1].adc_max_val = oRESOLVER->pDParams_str->hResMaxADCChannelB;

    oRESOLVER->DVars_str.pole_pair_ranges[0].adc_range =
            oRESOLVER->DVars_str.pole_pair_ranges[0].adc_max_val -
            oRESOLVER->DVars_str.pole_pair_ranges[0].adc_min_val;

    oRESOLVER->DVars_str.pole_pair_ranges[0].adc_midrange =
            oRESOLVER->DVars_str.pole_pair_ranges[0].adc_range/2;

    oRESOLVER->DVars_str.pole_pair_ranges[1].adc_range =
            oRESOLVER->DVars_str.pole_pair_ranges[1].adc_max_val -
            oRESOLVER->DVars_str.pole_pair_ranges[1].adc_min_val;

    oRESOLVER->DVars_str.pole_pair_ranges[1].adc_midrange =
            oRESOLVER->DVars_str.pole_pair_ranges[1].adc_range/2;

    oSpeednPosFdbk->Methods_str.pSPD_Init = &Res_Init;
    oSpeednPosFdbk->Methods_str.pSPD_Clear = &Res_Clear;
    oSpeednPosFdbk->Methods_str.pSPD_CalcAngle = &Res_CalcAngle;
    oSpeednPosFdbk->Methods_str.pSPD_CalcAvrgMecSpeed01Hz =&Res_CalcAvrgMecSpeed01Hz;
    oSpeednPosFdbk->Methods_str.pSPD_SetMecAngle =&Res_SetMecAngle;
    /* @violates @ref Resolver_SpeednPosFdbkClass_REF_3 place reliance on undefined or unspecified behavior. */
    /* @violates @ref Resolver_SpeednPosFdbkClass_REF_4 Cast from pointer to pointer. */       
    return ((CRES_SPD)oSpeednPosFdbk);
}

/**
* @brief        It initializes the hardware peripherals required for 
*               the speed position sensor management using RESOLVER 
*               sensors.
*
* @param[in]    this related object of class CSPD
*
* @return       void
*
* @api
*
*/
static void Res_Init(CSPD this)
{
  /* @violates @ref Resolver_SpeednPosFdbkClass_REF_3 place reliance on undefined or unspecified behavior. */
  /* @violates @ref Resolver_SpeednPosFdbkClass_REF_4 Cast from pointer to pointer. */       
  pDParams_t pDParams_str = DCLASS_PARAM;

  uint8 eTimer_ModuleID = pDParams_str->eTimerModule;
  
  /* Initialize CTU hardware (Resolver) */
  Res_CTU_Init(this, eTimer_ModuleID);
  
  /* generation of an exciting signal */
  Res_eTimer_Exciting_Signal(eTimer_ModuleID, pDParams_str->RES_Excitation_eTimer_ch);
}

/**
* @brief        This function must be called before starting the motor to initialize
*               the speed measurement process.
*
* @param[in]    this related object of class CSPD
*
* @return       void
*
* @api
*
*/
static void Res_Clear(CSPD this)
{

  /* @violates @ref Resolver_SpeednPosFdbkClass_REF_3 place reliance on undefined or unspecified behavior. */
  /* @violates @ref Resolver_SpeednPosFdbkClass_REF_4 Cast from pointer to pointer. */       
  pVars_t pVars_str = CLASS_VARS;  
    
  pVars_str->hMecAccel01HzP = 0;
}

static void Res_UpdatePolePairRanges(CSPD this, const void *pInputVars_str) {

    uint16 * adc_a = &(DCLASS_VARS->current_adc_values[0]);
    uint16 * adc_b = &(DCLASS_VARS->current_adc_values[1]);

    if (DCLASS_VARS->pole_pair_ranges[0].adc_max_val < *adc_a)
    { *adc_a = (uint16) (DCLASS_VARS->pole_pair_ranges[0].adc_max_val);}
    if (DCLASS_VARS->pole_pair_ranges[0].adc_min_val > *adc_a)
    { *adc_a = (uint16) (DCLASS_VARS->pole_pair_ranges[0].adc_min_val);}

    if (DCLASS_VARS->pole_pair_ranges[1].adc_max_val < *adc_b)
    { *adc_b = (uint16)  DCLASS_VARS->pole_pair_ranges[1].adc_max_val;}
    if (DCLASS_VARS->pole_pair_ranges[1].adc_min_val > *adc_b)
    { *adc_b = (uint16) DCLASS_VARS->pole_pair_ranges[1].adc_min_val;}

}


/**
* @brief        It calculates the rotor electrical and mechanical angle.
*
* @param[in]    this related object of class CSPD
* @param[in]    pInputVars_str Not used in this derived class implementation of
*               SPD_CalcElAngle
*
* @return       Measured electrical angle in s16degree format.
*
* @api
*
*/ 
static sint16 Res_CalcAngle(CSPD this, const void *pInputVars_str)
{
    sint16 htemp1;
    uint16 res_ch_b, res_ch_a;
    static sint32 adc_range_a_val, adc_range_b_val;
#if defined(RES_SIN_PH_EN)
    sint32 m, n, s32InY;
    sint32 motorAngle = 0;
#endif  
    sint32 s32InX,s32InXY;
    
    /* Computes and stores the rotor electrical angle */
    /* COS */
    res_ch_a = (uint16)((ResCTUFifoBuffer[1] & 0x00007FE0UL) >> 5);

    /* SIN */
    res_ch_b = (uint16)((ResCTUFifoBuffer[0] & 0x00007FE0UL) >> 5);

#if defined(RES_ANGLE_DEBUG) 
#if defined(RES_ANGLE_CALC_LOOKUP_TABLE)
    if(ResTmp < 1000U)
    {
       ResBufTestSin[ResTmp] = res_ch_b;
       ResBufTestCos[ResTmp] = res_ch_a;
       ResTmp++;
       if(ResTmp == 1000U)
       {
          ResTmp= 0U;
       }
    }
#endif 
#endif 

#ifdef RES_ANGLE_CALC_LOOKUP_TABLE
  if(res_ch_a > 512U)
  {
     htemp1 = arcsin_pos[res_ch_b];
  }
  else
  {
     htemp1= arcsin_neg[res_ch_b];
  }
 
#else
    DCLASS_VARS->current_adc_values[0] = res_ch_a;
    DCLASS_VARS->current_adc_values[1] = res_ch_b;

    Res_UpdatePolePairRanges(this, pInputVars_str);

    res_ch_a = DCLASS_VARS->current_adc_values[0];
    res_ch_b = DCLASS_VARS->current_adc_values[1];

    adc_range_a_val = DCLASS_VARS->pole_pair_ranges[0].adc_range;
    adc_range_b_val = DCLASS_VARS->pole_pair_ranges[1].adc_range;

    s32InX = (((((res_ch_a - DCLASS_VARS->pole_pair_ranges[0].adc_min_val)-adc_range_a_val/2) << 21)-1)/adc_range_a_val/2);
    s32InXY= (((((res_ch_b - DCLASS_VARS->pole_pair_ranges[1].adc_min_val)-adc_range_b_val/2) << 21)-1)/adc_range_b_val/2);
  
#if defined(RES_ANGLE_DEBUG)    
    if(ResTmp < 1000U)
    {
       ResBufTestSin[ResTmp] = s32InXY;
       ResBufTestCos[ResTmp] = s32InX;
       ResTmp++;
       if(ResTmp == 1000U)
       {
          ResTmp= 0U;
       }
    }
#endif    

#if defined(RES_SIN_PH_EN)
    m = DCLASS_PARAM->RES_COS_PHASE_CH_B;
    n = DCLASS_PARAM->RES_SIN_PHASE_CH_B;   

    s32InY = (s32InX * m - (s32InXY << 10)) / n;
    motorAngle = 0;
    htemp1 = (GFLIB_AtanXY(s32InX << 10, s32InY << 10) >> 16) + DCLASS_PARAM->hPhaseShift;
    motorAngle = (tS32) htemp1;
    htemp1 = (int16_t)(motorAngle*DCLASS_PARAM->hResPole2MotorDegreesMultiplier/1000);
#else
    htemp1 = (GFLIB_AtanXY(s32InX << 10, s32InXY << 10) >> 16) + DCLASS_PARAM->hPhaseShift; 
#endif

#endif /* #ifdef RES_ANGLE_CALC_LOOKUP_TABLE */
  
    if (DCLASS_PARAM->RES_Angle_Reading_Type == RES_ELECTRICAL_DEGREES) 
    {
       /* If we are using the Resolver in "Electrical Degrees" reading mode we don't need to do */
       /* any modification to the angle read */
       /* @violates @ref Resolver_SpeednPosFdbkClass_REF_3 place reliance on undefined or unspecified behavior. */
       /* @violates @ref Resolver_SpeednPosFdbkClass_REF_4 Cast from pointer to pointer. */
       ((_CSPD)this)->Vars_str.hElAngle =(sint16)(htemp1);

       /* If we are using the Resolver in "Electrical Degrees" reading mode we NEED to compute */
       /* the mechanical degrees by doing (htemp1 MOD POLE_PAIRS) but this needs to be implemented yet. */
       /* @violates @ref Resolver_SpeednPosFdbkClass_REF_3 place reliance on undefined or unspecified behavior. */
       /* @violates @ref Resolver_SpeednPosFdbkClass_REF_4 Cast from pointer to pointer. */
       ((_CSPD)this)->Vars_str.hMecAngle = htemp1; 
    } 
    else 
    {
       /* If we are using the Resolver in "Mechanical Degrees" reading mode we need to multiply */
       /* the angle read by the ElToMecRatio                                                    */
       /* @violates @ref Resolver_SpeednPosFdbkClass_REF_3 place reliance on undefined or unspecified behavior. */
       /* @violates @ref Resolver_SpeednPosFdbkClass_REF_4 Cast from pointer to pointer. */
       ((_CSPD)this)->Vars_str.hElAngle = (sint16)(htemp1) * (sint32)(((_CSPD)this)->pParams_str->bElToMecRatio);
       /* @violates @ref Resolver_SpeednPosFdbkClass_REF_3 place reliance on undefined or unspecified behavior. */
       /* @violates @ref Resolver_SpeednPosFdbkClass_REF_4 Cast from pointer to pointer. */
       ((_CSPD)this)->Vars_str.hMecAngle = htemp1;
    }

#if defined(RES_ANGLE_DEBUG)
    if (Res_angle_buffer_count_debug < 2000U)
    {
    Res_angle_buffer_debug[Res_angle_buffer_count_debug] = htemp1;
    Res_angle_buffer_count_debug++;
    }
    if (Res_angle_buffer_count_debug == 2000U)
    {
       Res_angle_buffer_count_debug = 0U;
    }
#endif 
    
    /*Returns rotor electrical angle*/
    return(htemp1);
}

/**
* @brief       This method must be called with the periodicity defined by parameter
*              hSpeedSamplingFreq01Hz. The method computes & stores average mechanical 
*              speed [01Hz], computes & stores average mechanical acceleration 
*              [01Hz/SpeedSamplingFreq], computes & stores the instantaneous electrical 
*              speed [dpp], updates the index of the speed buffer, then checks & stores 
*              & returns the reliability state of the sensor.
*
* @param[in]    this related object of class CSPD
* @param[in]    pMecSpeed01Hz pointer to sint16, used to return the rotor average
*               mechanical speed (01Hz)
*             
* @return       boolean Sensor information 
* @retval       TRUE sensor information is reliable
* @retval       FALSE sensor information is not reliable
*
* @api
*
*/
static bool Res_CalcAvrgMecSpeed01Hz(CSPD this, sint16 *pMecSpeed01Hz)
{
  static uint16 buffer_index; 
  uint32 h_angle_u_long_previous;
  static uint16 h_angle_u_previous;
  static uint32 h_angle_u_long;
  uint32 h_angle_u_diff;
    
  boolean bReliability = (boolean)TRUE;
  
  /* @violates @ref Resolver_SpeednPosFdbkClass_REF_3 place reliance on undefined or unspecified behavior. */
  /* @violates @ref Resolver_SpeednPosFdbkClass_REF_4 Cast from pointer to pointer. */       
  pDParams_t pDParams_str = DCLASS_PARAM;
  /* @violates @ref Resolver_SpeednPosFdbkClass_REF_3 place reliance on undefined or unspecified behavior. */
  /* @violates @ref Resolver_SpeednPosFdbkClass_REF_4 Cast from pointer to pointer. */       
  pDVars_t pDVars_str = DCLASS_VARS;
  /* @violates @ref Resolver_SpeednPosFdbkClass_REF_3 place reliance on undefined or unspecified behavior. */
  /* @violates @ref Resolver_SpeednPosFdbkClass_REF_4 Cast from pointer to pointer. */        
  pVars_t pVars_str = CLASS_VARS; 
 
  /* @violates @ref Resolver_SpeednPosFdbkClass_REF_3 place reliance on undefined or unspecified behavior. */
  /* @violates @ref Resolver_SpeednPosFdbkClass_REF_4 Cast from pointer to pointer. */       
  pDVars_str->h_u_angle = (uint16)(((_CSPD)this)->Vars_str.hElAngle);
  
  h_angle_u_long_previous = pDVars_str->speed_buffer[buffer_index];
                            
  if (( pDVars_str->h_u_angle > h_angle_u_previous) && 
     (( pDVars_str->h_u_angle - h_angle_u_previous) < ((uint16)0x7FFF)))
  {
     h_angle_u_long = h_angle_u_long + ((uint32)((uint32)( pDVars_str->h_u_angle) - (uint32)(h_angle_u_previous)));
  }
  if (( pDVars_str->h_u_angle > h_angle_u_previous) && 
     (( pDVars_str->h_u_angle - h_angle_u_previous) > ((uint16)0x7FFF)))
  {
     h_angle_u_long = h_angle_u_long - ((uint32)((h_angle_u_previous + (uint32)0xFFFFU) -  pDVars_str->h_u_angle));
  }
  if (( pDVars_str->h_u_angle < h_angle_u_previous) &&
     ((h_angle_u_previous -  pDVars_str->h_u_angle) > ((uint16)0x7FFF)))
  {
     h_angle_u_long = h_angle_u_long +((uint32)(( pDVars_str->h_u_angle) + (uint32) 0xFFFFU) - (uint32) (h_angle_u_previous));
  }
  if (( pDVars_str->h_u_angle < h_angle_u_previous) &&
      ((h_angle_u_previous -  pDVars_str->h_u_angle) < ((uint16)0x7FFF)))
  {
     h_angle_u_long = h_angle_u_long - ((uint32)((uint32)h_angle_u_previous - (uint32)( pDVars_str->h_u_angle)));
  }
  pDVars_str->speed_buffer[buffer_index] = h_angle_u_long;
  if (h_angle_u_long > h_angle_u_long_previous) 
  { 
       h_angle_u_diff = (h_angle_u_long - h_angle_u_long_previous);
       /* @violates @ref Resolver_SpeednPosFdbkClass_REF_6 Violates MISRA 2004 Required Rule 10.3, the value of a complex expression was cast to a type that is not narrower */
       Res_current_speed = (sint32)((h_angle_u_diff*((uint32)(pDParams_str->hSpeedSamplingFreqHz)>>1U))>>16U);     
  } /*h_angle_u_diff*(1200 /40/6)>>16 -->40=2msec/50usec  from rpm to hz 360gradi/60sec*/
  else 
  { 
       h_angle_u_diff = (h_angle_u_long_previous - h_angle_u_long);
       /* @violates @ref Resolver_SpeednPosFdbkClass_REF_6 Violates MISRA 2004 Required Rule 10.3, the value of a complex expression was cast to a type that is not narrower */
       Res_current_speed =-(sint32)((h_angle_u_diff*((uint32)(pDParams_str->hSpeedSamplingFreqHz)>>1U))>>16); 
  }
  h_angle_u_previous =  pDVars_str->h_u_angle; 
  buffer_index++; 
  if (buffer_index == 20U) 
  {
      buffer_index = 0U;
  }
  
  /* @violates @ref Resolver_SpeednPosFdbkClass_REF_3 place reliance on undefined or unspecified behavior. */
  /* @violates @ref Resolver_SpeednPosFdbkClass_REF_4 Cast from pointer to pointer. */  
  Res_current_speed /= ((sint32)(((_CSPD)this)->pParams_str->bElToMecRatio));
  
  *pMecSpeed01Hz = (sint16)Res_current_speed;
  
  /*Computes & stores mechanical acceleration */
  /* @violates @ref Resolver_SpeednPosFdbkClass_REF_3 place reliance on undefined or unspecified behavior. */
  /* @violates @ref Resolver_SpeednPosFdbkClass_REF_4 Cast from pointer to pointer. */
  pVars_str->hMecAccel01HzP = (sint16)(Res_current_speed - ((_CSPD)this)->Vars_str.hAvrMecSpeed01Hz);

  /*Stores  mechanical speed [01Hz]*/
  pVars_str->hAvrMecSpeed01Hz = (*pMecSpeed01Hz);
  
  return(bReliability);
}

/**
*  @brief  It could be used to set istantaneous information on rotor mechanical
*          angle.
*          Note: Mechanical angle management is not implemented in this 
*          version of Resolver sensor class.
*
* @param[in]    this related object of class CSPD
* @param[in]    hMecAngle istantaneous measure of rotor mechanical angle (s16degrees)
*
* @return       void
*
* @api
*
*/
static void Res_SetMecAngle(CSPD this, sint16 hMecAngle)
{
   /* @violates @ref Resolver_SpeednPosFdbkClass_REF_2 A pointer parameter in a function prototype 
   * should be declared as pointer to constif the pointer is not used to modify the addressed object */
}

/**
* @brief       This function  is used to generate a square wave signal (exciting signal for
*              resolver), synchronized with MRS.
*              Its configures the eTimer channels in Edge of secondary source
*              triggers primary count till compare.
*
* @param[in]    eTimer_ModuleID allocated for exciting signal 
* @param[in]    channel used for output signal
*
* @return       void
*
* @api
*
*/
static void Res_eTimer_Exciting_Signal(uint8 eTimer_ModuleID, uint8 channel)
{
    /* Init CNTR counter value */
    /* @violates @ref Resolver_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
    REG_WRITE16( ETIMER_CNTR(eTimer_ModuleID, channel),  (uint16)0x0000 );

    /* Init COMP1 and COMP2 counter value */
    /* @violates @ref Resolver_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
    REG_WRITE16( ETIMER_COMP1(eTimer_ModuleID, channel),  (uint16)0x0000 );
    /* @violates @ref Resolver_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
    REG_WRITE16( ETIMER_COMP2(eTimer_ModuleID, channel),  (uint16)0x0000 );

    /* Init CMPLD1 and CMPLD2 counter value */
    /* @violates @ref Resolver_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
    REG_WRITE16( ETIMER_CMPLD1(eTimer_ModuleID, channel),  (uint16)0x0000 );
    /* @violates @ref Resolver_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
    REG_WRITE16( ETIMER_CMPLD2(eTimer_ModuleID, channel),  (uint16)0x0000 );

    /* CNT MODE: Edge of secondaty source triggers primary count till compare  */
    /* DIRECTION: Count up                    */
    /* Count until compare, then reinitialize */
    /* PRCSRC: IP BUS                         */
    /* SECSRC: AUX 0 (output signal from CTU etimer #1 trigger event output)   */
    REG_WRITE16(ETIMER_CTRL1(eTimer_ModuleID, channel), 
               (uint16)(CTRL1_CNTMODE_ESECOND_TRIG<<CTRL1_CNTMODE_SHIFT) | 
               (uint16)(CTRL1_DIR_UP<<CTRL1_DIR_SHIFT) | 
               (uint16)(CTRL1_PRISRC_IP_DIV128 << CTRL1_PRISRC_SHIFT) |
               (uint16)(CTRL1_LENGTH_COMPARE<<CTRL1_LENGTH_SHIFT) |
               (uint16)((uint16)0x8U<<CTRL1_SECSRC_SHIFT)
    /* @violates @ref Resolver_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
    );

    /* OFLAG output signal will be driven on the external pin.     */
    REG_WRITE16(ETIMER_CTRL2(eTimer_ModuleID, channel), 
               (uint16)(CTRL2_OEN_OUTPUT<<CTRL2_OEN_SHIFT) | 
               (uint16)(CTRL2_OUTMODE_TOGGLE_CMP1_CMP2<<CTRL2_OUTMODE_SHIFT)
    /* @violates @ref Resolver_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
    );

    /* CMPMODE: COMP1 register is used when the counter is counting up.      */
    /*          COMP2 register is used when the counter is counting up.    */
    REG_WRITE16( ETIMER_CCCTRL(eTimer_ModuleID, channel), 
                 (uint16)(CCCTRL_CMPMODE_CMP1_UP_CMP2_UP<<CCCTRL_CMPMODE_SHIFT)
    /* @violates @ref Resolver_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
    );
}

/**
* @brief       This function  is used to configure the CTU for Resolver signals.
*              - CTU triggers 1 and 2 are used to generate the resolver
*                exciting signal phase shifted. The duty cycle is 50%.
*              - CTU trigger 3 is used to sampling the resolver signals at the
*                point of their maximal amplitude variation.
*
* @param[in]    eTimer_ModuleID allocated for exciting signal 
* @param[in]    channel used for output signal
*
* @return       void
*
* @api
*
*/
static void Res_CTU_Init(CSPD this, uint8 eTimer_ModuleID)
{
  
  uint32 CTU_temp; 
   
  /* @violates @ref Resolver_SpeednPosFdbkClass_REF_3 place reliance on undefined or unspecified behavior. */
  /* @violates @ref Resolver_SpeednPosFdbkClass_REF_4 Cast from pointer to pointer. */       
  pDParams_t pDParams_str = DCLASS_PARAM;
 
  /* Initialiaze DMA MUX */
  /* DMA channel for CTU FIFO 4 */
#if defined _SPC56ELxx_
  DmaMux_SetChannelRouting(pDParams_str->DmaChannel,(uint8)0x10,(uint8)STD_ON, (uint8)STD_OFF);
#else
  DmaMux_SetChannelRouting(pDParams_str->DmaChannel,(uint8)0x0D,(uint8)STD_ON, (uint8)STD_OFF);
#endif
  
  /* Enable DMA for FIFO 3 */
  /* @violates @ref Resolver_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
  REG_WRITE16(CTUV2_FDCR, (REG_READ16(CTUV2_FDCR) | 0x08U));
 
  /* Write the Trigger Compare Registers 1 to generate eTimer event output, which toggles */
  /* its output to generate rising edge of resolver exciting signal. T1CR is phase        */
  /* shifted to account for the delay casued by resolver HW circuitry.                    */
  /* @violates @ref Resolver_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer   */
  /* @violates @ref Resolver_SpeednPosFdbkClass_REF_7 cast after bitwise operation        */
  REG_WRITE16(CTUV2_TxCR(1U), (uint16)(pDParams_str->RES_Filtering_shift)); 
  /* Write the Trigger Compare Registers 2 to generate eTimer event output, which toggles */
  /* its output to generate falling edge of resolver exciting signal.                     */
  REG_WRITE16(CTUV2_TxCR(2U), (pDParams_str->RES_Filtering_shift-1U) +
  /* @violates @ref Resolver_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer   */  
  /* @violates @ref Resolver_SpeednPosFdbkClass_REF_7 cast after bitwise operation        */  
             (pDParams_str->RES_Excitation_Signal_freq >> 0x1U)); 
  
  /* Write the Trigger Compare Registers in order to start the ADC command                       */
  /* T3CR - Trigger 3 compare register value 3 for ADC:  when a T3CR trigger events occurs,      */
  /* it generates the ADC start of conversion request to sample both resolver signals at point   */
  /* of their maximal amplitude variation.                                                       */
  /* @violates @ref Resolver_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer   */
  /* @violates @ref Resolver_SpeednPosFdbkClass_REF_7 cast after bitwise operation        */    
  REG_WRITE16(CTUV2_TxCR(3U), (pDParams_str->RES_Excitation_Signal_freq - 800U)); /* Max 11200 */
  
  /* Configure trigger handler */
  /* @violates @ref Resolver_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
  CTU_temp = REG_READ32(CTUV2_THCR1);  
  
#if defined _SPC56ELxx_  
  /* TBD */
#else
  if(eTimer_ModuleID == RES_ETIMER_Module_0)
  {
     /* Enable Trigger 1 and 2 output enable for etimer 0 */
     /* Enable Trigger 3 ADC command for resolver signal  */
     CTU_temp = ((CTU_temp & 0xFFFFFFFDUL) | RES_PICTUS_TRIG_1_ETIMER_0 | 
                 RES_PICTUS_TRIG_2_ETIMER_0 | RES_PICTUS_TRIG_3_ADC);
  }
  else
  {
     /* Enable Trigger 1 and 2 output enable for etimer 1 */
     /* Enable Trigger 3 ADC command for resolver signal  */     
     CTU_temp = ((CTU_temp & 0xFFFFFFFBUL) | RES_PICTUS_TRIG_1_ETIMER_1 | 
                 RES_PICTUS_TRIG_2_ETIMER_1 | RES_PICTUS_TRIG_3_ADC) ; 
  }
#endif  

  /* @violates @ref Resolver_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
  REG_WRITE32(CTUV2_THCR1, CTU_temp);
  /* @violates @ref Resolver_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
  CTU_temp = REG_READ32(CTUV2_CLCR1);
  
  /* Program the command list control register */
  /* Address command register 1 (CLR1): Command List address 2 for Resolver */
  /* @violates @ref Resolver_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
  REG_WRITE32(CTUV2_CLCR1, CTU_temp | RES_TRIGGER_ADC_T3_INDEX);
  /* @violates @ref Resolver_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
  CTU_temp = REG_READ32(CTUV2_FTH);
    
  /* Specify which fifo has what threshold                                            */
  /* Threshold value of the fifo. FIFOs generate an interrupt when                    */ 
  /* the number of elements in the FIFO exceeds the values present in                 */
  /* the threshold register. Threshold must have a maximum value of ((FIFO depth)-1). */
  /* FIFO is configured as 2 depth, so threshold is configured: 1. (dual conversion   */ 
  /* channels for Resolver signals). */
  /* @violates @ref Resolver_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
  REG_WRITE32(CTUV2_FTH, CTU_temp | 0x01000000UL);
 
#ifndef _SPC560PXX_SMALL_
  /* Program the CTU ADC command register 4 in DUAL CONVERSION */
  /* - Command Interrupt Request: disabled                     */
  /* - Not first command                                       */
  /* - Dual conversion mode                                    */
  /* - FIFO for ADC unit A/B: FIFO 3                           */
  /* - ADC unit A channel (Res_SIN_ADC_0_Channel) and ADC unit */ 
  /*   B channel (Res_COS_ADC_1_Channel)                       */    
  REG_WRITE16(CTUV2_CLRx(4), CTUV2_CLR_CIR_DISABLE | CTUV2_CLR_LC_NOT_LAST | CTUV2_CLR_CMS_DUAL |
              CTUV2_CLR_FIFO(3) | CTUV2_SU_CH_A(pDParams_str->Res_SIN_ADC_0_Channel) |
              CTUV2_SU_CH_B(pDParams_str->Res_COS_ADC_1_Channel)
  /* @violates @ref Resolver_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
  );

  /* Program the CTU ADC command register Resolver                    */
  /* for regular conversion                                           */
  /* - Command Interrupt Request: disabled,                           */
  /* - Last command                                                   */
  REG_WRITE16(CTUV2_CLRx(5), CTUV2_CLR_CIR_DISABLE | CTUV2_CLR_LC_LAST | CTUV2_CLR_CMS_SINGLE |
              CTUV2_CLR_FIFO(3));

#endif

#ifdef _SPC560PXX_SMALL_
  /* Program the CTU ADC command register 0 in SINGLE CONVERSION   */
  /* - Command Interrupt Request: disabled,                        */
  /* - First command                                               */
  /* - Single conversion mode                                      */
  /* - FIFO for ADC: FIFO 3                                        */
  /* - ADC channel (hIaChannel)                                    */
  REG_WRITE16(CTUV2_CLRx(5), CTUV2_CLR_CIR_DISABLE | CTUV2_CLR_LC_NOT_LAST | CTUV2_CLR_CMS_SINGLE |
              CTUV2_CLR_FIFO(3) | CTUV2_SU_CH(pDParams_str->Res_SIN_ADC_0_Channel)
  );

  /* Program the CTU ADC command register 1 in SINGLE CONVERSION   */
  /* - Command Interrupt Request: disabled,                        */
  /* - First command                                               */
  /* - Single conversion mode                                      */
  /* - FIFO for ADC: FIFO 3                                        */
  /* - ADC channel (hIbChannel)                                    */
  REG_WRITE16(CTUV2_CLRx(6), CTUV2_CLR_CIR_DISABLE | CTUV2_CLR_LC_NOT_LAST | CTUV2_CLR_CMS_SINGLE |
              CTUV2_CLR_FIFO(3) | CTUV2_SU_CH(pDParams_str->Res_COS_ADC_1_Channel)
  );

  REG_WRITE16(CTUV2_CLRx(7), CTUV2_CLR_CIR_DISABLE | CTUV2_CLR_LC_LAST | CTUV2_CLR_CMS_SINGLE |
              CTUV2_CLR_FIFO(3)
  );

#endif

  /* Set-up DMA TCD descriptor */
  Dma_configure_channel(
  /* dma_channel */
  (uint8)pDParams_str->DmaChannel,
  /* src_addr - FIFO 1 start addres (index) */
  (uint32 )CTUV2_FIFO_ADDR(3),
  /* src_transfer_size */
  DMA_DATA_TRANSFER_32_BIT,
  /* dest_transfer_size */
  DMA_DATA_TRANSFER_32_BIT,
  /* src_next_offset */
  (uint16)0,
  /* n_bytes_to_transfer */
  (uint16)((uint16)CTUV2_DMA_TRANSFER_WORD_SIZE*(2)),
  /* src_last_adj */
  (uint32)(0),
  /* dest_addr */
  (uint32)(ResCTUFifoBuffer),
  /* current_iteration_count */
  (uint16)1,
  /* dest_next_offset */
  (uint16)(CTUV2_DMA_TRANSFER_WORD_SIZE),
  /* dest_last_adj - adjustment will be done in the dma completion isr
     when all samples have been completed */
  (uint32)((sint32)-8),
  /* begin_iteration_count */
  (uint16)1,
  /* @violates @ref Resolver_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
  /* @violates @ref Resolver_SpeednPosFdbkClass_REF_5 cast from integer type to a other type or a wider integer type */  
  (uint8)DMA_TCD_INT_MAJOR);
  
  /* Update the double buffered registers */
  /* @violates @ref Resolver_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */  
  REG_WRITE16(CTUV2_CTUCR, CTUV2_CTUCR_GRE);
  
  /* Enable HW DMA request */
  /* @violates @ref Resolver_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
  LLM_Wr_DMA_SERQ(pDParams_str->DmaChannel);
  /* @violates @ref Resolver_SpeednPosFdbkClass_REF_2 A pointer parameter in a function prototype 
   * should be declared as pointer to constif the pointer is not used to modify the addressed object */  
}


  
