/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    RESOLVER_SpeednPosFdbkClass.h
*   @version BETA 0.9.1
*   @brief   This file contains interface of RESOLVER class.
*          
*   @details Resolver implementation.
*
*/
#ifndef __RESOLVER_SPEEDNPOSFDBKCLASS_H
#define __RESOLVER_SPEEDNPOSFDBKCLASS_H

/*===============================================================================================
*                                         INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
===============================================================================================*/


/*===============================================================================================
*                                       CONSTANTS
===============================================================================================*/
#define RES_ETIMER_Module_0 ((uint8_t)(0))
#define RES_ETIMER_Module_1 ((uint8_t)(1))

/*===============================================================================================
*                          LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
===============================================================================================*/

/** @addtogroup SPC5_PMSM_MC_Library
  * @{
  */
  
/** @addtogroup SpeednPosFdbk_RESOLVER
  * @{
  */

/** @defgroup RESOLVER_class_exported_types RESOLVER class exported types
* @{
*/

/** 
  * @brief  Public RESOLVER class definition
  *
  */
typedef struct CRES_SPD_t *CRES_SPD;

/** 
  * @brief  RESOLVER class parameters definition
  *
  */
typedef const struct
{
  /* HW Settings */
  /** @brief  Timer used for RESOLVER sensor management.*/
  uint8_t eTimerModule;
  /** @brief  Resolver excitation eTimer channel */
  uint8_t RES_Excitation_eTimer_ch;
  /** @brief  Resolver Filtering shift */
  uint16_t RES_Filtering_shift;
  /** @brief  Resolver Signal Excitation frequency */
  uint16_t RES_Excitation_Signal_freq;
  /** @brief  Resolver feedback SIN signal. This signal is mapped on 
              ADC_0 UNIT. It must be in the range: ADC_AN_0 .. ADC_AN_14 */  
  uint8_t Res_SIN_ADC_0_Channel;   
  /** @brief  Resolver feedback COS signal. This signal is mapped on 
              ADC_0 UNIT. It must be in the range: ADC_AN_0 .. ADC_AN_14 */   
  uint8_t Res_COS_ADC_1_Channel; 
  /** @brief Frequency (Hz) at which motor speed is to be computed. 
      It must be equal to the frequency at which function 
      SPD_CalcAvrgMecSpeed01Hz is called.*/
  uint16_t hSpeedSamplingFreqHz; 
  /** @brief  Dma channel */
  uint8_t DmaChannel;
  /** @brief  Dma channel */
  /** @brief Define here in s16degree the electrical phase shift
      between the Resolver Channel A and
      the Bemf induced on phase A.*/
  int16_t hPhaseShift;
  /** @brief Sin of Channel B Phase is the sin (multiplied by 2014)
      of the offset angle between channel A and channel B.
      e.g. phase = 120 -> RES_SIN_PHASE_CH_B =  -0.5 * 1024 = 512 */
  int32_t RES_SIN_PHASE_CH_B;
  /** @brief Sin of Channel B Phase is the sin (multiplied by 2014)
       of the offset angle between channel A and channel B.
       e.g. phase = 120 -> RES_COS_PHASE_CH_B =  -0.886 * 1024 = 887 */
   int32_t RES_COS_PHASE_CH_B;
   /** @brief This is RES_ELECTRICAL_DEGREE if the resolver is reading electrical degree,
       otherwise is RES_MECHANICAL_DEGREE */
   ResolverSensorType_t RES_Angle_Reading_Type;
   /** @brief This is the multiplier that convert resolver pole pair into motor pole pair */
   int32_t hResPole2MotorDegreesMultiplier;
   /** @brief This is the minimum value ranged for the wave in ADC CH A */
   int32_t hResMinADCChannelA;
   /** @brief This is the maximum value ranged for the wave in ADC CH A */
   int32_t hResMaxADCChannelA;
   /** @brief This is the minimum value ranged for the wave in ADC CH B */
   int32_t hResMinADCChannelB;
   /** @brief This is the maximum value ranged for the wave in ADC CH B */
   int32_t hResMaxADCChannelB;
}RESOLVERParams_t, *pRESOLVERParams_t;
/**
  * @}
  */
  
/*===============================================================================================
*                                      FUNCTION PROTOTYPES
===============================================================================================*/
/** @defgroup RESOLVER_class_exported_methods RESOLVER class exported methods
  * @{
  */

CRES_SPD Res_NewObject(pSpeednPosFdbkParams_t pSpeednPosFdbkParams, pRESOLVERParams_t pRESOLVERParams);

/**
  * @}
  */
  
/**
  * @}
  */

/**
  * @}
  */

#endif /*__RESOLVER_SPEEDNPOSFDBKCLASS_H*/
