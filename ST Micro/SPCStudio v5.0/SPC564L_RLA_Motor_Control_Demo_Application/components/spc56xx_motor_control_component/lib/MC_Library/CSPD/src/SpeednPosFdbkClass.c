/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    SpeednPosFdbkClass.c
*   @version BETA 0.9.1
*   @brief   This file contains implementation of Speed Position Feedback class.
*          
*   @details This file contains implementation of Speed Position Feedback class.
*
*/

/*
* @page misra_violations MISRA-C:2004 violations
*
* @section SpeednPosFdbkClass_REF_1
* Violates MISRA 2004 Required Rule 1.2, Place reliance on undefined or unspecified behaviour
* Violation is needed to implement Data Hiding. It is used to preventing the unintended  changes on private 
* parameters and functions.
*
* @section SpeednPosFdbkClass_REF_2
* Violates MISRA 2004 Advisory Rule 11.4, Cast from pointer to pointer. 
* Violation is needed to implement Data Hiding. It is used to preventing the unintended  changes on private 
* parameters and functions.
*
* @section SpeednPosFdbkClass_REF_3
* Violates MISRA 2004 Advisory Rule 16.7,
* A pointer parameter in a function prototype should be declared as
* pointer to constif the pointer is not used to modify the addressed
* object. This rule can not be avoided because this pointer is used also to modify the addressed object.
*
*/

/*===============================================================================================
*                                         INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
===============================================================================================*/
#include "SpeednPosFdbkClass.h"
#include "SpeednPosFdbkPrivate.h"
#include "MCLibraryConf.h"

#ifdef MC_CLASS_DYNAMIC
  #include "stdlib.h" /* Used for dynamic allocation */
#endif

/*===============================================================================================
*                          LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
===============================================================================================*/

/*===============================================================================================
*                                       CONSTANTS
===============================================================================================*/

/*===============================================================================================
*                                       LOCAL VARIABLES
===============================================================================================*/

/*===============================================================================================
*                                       GLOBAL CONSTANTS
===============================================================================================*/

/*===============================================================================================
*                                       GLOBAL VARIABLES
===============================================================================================*/

/*===============================================================================================
*                                   LOCAL FUNCTION PROTOTYPES
===============================================================================================*/

/*===============================================================================================
*                                       LOCAL FUNCTIONS
===============================================================================================*/

/*===============================================================================================
*                                       GLOBAL FUNCTIONS
===============================================================================================*/

/**
* @brief        Creates an object of the class SpeednPosFdbk
*
* @param[in]    pSpeednPosFdbkParams pointer to an SpeednPosFdbk parameters structure
*
* @return       CSPD new instance of SpeednPosFdbk object
*
* @api
*
*/
CSPD SPD_NewObject(pSpeednPosFdbkParams_t pSpeednPosFdbkParams)
{
  _CSPD oSPD;

#ifndef MC_CLASS_DYNAMIC
  static _CSPD_t SPDpool[MAX_SPD_NUM];
  static  uint8_t SPD_Allocated = 0u;
#endif

  #ifdef MC_CLASS_DYNAMIC
    oSPD = (_CSPD)calloc(1u,sizeof(_CSPD_t));
  #else
    if (SPD_Allocated  < MAX_SPD_NUM)
    {
      oSPD = &SPDpool[SPD_Allocated];
      SPD_Allocated = SPD_Allocated + 1U;
    }
    else
    {
      oSPD = (_CSPD)MC_NULL;
    }
  #endif
  
  oSPD->pParams_str = pSpeednPosFdbkParams;
  /* @violates @ref SpeednPosFdbkClass_REF_1 place reliance on undefined or unspecified behavior. */
  /* @violates @ref SpeednPosFdbkClass_REF_2 Cast from pointer to pointer. */
  return (CSPD)(oSPD);
}

/**
* @brief        Initiliazes all the object variables and MCU peripherals, usually
*               it has to be called once right after object creation
*
* @param[in]    this related object of class CSPD
*
* @return       void
*
* @api
*
*/
void SPD_Init(CSPD this)
{
  /* @violates @ref SpeednPosFdbkClass_REF_1 place reliance on undefined or unspecified behavior. */
  /* @violates @ref SpeednPosFdbkClass_REF_2 Cast from pointer to pointer. */
  ((_CSPD)this)->Methods_str.pSPD_Init(this);
}

/**
* @brief        It returns the last computed rotor electrical angle, expressed in
*               s16degrees. 1 s16degree = 360 degree/65536
*
* @param[in]    this related object of class CSPD
*
* @return       the rotor electrical angle (s16degrees)
*
* @api
*
*/
int16_t SPD_GetElAngle(CSPD this)
{
  /* @violates @ref SpeednPosFdbkClass_REF_1 place reliance on undefined or unspecified behavior. */
  /* @violates @ref SpeednPosFdbkClass_REF_2 Cast from pointer to pointer. */
  return(((_CSPD)this)->Vars_str.hElAngle);
  /* @violates @ref SpeednPosFdbkClass_REF_3 A pointer parameter in a function prototype 
   * should be declared as pointer to constif the pointer is not used to modify the addressed object */
}

/**
* @brief       It returns the last computed rotor mechanical angle, expressed in
*              s16degrees. Mechanical angle frame is based on parameter bElToMecRatio
*              and, if occasionally provided - through function SPD_SetMecAngle -
*              of a measured mechanical angle, on information computed thereof.
*              Note: both Hall sensor and Sensor-less do not implement either 
*              mechanical angle computation or acceleration computation. 
*
* @param[in]    this related object of class CSPD
*
* @return       the rotor mechanical angle (s16degrees)
*
* @api
*
*/
int16_t SPD_GetMecAngle(CSPD this)
{
  /* @violates @ref SpeednPosFdbkClass_REF_1 place reliance on undefined or unspecified behavior. */
  /* @violates @ref SpeednPosFdbkClass_REF_2 Cast from pointer to pointer. */
  return(((_CSPD)this)->Vars_str.hMecAngle);
  /* @violates @ref SpeednPosFdbkClass_REF_3 A pointer parameter in a function prototype 
   * should be declared as pointer to constif the pointer is not used to modify the addressed object */
}

/**
* @brief       It returns the last computed average mechanical speed, expressed in
*              01Hz (tenth of Hertz).
*
* @param[in]    this related object of class CSPD
*
* @return       the rotor average mechanical speed (01Hz)
*
* @api
*
*/
int16_t SPD_GetAvrgMecSpeed01Hz(CSPD this)
{
  /* @violates @ref SpeednPosFdbkClass_REF_1 place reliance on undefined or unspecified behavior. */
  /* @violates @ref SpeednPosFdbkClass_REF_2 Cast from pointer to pointer. */
  return(((_CSPD)this)->Vars_str.hAvrMecSpeed01Hz);
  /* @violates @ref SpeednPosFdbkClass_REF_3 A pointer parameter in a function prototype 
   * should be declared as pointer to constif the pointer is not used to modify the addressed object */
}

/**
* @brief       It returns the last computed electrical speed, expressed in Dpp.
*              1 Dpp = 1 s16Degree/control Period. The control period is the period
*              on which the rotor electrical angle is computed (through function
*              SPD_CalcElectricalAngle).
*
* @param[in]    this related object of class CSPD
*
* @return       the rotor electrical speed (Dpp)
*
* @api
*
*/
int16_t SPD_GetElSpeedDpp(CSPD this)
{
  /* @violates @ref SpeednPosFdbkClass_REF_1 place reliance on undefined or unspecified behavior. */
  /* @violates @ref SpeednPosFdbkClass_REF_2 Cast from pointer to pointer. */
  return(((_CSPD)this)->Vars_str.hElSpeedDpp);
  /* @violates @ref SpeednPosFdbkClass_REF_3 A pointer parameter in a function prototype 
   * should be declared as pointer to constif the pointer is not used to modify the addressed object */
}

/**
* @brief       It could be used to set istantaneous information on rotor mechanical
*              angle. As a consequence, the offset relationship between electrical
*              angle and mechanical angle is computed.
*              Note: both Hall sensor and Sensor-less do not implement either 
*              mechanical angle computation or acceleration computation.
*
* @param[in]    this related object of class CSPD
* @param[in]    hMecAngle istantaneous measure of rotor mechanical angle (s16degrees)
*
* @return       void
*
* @api
*
*/
void SPD_SetMecAngle(CSPD this, int16_t hMecAngle)
{
  /* @violates @ref SpeednPosFdbkClass_REF_1 place reliance on undefined or unspecified behavior. */
  /* @violates @ref SpeednPosFdbkClass_REF_2 Cast from pointer to pointer. */
  ((_CSPD)this)->Methods_str.pSPD_SetMecAngle(this,hMecAngle);  
}

/**
* @brief       It restore all the functional variables to initial values.
* 
*
* @param[in]    this related object of class CSPD
*
* @return       void
*
* @api
*
*/
void SPD_Clear(CSPD this)
{  
  /* Clear previous speed error conditions */
  /* @violates @ref SpeednPosFdbkClass_REF_1 place reliance on undefined or unspecified behavior. */
  /* @violates @ref SpeednPosFdbkClass_REF_2 Cast from pointer to pointer. */
  ((_CSPD)this)->Vars_str.bSpeedErrorNumber = 0u;
  /* @violates @ref SpeednPosFdbkClass_REF_1 place reliance on undefined or unspecified behavior. */
  /* @violates @ref SpeednPosFdbkClass_REF_2 Cast from pointer to pointer. */
 
  ((_CSPD)this)->Methods_str.pSPD_Clear(this);
}

/**
* @brief       It returns the result of the last reliability check performed.
*              Reliability is measured with reference to parameters
*              hMaxReliableElSpeed01Hz, hMinReliableElSpeed01Hz,
*              bMaximumSpeedErrorsNumber and/or specific parameters of the derived
*              true = sensor information is reliable
*              false = sensor information is not reliable
* 
* @param[in]    this related object of class CSPD
*
* @return       bool sensor reliability state
*
* @api
*
*/
bool SPD_Check(CSPD this)
{
  bool SpeedSensorReliability = (bool)true;
  /* @violates @ref SpeednPosFdbkClass_REF_1 place reliance on undefined or unspecified behavior. */
  /* @violates @ref SpeednPosFdbkClass_REF_2 Cast from pointer to pointer. */
  if (((_CSPD)this)->Vars_str.bSpeedErrorNumber ==
       /* @violates @ref SpeednPosFdbkClass_REF_1 place reliance on undefined or unspecified behavior. */
       /* @violates @ref SpeednPosFdbkClass_REF_2 Cast from pointer to pointer. */
      ((_CSPD)this)->pParams_str->bMaximumSpeedErrorsNumber)
  {
    SpeedSensorReliability = (bool)false;
  }
  return(SpeedSensorReliability);
  /* @violates @ref SpeednPosFdbkClass_REF_3 A pointer parameter in a function prototype 
   * should be declared as pointer to constif the pointer is not used to modify the addressed object */
}

/**
* @brief        This method must be called with the same periodicity on which FOC
*               is executed. It computes and returns the rotor electrical angle,
*               expressed in s16Degrees. 1 s16Degree = 360 degree/65536
*
* @param[in]    this related object of class CSPD
* @param[in]    pInputVars_str pointer to input structure. For derived 
*               class STO input structure type is Observer_Inputs_t, for HALL and
*               ENCODER this parameter will not be used (thus it can be equal to
*               MC_NULL).
*
* @return       the rotor electrical angle (s16Degrees)
*
* @api
*
*/
int16_t SPD_CalcAngle(CSPD this, const void *pInputVars_str)
{
  /* @violates @ref SpeednPosFdbkClass_REF_1 place reliance on undefined or unspecified behavior. */
  /* @violates @ref SpeednPosFdbkClass_REF_2 Cast from pointer to pointer. */
  return(((_CSPD)this)->Methods_str.pSPD_CalcAngle(this, pInputVars_str));
}

/**
* @brief        This method must be called - at least - with the same periodicity
*               on which speed control is executed. It computes and returns - through
*               parameter pMecSpeed01Hz - the rotor average mechanical speed,
*               expressed in 01Hz. It computes and returns the reliability state of
*               the sensor; reliability is measured with reference to parameters
*               hMaxReliableElSpeed01Hz, hMinReliableElSpeed01Hz,
*               bMaximumSpeedErrorsNumber and/or specific parameters of the derived
*               true = sensor information is reliable
*              false = sensor information is not reliable
*
* @param[in]    this related object of class CSPD
* @param[in]    pMecSpeed01Hz pointer to int16_t, used to return the rotor average
*               mechanical speed (01Hz)
*
* @return       bool sensor information is reliable
*
* @api
*
*/
bool SPD_CalcAvrgMecSpeed01Hz(CSPD this, int16_t *pMecSpeed01Hz)
{
  bool SpeedSensorReliability = (bool)true;
  uint8_t bSpeedErrorNumber;
  /* @violates @ref SpeednPosFdbkClass_REF_1 place reliance on undefined or unspecified behavior. */
  /* @violates @ref SpeednPosFdbkClass_REF_2 Cast from pointer to pointer. */
  uint8_t bMaximumSpeedErrorsNumber = ((_CSPD)this)->pParams_str->bMaximumSpeedErrorsNumber;
  
  /* @violates @ref SpeednPosFdbkClass_REF_1 place reliance on undefined or unspecified behavior. */
  /* @violates @ref SpeednPosFdbkClass_REF_2 Cast from pointer to pointer. */
  if (((_CSPD)this)->Methods_str.pSPD_CalcAvrgMecSpeed01Hz(this,pMecSpeed01Hz) 
      == (bool)false)
  {
    /* If derived class speed error check is false the reliabilty of the base
       class is assert */
    bSpeedErrorNumber = bMaximumSpeedErrorsNumber;
    SpeedSensorReliability = (bool)false;
  }
  else
  {
    bool SpeedError = (bool)false;
    uint16_t hAbsMecSpeed01Hz, hAbsMecAccel01HzP;
    int16_t hAux;

    /* @violates @ref SpeednPosFdbkClass_REF_1 place reliance on undefined or unspecified behavior. */
    /* @violates @ref SpeednPosFdbkClass_REF_2 Cast from pointer to pointer. */
    bSpeedErrorNumber = ((_CSPD)this)->Vars_str.bSpeedErrorNumber;
    
    /* Compute absoulte value of mechanical speed */
    if (*pMecSpeed01Hz < 0)
    {
      hAux = -(*pMecSpeed01Hz);
      hAbsMecSpeed01Hz = (uint16_t)(hAux);
    }
    else
    {
      hAbsMecSpeed01Hz = (uint16_t)(*pMecSpeed01Hz);
    }
    
    /* @violates @ref SpeednPosFdbkClass_REF_1 place reliance on undefined or unspecified behavior. */
    /* @violates @ref SpeednPosFdbkClass_REF_2 Cast from pointer to pointer. */
    if (hAbsMecSpeed01Hz > ((_CSPD)this)->pParams_str->hMaxReliableMecSpeed01Hz)
    {
      SpeedError = (bool)true;
    }
    /* @violates @ref SpeednPosFdbkClass_REF_1 place reliance on undefined or unspecified behavior. */
    /* @violates @ref SpeednPosFdbkClass_REF_2 Cast from pointer to pointer. */
    if (hAbsMecSpeed01Hz < ((_CSPD)this)->pParams_str->hMinReliableMecSpeed01Hz)
    {
      SpeedError = (bool)true;
    }
    
    /* Compute absoulte value of mechanical acceleration */
    /* @violates @ref SpeednPosFdbkClass_REF_1 place reliance on undefined or unspecified behavior. */
    /* @violates @ref SpeednPosFdbkClass_REF_2 Cast from pointer to pointer. */
    if (((_CSPD)this)->Vars_str.hMecAccel01HzP < 0)
    {
      /* @violates @ref SpeednPosFdbkClass_REF_1 place reliance on undefined or unspecified behavior. */
      /* @violates @ref SpeednPosFdbkClass_REF_2 Cast from pointer to pointer. */
      hAux = -(((_CSPD)this)->Vars_str.hMecAccel01HzP);
      hAbsMecAccel01HzP = (uint16_t)(hAux);
    }
    else
    {
      /* @violates @ref SpeednPosFdbkClass_REF_1 place reliance on undefined or unspecified behavior. */
      /* @violates @ref SpeednPosFdbkClass_REF_2 Cast from pointer to pointer. */
      hAbsMecAccel01HzP = (uint16_t)(((_CSPD)this)->Vars_str.hMecAccel01HzP);
    }
    /* @violates @ref SpeednPosFdbkClass_REF_1 place reliance on undefined or unspecified behavior. */
    /* @violates @ref SpeednPosFdbkClass_REF_2 Cast from pointer to pointer. */
    if ( hAbsMecAccel01HzP > ((_CSPD)this)->pParams_str->hMaxReliableMecAccel01HzP)
    {
      SpeedError = (bool)true;
    }
    
    if (SpeedError == (bool)true)
    {
      if (bSpeedErrorNumber < bMaximumSpeedErrorsNumber)
      {
        bSpeedErrorNumber++;
      }
    }
    else
    {
      if (bSpeedErrorNumber < bMaximumSpeedErrorsNumber)
      {
        bSpeedErrorNumber = 0u;
      }
    }
    
    if (bSpeedErrorNumber == bMaximumSpeedErrorsNumber)
    { 
      SpeedSensorReliability = (bool)false;
    }
  }
  /* @violates @ref SpeednPosFdbkClass_REF_1 place reliance on undefined or unspecified behavior. */
  /* @violates @ref SpeednPosFdbkClass_REF_2 Cast from pointer to pointer. */
  ((_CSPD)this)->Vars_str.bSpeedErrorNumber = bSpeedErrorNumber;
  return(SpeedSensorReliability);
}

/**
* @brief         This method returns the average mechanical rotor speed expressed in
*                "S16Speed". It means that:
*                 - it is zero for zero speed,
*                 - it become S16_MAX when the average mechanical speed is equal to 
*                   hMaxReliableMecSpeed01Hz,
*                 - it becomes -S16_MAX when the average mechanical speed is equal to
*                 -hMaxReliableMecSpeed01Hz.
*
* @param[in]    this related object of class CSPD
*
* @return       The average mechanical rotor speed expressed in
*               "S16Speed".
*
* @api
*
*/
int16_t SPD_GetS16Speed(CSPD this)
{
  /* @violates @ref SpeednPosFdbkClass_REF_1 place reliance on undefined or unspecified behavior. */
  /* @violates @ref SpeednPosFdbkClass_REF_2 Cast from pointer to pointer. */
  int32_t wAux = (int32_t)((_CSPD)this)->Vars_str.hAvrMecSpeed01Hz;
  wAux *= S16_MAX;
  /* @violates @ref SpeednPosFdbkClass_REF_1 place reliance on undefined or unspecified behavior. */
  /* @violates @ref SpeednPosFdbkClass_REF_2 Cast from pointer to pointer. */
  wAux /= (int16_t)((_CSPD)this)->pParams_str->hMaxReliableMecSpeed01Hz;
  return (int16_t)wAux;
  /* @violates @ref SpeednPosFdbkClass_REF_3 A pointer parameter in a function prototype 
   * should be declared as pointer to constif the pointer is not used to modify the addressed object */
}
