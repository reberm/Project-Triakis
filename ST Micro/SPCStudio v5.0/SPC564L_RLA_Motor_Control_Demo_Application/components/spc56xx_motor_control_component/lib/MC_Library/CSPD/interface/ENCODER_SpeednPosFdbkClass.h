/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    ENCODER_SpeednPosFdbkClass.h
*   @version BETA 0.9.1
*   @brief   This file contains interface of ENCODER class
*          
*   @details Encoder implementation using eTimer.
*
*/
#ifndef __ENCODER_SPEEDNPOSFDBKCLASS_H
#define __ENCODER_SPEEDNPOSFDBKCLASS_H

/*===============================================================================================
*                                         INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
===============================================================================================*/

/** @addtogroup SPC5_PMSM_MC_Library
  * @{
  */
  
/** @addtogroup SpeednPosFdbk_ENCODER
  * @{
  */

/** @defgroup ENCODER_class_exported_types ENCODER class exported types
* @{
*/

/*===============================================================================================
*                                       CONSTANTS
===============================================================================================*/
/*===============================================================================================
*                          LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
===============================================================================================*/
/** 
  * @brief  Public ENCODER class definition
  *
  */
typedef struct CENC_SPD_t *CENC_SPD;

/** 
  * @brief  ENCODER class parameters definition
  *
  */
typedef const struct
{
  /* SW Settings */
  /** @brief MC IRQ number used for eTimer capture index signal event.*/
  uint8_t IRQnb;
  /** @brief Number of pulses per revolution, provided by each of the two 
      encoder signals, multiplied by 4 */  
  uint16_t hPulseNumber;
  /** @brief To be enabled if measured speed is opposite to real one (ENABLE/DISABLE)*/ 
  FunctionalState RevertSignal;
  /** @brief Frequency (01Hz) at which motor speed is to be computed. It must 
      be equal to the frequency at which function SPD_CalcAvrgMecSpeed01Hz is called.*/
  uint16_t hSpeedSamplingFreq01Hz;
  /** @brief Size of the buffer used to calculate the average speed. It must be <= 16.*/  
  uint8_t bSpeedBufferSize;
  /** @brief Input filter applied to ENCODER sensor capture channels (for channel A 
      and channel B). This value configure the Input Filter register (FILT) of eTimer channel */
  uint16_t hInpCaptFilter_chA_B;
  /** @brief Input filter applied to index capture channel. This value 
      configure the Input Filter register (FILT) of eTimer channel */
  uint16_t hInpCaptFilter_index_ch;
  /* HW Settings */
  /** @brief  Timer used for ENCODER sensor management.*/
  uint8_t eTimerModule;
  /** @brief  eTimer channel configure in quadrature mode */
  uint8_t quad_eTimer_ch;
  /** @brief  ENCODER CH. A - INPUT eTimer channel */
  uint8_t CH_A_eTimer_ch;
  /** @brief  ENCODER CH. B - INPUT eTimer channel */
  uint8_t CH_B_eTimer_ch;
  /** @brief  TRUE:  The index signal is used.
       FALSE: The index signal is not used. */
  bool index_enable;
  /** @brief  eTimer channel used for position acquisition */  
  uint8_t position_eTimer_ch;
  /** @brief  eTimer channel used for index counter */
  uint8_t index_counter_eTimer_ch;
  /** @brief  input capture eTimer channel used for reset signal from encoder */
  uint8_t index_input_capture_eTimer_ch;
  /** @brief  eTimer channel used for manage overflow (for speed calculation) */
  uint8_t Overflow_eTimer_ch;
  /** @brief  TRUE:  The rotor electrical and mechanical angle are
       synchronize with position acquisition with PWM period (CTU trigger)
       FALSE: It calculates the rotor electrical and mechanical angle, on the basis
       of the instantaneous value of the timer counter. */
  bool  position_acq_pwm;

}ENCODERParams_t, *pENCODERParams_t;
/**
  * @}
  */
  
/*===============================================================================================
*                                      FUNCTION PROTOTYPES
===============================================================================================*/
/** @defgroup ENCODER_class_exported_methods ENCODER class exported methods
  * @{
  */

CENC_SPD Enc_NewObject(pSpeednPosFdbkParams_t pSpeednPosFdbkParams, pENCODERParams_t pENCODERParams);

/**
  * @}
  */
  
/**
  * @}
  */

/**
  * @}
  */

#endif /*__ENCODER_SPEEDNPOSFDBKCLASS_H*/
