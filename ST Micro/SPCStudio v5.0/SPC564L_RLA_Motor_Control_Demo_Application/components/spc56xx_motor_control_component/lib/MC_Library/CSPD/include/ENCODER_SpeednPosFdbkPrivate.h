/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    ENCODER_SpeednPosFdbkPrivate.h
*   @version BETA 0.9.1
*   @brief   This file contains private implementation of ENCODER class. 
*          
*   @details Encoder implementation using eTimer.
*
*/
#ifndef __ENCODER_SPEEDNPOSFDBKPRIVATE_H
#define __ENCODER_SPEEDNPOSFDBKPRIVATE_H

/*===============================================================================================
*                                         INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
===============================================================================================*/

/*===============================================================================================
*                                       CONSTANTS
===============================================================================================*/
#define ENC_SPEED_ARRAY_SIZE    ((uint8)16)    /* 2^4 */
#define ENC_MAX_OVERFLOW_NB     ((uint16)2048) /* 2^11*/

/*===============================================================================================
*                          LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
===============================================================================================*/
/** 
  * @brief  ENCODER class members definition 
  */
typedef struct
{
  volatile uint16 hTimerOverflowNb;    /*!< Number of overflows occurred since
                                        last speed measurement event*/
  
  boolean SensorIsReliable;            /*!< Flag to indicate sensor/decoding is not
                                         properly working.*/  
  
  uint16 hPreviousCapture;            /*!< Timer counter value captured during
                                        previous speed measurement event*/
  
  sint32 wDeltaCapturesBuffer[ENC_SPEED_ARRAY_SIZE]; /*!< Buffer used to store
                                        captured variations of timer counter*/
  
  volatile uint8 bDeltaCapturesIndex; /*! <Buffer index*/
  
  uint32 wU32MAXdivPulseNumber;       /*! <It stores U32MAX/hPulseNumber*/
  
  uint16 hSpeedSamplingFreqHz;        /*! <Frequency (Hz) at which motor speed
                                        is to be computed. */
  
  boolean TimerOverflowError;              /*!< TRUE if the number of overflow
                                        occurred is greater than 'define'
                                        ENC_MAX_OVERFLOW_NB*/
  uint16 e_angle_first_index_value;   /*!< First Electric angle for encoder index signal */

  uint16 e_angle_current_index_value;   /*!< Current Electric angle for encoder index signal */

  uint8  direction;                     /*!< Encoder direction */

  boolean bIs_First_Measurement;    /*!< First measurement in motor speed calculation. */
  
  boolean bIndex_Signal;           /*!< Index signal captured (used in motor speed calculation) */
}DVars_t,*pDVars_t;

/** 
  * @brief  Redefinition of parameter structure
  */
typedef ENCODERParams_t DParams_t, *pDParams_t; 

/** 
  * @brief Private ENCODER class definition 
  */
typedef struct
{
    DVars_t DVars_str;          /*!< Derived class members container */
    pDParams_t pDParams_str;    /*!< Derived class parameters container */
}_DCENC_SPD_t, *_DCENC_SPD;

/*===============================================================================================
*                                       LOCAL VARIABLES
===============================================================================================*/

/*===============================================================================================
*                                       GLOBAL CONSTANTS
===============================================================================================*/

/*===============================================================================================
*                                       GLOBAL VARIABLES
===============================================================================================*/

/*===============================================================================================
*                                   LOCAL FUNCTION PROTOTYPES
===============================================================================================*/

#endif /*__ENCODER_SPEEDNPOSFDBKPRIVATE_H*/
