/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    STO_SpeednPosFdbkClass.c
*   @version BETA 0.9.1
*   @brief   This file contains private impelementation of STO class.
*          
*   @details This file contains private impelementation of STO class.
*
*/

/* Includes ------------------------------------------------------------------*/
#include "SpeednPosFdbkClass.h"
#include "SpeednPosFdbkPrivate.h"
#include "STO_SpeednPosFdbkClass.h"
#include "STO_SpeednPosFdbkPrivate.h"
#include "MC_Math.h"
#include "MCLibraryConf.h"
#include "MC_type.h"

#define DCLASS_PARAMS   ((_DCSTO_SPD)(((_CSPD) this)->DerivedClass))->pDParams_str
#define DCLASS_VARS     ((_DCSTO_SPD)(((_CSPD) this)->DerivedClass))->DVars_str
#define CLASS_VARS      (&(((_CSPD)this)->Vars_str))
#define CLASS_PARAMS    (((_CSPD)this)->pParams_str)
#define C6_COMP_CONST1  (int32_t) 1043038
#define C6_COMP_CONST2  (int32_t) 10430

/*===============================================================================================
*                                       GLOBAL VARIABLES
===============================================================================================*/
/* Only for debug purpose */
/* #define STO_BEMF_DEBUG */
/* #define STO_SPEED_DEBUG */

#if defined(STO_BEMF_DEBUG)
#define STO_BEMF_DEBUG_SIZE	1000
uint32_t Sto_bemf_buffer_count_debug;
uint16_t Sto_bemf_buffer_debug[STO_BEMF_DEBUG_SIZE];
#endif

#if defined(STO_SPEED_DEBUG)
#define STO_SPEED_DEBUG_SIZE	1000
#define STO_SAMPLE_POINT 		5
uint32_t Sto_speed_buffer_count_debug;
uint32_t Sto_speed_sample_point_debug=0;
int16_t  Sto_speed_buffer_debug[STO_SPEED_DEBUG_SIZE];
#endif

#ifdef MC_CLASS_DYNAMIC
    #include "stdlib.h" /* Used for dynamic allocation */
#else
    static _DCSTO_SPD_t STO_SPDpool[MAX_STO_SPD_NUM];
    static unsigned char STO_SPD_Allocated = 0u;
#endif

/*===============================================================================================
*                                   LOCAL FUNCTION PROTOTYPES
===============================================================================================*/
static void STO_Init(CSPD this);
static void STO_Return(void *this, uint8_t flag);
static void STO_Clear(CSPD this);
static int16_t STO_CalcElAngle(CSPD this, const void *pInputVars_str);
static bool STO_CalcAvrgMecSpeed01Hz(CSPD this, int16_t *pMecSpeed01Hz);
static inline void STO_Store_Rotor_Speed(CSPD this, int16_t hRotor_Speed);
static inline int16_t STO_ExecutePLL(CSPD this, int16_t hBemf_alfa_est, 
                                                        int16_t hBemf_beta_est);
static void STO_InitSpeedBuffer(CSTO_SPD this);
static void STO_SetMecAngle(CSPD this, int16_t hMecAngle);

/**
  * @brief  Creates an object of the class STO
  * @param  pSpeednPosFdbkParams pointer to an SpeednPosFdbk parameters structure
  * @param  pSTOParams pointer to an STO parameters structure
  * @retval CSTO_SPD new instance of STO object
  */
CSTO_SPD STO_NewObject(pSpeednPosFdbkParams_t pSpeednPosFdbkParams, pSTOParams_t 
                       pSTOParams)
{
    _CSPD _oSpeednPosFdbk;
    _DCSTO_SPD _oSTO;

    _oSpeednPosFdbk = (_CSPD)SPD_NewObject(pSpeednPosFdbkParams);

    #ifdef MC_CLASS_DYNAMIC
        _oSTO = (_DCSTO_SPD)calloc(1u,sizeof(_DCSTO_SPD_t));
    #else
        if (STO_SPD_Allocated  < MAX_STO_SPD_NUM)
        {
            _oSTO = &STO_SPDpool[STO_SPD_Allocated++];
        }
        else
        {
            _oSTO = MC_NULL;
        }
    #endif
  
    _oSTO->pDParams_str = pSTOParams;
    _oSpeednPosFdbk->DerivedClass = (void*)_oSTO;
    _oSpeednPosFdbk->Methods_str.pIRQ_Handler = &STO_Return;
    _oSpeednPosFdbk->Methods_str.pSPD_Init = &STO_Init;
    _oSpeednPosFdbk->Methods_str.pSPD_Clear = &STO_Clear;
    _oSpeednPosFdbk->Methods_str.pSPD_CalcAngle = &STO_CalcElAngle; 
    _oSpeednPosFdbk->Methods_str.pSPD_CalcAvrgMecSpeed01Hz = &STO_CalcAvrgMecSpeed01Hz;          
    _oSpeednPosFdbk->Methods_str.pSPD_SetMecAngle = &STO_SetMecAngle;
    ((_DCSTO_SPD)(((_CSPD) _oSpeednPosFdbk)->DerivedClass))->DVars_str.oPIRegulator = 
    PI_NewObject(&(((_DCSTO_SPD)(((_CSPD) _oSpeednPosFdbk)->DerivedClass))->pDParams_str->PIParams));   
    return ((CSTO_SPD)_oSpeednPosFdbk);
}

/**
  * @brief  It initializes the state observer object 
  * @param  this related object of class CSPD
  * @retval none
  */
static void STO_Init(CSPD this)
{
  pDVars_t pDVars_str = &DCLASS_VARS;  
  pDParams_t pDParams_str = DCLASS_PARAMS;
  int16_t htempk;
  int32_t wAux;
   
  pDVars_str->hC1 = pDParams_str->hC1;
  pDVars_str->hC2 = pDParams_str->hC2;  
  pDVars_str->hC3 = pDParams_str->hC3;
      
  pDVars_str->hC4 = pDParams_str->hC4;
  pDVars_str->hC5 = pDParams_str->hC5;
  pDVars_str->hF1 = pDParams_str->hF1;
  
  pDVars_str->hF2 = pDParams_str->hF2;    
  pDVars_str->bConsistencyCounter = pDParams_str->bStartUpConsistThreshold;
  pDVars_str->bEnableDualCheck = true;
  
  wAux = (int32_t)1;
  pDVars_str->hF3POW2 = 0u;
  
  htempk = (int16_t)(C6_COMP_CONST1/(pDParams_str->hF2));
  
  while (htempk != 0)
  {
    htempk /= (int16_t)2;
    wAux *=(int32_t)2;
    pDVars_str->hF3POW2++;
  }
  
  pDVars_str->hF3 = (int16_t)wAux;
  wAux = (int32_t)(pDParams_str->hF2) * pDVars_str->hF3;
  pDVars_str->hC6 = (int16_t)(wAux/C6_COMP_CONST2);
  
  STO_Clear(this);
  
  PI_ObjectInit(pDVars_str->oPIRegulator);
  
  /* Acceleration measurement set to zero */
  CLASS_VARS->hMecAccel01HzP = 0;
  
  return;
}

/**
  * @brief  It only returns, necessary to implement fictitious IRQ_Handler
  * @param  uint8_t Fictitious interrupt flag
  * @retval none
  */

static void STO_Return(void *this, uint8_t flag)
{
  return;
}

#if defined (CCMRAM)
#if defined (__ICCARM__)
#pragma location = ".ccmram"
#elif defined (__CC_ARM)
__attribute__((section ("ccmram")))
#endif
#endif
/**
  * @brief  This method executes Luenberger state observer equations and calls 
  *         PLL with the purpose of computing a new speed estimation and 
  *         updating the estimated electrical angle. 
  * @param  this related object of class CSPD
  *         pInputs pointer to the observer inputs structure 
  * @retval int16_t rotor electrical angle (s16Degrees)
  */
static int16_t STO_CalcElAngle(CSPD this, const void *pInputVars_str)
{
  pDVars_t pDVars_str = &DCLASS_VARS;  
  pDParams_t pDParams_str = DCLASS_PARAMS;

  int32_t wAux, wDirection;
  int32_t wIalfa_est_Next,wIbeta_est_Next;
  int32_t wBemf_alfa_est_Next, wBemf_beta_est_Next;
  int16_t hAux, hAux_Alfa, hAux_Beta, hIalfa_err, hIbeta_err, hRotor_Speed, 
                                                                 hValfa, hVbeta;
  
  Observer_Inputs_t * pInputs;
    
  pInputs= (Observer_Inputs_t *)pInputVars_str;
  
  if (pDVars_str->wBemf_alfa_est > (int32_t)(pDVars_str->hF2)*S16_MAX)
  {
    pDVars_str->wBemf_alfa_est = S16_MAX * (int32_t)(pDVars_str->hF2);
  }
  else if (pDVars_str->wBemf_alfa_est <= S16_MIN * (int32_t)(pDVars_str->hF2))
  {
    pDVars_str->wBemf_alfa_est = -S16_MAX * (int32_t)(pDVars_str->hF2);
  }
  else
  {
  }
  hAux_Alfa = (int16_t)(pDVars_str->wBemf_alfa_est>>pDParams_str->hF2LOG);
  
  if (pDVars_str->wBemf_beta_est > S16_MAX * (int32_t)(pDVars_str->hF2))
  {
    pDVars_str->wBemf_beta_est = S16_MAX * (int32_t)(pDVars_str->hF2);
  }
  else if (pDVars_str->wBemf_beta_est <= S16_MIN * (int32_t)(pDVars_str->hF2))
  {
    pDVars_str->wBemf_beta_est = -S16_MAX * (int32_t)(pDVars_str->hF2);
  }
  else
  {
  }
  hAux_Beta = (int16_t)(pDVars_str->wBemf_beta_est>>pDParams_str->hF2LOG);
  
  if (pDVars_str->wIalfa_est > S16_MAX * (int32_t)(pDVars_str->hF1))
  {
    pDVars_str->wIalfa_est = S16_MAX * (int32_t)(pDVars_str->hF1);
  }
  else if (pDVars_str->wIalfa_est <= S16_MIN * (int32_t)(pDVars_str->hF1))
  {
    pDVars_str->wIalfa_est = -S16_MAX * (int32_t)(pDVars_str->hF1);
  }
  else
  {
  } 
    
  if (pDVars_str->wIbeta_est > S16_MAX * (int32_t)(pDVars_str->hF1))
  {
    pDVars_str->wIbeta_est = S16_MAX * (int32_t)(pDVars_str->hF1);
  }
  else if (pDVars_str->wIbeta_est <= S16_MIN * (int32_t)(pDVars_str->hF1))
  {
    pDVars_str->wIbeta_est = -S16_MAX * (int32_t)(pDVars_str->hF1);
  }   
  else
  {
  }
  
  hIalfa_err = (int16_t)(pDVars_str->wIalfa_est>>pDParams_str->hF1LOG);
  
  hIalfa_err = hIalfa_err - pInputs->Ialfa_beta.qI_Component1;
  
  hIbeta_err = (int16_t)(pDVars_str->wIbeta_est>>pDParams_str->hF1LOG);
  
  hIbeta_err = hIbeta_err - pInputs->Ialfa_beta.qI_Component2;  
  
  wAux = (int32_t)(pInputs->Vbus) * pInputs->Valfa_beta.qV_Component1; 
  hValfa = (int16_t) (wAux >> 16);
  
  wAux = (int32_t)(pInputs->Vbus) * pInputs->Valfa_beta.qV_Component2; 
  hVbeta = (int16_t) (wAux >>16);  
  
  /*alfa axes observer*/
  hAux = (int16_t) (pDVars_str->wIalfa_est>>pDParams_str->hF1LOG);
  
  wAux = (int32_t) (pDVars_str->hC1)* hAux; 
  wIalfa_est_Next = pDVars_str->wIalfa_est - wAux;
  
  wAux = (int32_t) (pDVars_str->hC2) * hIalfa_err;
  wIalfa_est_Next += wAux;
  
  wAux = (int32_t) (pDVars_str->hC5) * hValfa;
  wIalfa_est_Next += wAux;
  
  wAux = (int32_t)  (pDVars_str->hC3)* hAux_Alfa;
  wIalfa_est_Next -=wAux;
  
  wAux = (int32_t)(pDVars_str->hC4) *hIalfa_err;
  wBemf_alfa_est_Next = pDVars_str->wBemf_alfa_est + wAux;

  wAux = (int32_t) hAux_Beta >> pDVars_str->hF3POW2;
  
  wAux = wAux * pDVars_str->hC6;
  wAux = CLASS_VARS->hElSpeedDpp * wAux;
  wBemf_alfa_est_Next += wAux;
      
  /*beta axes observer*/  
  hAux = (int16_t) (pDVars_str->wIbeta_est>>pDParams_str->hF1LOG);
  
  wAux = (int32_t)  (pDVars_str->hC1)* hAux;
  wIbeta_est_Next = pDVars_str->wIbeta_est - wAux;
  
  wAux = (int32_t) (pDVars_str->hC2) * hIbeta_err;
  wIbeta_est_Next += wAux;
  
  wAux = (int32_t) (pDVars_str->hC5) * hVbeta;
  wIbeta_est_Next += wAux;  
  
  wAux = (int32_t)  (pDVars_str->hC3)* hAux_Beta;
  wIbeta_est_Next -=wAux;
  
  wAux = (int32_t)(pDVars_str->hC4) *hIbeta_err;
  wBemf_beta_est_Next = pDVars_str->wBemf_beta_est + wAux;
  
  wAux = (int32_t) hAux_Alfa >> pDVars_str->hF3POW2;
  
  wAux = wAux * pDVars_str->hC6;
  wAux = CLASS_VARS->hElSpeedDpp * wAux;
  wBemf_beta_est_Next -= wAux;
  
  if (CLASS_VARS->hElSpeedDpp >=0)
  {
    wDirection = 1;
  }
  else
  {
    wDirection = -1;
  }  
    
  /*Calls the PLL blockset*/
  pDVars_str->hBemf_alfa_est = hAux_Alfa;
  pDVars_str->hBemf_beta_est = hAux_Beta;
  
  hAux_Alfa = (int16_t)(hAux_Alfa * wDirection);
  hAux_Beta = (int16_t)(hAux_Beta * wDirection);
  
  hRotor_Speed = STO_ExecutePLL(this, hAux_Alfa, -hAux_Beta);
  
  STO_Store_Rotor_Speed(this, hRotor_Speed);
  
  CLASS_VARS->hElAngle +=hRotor_Speed;
        
#if defined(STO_BEMF_DEBUG)
    if (Sto_bemf_buffer_count_debug < STO_BEMF_DEBUG_SIZE) 
    {
        Sto_bemf_buffer_debug[Sto_bemf_buffer_count_debug]=hAux_Alfa; 
        Sto_bemf_buffer_count_debug++;
    }
    if (Sto_bemf_buffer_count_debug == STO_BEMF_DEBUG_SIZE)  
    {
        Sto_bemf_buffer_count_debug = 0;
    }
#endif
 
  /*storing previous values of currents and bemfs*/
  pDVars_str->wIalfa_est = wIalfa_est_Next;
  pDVars_str->wBemf_alfa_est = wBemf_alfa_est_Next;
  
  pDVars_str->wIbeta_est = wIbeta_est_Next;
  pDVars_str->wBemf_beta_est = wBemf_beta_est_Next;
  
  return (CLASS_VARS->hElAngle);
}

/**
  * @brief  This method must be called - at least - with the same periodicity
  *         on which speed control is executed. It computes and returns - through
  *         parameter hMecSpeed01Hz - the rotor average mechanical speed,
  *         expressed in 01Hz. Average is computed considering a FIFO depth 
  *         equal to bSpeedBufferSize01Hz. Moreover it also computes and returns
  *         the reliability state of the sensor.
  * @param  this related object of class CSPD
  * @param  hMecSpeed01Hz pointer to int16_t, used to return the rotor average
  *         mechanical speed (01Hz)
  * @retval bool speed sensor reliability, measured with reference to parameters 
  *         bReliability_hysteresys, hVariancePercentage and bSpeedBufferSize
  *         true = sensor information is reliable
  *         false = sensor information is not reliable
  */

static bool STO_CalcAvrgMecSpeed01Hz(CSPD this, int16_t *pMecSpeed01Hz)
{
  int32_t wAvrSpeed_dpp = (int32_t)0;
  int32_t wError, wAux, wAvrSquareSpeed, wAvrQuadraticError=0;
  uint8_t i, bSpeedBufferSize01Hz = DCLASS_PARAMS->bSpeedBufferSize01Hz;
  int32_t wObsBemf, wEstBemf;
  int32_t wObsBemfSq = 0, wEstBemfSq = 0;
  int32_t wEstBemfSqLo;
  
  pDVars_t pDVars_str = &DCLASS_VARS; 
  bool bIs_Speed_Reliable = false, bAux = false;
  bool bIs_Bemf_Consistent = false;
  
  for (i=0u; i<bSpeedBufferSize01Hz; i++)
  {
    wAvrSpeed_dpp += (int32_t)(pDVars_str->hSpeed_Buffer[i]);
  }
  
  wAvrSpeed_dpp = wAvrSpeed_dpp / (int16_t)bSpeedBufferSize01Hz;
 
  for (i=0u; i < bSpeedBufferSize01Hz; i++)
  {
    wError = (int32_t)(pDVars_str->hSpeed_Buffer[i]) - wAvrSpeed_dpp;
    wError = (wError * wError);
    wAvrQuadraticError += wError;
  }
  
  /*It computes the measurement variance   */
  wAvrQuadraticError = wAvrQuadraticError / (int16_t)bSpeedBufferSize01Hz;
  
  /* The maximum variance acceptable is here calculated as a function of average 
     speed                                                                    */
  wAvrSquareSpeed = wAvrSpeed_dpp * wAvrSpeed_dpp;
  wAvrSquareSpeed = (wAvrSquareSpeed/(int16_t)128) * (int32_t)(DCLASS_PARAMS->
                                                           hVariancePercentage);
  
  if (wAvrQuadraticError < wAvrSquareSpeed)
  {
    bIs_Speed_Reliable = true;
  }

  /*Computation of Mechanical speed 01Hz*/
  wAux = wAvrSpeed_dpp * (int32_t)(CLASS_PARAMS->hMeasurementFrequency);
  wAux = wAux * (int32_t) 10;
  wAux = wAux/(int32_t)65536;
  wAux = wAux /(int16_t)(CLASS_PARAMS->bElToMecRatio);
  
  *pMecSpeed01Hz = (int16_t)wAux;
  CLASS_VARS->hAvrMecSpeed01Hz = (int16_t)wAux;
  
  
#if defined(STO_SPEED_DEBUG)
  if ( (Sto_speed_sample_point_debug % STO_SAMPLE_POINT) == 0)
  {
    if (Sto_speed_buffer_count_debug < STO_SPEED_DEBUG_SIZE)   
    {
      Sto_speed_buffer_debug[Sto_speed_buffer_count_debug]= *pMecSpeed01Hz;
      Sto_speed_buffer_count_debug++;
    }
    if (Sto_speed_buffer_count_debug == STO_SPEED_DEBUG_SIZE)  
    {
      Sto_speed_buffer_count_debug = 0;
    }
  }
  Sto_speed_sample_point_debug++;
#endif

  
  pDVars_str->bIsSpeedReliable = bIs_Speed_Reliable;
  
  /*Bemf Consistency Check algorithm*/
  if (pDVars_str->bEnableDualCheck == (bool)true)  /*do algorithm if it's enabled*/
  {
    wAux = (wAux < 0 ? (-wAux) : (wAux));  /* wAux abs value   */
    if (wAux < (int32_t)(DCLASS_PARAMS->hMaxAppPositiveMecSpeed01Hz))
    {    
      /*Computation of Observed back-emf*/
      wObsBemf = (int32_t)(pDVars_str->hBemf_alfa_est);
      wObsBemfSq = wObsBemf * wObsBemf;
      wObsBemf = (int32_t)(pDVars_str->hBemf_beta_est);
      wObsBemfSq += wObsBemf * wObsBemf;
      
      /*Computation of Estimated back-emf*/    
      wEstBemf = (wAux * 32767)/(int16_t)(CLASS_PARAMS->hMaxReliableMecSpeed01Hz);
      wEstBemfSq = (wEstBemf * (int32_t)(DCLASS_PARAMS->bBemfConsistencyGain))/64;
      wEstBemfSq *= wEstBemf;
      
      /*Computation of threshold*/
      wEstBemfSqLo = wEstBemfSq -
        (wEstBemfSq/64)*(int32_t)(DCLASS_PARAMS->bBemfConsistencyCheck);
      
      /*Check*/
      if (wObsBemfSq > wEstBemfSqLo)
      {
        bIs_Bemf_Consistent = true;
      }
    }
    
    pDVars_str->bIsBemfConsistent = bIs_Bemf_Consistent;
    pDVars_str->wObs_Bemf_Level = wObsBemfSq;
    pDVars_str->wEst_Bemf_Level = wEstBemfSq;
  }
  else
  {
    bIs_Bemf_Consistent = true;
  }    
  
  /*Decision making*/
  if(pDVars_str->bIsAlgorithmConverged == (bool)false)
  {
    bAux = true;
  }
  else
  {
    if((pDVars_str->bIsSpeedReliable == (bool)false)||(bIs_Bemf_Consistent == (bool)false))
    {
      pDVars_str->bReliabilityCounter++;
      if (pDVars_str->bReliabilityCounter >= DCLASS_PARAMS->bReliability_hysteresys)
      {
        pDVars_str->bReliabilityCounter = 0u;
        bAux = false;
      }
      else
      {
        bAux = true;
      }
    }
    else
    {
      pDVars_str->bReliabilityCounter = 0u;
      bAux = true;
    }
  }
  return(bAux);  
}

#if defined (CCMRAM)
#if defined (__ICCARM__)
#pragma location = ".ccmram"
#elif defined (__CC_ARM)
__attribute__((section ("ccmram")))
#endif
#endif
/**
  * @brief  This method must be called - at least - with the same periodicity
  *         on which speed control is executed. It computes and update object 
  *         variable hElSpeedDpp that is estimated average electrical speed
  *         expressed in dpp used for instance in observer equations. 
  *         Average is computed considering a FIFO depth equal to 
  *         bSpeedBufferSizedpp. 
  * @param  this related object of class CSPD
  * @retval none
  */
void STO_CalcAvrgElSpeedDpp(CSTO_SPD this)
{
  pDVars_t pDVars_str = &DCLASS_VARS;
  int16_t hIndexNew = (int16_t)pDVars_str->bSpeed_Buffer_Index;
  int16_t hIndexOld;
  int16_t hIndexOldTemp;
  int32_t wSum = pDVars_str->wDppBufferSum;
  int32_t wAvrSpeed_dpp;
  int16_t hSpeedBufferSizedpp = (int16_t)(DCLASS_PARAMS->bSpeedBufferSizedpp);
  int16_t hSpeedBufferSize01Hz = (int16_t)(DCLASS_PARAMS->bSpeedBufferSize01Hz);
  int16_t hBufferSizeDiff;
  
  hBufferSizeDiff = hSpeedBufferSize01Hz - hSpeedBufferSizedpp;
  
  if (hBufferSizeDiff == 0)
  {
    wSum = wSum + pDVars_str->hSpeed_Buffer[hIndexNew] -
      pDVars_str->hSpeedBufferOldestEl;
  }
  else
  {    
    hIndexOldTemp = hIndexNew + hBufferSizeDiff;
    
    if (hIndexOldTemp >= hSpeedBufferSize01Hz)
    {
      hIndexOld = hIndexOldTemp - hSpeedBufferSize01Hz;
    }
    else
    {
      hIndexOld = hIndexOldTemp;
    } 
    
    wSum = wSum + pDVars_str->hSpeed_Buffer[hIndexNew] -
      pDVars_str->hSpeed_Buffer[hIndexOld];
  }
  
  wAvrSpeed_dpp = wSum >> DCLASS_PARAMS->bSpeedBufferSizedppLOG;
  
  CLASS_VARS->hElSpeedDpp = (int16_t)wAvrSpeed_dpp;
  pDVars_str->wDppBufferSum = wSum;
}
   
/**
  * @brief  It clears state observer object by re-initializing private variables
  * @param  this related object of class CSTO_SPD
  * @retval none
  */
static void STO_Clear(CSPD this)
{
  pDVars_t pDVars_str = &DCLASS_VARS;  
 
  pDVars_str->wIalfa_est = (int32_t)0;
  pDVars_str->wIbeta_est = (int32_t)0;
  pDVars_str->wBemf_alfa_est = (int32_t)0;
  pDVars_str->wBemf_beta_est = (int32_t)0;
  CLASS_VARS->hElAngle = (int16_t)0;
  CLASS_VARS->hElSpeedDpp = (int16_t)0;
  pDVars_str->bConsistencyCounter = 0u;  
  pDVars_str->bReliabilityCounter = 0u;  
  pDVars_str->bIsAlgorithmConverged = false;
  pDVars_str->bIsBemfConsistent = false;
  pDVars_str->wObs_Bemf_Level = (int32_t)0;
  pDVars_str->wEst_Bemf_Level = (int32_t)0;
  pDVars_str->wDppBufferSum = (int32_t)0; 

  STO_InitSpeedBuffer((CSTO_SPD)this);
  PI_SetIntegralTerm(pDVars_str->oPIRegulator, (int32_t)0);
}

/**
  * @brief  It stores in estimated speed FIFO latest calculated value of motor 
  *         speed
  * @param  this related object of class CSTO_SPD
  * @retval none
  */
static inline void STO_Store_Rotor_Speed(CSPD this, int16_t hRotor_Speed)
{
  pDVars_t pDVars_str = &DCLASS_VARS; 
  uint8_t bBuffer_index = pDVars_str->bSpeed_Buffer_Index;
  
  bBuffer_index++;
  if (bBuffer_index == DCLASS_PARAMS->bSpeedBufferSize01Hz) 
  {
    bBuffer_index = 0u;
  }

  pDVars_str->hSpeedBufferOldestEl = pDVars_str->hSpeed_Buffer[bBuffer_index];  

  pDVars_str->hSpeed_Buffer[bBuffer_index] = hRotor_Speed;  
  pDVars_str->bSpeed_Buffer_Index = bBuffer_index; 
}

/**
  * @brief  It executes PLL algorithm for rotor position extraction from B-emf  
  *         alpha and beta
  * @param  this related object of class CSTO_SPD
  *         hBemf_alfa_est estimated Bemf alpha on the stator reference frame
  *         hBemf_beta_est estimated Bemf beta on the stator reference frame
  * @retval none
  */
static inline int16_t STO_ExecutePLL(CSPD this, int16_t hBemf_alfa_est, int16_t 
                                                                 hBemf_beta_est)
{
  int32_t wAlfa_Sin_tmp, wBeta_Cos_tmp;
  int16_t hOutput;
  Trig_Components Local_Components;
  int16_t hAux1, hAux2;
    
  Local_Components = MCM_Trig_Functions(CLASS_VARS->hElAngle);
  
  /* Alfa & Beta BEMF multiplied by Cos & Sin*/
  wAlfa_Sin_tmp = (int32_t)(hBemf_alfa_est)*(int32_t)Local_Components.hSin;
  wBeta_Cos_tmp = (int32_t)(hBemf_beta_est)*(int32_t)Local_Components.hCos;
  
  hAux1 = (int16_t)(wBeta_Cos_tmp >> 15);
  hAux2 = (int16_t)(wAlfa_Sin_tmp >> 15);
  
  /* Speed PI regulator */
  hOutput = PI_Controller(DCLASS_VARS.oPIRegulator, (int32_t)(hAux1)-hAux2);
 
  return (hOutput);
}

/**
  * @brief  It clears the estimated speed buffer 
  * @param  this related object of class CSTO_SPD
  * @retval none
  */
static void STO_InitSpeedBuffer(CSTO_SPD this)
{
  uint8_t b_i;
  pDVars_t pDVars_str = &DCLASS_VARS;    
  uint8_t bSpeedBufferSize01Hz = DCLASS_PARAMS->bSpeedBufferSize01Hz;
  
  /*init speed buffer*/
  for (b_i = 0u; b_i<bSpeedBufferSize01Hz; b_i++)
  {
    pDVars_str->hSpeed_Buffer[b_i] = (int16_t)0;
  }
  pDVars_str->bSpeed_Buffer_Index = 0u;
  pDVars_str->hSpeedBufferOldestEl = (int16_t)0;
  
  return;
}

/**
  * @brief  It internally performes a set of checks necessary to state whether
  *         the state observer algorithm converged. To be periodically called 
  *         during motor open-loop ramp-up (e.g. at the same frequency of 
  *         SPD_CalcElAngle), it returns true if the estimated angle and speed
  *         can be considered reliable, false otherwise
  * @param  this related object of class CSTO_SPD
  * @param  hForcedMecSpeed01Hz Mechanical speed in 0.1Hz unit as forced by VSS
  * @retval bool sensor reliability state
  */
bool STO_IsObserverConverged(CSTO_SPD this, int16_t hForcedMecSpeed01Hz)
{
  int16_t hEstimatedSpeed01Hz, hUpperThreshold, hLowerThreshold;
  int32_t wAux;
  pDParams_t pDParams_str = DCLASS_PARAMS;
  pDVars_t pDVars_str = &DCLASS_VARS; 
  bool bAux = false;
  int32_t wtemp;
  
  hEstimatedSpeed01Hz = CLASS_VARS->hAvrMecSpeed01Hz;
  
  wtemp = (int32_t)hEstimatedSpeed01Hz * (int32_t)hForcedMecSpeed01Hz;
  
  if (wtemp > 0)
  {    
    if(hEstimatedSpeed01Hz < 0)
    {
      hEstimatedSpeed01Hz = -hEstimatedSpeed01Hz;   
    }
    
    if (hForcedMecSpeed01Hz<0)
    {
      hForcedMecSpeed01Hz = -hForcedMecSpeed01Hz;
    }
    wAux = (int32_t) (hForcedMecSpeed01Hz) * (int16_t)pDParams_str->bSpeedValidationBand_H;
    hUpperThreshold = (int16_t)(wAux/(int32_t)16);
    
    wAux = (int32_t) (hForcedMecSpeed01Hz) * (int16_t)pDParams_str->bSpeedValidationBand_L;
    hLowerThreshold = (int16_t)(wAux/(int32_t)16);
    
    /* If the variance of the estimated speed is low enough...*/
    if(pDVars_str->bIsSpeedReliable == (bool)true)
    { 
      if((uint16_t)hEstimatedSpeed01Hz > pDParams_str->hMinStartUpValidSpeed)
      {
        /*...and the estimated value is quite close to the expected value... */
        if(hEstimatedSpeed01Hz >= hLowerThreshold)
        {
          if(hEstimatedSpeed01Hz <= hUpperThreshold)
          {    
            pDVars_str->bConsistencyCounter++;
            
            /*... for hConsistencyThreshold consecutive times... */   
            if (pDVars_str->bConsistencyCounter >= 
                pDParams_str->bStartUpConsistThreshold)
            {           
              
              /* the algorithm converged.*/
              bAux = true;
              pDVars_str->bIsAlgorithmConverged= true;
              CLASS_VARS->bSpeedErrorNumber = 0u;
            }          
          }
          else
          { 
            pDVars_str->bConsistencyCounter = 0u;
          }              
        }
        else
        { 
          pDVars_str->bConsistencyCounter = 0u;
        } 
      }
      else
      { 
        pDVars_str->bConsistencyCounter = 0u;
      } 
    }
    else
    { 
      pDVars_str->bConsistencyCounter = 0u;
    }
  }
  
  return (bAux);
}

/**
  * @brief  It exports estimated Bemf alpha-beta in Volt_Components format
  * @param  this related object of class CSTO_SPD
  * @retval Volt_Components Bemf alpha-beta 
  */
Volt_Components STO_GetEstimatedBemf(CSTO_SPD this)
{
  Volt_Components Vaux;  
  Vaux.qV_Component1 = DCLASS_VARS.hBemf_alfa_est;
  Vaux.qV_Component2 = DCLASS_VARS.hBemf_beta_est;
  return (Vaux);
}


/**
  * @brief  It exports the stator current alpha-beta as estimated by state 
  *         observer
  * @param  this related object of class CSTO_SPD
  * @retval Curr_Components State observer estimated stator current Ialpha-beta 
  */
Curr_Components STO_GetEstimatedCurrent(CSTO_SPD this)
{
  Curr_Components Iaux;  
  pDVars_t pDVars_str = &DCLASS_VARS;
  pDParams_t pDParams_str = DCLASS_PARAMS;
  
  Iaux.qI_Component1 = (int16_t)(pDVars_str->wIalfa_est >> pDParams_str->hF1LOG); 
  Iaux.qI_Component2 = (int16_t)(pDVars_str->wIbeta_est >> pDParams_str->hF1LOG);

  
  return (Iaux);
}

/**
  * @brief  It exports current observer gains through parameters hC2 and hC4
  * @param  this related object of class CSTO_SPD
  * @param  pC2 pointer to int16_t used to return parameters hC2
  * @param  pC4 pointer to int16_t used to return parameters hC4
  * @retval none 
  */
void STO_GetObserverGains(CSTO_SPD this, int16_t *pC2, int16_t *pC4)
{
  pDVars_t pDVars_str = &DCLASS_VARS; 
  *pC2 = pDVars_str->hC2;
  *pC4 = pDVars_str->hC4; 
}


/**
  * @brief  It allows setting new values for observer gains
  * @param  this related object of class CSTO_SPD
  * @param  wK1 new value for observer gain hC1
  * @param  wK2 new value for observer gain hC2
  * @retval none 
  */
void STO_SetObserverGains(CSTO_SPD this, int16_t hC1, int16_t hC2)
{
  pDVars_t pDVars_str = &DCLASS_VARS; 
  pDVars_str->hC2 = hC1;
  pDVars_str->hC4 = hC2;
}

/**
  * @brief  It exports current PLL gains through parameters pPgain and pIgain
  * @param  this related object of class CSTO_SPD
  * @param  pPgain pointer to int16_t used to return PLL proportional gain
  * @param  pIgain pointer to int16_t used to return PLL integral gain
  * @retval none 
  */
void STO_GetPLLGains(CSTO_SPD this, int16_t *pPgain, int16_t *pIgain)
{
  pDVars_t pDVars_str = &DCLASS_VARS; 
  *pPgain = PI_GetKP(pDVars_str->oPIRegulator);
  *pIgain = PI_GetKI(pDVars_str->oPIRegulator);
}


/**
  * @brief  It allows setting new values for PLL gains
  * @param  this related object of class CSTO_SPD
  * @param  hPgain new value for PLL proportional gain 
  * @param  hIgain new value for PLL integral gain 
  * @retval none 
  */
void STO_SetPLLGains(CSTO_SPD this, int16_t hPgain, int16_t hIgain)
{
  pDVars_t pDVars_str = &DCLASS_VARS; 
  PI_SetKP(pDVars_str->oPIRegulator, hPgain);
  PI_SetKI(pDVars_str->oPIRegulator, hIgain);
}


/**
  * @brief  It could be used to set istantaneous information on rotor mechanical
  *         angle.
  *         Note: Mechanical angle management is not implemented in this 
  *         version of State observer sensor class.
  * @param  this related object of class CSPD
  * @param  hMecAngle istantaneous measure of rotor mechanical angle
  * @retval none
  */
static void STO_SetMecAngle(CSPD this, int16_t hMecAngle)
{
}

#if defined (CCMRAM)
#if defined (__ICCARM__)
#pragma location = ".ccmram"
#elif defined (__CC_ARM)
__attribute__((section ("ccmram")))
#endif
#endif
/**
  * @brief  It resets integral term of PLL
  * @param  this related object of class CSTO_SPD 
  * @retval none 
  */
void STO_ResetPLL(CSTO_SPD this)
{
  PI_SetIntegralTerm(DCLASS_VARS.oPIRegulator, (int32_t)0);  
}

/**
  * @brief  It sends locking info for PLL
  * @param  this related object of class CSTO_SPD 
  * @retval none 
  */
void STO_SetPLL(CSTO_SPD this, int16_t hElSpeedDpp, int16_t hElAngle)
{
  PI_SetIntegralTerm(DCLASS_VARS.oPIRegulator,
                     (int32_t)hElSpeedDpp*(int32_t)PI_GetKIDivisor(DCLASS_VARS.oPIRegulator));
  CLASS_VARS->hElAngle = hElAngle;
}

/**
  * @brief  It exports estimated Bemf squared level
  * @param  this related object of class CSTO_SPD
  * @retval int32_t 
  */
int32_t STO_GetEstimatedBemfLevel(CSTO_SPD this)
{
  return (DCLASS_VARS.wEst_Bemf_Level);
}

/**
  * @brief  It exports observed Bemf squared level
  * @param  this related object of class CSTO_SPD
  * @retval int32_t 
  */
int32_t STO_GetObservedBemfLevel(CSTO_SPD this)
{
  return (DCLASS_VARS.wObs_Bemf_Level);
}

/**
  * @brief  It enables/disables the bemf consistency check
  * @param  this related object of class CSTO_SPD
  * @param  bSel boolean; true enables check; false disables check
  */
void STO_BemfConsistencyCheckSwitch(CSTO_SPD this, bool bSel)
{
  DCLASS_VARS.bEnableDualCheck = bSel;
}

/**
  * @brief  It returns the result of the Bemf consistency check
  * @param  this related object of class CSTO_SPD
  * @retval bool Bemf consistency state
  */
bool STO_IsBemfConsistent(CSTO_SPD this)
{
  return (DCLASS_VARS.bIsBemfConsistent);
}
