/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    ENCODER_SpeednPosFdbkClass.c
*   @version BETA 0.9.1
*   @brief   This file contains private implementation of ENCODER class.
*          
*   @details Encoder implementation using eTimer.
*
*/

/** @addtogroup SPC5_PMSM_MC_Library
  * @{
  */

/** @addtogroup SpeednPosFdbk_ENCODER
  * @{
*/

/**
*   @defgroup ENCODER Encoder Description
*   @brief   Encoder implementation using eTimer.
*   @details An incremental encoder provides a specified amount of pulses in one rotation of the encoder.
*            The output is composed by two lines of pulses (an A and B channel) that are offset in order
*            to determine rotation. This phasing between the two signals is called quadrature. An index
*            channel is used to generate a supplementary pulse per rotation. The index signal is used to
*            check and adjust the absolute reference.\n\n
*            Quadrature incremental encoders are widely used to read the rotor position of electric
*            machines. As the name implies, incremental encoders actually read angular displacements
*            with respect to an initial position: if that position is known, then the rotor absolute angle is
*            known too. For this reason, it is always necessary, when processing the encoder feedback,
*            to perform a rotor prepositioning before the first startup.\n\n
*            The quadrature encoder is a relative position sensor, but the absolute informations is
*            required for performing field-oriented control. To obtain absolute reference, the alignment
*            phase is needed. This task is performed by means of Encoder Alignment Controller (CEAC) class,
*            and shall be carried out at the first motor startup.\n\n
*            The CENC_SPD derived class use the eTimer hardware peripheral:
*            \image html enc_etimer.jpg "ENCODER - eTimer module"
*            - The two square wave signals of the incremental encoder, channel A and channel B, are
*              connected to primary and secondary input of channel 2 of ETIMER module\n
*            - Channel 2 is in QUADRATURE-COUNT Mode and it provides the decoding of the
*              incremental encoder signals. Channel 2 does not count, but it generates counting pulses
*              (up or down according to the incremental encoder direction) for channel 1 and channel 5.\n
*            - Channel 1 and Channel 5 are in CASCADE-COUNT Mode which really count\n
*            - If position_acq_pwm is configured as TRUE, the position acquisition is synchronized with
*              PWM period, so, the secondary input of channel 5 is connected to CTU trigger. On each
*              rising edge of CTU trigger, current value of channel 5 is latched. If position_acq_pwm is
*              configured as FALSE, the angle is obtained as the instantaneous value of the timer counter
*              of channel 5 value.\n
*            - Channel 0 (configured in Count rising and falling edges of primary source mode) is used to
*              manage the overflow of the encoder reference counter. It is incremented at each overflow of
*              channel 1 to take in account the number of overflow during speed calculation.\n
*            - When the index signal is available and enabled, (and after the alignment phase), the class
*              stores the first value of the index counter (first index rotor angle counter); this value is
*              compared with the reference index counter (current index rotor angle counter) which
*              happens once every turn in order to verify the correct operation of the encoder. Moreover,
*              the function Enc_CalcAngle and able to adjust the value of the angle considering the
*              following formula: rotor angle counter = current rotor angle counter + (first index rotor angle
*              counter - current index rotor angle counter).
*/ 

/**
  * @}
  */
  
/**
  * @}
  */


/*
* @page misra_violations MISRA-C:2004 violations
*
* @section Encoder_SpeednPosFdbkClass_REF_1
* Violates MISRA 2004 Required Rule 11.1, Cast from unsigned int to pointer.
* This rule can not be avoided because it appears when addressing memory mapped registers or other hardware specific feature
*
* @section Encoder_SpeednPosFdbkClass_REF_2
* Violates MISRA 2004 Advisory Rule 16.7,
* A pointer parameter in a function prototype should be declared as
* pointer to constif the pointer is not used to modify the addressed
* object. This rule can not be avoided because this pointer is used also to modify the addressed object.
*
* @section Encoder_SpeednPosFdbkClass_REF_3
* Violates MISRA 2004 Required Rule 1.2, Place reliance on undefined or unspecified behaviour.
* Violation is needed to implement Data Hiding. It is used to preventing the unintended  changes on private 
* parameters and functions.
*
* @section Encoder_SpeednPosFdbkClass_REF_4
* Violates MISRA 2004 Advisory Rule 11.4, Cast from pointer to pointer. 
* Violation is needed to implement Data Hiding. It is used to preventing the unintended  changes on private 
* parameters and functions.
*
*/

/*===============================================================================================
*                                         INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
===============================================================================================*/
#include "Reg_eSys_eTimer.h"
#include "SpeednPosFdbkClass.h"
#include "SpeednPosFdbkPrivate.h"
#include "ENCODER_SpeednPosFdbkClass.h"
#include "ENCODER_SpeednPosFdbkPrivate.h"
#include "MCIRQHandlerPrivate.h"
#include "MCLibraryConf.h"
#include "MC_type.h"

/*===============================================================================================
*                          LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
===============================================================================================*/

/*===============================================================================================
*                                       CONSTANTS
===============================================================================================*/
#define DCLASS_PARAM (((_DCENC_SPD)(((_CSPD) this)->DerivedClass))->pDParams_str)
#define DCLASS_VARS  (&(((_DCENC_SPD)(((_CSPD) this)->DerivedClass))->DVars_str))
#define  CLASS_VARS  (&(((_CSPD)this)->Vars_str))
#define  CLASS_PARAM (((_CSPD)this)->pParams_str)

#ifdef MC_CLASS_DYNAMIC
    #include "stdlib.h" /* Used for dynamic allocation */
#endif
/*===============================================================================================
*                                       LOCAL VARIABLES
===============================================================================================*/

/*===============================================================================================
*                                       GLOBAL CONSTANTS
===============================================================================================*/
#define ENC_UP 1U
#define ENC_DOWN 2U
/*===============================================================================================
*                                       GLOBAL VARIABLES
===============================================================================================*/

/*===============================================================================================
*                                   LOCAL FUNCTION PROTOTYPES
===============================================================================================*/
static void Enc_IRQHandler(void *this, uint8 flag);
static void Enc_Init(CSPD this);
static void Enc_Clear(CSPD this);
static sint16 Enc_CalcAngle(CSPD this, const void *pInputVars_str);
static bool Enc_CalcAvrgMecSpeed01Hz(CSPD this, sint16 *pMecSpeed01Hz);
static void Enc_SetMecAngle(CSPD this, sint16 hMecAngle);
static void Enc_eTimer_Quadrature_Mode(uint8 eTimer_ModuleID, uint8 channel, uint8 prisrc, uint8 secsrc);
static void Enc_eTimer_Cascade_Mode(uint8 eTimer_ModuleID, uint8 channel, uint8 prisrc, uint8 secsrc, uint16 hPulseNumber);
static void Enc_eTimer_CRFEPS_Mode(uint8 eTimer_ModuleID, uint8 channel, uint8 counter_ch);

/*===============================================================================================
*                                       LOCAL FUNCTIONS
===============================================================================================*/

/*===============================================================================================
*                                       GLOBAL FUNCTIONS
===============================================================================================*/

/**
* @brief        Creates an object of the class ENCODER
*
* @param[in]    pSpeednPosFdbkParams pointer to an SpeednPosFdbk parameters structure
* @param[in]    pENCODERParams pointer to an ENCODER parameters structure
*
* @return       CENC_SPD new instance of ENCODER object
*
* @api
*
*/
CENC_SPD Enc_NewObject(pSpeednPosFdbkParams_t pSpeednPosFdbkParams, pENCODERParams_t pENCODERParams)
{
    _CSPD oSpeednPosFdbk;
    _DCENC_SPD oENCODER;

#ifndef MC_CLASS_DYNAMIC
    static _DCENC_SPD_t Enc_SPDpool[MAX_ENC_SPD_NUM];
    static uint8 Enc_SPD_Allocated = 0u;
#endif

    /* @violates @ref Encoder_SpeednPosFdbkClass_REF_3 place reliance on undefined or unspecified behavior. */
    /* @violates @ref Encoder_SpeednPosFdbkClass_REF_4 Cast from pointer to pointer. */
    oSpeednPosFdbk = (_CSPD)SPD_NewObject(pSpeednPosFdbkParams);

    #ifdef MC_CLASS_DYNAMIC
        oENCODER = (_DCENC_SPD)calloc(1u,sizeof(_DCENC_SPD_t));
    #else
        if (Enc_SPD_Allocated  < MAX_ENC_SPD_NUM)
        {           
            oENCODER = &Enc_SPDpool[Enc_SPD_Allocated];
            Enc_SPD_Allocated = Enc_SPD_Allocated  + 1U;
        }
        else
        {
            oENCODER = (_DCENC_SPD)MC_NULL;
        }
    #endif
  
    oENCODER->pDParams_str = pENCODERParams;
    oSpeednPosFdbk->DerivedClass = (void*)oENCODER;
    
    oSpeednPosFdbk->Methods_str.pIRQ_Handler = &Enc_IRQHandler;
    /* @violates @ref Encoder_SpeednPosFdbkClass_REF_3 place reliance on undefined or unspecified behavior. */
    /* @violates @ref Encoder_SpeednPosFdbkClass_REF_4 Cast from pointer to pointer. */
    Set_IRQ_Handler(pENCODERParams->IRQnb, (_CMCIRQ)oSpeednPosFdbk);

        oSpeednPosFdbk->Methods_str.pSPD_Init = &Enc_Init;
        oSpeednPosFdbk->Methods_str.pSPD_Clear = &Enc_Clear;
        oSpeednPosFdbk->Methods_str.pSPD_CalcAngle = &Enc_CalcAngle;
        oSpeednPosFdbk->Methods_str.pSPD_CalcAvrgMecSpeed01Hz = &Enc_CalcAvrgMecSpeed01Hz;
        oSpeednPosFdbk->Methods_str.pSPD_SetMecAngle = &Enc_SetMecAngle;
    /* @violates @ref Encoder_SpeednPosFdbkClass_REF_3 place reliance on undefined or unspecified behavior. */
    /* @violates @ref Encoder_SpeednPosFdbkClass_REF_4 Cast from pointer to pointer. */       
    return ((CENC_SPD)oSpeednPosFdbk);
}

/**
* @brief        It initializes the hardware peripherals (eTimer) 
*               required for the speed position sensor management using ENCODER 
*               sensors.
*
* @param[in]    this related object of class CSPD
*
* @return       void
*
* @api
*
*/
static void Enc_Init(CSPD this)
{
  uint8 bBufferSize;
  /* @violates @ref Encoder_SpeednPosFdbkClass_REF_3 place reliance on undefined or unspecified behavior. */
  /* @violates @ref Encoder_SpeednPosFdbkClass_REF_4 Cast from pointer to pointer. */       
  pDParams_t pDParams_str = DCLASS_PARAM;
  /* @violates @ref Encoder_SpeednPosFdbkClass_REF_3 place reliance on undefined or unspecified behavior. */
  /* @violates @ref Encoder_SpeednPosFdbkClass_REF_4 Cast from pointer to pointer. */       
  pDVars_t pDVars_str = DCLASS_VARS;
  uint8 bIndex;
  uint8 eTimer_ModuleID = pDParams_str->eTimerModule;

  /* Disable Channel - Channel Enable Register (ENBL) */
  /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
  REG_BIT_CLEAR16(ETIMER_ENBL(eTimer_ModuleID), (uint16)(BIT0 << (pDParams_str->Overflow_eTimer_ch)));
  /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
  REG_BIT_CLEAR16(ETIMER_ENBL(eTimer_ModuleID), (uint16)(BIT0 << (pDParams_str->index_counter_eTimer_ch)));
  /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
  REG_BIT_CLEAR16(ETIMER_ENBL(eTimer_ModuleID), (uint16)(BIT0 << (pDParams_str->quad_eTimer_ch)));
  /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
  REG_BIT_CLEAR16(ETIMER_ENBL(eTimer_ModuleID), (uint16)(BIT0 << (pDParams_str->position_eTimer_ch)));
  /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
  REG_BIT_CLEAR16(ETIMER_ENBL(eTimer_ModuleID), (uint16)(BIT0 << (pDParams_str->CH_A_eTimer_ch)));
  /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
  REG_BIT_CLEAR16(ETIMER_ENBL(eTimer_ModuleID), (uint16)(BIT0 << (pDParams_str->CH_B_eTimer_ch)));

  /* Configure eTimer channel in Quadrature mode */
  Enc_eTimer_Quadrature_Mode(eTimer_ModuleID,pDParams_str->quad_eTimer_ch,
                             pDParams_str->CH_A_eTimer_ch,pDParams_str->CH_B_eTimer_ch);
  /* Configures the eTimer channels to calculate the rotor electrical and mechanical angle */
  Enc_eTimer_Cascade_Mode(eTimer_ModuleID,pDParams_str->position_eTimer_ch,
                          pDParams_str->quad_eTimer_ch,0x8U,(pDParams_str->hPulseNumber));
  /* Configures the eTimer channels to capture the index signal from encoder */
  Enc_eTimer_Cascade_Mode(eTimer_ModuleID,pDParams_str->index_counter_eTimer_ch,
                          pDParams_str->quad_eTimer_ch,pDParams_str->index_input_capture_eTimer_ch,
                         (pDParams_str->hPulseNumber));
  /* Configures the eTimer channel to manage the counter overflow (Speed calculation) */
  Enc_eTimer_CRFEPS_Mode(eTimer_ModuleID, pDParams_str->Overflow_eTimer_ch, pDParams_str->index_counter_eTimer_ch);

  /* Configures the eTimer channel to manage OFLAG bit in order to retrieve the direction */
  REG_WRITE16(ETIMER_CTRL1(eTimer_ModuleID, pDParams_str->CH_A_eTimer_ch), 
              (uint16)(CTRL1_CNTMODE_CREPS<<CTRL1_CNTMODE_SHIFT) | 
              (uint16)(((uint16)pDParams_str->quad_eTimer_ch + (uint16)16)<<CTRL1_SECSRC_SHIFT)
  /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
  );

  /* configures the Input filter applied to ENCODER sensor capture channels */
  /* (for channel A and channel B).                                         */
  REG_WRITE16( ETIMER_FILT(eTimer_ModuleID, pDParams_str->CH_A_eTimer_ch),
              (uint16)pDParams_str->hInpCaptFilter_chA_B
  /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
  );
  REG_WRITE16( ETIMER_FILT(eTimer_ModuleID, pDParams_str->CH_B_eTimer_ch),  
              (uint16)pDParams_str->hInpCaptFilter_chA_B
  /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
  );
  /* configures the Input filter applied to index capture channel */
  REG_WRITE16( ETIMER_FILT(eTimer_ModuleID, pDParams_str->index_input_capture_eTimer_ch),  
              (uint16)pDParams_str->hInpCaptFilter_index_ch
  /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
  );

  /* enable Input Capture interrupt for index signal from encoder */
  /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
  REG_BIT_SET16(ETIMER_INTDMA(eTimer_ModuleID, pDParams_str->index_counter_eTimer_ch), INTDMA_ICF1IE_MASK);

  /*Calculations of convenience*/
  pDVars_str->wU32MAXdivPulseNumber = U32_MAX/(uint32)(pDParams_str->hPulseNumber);
  pDVars_str->hSpeedSamplingFreqHz = pDParams_str->hSpeedSamplingFreq01Hz/ 10u;

  pDVars_str->hTimerOverflowNb = (uint16)1;

  /* Erase speed buffer */
  bBufferSize = pDParams_str->bSpeedBufferSize;
  
  for (bIndex=0u;bIndex<bBufferSize;bIndex++)
  {
    pDVars_str->wDeltaCapturesBuffer[bIndex]=0;
  }
  /* Init value */
  pDVars_str->bIs_First_Measurement = (boolean)FALSE;
  pDVars_str->bIndex_Signal = (boolean)FALSE;

  /* Enable channels - Channel Enable Register (ENBL) */
  /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
  REG_BIT_SET16(ETIMER_ENBL(eTimer_ModuleID), (uint16)(BIT0 << (pDParams_str->Overflow_eTimer_ch)));
  /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
  REG_BIT_SET16(ETIMER_ENBL(eTimer_ModuleID), (uint16)(BIT0 << (pDParams_str->index_counter_eTimer_ch)));
  /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
  REG_BIT_SET16(ETIMER_ENBL(eTimer_ModuleID), (uint16)(BIT0 << (pDParams_str->quad_eTimer_ch)));
  /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
  REG_BIT_SET16(ETIMER_ENBL(eTimer_ModuleID), (uint16)(BIT0 << (pDParams_str->position_eTimer_ch)));
  /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
  REG_BIT_SET16(ETIMER_ENBL(eTimer_ModuleID), (uint16)(BIT0 << (pDParams_str->CH_A_eTimer_ch)));
  /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
  REG_BIT_SET16(ETIMER_ENBL(eTimer_ModuleID), (uint16)(BIT0 << (pDParams_str->CH_B_eTimer_ch)));
  /* @violates @ref Encoder_SpeednPosFdbkClass_REF_2 A pointer parameter in a function prototype 
   * should be declared as pointer to constif the pointer is not used to modify the addressed object */
}

/**
* @brief        Clear software FIFO where are "pushed" rotor angle variations captured
*               This function must be called before starting the motor to initialize
*               the speed measurement process.
*
* @param[in]    this related object of class CSPD
*
* @return       void
*
* @api
*
*/
static void Enc_Clear(CSPD this)
{
  /* @violates @ref Encoder_SpeednPosFdbkClass_REF_3 place reliance on undefined or unspecified behavior. */
  /* @violates @ref Encoder_SpeednPosFdbkClass_REF_4 Cast from pointer to pointer. */       
  pDVars_t pDVars_str = DCLASS_VARS;
    
  pDVars_str->SensorIsReliable = (boolean)TRUE;

  pDVars_str->bIs_First_Measurement = (boolean)FALSE;
  pDVars_str->bIndex_Signal = (boolean)FALSE;
  pDVars_str->e_angle_first_index_value = 0U;
  pDVars_str->e_angle_current_index_value = 0U;
  /* @violates @ref Encoder_SpeednPosFdbkClass_REF_2 A pointer parameter in a function prototype 
   * should be declared as pointer to constif the pointer is not used to modify the addressed object */
}

/**
* @brief        It calculates the rotor electrical and mechanical angle, on the basis of
*               the instantaneous value of the timer counter (if position_acq_pwm is
*               FALSE), or synchronized with PWM (CTU trigger) period (if 
*               position_acq_pwm is TRUE).
*
* @param[in]    this related object of class CSPD
* @param[in]    pInputVars_str Not used in this derived class implementation of
*               SPD_CalcElAngle
*
* @return       Measured electrical angle in s16degree format.
*
* @api
*
*/
static sint16 Enc_CalcAngle(CSPD this, const void *pInputVars_str)
{
 
  sint32 wtemp1;
  sint32 wtemp2;
  sint16 htemp1;
  sint16 htemp2;
  uint8 eTimer_ModuleID;

  /* @violates @ref Encoder_SpeednPosFdbkClass_REF_3 place reliance on undefined or unspecified behavior. */
  /* @violates @ref Encoder_SpeednPosFdbkClass_REF_4 Cast from pointer to pointer. */       
  pDParams_t pDParams_str = DCLASS_PARAM;
  /* @violates @ref Encoder_SpeednPosFdbkClass_REF_3 place reliance on undefined or unspecified behavior. */
  /* @violates @ref Encoder_SpeednPosFdbkClass_REF_4 Cast from pointer to pointer. */       
  pDVars_t pDVars_str = DCLASS_VARS;

  eTimer_ModuleID = pDParams_str->eTimerModule;
  
  if(pDParams_str->position_acq_pwm)
  {
     /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
     wtemp1 = (sint32) REG_READ16(ETIMER_CAPT1(eTimer_ModuleID, pDParams_str->position_eTimer_ch));
  }
  else
  {
     /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
     wtemp1 = (sint32) REG_READ16(ETIMER_CNTR(eTimer_ModuleID, pDParams_str->position_eTimer_ch));
  }

  if(wtemp1 > ((sint32)pDParams_str->hPulseNumber - (sint32)1))
  {
    wtemp1 = (wtemp1 - (((sint32)0xFFFFU - (sint32)((sint32)pDParams_str->hPulseNumber - (sint32)1))));
  }

  if(pDParams_str->index_enable == (bool)true)
  {  
     wtemp2 = wtemp1 + ((sint32)pDVars_str->e_angle_first_index_value - (sint32)pDVars_str->e_angle_current_index_value);
     wtemp1 = (sint32)(wtemp2) * (sint32)(pDVars_str->wU32MAXdivPulseNumber); 
  }
  else
  {
     wtemp1 = (sint32)(wtemp1) * (sint32)(pDVars_str->wU32MAXdivPulseNumber);
  }  

  /* Computes and stores the rotor electrical angle */
  /* @violates @ref Encoder_SpeednPosFdbkClass_REF_3 place reliance on undefined or unspecified behavior. */
  /* @violates @ref Encoder_SpeednPosFdbkClass_REF_4 Cast from pointer to pointer. */
  wtemp2 = wtemp1 * (sint32)((_CSPD)this)->pParams_str->bElToMecRatio;
  htemp1 = (sint16)(wtemp2/65536);  

  /* @violates @ref Encoder_SpeednPosFdbkClass_REF_3 place reliance on undefined or unspecified behavior. */
  /* @violates @ref Encoder_SpeednPosFdbkClass_REF_4 Cast from pointer to pointer. */       
  ((_CSPD)this)->Vars_str.hElAngle = htemp1;
  
  /*Computes and stores the rotor mechanical angle*/
  htemp2 = (sint16)(wtemp1/65536);
  
  /* @violates @ref Encoder_SpeednPosFdbkClass_REF_3 place reliance on undefined or unspecified behavior. */
  /* @violates @ref Encoder_SpeednPosFdbkClass_REF_4 Cast from pointer to pointer. */       
  ((_CSPD)this)->Vars_str.hMecAngle = htemp2;

  /*Returns rotor electrical angle*/  
  return(htemp1);
}


/**
* @brief       This method must be called with the periodicity defined by parameter
*              hSpeedSamplingFreq01Hz. The method computes & stores average mechanical 
*              speed [01Hz], computes & stores average mechanical acceleration 
*              [01Hz/SpeedSamplingFreq], computes & stores the instantaneous electrical 
*              speed [dpp], updates the index of the speed buffer, then checks & stores 
*              & returns the reliability state of the sensor.
*
* @param[in]    this related object of class CSPD
* @param[in]    pMecSpeed01Hz pointer to sint16, used to return the rotor average
*               mechanical speed (01Hz)
*             
* @return       boolean Sensor information 
* @retval       TRUE sensor information is reliable
* @retval       FALSE sensor information is not reliable
*
* @api
*
*/
static bool Enc_CalcAvrgMecSpeed01Hz(CSPD this, sint16 *pMecSpeed01Hz)
{
  boolean bReliability = (boolean)TRUE;

  /* @violates @ref Encoder_SpeednPosFdbkClass_REF_3 place reliance on undefined or unspecified behavior. */
  /* @violates @ref Encoder_SpeednPosFdbkClass_REF_4 Cast from pointer to pointer. */       
  pDParams_t pDParams_str = DCLASS_PARAM;
  /* @violates @ref Encoder_SpeednPosFdbkClass_REF_3 place reliance on undefined or unspecified behavior. */
  /* @violates @ref Encoder_SpeednPosFdbkClass_REF_4 Cast from pointer to pointer. */       
  pDVars_t pDVars_str = DCLASS_VARS;

  sint32 wOverallAngleVariation = 0;
  sint32 wtemp1;
  sint32 wtemp2;
  uint8 bBufferIndex = 0u;
  uint8 eTimer_ModuleID;
  uint16 hNewCapture_sample_one, hNewCapture_sample_two;
  uint16 hOverflowCapture_sample_one, hOverflowCapture_sample_two;
  uint16 haux;

  uint8 bBufferSize = pDParams_str->bSpeedBufferSize;
  eTimer_ModuleID = pDParams_str->eTimerModule;

  if (pDVars_str->bIs_First_Measurement == (boolean)TRUE)
  {

      /* 1st reading of overflow counter */
      /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
      hOverflowCapture_sample_one = (uint16)REG_READ16(ETIMER_CNTR(eTimer_ModuleID, pDParams_str->Overflow_eTimer_ch));
      /* 1st reading of encoder timer counter */
      /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
      hNewCapture_sample_one = (uint16)REG_READ16(ETIMER_CNTR(eTimer_ModuleID, pDParams_str->position_eTimer_ch));
      /* 2nd reading of overflow counter */
      /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
      hOverflowCapture_sample_two = (uint16)REG_READ16(ETIMER_CNTR(eTimer_ModuleID, pDParams_str->Overflow_eTimer_ch));
      /* 2nd reading of encoder timer counter */
      /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
      hNewCapture_sample_two = (uint16)REG_READ16(ETIMER_CNTR(eTimer_ModuleID, pDParams_str->position_eTimer_ch));

      /* Reset Counter Timer Overflow and read the counter   */
      /* value for the next  measurement                      */
      /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
      REG_WRITE16( ETIMER_CNTR(eTimer_ModuleID, pDParams_str->Overflow_eTimer_ch),  (uint16)0x0000 );
      /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
      haux = (uint16)REG_READ16(ETIMER_CNTR(eTimer_ModuleID, pDParams_str->position_eTimer_ch));

      /* if overflow is occured */
      /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
      if ((uint16)REG_READ16(ETIMER_CNTR(eTimer_ModuleID, pDParams_str->Overflow_eTimer_ch)) != 0U) 
      {
         /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
         haux = (uint16)REG_READ16(ETIMER_CNTR(eTimer_ModuleID, pDParams_str->position_eTimer_ch));
         /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
         REG_WRITE16( ETIMER_CNTR(eTimer_ModuleID, pDParams_str->Overflow_eTimer_ch),  (uint16)0x0000 );
      }
         
      if (hOverflowCapture_sample_one != hOverflowCapture_sample_two)
      { /* Compare sample 1 & 2 and check if an overflow has been generated right */
        /* after the reading of encoder timer. If yes, copy sample 2 result in    */
        /* sample 1 for next process.                                             */
        hNewCapture_sample_one = hNewCapture_sample_two;
        hOverflowCapture_sample_one = hOverflowCapture_sample_two;
      }

      /* Capture OFLAG bit (INPUT bit of CTRL2 register) */
      /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
      if((REG_READ16(ETIMER_CTRL2(eTimer_ModuleID, (pDParams_str->CH_A_eTimer_ch))) & CTRL2_INPUT_MASK) == CTRL2_INPUT_MASK)
      {
          /* encoder timer up-counting */
          pDVars_str->wDeltaCapturesBuffer[pDVars_str->bDeltaCapturesIndex]= 
                ((sint32)(hNewCapture_sample_one) - (sint32)(pDVars_str->hPreviousCapture)) +
                (sint32)(((sint32)(hOverflowCapture_sample_one)) * (sint32)(pDParams_str->hPulseNumber));
      }
      else
      {
          /* encoder timer down-counting */
          pDVars_str->wDeltaCapturesBuffer[pDVars_str->bDeltaCapturesIndex]= 
                ((sint32)(hNewCapture_sample_one) - (sint32)(pDVars_str->hPreviousCapture)) -
                (sint32)(((sint32)(hOverflowCapture_sample_one)) * (sint32)(pDParams_str->hPulseNumber));
      }

      pDVars_str->hTimerOverflowNb = hOverflowCapture_sample_one;

      /* Computes & returns average mechanical speed [01Hz], var wtemp1 */
      for (bBufferIndex=0u;bBufferIndex<bBufferSize;bBufferIndex++)
      {
        wOverallAngleVariation += pDVars_str->wDeltaCapturesBuffer[bBufferIndex];
      }

      wtemp1 = wOverallAngleVariation * (sint32)(pDParams_str->hSpeedSamplingFreq01Hz);
      wtemp2 = (sint32)(pDParams_str->hPulseNumber)*
        (sint32)(pDParams_str->bSpeedBufferSize);
      wtemp1 /= wtemp2;  
      *pMecSpeed01Hz = (sint16)(wtemp1);
   
      /*Computes & stores average mechanical acceleration [01Hz/SpeedSamplingFreq]*/
      /* @violates @ref Encoder_SpeednPosFdbkClass_REF_3 place reliance on undefined or unspecified behavior. */
      /* @violates @ref Encoder_SpeednPosFdbkClass_REF_4 Cast from pointer to pointer. */
      ((_CSPD)this)->Vars_str.hMecAccel01HzP = (sint16)(wtemp1 - ((_CSPD)this)->Vars_str.hAvrMecSpeed01Hz);
        
      /*Stores average mechanical speed [01Hz]*/
      /* @violates @ref Encoder_SpeednPosFdbkClass_REF_3 place reliance on undefined or unspecified behavior. */
      /* @violates @ref Encoder_SpeednPosFdbkClass_REF_4 Cast from pointer to pointer. */
      ((_CSPD)this)->Vars_str.hAvrMecSpeed01Hz = (sint16)wtemp1;
       
      /*Computes & stores the instantaneous electrical speed [dpp], var wtemp1*/
      wtemp1 = pDVars_str->wDeltaCapturesBuffer[pDVars_str->bDeltaCapturesIndex] *
          /* @violates @ref Encoder_SpeednPosFdbkClass_REF_3 place reliance on undefined or unspecified behavior. */
          /* @violates @ref Encoder_SpeednPosFdbkClass_REF_4 Cast from pointer to pointer. */
          (sint32)(pDVars_str->hSpeedSamplingFreqHz) * (sint32)((_CSPD)this)->pParams_str->bElToMecRatio;

      wtemp1 /= (sint32)(pDParams_str->hPulseNumber);
      wtemp1 *= (sint32)U16_MAX;  
    
      /* @violates @ref Encoder_SpeednPosFdbkClass_REF_3 place reliance on undefined or unspecified behavior. */
      /* @violates @ref Encoder_SpeednPosFdbkClass_REF_4 Cast from pointer to pointer. */
      wtemp1 /= (sint32)(((_CSPD)this)->pParams_str->hMeasurementFrequency); 

      /* @violates @ref Encoder_SpeednPosFdbkClass_REF_3 place reliance on undefined or unspecified behavior. */
      /* @violates @ref Encoder_SpeednPosFdbkClass_REF_4 Cast from pointer to pointer. */
      ((_CSPD)this)->Vars_str.hElSpeedDpp = (sint16)wtemp1;

      /*Buffer index update*/
      pDVars_str->bDeltaCapturesIndex++;
      if (pDVars_str->bDeltaCapturesIndex == pDParams_str->bSpeedBufferSize)
      {
        pDVars_str->bDeltaCapturesIndex = 0u;
      }
  } /* is first measurement, discard it */
  else
  {
    pDVars_str->bIs_First_Measurement = (boolean)TRUE;
    /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
    REG_WRITE16( ETIMER_CNTR(eTimer_ModuleID, pDParams_str->Overflow_eTimer_ch),  (uint16)0x0000 );
    /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
    haux = (uint16)REG_READ16(ETIMER_CNTR(eTimer_ModuleID, pDParams_str->position_eTimer_ch));
  }

  /*last captured value update*/
  pDVars_str->hPreviousCapture = haux;

  /*Updates the number of overflows occurred*/      
  if (pDVars_str->hTimerOverflowNb > ENC_MAX_OVERFLOW_NB)  
  {
     pDVars_str->TimerOverflowError = (boolean)TRUE;
  }
 
  /*Checks the reliability status, then stores and returns it*/
  if (pDVars_str->TimerOverflowError)
  {
     bReliability = (boolean)FALSE;
     pDVars_str->SensorIsReliable = (boolean)FALSE;
  }

  return(bReliability);
}

/**
* @brief       It could be used to set istantaneous information on rotor mechanical
*              angle. As a consequence, timer counted is computed and updated.
*
* @param[in]    this related object of class CSPD
* @param[in]    hMecAngle istantaneous measure of rotor mechanical angle (s16degrees)
*
* @return       void
*
* @api
*
*/
static void Enc_SetMecAngle(CSPD this, sint16 hMecAngle)
{
  /* @violates @ref Encoder_SpeednPosFdbkClass_REF_3 place reliance on undefined or unspecified behavior. */
  /* @violates @ref Encoder_SpeednPosFdbkClass_REF_4 Cast from pointer to pointer. */       
  pDParams_t pDParams_str = DCLASS_PARAM;
  uint8 eTimer_ModuleID;
  uint16_t hAngleCounts;
  uint16_t hMecAngleuint;
  
  eTimer_ModuleID = pDParams_str->eTimerModule;  

  if (hMecAngle < 0)
  {
    hMecAngle *= -1;
    hMecAngleuint = 65535u - (uint16_t)hMecAngle;
  }
  else
  {
    hMecAngleuint = (uint16_t)hMecAngle;
  }
  
  hAngleCounts = (uint16_t)(((uint32_t)hMecAngleuint *
                    (uint32_t)pDParams_str->hPulseNumber)/65535u);
   
  /* Disable Channels - Channel Enable Register (ENBL) */
  /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
  REG_BIT_CLEAR16(ETIMER_ENBL(eTimer_ModuleID), (uint16)(BIT0 << (pDParams_str->index_counter_eTimer_ch)));
  /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
  REG_BIT_CLEAR16(ETIMER_ENBL(eTimer_ModuleID), (uint16)(BIT0 << (pDParams_str->position_eTimer_ch)));
  
  /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
  REG_WRITE16(ETIMER_CNTR((pDParams_str->eTimerModule), pDParams_str->index_counter_eTimer_ch),(uint16_t)(hAngleCounts)); 
  /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
  REG_WRITE16(ETIMER_CNTR((pDParams_str->eTimerModule), pDParams_str->position_eTimer_ch),(uint16_t)(hAngleCounts));

  /* Enable Channels - Channel Enable Register (ENBL) */
  /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
  REG_BIT_SET16(ETIMER_ENBL(eTimer_ModuleID), (uint16)(BIT0 << (pDParams_str->index_counter_eTimer_ch)));
  /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
  REG_BIT_SET16(ETIMER_ENBL(eTimer_ModuleID), (uint16)(BIT0 << (pDParams_str->position_eTimer_ch)));
  /* @violates @ref Encoder_SpeednPosFdbkClass_REF_2 A pointer parameter in a function prototype 
   * should be declared as pointer to constif the pointer is not used to modify the addressed object */
}

/**
* @brief       Private method of the class ENCODER to implement an MC IRQ function.
*
* @param[in]    this related object 
* @param[in]    flag used to distinguish between various IRQ sources
*
* @return       void
*
* @api
*
*/
static void Enc_IRQHandler(void *this, uint8 flag)
{
    uint8 eTimer_ModuleID; 
    uint8 curr_enc_direction;
    pDVars_t pDVars_str = DCLASS_VARS;
    pDParams_t pDParams_str = DCLASS_PARAM;

    eTimer_ModuleID = pDParams_str->eTimerModule;
    pDVars_str->e_angle_current_index_value = REG_READ16(ETIMER_CAPT1(eTimer_ModuleID, pDParams_str->index_counter_eTimer_ch));

    if((REG_READ16(ETIMER_CTRL2(eTimer_ModuleID, (pDParams_str->CH_A_eTimer_ch))) & CTRL2_INPUT_MASK) == CTRL2_INPUT_MASK)
    {
         /* encoder timer up-counting */
         curr_enc_direction = ENC_UP;
    }
    else
    {
       /* encoder timer down-counting */
         curr_enc_direction = ENC_DOWN;
    }
    
    if(pDVars_str->bIndex_Signal == (boolean)TRUE)
    {
      if(curr_enc_direction != pDVars_str->direction)
      {
          pDVars_str->e_angle_first_index_value = pDVars_str->e_angle_current_index_value;
          pDVars_str->direction = curr_enc_direction;
      }
    }
    else
    {
       pDVars_str->e_angle_first_index_value = pDVars_str->e_angle_current_index_value;
       pDVars_str->bIndex_Signal = (boolean)TRUE;
       pDVars_str->direction = curr_enc_direction;
    }
}

/**
* @brief       This method configures the eTimer channels in Quadrature mode provides
*              the decoding of the incremental encoder signals.
*              The two square wave signals of the incremental encoder, channel A and
*              channel B, are connected to primary and secondary input of channel.
*
* @param[in]    eTimer_ModuleID allocated for Quadrature 
* @param[in]    channel used for quadrature
* @param[in]    prisrc Channel A
* @param[in]    secsrc Channel B
*
* @return       void
*
* @api
*
*/
static void Enc_eTimer_Quadrature_Mode(uint8 eTimer_ModuleID, uint8 channel, uint8 prisrc, uint8 secsrc)
{
    /* Init CNTR counter value */
    /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
    REG_WRITE16( ETIMER_CNTR(eTimer_ModuleID, channel),  (uint16)0x0000 );

    /* Init COMP1 and COMP2 counter value */
    /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
    REG_WRITE16( ETIMER_COMP1(eTimer_ModuleID, channel),  (uint16)0x0000 );
    /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
    REG_WRITE16( ETIMER_COMP2(eTimer_ModuleID, channel),  (uint16)0x0000 );

    /* Init CMPLD1 and CMPLD2 counter value */
    /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
    REG_WRITE16( ETIMER_CMPLD1(eTimer_ModuleID, channel),  (uint16)0x0000 );
    /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
    REG_WRITE16( ETIMER_CMPLD2(eTimer_ModuleID, channel),  (uint16)0x0000 );

    /* Load CNTR with CMPLD1 upon successful compare with the value in COMP1 */
    /* Load CNTR with CMPLD2 upon successful compare with the value in COMP2 */
    /* CMPMODE: COMP1 register is used when the counter is counting up.      */
    /*          COMP2 register is used when the counter is counting down.    */
    REG_WRITE16( ETIMER_CCCTRL(eTimer_ModuleID, channel), 
                 (uint16)(CCCTRL_CLC1_LOAD_CNTR_W_CMPLD1_ON_COMP1<<CCCTRL_CLC1_SHIFT) | 
                 (uint16)(CCCTRL_CLC2_LOAD_CNTR_W_CMPLD2_ON_COMP2<<CCCTRL_CLC2_SHIFT) |
                 (uint16)(CCCTRL_CMPMODE_CMP1_UP_CMP2_DOWN<<CCCTRL_CMPMODE_SHIFT)
    /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
    );

    /* CNT MODE: Quadrature count mode        */
    /* DIRECTION: Count up                    */
    /* Count until compare, then reinitialize */
    /* PRCSRC: channel A                      */
    /* SECSRC: channel B                      */
    REG_WRITE16(ETIMER_CTRL1(eTimer_ModuleID, channel), 
               (uint16)(CTRL1_CNTMODE_QCOUNT<<CTRL1_CNTMODE_SHIFT) | 
               (uint16)(CTRL1_DIR_UP<<CTRL1_DIR_SHIFT) | 
               (uint16)((uint16)prisrc << CTRL1_PRISRC_SHIFT) |
               (uint16)(CTRL1_LENGTH_COMPARE<<CTRL1_LENGTH_SHIFT) |
               (uint16)((uint16)secsrc<<CTRL1_SECSRC_SHIFT)
    /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
    );
    /* OFLAG output signal will be driven on the external pin. Other timer channels */
    /* using this external pin as their input will see the driven value.            */
    REG_WRITE16(ETIMER_CTRL2(eTimer_ModuleID, channel), 
               (uint16)(CTRL2_OEN_OUTPUT<<CTRL2_OEN_SHIFT) | 
               (uint16)(CTRL2_OUTMODE_ASSERTED_UP_CLEARED_DOWN<<CTRL2_OUTMODE_SHIFT)
    /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
    );

    /* Configure PRISRC (Channel A) as external PIN (INPUT) */
    REG_WRITE16(ETIMER_CTRL2(eTimer_ModuleID, prisrc),
               (uint16)(CTRL2_OEN_INPUT<<CTRL2_OEN_SHIFT) | 
               (uint16)(CTRL2_OUTMODE_SOFTWARE<<CTRL2_OUTMODE_SHIFT)
    /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
    );
    /* Configure SECSRC (Channel B) as external PIN (INPUT) */
    REG_WRITE16(ETIMER_CTRL2(eTimer_ModuleID, secsrc),
               (uint16)(CTRL2_OEN_INPUT<<CTRL2_OEN_SHIFT) | 
               (uint16)(CTRL2_OUTMODE_SOFTWARE<<CTRL2_OUTMODE_SHIFT)
    /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
    );
}

/**
* @brief       This method configures the eTimer channel in Cascade mode.
*              This channel provides the reference counter to calculate the
*              rotor electrical and mechanical angle.
*
* @param[in]    eTimer_ModuleID allocated for Quadrature 
* @param[in]    channel used in Cascade mode
* @param[in]    prisrc counting pulses from eTimer channel (Quadrature mode)
* @param[in]    secsrc index input PIN or AUX0 (trigger from CTU)
* @param[in]    hPulseNumber  Number of pulses per revolution, provided by each
*               of the two encoder signals, multiplied by 4
*
* @return       void
*
* @api
*
*/
static void Enc_eTimer_Cascade_Mode(uint8 eTimer_ModuleID, uint8 channel, uint8 prisrc, uint8 secsrc, uint16 hPulseNumber)
{

    /* Init CNTR counter value */
    /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
    REG_WRITE16( ETIMER_CNTR(eTimer_ModuleID, channel),  (uint16)0x0000 );

    /* Init COMP1 and COMP2 counter value */
    /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
    REG_WRITE16( ETIMER_COMP1(eTimer_ModuleID, channel),  (uint16)(hPulseNumber - 1U) );
    /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
    REG_WRITE16( ETIMER_COMP2(eTimer_ModuleID, channel),  (uint16)((uint16)~hPulseNumber)+1U); /* even period */

    /* Init CMPLD1 and CMPLD2 counter value */
    /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
    REG_WRITE16( ETIMER_CMPLD1(eTimer_ModuleID, channel),  (uint16)0x0000 );
    /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
    REG_WRITE16( ETIMER_CMPLD2(eTimer_ModuleID, channel),  (uint16)0xFFFF );

    /* Load CNTR with CMPLD1 upon successful compare with the value in COMP1 */
    /* Load CNTR with CMPLD2 upon successful compare with the value in COMP2 */
    /* CMPMODE: COMP1 register is used when the counter is counting up.      */
    /*          COMP2 register is used when the counter is counting down.    */
    /* Capture rising edges in CAPT1                                         */
    /* ARM capture MODE enabled                                              */
    /* Threshold for number of word stored in the input capture FIFO         */
    REG_WRITE16( ETIMER_CCCTRL(eTimer_ModuleID, channel), 
                 (uint16)(CCCTRL_CLC1_LOAD_CNTR_W_CMPLD1_ON_COMP1<<CCCTRL_CLC1_SHIFT) | 
                 (uint16)(CCCTRL_CLC2_LOAD_CNTR_W_CMPLD2_ON_COMP2<<CCCTRL_CLC2_SHIFT) |
                 (uint16)(CCCTRL_CMPMODE_CMP1_UP_CMP2_DOWN<<CCCTRL_CMPMODE_SHIFT) |
                 (uint16)(CCCTRL_CPT1MODE_FALLING<<CCCTRL_CPT1MODE_SHIFT) |
                 (uint16)(CCCTRL_ARM_ENABLED<<CCCTRL_ARM_SHIFT) |
                 (uint16)(CCCTRL_CFWM_0<<CCCTRL_CPWM_SHIFT)
    /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
    );

    /* CNT MODE: Cascade count mode           */
    /* DIRECTION: Count up                    */
    /* Count until compare, then reinitialize */
    /* PRCSRC: Output of channel              */
    /* SECSRC: Quadrature channel             */
    REG_WRITE16(ETIMER_CTRL1(eTimer_ModuleID, channel), 
               (uint16)(CTRL1_CNTMODE_CASCADE<<CTRL1_CNTMODE_SHIFT) | 
               (uint16)(CTRL1_DIR_UP<<CTRL1_DIR_SHIFT) | 
               (uint16)((uint16)(prisrc + (uint16)16)<<CTRL1_PRISRC_SHIFT) |
               (uint16)(CTRL1_LENGTH_COMPARE<<CTRL1_LENGTH_SHIFT) |
               (uint16)(CTRL1_ONCE_CONTINUOUS<<CTRL1_ONCE_SHIFT) |
               (uint16)((uint16)secsrc<<CTRL1_SECSRC_SHIFT)
    /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
    );

    /* OFLAG output signal will be driven on the external pin. Other timer channels */
    /* using this external pin as their input will see the driven value.            */
    /* Toggle OFLAG output on successful compare (COMP1 or COMP2)                   */
    REG_WRITE16(ETIMER_CTRL2(eTimer_ModuleID, channel), 
               (uint16)(CTRL2_OEN_OUTPUT<<CTRL2_OEN_SHIFT) | 
               (uint16)(CTRL2_OUTMODE_TOGGLE_CMP1_CMP2<<CTRL2_OUTMODE_SHIFT)
    /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
    );
}

/**
* @brief       This method configures the eTimer channel in Count rising and 
*              falling edges of primary source mode.
*              This channel is used to manage the overflow of the encoder 
*              reference counter. It is incremented at each overflow.
*
* @param[in]   eTimer_ModuleID allocated for quadrature
* @param[in]   channel used in CRFEPS mode
* @param[in]   counter_ch eTimer channel reference counter to calculate the
*              rotor electrical and mechanical angle.
*
* @return       void
*
* @api
*
*/
static void Enc_eTimer_CRFEPS_Mode(uint8 eTimer_ModuleID, uint8 channel, uint8 counter_ch)
{

    /* Init CNTR counter value */
    /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
    REG_WRITE16( ETIMER_CNTR(eTimer_ModuleID, channel),  (uint16)0x0000 );

    /* Init COMP1 and COMP2 counter value */
    /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
    REG_WRITE16( ETIMER_COMP1(eTimer_ModuleID, channel),  (uint16)0xFFFF );
    /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
    REG_WRITE16( ETIMER_COMP2(eTimer_ModuleID, channel),  (uint16)0x0000 );        

    /* Init CMPLD1 and CMPLD2 counter value */
    /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
    REG_WRITE16( ETIMER_CMPLD1(eTimer_ModuleID, channel),  (uint16)0x0000 );
    /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
    REG_WRITE16( ETIMER_CMPLD2(eTimer_ModuleID, channel),  (uint16)0x0000 );

    /* CNT MODE: Count rising and falling edges of primary source  */
    /* DIRECTION: Count up                                         */
    /* Count until compare, then reinitialize                      */
    /* PRCSRC: Output of channel (electric angle)                  */
    REG_WRITE16(ETIMER_CTRL1(eTimer_ModuleID, channel),(uint16)(CTRL1_CNTMODE_CRFEPS<<CTRL1_CNTMODE_SHIFT) | 
               (uint16)(CTRL1_DIR_UP<<CTRL1_DIR_SHIFT) | 
               (uint16)(((uint16)counter_ch + 16U)<<CTRL1_PRISRC_SHIFT) |
               (uint16)(CTRL1_LENGTH_COMPARE<<CTRL1_LENGTH_SHIFT) |
               (uint16)(CTRL1_ONCE_CONTINUOUS<<CTRL1_ONCE_SHIFT) |
               (uint16)((uint16)0x0U<<CTRL1_SECSRC_SHIFT)
    /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
    );

    /* CMPMODE: COMP1 register is used when the counter is counting up.      */
    /*          COMP2 register is used when the counter is counting down.    */
    REG_WRITE16( ETIMER_CCCTRL(eTimer_ModuleID, channel), 
                 (uint16)(CCCTRL_CMPMODE_CMP1_UP_CMP2_DOWN<<CCCTRL_CMPMODE_SHIFT)
    /* @violates @ref Encoder_SpeednPosFdbkClass_REF_1 cast from unsigned int to pointer */
    );
}

