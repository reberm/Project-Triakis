/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    SpeednPosFdbkPrivate.h
*   @version BETA 0.9.1
*   @brief   This file contains the private definition for Speed Position Feedback class.
*          
*   @details This file contains the private definition for Speed Position Feedback class.
*
*/
#ifndef __SPEEDNPOSFDBKPRIVATE_H
#define __SPEEDNPOSFDBKPRIVATE_H

/*==================================================================================================
*                                        INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/
/*==================================================================================================
*                                          CONSTANTS
==================================================================================================*/

/*==================================================================================================
*                                      DEFINES AND MACROS
==================================================================================================*/

/*==================================================================================================
*                                             ENUMS
==================================================================================================*/

/*==================================================================================================
*                                STRUCTURES AND OTHER TYPEDEFS
==================================================================================================*/

/**
* @brief SpeednPosFdbk class members definition
*
*/
typedef struct
{
        int16_t hElAngle;
        int16_t hMecAngle;
        int16_t hAvrMecSpeed01Hz;
        int16_t hElSpeedDpp;
        int16_t hMecAccel01HzP;
        uint8_t bSpeedErrorNumber;
}Vars_t,*pVars_t;

/** 
  * @brief  Redefinition of parameter structure
  */
typedef SpeednPosFdbkParams_t Params_t, *pParams_t;

/**
  * @brief Virtual methods container
  */
typedef struct
{
    void (*pIRQ_Handler)(void *this, unsigned char flag);
        void (*pSPD_Init)(CSPD this);
        void (*pSPD_Clear)(CSPD this);
        int16_t (*pSPD_CalcAngle)(CSPD this, const void *pInputVars_str);
        bool (*pSPD_CalcAvrgMecSpeed01Hz)(CSPD this, int16_t *pMecSpeed01Hz);
        void (*pSPD_SetMecAngle)(CSPD this, int16_t hMecAngle);
}Methods_t,*pMethods_t;

/** 
  * @brief  Private SpeednPosFdbk class definition 
  */
typedef struct
{
    Methods_t Methods_str;  /*!< Virtual methods container */
    Vars_t Vars_str;        /*!< Class members container */
    pParams_t pParams_str;  /*!< Class parameters container */
    void *DerivedClass;     /*!< Pointer to derived class */
}_CSPD_t, *_CSPD;


/*==================================================================================================
*                                    FUNCTION PROTOTYPES
==================================================================================================*/


#endif /*__SPEEDNPOSFDBKPRIVATE_H*/
