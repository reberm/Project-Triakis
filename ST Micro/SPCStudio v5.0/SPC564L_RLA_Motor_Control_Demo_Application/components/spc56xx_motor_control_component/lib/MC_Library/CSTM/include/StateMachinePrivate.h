/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    StateMachinePrivate.h
*   @version BETA 0.9.1
*   @brief   This file contains private definition of StateMachine class.
*          
*   @details This file contains private definition of StateMachine class.
*
*/
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __STATEMACHINEPRIVATE_H
#define __STATEMACHINEPRIVATE_H

/** @addtogroup SPC5_PMSM_MC_Library
  * @{
  */

/** @addtogroup StateMachine
  * @{
  */

/** @defgroup StateMachine_class_private_types StateMachine class private types
* @{
*/

/** 
  * @brief  StateMachine class members definition
  */
typedef struct
{
    State_t   bState;          /*!< Variable containing state machine current
                                    state */
    uint16_t  hFaultNow;       /*!< Bit fields variable containing faults 
                                    currently present */
    uint16_t  hFaultOccurred;  /*!< Bit fields variable containing faults 
                                    historically occurred since the state 
                                    machine has been moved to FAULT_NOW state */
} Vars_t,*pVars_t;

/** 
  * @brief  Private StateMachine class definition 
  */
typedef struct
{
    Vars_t Vars_str;        /*!< Class members container */
}_CSTM_t, *_CSTM;
/**
  * @}
  */
  
/**
  * @}
  */

/**
  * @}
  */

#endif /*__STATEMACHINEPRIVATE_H*/
