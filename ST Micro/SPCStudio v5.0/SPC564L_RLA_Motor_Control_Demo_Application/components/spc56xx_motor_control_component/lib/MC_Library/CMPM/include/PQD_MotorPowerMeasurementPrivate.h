/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    PQD_MotorPowerMeasurementPrivate.h
*   @version BETA 0.9.1
*   @brief   This file contains private definition of PQD class
*          
*   @details This file contains private definition of PQD class
*
*/
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __PQD_MOTORPOWERMEASUREMENTPRIVATE_H
#define __PQD_MOTORPOWERMEASUREMENTPRIVATE_H

/** @addtogroup SPC5_PMSM_MC_Library
  * @{
  */
  
/** @addtogroup MotorPowerMeasurement_PQD
  * @{
  */

/** @defgroup PQD_private_types PQD private types
* @{
*/

/** 
  * @brief  PQD class members definition 
  */
typedef struct
{
  pFOCVars_t pFOCVars;    /*!< Pointer to FOC vars used by MPM.*/
  CVBS oVBS;              /*!< Bus voltage sensor object used by PQD.*/ 
} DVars_t,*pDVars_t;

/** 
  * @brief  Redefinition of parameter structure
  */
typedef PQDParams_t DParams_t, *pDParams_t; 

/** 
  * @brief Private PQD class definition 
  */
typedef struct
{
    DVars_t DVars_str;          /*!< Derived class members container */
    pDParams_t pDParams_str;    /*!< Derived class parameters container */
} _DCPQD_MPM_t, *_DCPQD_MPM;
/**
  * @}
  */
  
/**
  * @}
  */

/**
  * @}
  */

#endif /*__PQD_MOTORPOWERMEASUREMENTPRIVATE_H*/
