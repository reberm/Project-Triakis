/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    CircleLimitationClass.h
*   @version BETA 0.9.1
*   @brief   This file contains interface of CircleLimitation class
*          
*   @details This file contains interface of CircleLimitation class
*
*/
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __CIRCLELIMITATIONCLASS_H
#define __CIRCLELIMITATIONCLASS_H

/* Includes ------------------------------------------------------------------*/
#include "MC_type.h"

/** @addtogroup SPC5_PMSM_MC_Library
  * @{
  */

/** @addtogroup CircleLimitation
  * @{
  */
  
/** @defgroup CircleLimitation_class_exported_types CircleLimitation class exported types
* @{
*/

/** 
  * @brief  Public CircleLimitation class definition 
  */
typedef struct CCLM_t *CCLM;

/** 
  * @brief  CircleLimitation class parameters definition  
  */
typedef const struct
{
  uint16_t hMaxModule;                /*!< Circle limitation maximum allowed 
                                          module */
  uint16_t hCircle_limit_table[87];   /*!< Circle limitation table */
  uint8_t bStart_index;               /*!< Circle limitation table indexing 
                                            start */
} CircleLimitationParams_t, *pCircleLimitationParams_t;
  
/**
* @}
*/

/** @defgroup CircleLimitation_class_exported_methods CircleLimitation class exported methods
  * @{
  */

/**
  * @brief  Creates an object of the class CircleLimitation
  * @param  pCircleLimitationParams pointer to an CircleLimitation parameters structure
  * @retval CCLM new instance of CircleLimitation object
  */
CCLM CLM_NewObject(pCircleLimitationParams_t pCircleLimitationParams);

/**
  * @brief Check whether Vqd.qV_Component1^2 + Vqd.qV_Component2^2 <= 32767^2 
  *        and if not it applies a limitation keeping constant ratio 
  *        Vqd.qV_Component1 / Vqd.qV_Component2
  * @param  this related object of class CCLM
  * @param  Vqd Voltage in qd reference frame  
  * @retval Volt_Components Limited Vqd vector
  */
Volt_Components Circle_Limitation(CCLM this, Volt_Components Vqd);

/**
  * @}
  */
  
/**
  * @}
  */

/**
  * @}
  */

#endif /* __CIRCLELIMITATIONCLASS_H */
