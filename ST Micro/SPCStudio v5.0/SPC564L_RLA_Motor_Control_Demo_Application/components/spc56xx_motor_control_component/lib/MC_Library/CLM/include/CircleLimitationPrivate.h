/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    CircleLimitationPrivate.h
*   @version BETA 0.9.1
*   @brief   This file contains private definition of CircleLimitation class
*          
*   @details This file contains private definition of CircleLimitation class
*
*/
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __CIRCLELIMITATIONPRIVATE_H
#define __CIRCLELIMITATIONPRIVATE_H

/** @addtogroup SPC5_PMSM_MC_Library
  * @{
  */

/** @addtogroup CircleLimitation
  * @{
  */

/** @defgroup CircleLimitation_class_private_types CircleLimitation class private types
* @{
*/

/** 
  * @brief  Redefinition of parameter structure
  */
typedef CircleLimitationParams_t Params_t, *pParams_t;

/** 
  * @brief  Private CircleLimitation class definition 
  */
typedef struct
{
  pParams_t pParams_str;    /*!< Class parameters container */
} _CCLM_t, *_CCLM;
/**
  * @}
  */
  
/**
  * @}
  */

/**
  * @}
  */

#endif /*__CIRCLELIMITATIONPRIVATE_H*/
