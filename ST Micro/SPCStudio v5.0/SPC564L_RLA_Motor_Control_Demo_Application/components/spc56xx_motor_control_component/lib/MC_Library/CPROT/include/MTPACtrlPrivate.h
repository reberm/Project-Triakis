/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    MTPACtrlPrivate.h
*   @version BETA 0.9.1
*   @brief   This file contains private definition of MTPACtrl class
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MTPACTRLPRIVATE_H
#define __MTPACTRLPRIVATE_H

/** @addtogroup SPC5_PMSM_MC_Library
  * @{
  */

/** @addtogroup MTPACtrl
  * @{
  */

/** @defgroup MTPACtrl_class_private_types MTPACtrl class private types
* @{
*/

/** 
  * @brief  Redefinition of parameter structure
  */
typedef MTPACtrlParams_t Params_t, *pParams_t;

/** 
  * @brief  Private MTPACtrl class definition 
  */
typedef struct
{
    pParams_t pParams_str;  /*!< Class parameters container */
} _CMTPA_t, *_CMTPA;

/**
  * @}
  */
  
/**
  * @}
  */

/**
  * @}
  */

#endif /*__MTPACTRLPRIVATE_H*/

