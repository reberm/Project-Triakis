/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    FluxWeakeningCtrlClass.c
*   @version BETA 0.9.1
*   @brief   This file contains interface of FluxWeakeningCtrl class
*          
*   @details This file contains interface of FluxWeakeningCtrl class 
*
*/
/* Includes ------------------------------------------------------------------*/
#include "FluxWeakeningCtrlClass.h"
#include "FluxWeakeningCtrlPrivate.h"
#include "MC_Math.h"
#include "MCLibraryConf.h"
#include "MC_type.h"

#ifdef MC_CLASS_DYNAMIC
  #include "stdlib.h" /* Used for dynamic allocation */
#else
  static _CFW_t FWpool[MAX_FW_NUM];
  static unsigned char FW_Allocated = 0u;
#endif

#define CLASS_VARS   &((_CFW)this)->Vars_str
#define CLASS_PARAMS  ((_CFW)this)->pParams_str

/**
  * @brief  Creates an object of the class FluxWeakeningCtrl
  * @param  pFluxWeakeningCtrlParams pointer to an FluxWeakeningCtrl parameters structure
  * @retval CFW new instance of FluxWeakeningCtrl object
  */
CFW FW_NewObject(pFluxWeakeningCtrlParams_t pFluxWeakeningCtrlParams)
{
  _CFW _oFW;
  
  #ifdef MC_CLASS_DYNAMIC
    _oFW = (_CFW)calloc(1u,sizeof(_CFW_t));
  #else
    if (FW_Allocated  < MAX_FW_NUM)
    {
      _oFW = &FWpool[FW_Allocated++];
    }
    else
    {
      _oFW = MC_NULL;
    }
  #endif
  
  _oFW->pParams_str = (pParams_t)pFluxWeakeningCtrlParams;
  
  return ((CFW)_oFW);
}

/**
  * @brief  Initializes all the object variables, usually it has to be called 
  *         once right after object creation.
  * @param  this related object of class CFW.
  * @param  pFWInitStr Flux weakening init strutcture.
  * @retval none.
  */
void FW_Init(CFW this, pFWInit_t pFWInitStr)
{
  pParams_t pParams = CLASS_PARAMS;
  pVars_t pVars = CLASS_VARS;
   
  pVars->hFW_V_Ref = pParams->hDefaultFW_V_Ref;
    
  pVars->oFluxWeakeningPI= pFWInitStr->oFluxWeakeningPI;
    
  pVars->oSpeedPI = pFWInitStr->oSpeedPI;
}

/**
  * @brief  It should be called before each motor restart and clears the Flux 
  *         weakening internal variables with the exception of the target 
  *         voltage (hFW_V_Ref).
  * @param  this related object of class CFW.
  * @retval none
  */
void FW_Clear(CFW this)
{
  pVars_t pVars = CLASS_VARS;
  Volt_Components V_null = {(int16_t)0,(int16_t)0};
  
  PI_SetIntegralTerm(pVars->oFluxWeakeningPI, (int32_t)0);
  pVars->AvVolt_qd = V_null; 
  pVars->AvVoltAmpl = (int16_t)0;
  pVars->hIdRefOffset= (int16_t)0;  
}

/**
  * @brief  It computes Iqdref according the flux weakening algorithm.  Inputs
  *         are the starting Iqref components.
  *         As soon as the speed increases beyond the nominal one, fluxweakening 
  *         algorithm take place and handles Idref value. Finally, accordingly
  *         with new Idref, a new Iqref saturation value is also computed and
  *         put into speed PI.
  * @param  this related object of class CFW
  * @param  Iqdref The starting current components that have to be 
  *         manipulated by the flux weakening algorithm.
  * @retval Curr_Components Computed Iqdref.
  */
Curr_Components FW_CalcCurrRef(CFW this, Curr_Components Iqdref)
{
  int32_t wIdRef, wIqSatSq, wIqSat, wAux1, wAux2;      
  uint32_t wVoltLimit_Ref;
  int16_t hId_fw;
  
  pParams_t pParams = CLASS_PARAMS;
  pVars_t pVars = CLASS_VARS;
  
  /* Computation of the Id contribution coming from flux weakening algorithm */
  wVoltLimit_Ref = ((uint32_t)(pVars->hFW_V_Ref)*pParams->hMaxModule)
    /1000u;
  wAux1 = (int32_t)(pVars->AvVolt_qd.qV_Component1) * 
    pVars->AvVolt_qd.qV_Component1;
  wAux2 = (int32_t)(pVars->AvVolt_qd.qV_Component2) * 
    pVars->AvVolt_qd.qV_Component2;
  wAux1 += wAux2;
  
  wAux1 = MCM_Sqrt(wAux1);
  pVars->AvVoltAmpl = (int16_t)wAux1;
  
  /* Just in case sqrt rounding exceeded S16_MAX */
  if (wAux1 > S16_MAX)
  {
    wAux1 = (int32_t)S16_MAX;
  }
  
  hId_fw = PI_Controller(pVars->oFluxWeakeningPI, (int32_t)wVoltLimit_Ref 
                         - wAux1);
  
  /* If the Id coming from flux weakening algorithm (Id_fw) is positive, keep 
  unchanged Idref, otherwise sum it to last Idref available when Id_fw was 
  zero */
  if (hId_fw >= (int16_t)0)
  {
    pVars->hIdRefOffset = Iqdref.qI_Component2;
    wIdRef = (int32_t)Iqdref.qI_Component2;
  }
  else
  {
    wIdRef = (int32_t)pVars->hIdRefOffset + hId_fw;
  }
  
  /* Saturate new Idref to prevent the rotor from being demagnetized */  
  if (wIdRef < pParams->hDemagCurrent)
  {
    wIdRef =  pParams->hDemagCurrent;
  }
  
  Iqdref.qI_Component2 = (int16_t)wIdRef;
  
  /* New saturation for Iqref */
  wIqSatSq =  pParams->wNominalSqCurr - wIdRef*wIdRef;
  wIqSat = MCM_Sqrt(wIqSatSq);
  
  /* Iqref saturation value used for updating integral term limitations of 
  speed PI */
  wAux1 = wIqSat * (int32_t)PI_GetKIDivisor(pVars->oSpeedPI);
  
  PI_SetLowerIntegralTermLimit(pVars->oSpeedPI, -wAux1);
  PI_SetUpperIntegralTermLimit(pVars->oSpeedPI, wAux1);
  
  /* Iqref saturation value used for updating integral term limitations of 
  speed PI */
  if (Iqdref.qI_Component1 > wIqSat)
  {
    Iqdref.qI_Component1 = (int16_t)wIqSat;
  }
  else if (Iqdref.qI_Component1 < -wIqSat)
  {
    Iqdref.qI_Component1 = -(int16_t)wIqSat;
  }
  else
  {
  }
  
  return (Iqdref);
}

#if defined (CCMRAM)
#if defined (__ICCARM__)
#pragma location = ".ccmram"
#elif defined (__CC_ARM)
__attribute__((section ("ccmram")))
#endif
#endif
/**
  * @brief  It low-pass filters both the Vqd voltage components. Filter 
  *         bandwidth depends on hVqdLowPassFilterBW parameter 
  * @param  this related object of class CFW.
  * @param  Vqd Voltage componets to be averaged.
  * @retval none
  */
void FW_DataProcess(CFW this, Volt_Components Vqd)
{
  pParams_t pParams = CLASS_PARAMS;
  pVars_t pVars = CLASS_VARS;
  int32_t wAux;
  
#ifdef FULL_MISRA_C_COMPLIANCY
  wAux = (int32_t)(pVars->AvVolt_qd.qV_Component1)*
                     ((int32_t)(pParams->hVqdLowPassFilterBW)-(int32_t)1);
  wAux += Vqd.qV_Component1;
    
  pVars->AvVolt_qd.qV_Component1 = (int16_t)(wAux/
                                 (int32_t)(pParams->hVqdLowPassFilterBW));
  
  wAux = (int32_t)(pVars->AvVolt_qd.qV_Component2)*
                     ((int32_t)(pParams->hVqdLowPassFilterBW)-(int32_t)1);
  wAux += Vqd.qV_Component2;
    
  pVars->AvVolt_qd.qV_Component2 = (int16_t)(wAux/
                                   (int32_t)pParams->hVqdLowPassFilterBW);
#else
  wAux = (int32_t)(pVars->AvVolt_qd.qV_Component1)<<
                                           (pParams->hVqdLowPassFilterBWLOG);
 
  wAux -= (pVars->AvVolt_qd.qV_Component1 - Vqd.qV_Component1);
                          
  pVars->AvVolt_qd.qV_Component1 = (int16_t)(wAux >> 
                                            pParams->hVqdLowPassFilterBWLOG);
  
  wAux = (int32_t)(pVars->AvVolt_qd.qV_Component2)<<
                                           (pParams->hVqdLowPassFilterBWLOG);
 
  wAux -= (pVars->AvVolt_qd.qV_Component2 - Vqd.qV_Component2);
                          
  pVars->AvVolt_qd.qV_Component2 = (int16_t)(wAux >> 
                                            pParams->hVqdLowPassFilterBWLOG);
#endif
  return;
}

/**
  * @brief  Use this method to set a new value for the voltage reference used by 
  *         flux weakening algorithm.
  * @param  this related object of class CFW.
  * @param  uint16_t New target voltage value, expressend in tenth of percentage
  *         points of available voltage.
  * @retval none
  */
void FW_SetVref(CFW this, uint16_t hNewVref)
{
  pVars_t pVars = CLASS_VARS;
  pVars->hFW_V_Ref= hNewVref;
}

/**
  * @brief  It returns the present value of target voltage used by flux 
  *         weakening algorihtm.
  * @param  this related object of class CFW.
  * @retval int16_t Present target voltage value expressed in tenth of 
  *         percentage points of available voltage.
  */
uint16_t FW_GetVref(CFW this)
{
  pVars_t pVars = CLASS_VARS;
  return(pVars->hFW_V_Ref);
}

/**
  * @brief  It returns the present value of voltage actually used by flux 
  *         weakening algorihtm.
  * @param  this related object of class CFW.
  * @retval int16_t Present averaged phase stator voltage value, expressed 
  *         in s16V (0-to-peak), where 
  *         PhaseVoltage(V) = [PhaseVoltage(s16A) * Vbus(V)] /[sqrt(3) *32767].
  */
int16_t FW_GetAvVAmplitude(CFW this)
{
  pVars_t pVars = CLASS_VARS;
  return(pVars->AvVoltAmpl);
}

/**
  * @brief  It returns the measure of present voltage actually used by flux 
  *         weakening algorihtm as percentage of available voltage.
  * @param  this related object of class CFW.
  * @retval uint16_t Present averaged phase stator voltage value, expressed in 
  *         tenth of percentage points of available voltage.
  */
uint16_t FW_GetAvVPercentage(CFW this)
{
  pParams_t pParams = CLASS_PARAMS;
  pVars_t pVars = CLASS_VARS;
  return(uint16_t)((uint32_t)(pVars->AvVoltAmpl)*1000u/
                   (uint32_t)(pParams->hMaxModule));
}
