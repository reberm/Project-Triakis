/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    FeedForwardCtrlPrivate.h
*   @version BETA 0.9.1
*   @brief   This file contains private definition of FeedForwardCtrl class  
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __FEEDFORWARDCTRLPRIVATE_H
#define __FEEDFORWARDCTRLPRIVATE_H

/** @addtogroup SPC5_PMSM_MC_Library
  * @{
  */

/** @addtogroup FeedForwardCtrl
  * @{
  */

/** @defgroup FeedForwardCtrl_class_private_types FeedForwardCtrl class private types
* @{
*/

/** 
  * @brief  FeedForwardCtrl class members definition
  */
typedef struct
{
  Volt_Components   Vqdff;      /*!< Feed-forward Iqd controller contribution to 
                                     Vqd */
  Volt_Components   VqdPIout;   /*!< Vqd as output by PI controller */
  Volt_Components   VqdAvPIout; /*!< Averaged Vqd as output by PI controller */
  int32_t  wConstant_1D;        /*!< Feed forward default constant for d axes */
  int32_t  wConstant_1Q;        /*!< Feed forward default constant for q axes */
  int32_t  wConstant_2;         /*!< Default constant value used by Feed-Forward
                                     algorithm */
  CVBS oVBS;                    /*!< Related bus voltage sensor */
  CPI  oPI_q;                   /*!< Related PI for Iq regulator */
  CPI  oPI_d;                   /*!< Related PI for Id regulator */
} Vars_t, *pVars_t;

/** 
  * @brief  Redefinition of parameter structure
  */
typedef FeedForwardCtrlParams_t Params_t, *pParams_t;

/** 
  * @brief  Private FeedForwardCtrl class definition 
  */
typedef struct
{
    Vars_t Vars_str;        /*!< Class members container */
    pParams_t pParams_str;  /*!< Class parameters container */
} _CFF_t, *_CFF;
/**
  * @}
  */
  
/**
  * @}
  */

/**
  * @}
  */

#endif /*__FEEDFORWARDCTRLPRIVATE_H*/

