/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    FluxWeakeningCtrlPrivate.h
*   @version BETA 0.9.1
*   @brief   This file contains private definition of FluxWeakeningCtrl class 
*          
*   @details This file contains private definition of FluxWeakeningCtrl class  
*
*/
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __FLUXWEAKENINGCTRLPRIVATE_H
#define __FLUXWEAKENINGCTRLPRIVATE_H

/** @addtogroup SPC5_PMSM_MC_Library
  * @{
  */

/** @addtogroup FluxWeakeningCtrl
  * @{
  */

/** @defgroup FluxWeakeningCtrl_class_private_types FluxWeakeningCtrl class private types
* @{
*/

/** 
  * @brief  FluxWeakeningCtrl class members definition
  */
typedef struct
{
  CPI               oFluxWeakeningPI; /*!< PI object used for flux weakening */
  CPI               oSpeedPI;         /*!< PI object used for speed control */
  uint16_t          hFW_V_Ref;        /*!< Voltage reference, tenth of 
                                           percentage points */
  Volt_Components   AvVolt_qd;        /*!< Average stator voltage in qd 
                                           reference frame */
  int16_t           AvVoltAmpl;       /*!< Average stator voltage amplitude */
  int16_t           hIdRefOffset;     /*!< Id reference offset */
} Vars_t,*pVars_t;

/** 
  * @brief  Redefinition of parameter structure
  */
typedef FluxWeakeningCtrlParams_t Params_t, *pParams_t;

/** 
  * @brief  Private FluxWeakeningCtrl class definition 
  */
typedef struct
{
    Vars_t Vars_str;        /*!< Class members container */
    pParams_t pParams_str;  /*!< Class parameters container */
}_CFW_t, *_CFW;

/**
  * @}
  */
  
/**
  * @}
  */

/**
  * @}
  */

#endif /*__FLUXWEAKENINGCTRLPRIVATE_H*/
