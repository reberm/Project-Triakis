/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    UITask.c
*   @version BETA 0.9.1
*   @brief   This file implementes user interface tasks definition
*          
*   @details This file implementes user interface tasks definition.
*
*/

/* Includes ------------------------------------------------------------------*/
/* Pre-compiler coherency check */
#ifdef SKIP_THIS_CODE
#define PROJECT_CHK
#include "CrossCheck.h"
#endif

#undef PROJECT_CHK

#include "UITask.h"

#include "USARTParams.h"
#include "serial_lld.h"

#include "CANParams.h"
#include "can_lld.h"

#ifdef SKIP_THIS_CODE
    #include "MCLibraryISRPriorityConf.h"


    #include "DACParams.h"
#endif

#include "UIExportedFunctions.h"
#include "Parameters conversion.h"

#if !defined(DEFAULT_DAC_CHANNEL_1)
#define DEFAULT_DAC_CHANNEL_1 MC_PROTOCOL_REG_I_A
#endif
#if !defined(DEFAULT_DAC_CHANNEL_2)
#define DEFAULT_DAC_CHANNEL_2 MC_PROTOCOL_REG_I_B
#endif

#ifdef DUALDRIVE
  #include "Parameters conversion motor 2.h"
#endif

#define OPT_DACX  0x20 /*!<Bit field indicating that the UI uses SPI AD7303 DAC.*/

CUI oLCD = MC_NULL;

CUSART_COM oUSART = MC_NULL;

CCAN_COM oCAN = MC_NULL;
CFCP oFCP = MC_NULL;
CMCP_UI oMCP = MC_NULL;

CUI oDAC = MC_NULL;

CUFC_UI oUFC = MC_NULL;

static volatile uint16_t  bUITaskCounter;
static volatile uint16_t  bCOMTimeoutCounter;

/* Setup the exported functions see UIExportedFunctions.h enum. */
void* const exportedFunctions[EF_UI_NUMBERS] =
{
  (void*)(&UI_GetReg),
  (void*)(&UI_ExecSpeedRamp),
  (void*)(&UI_SetReg),
  (void*)(&UI_ExecCmd),
  (void*)(&UI_GetSelectedMCConfig),
  (void*)(&UI_SetRevupData),
  (void*)(&UI_GetRevupData),
  (void*)(&UI_SetDAC),
  (void*)(&UI_SetCurrentReferences)
};

void UI_TaskInit(uint8_t cfg, uint32_t* pUICfg, uint8_t bMCNum, CMCI oMCIList[],
                 CMCT oMCTList[],const char* s_fwVer)
{  

#if (defined(DAC_FUNCTIONALITY))
  #if (DAC_OPTION == OPT_DACT)
    if (cfg & OPT_DACT)
    {
      oDAC = (CUI)DACT_NewObject(MC_NULL,MC_NULL);
      
      UI_Init((CUI)oDAC, bMCNum, oMCIList, oMCTList, pUICfg); /* Init UI and link MC obj */
      UI_DACInit((CUI)oDAC); /* Init DAC */
      UI_SetDAC((CUI)oDAC, DAC_CH0, DEFAULT_DAC_CHANNEL_1);
      UI_SetDAC((CUI)oDAC, DAC_CH1, DEFAULT_DAC_CHANNEL_2);  
    }
  #endif

  #if (DAC_OPTION == OPT_DAC)
    if (cfg & OPT_DAC)
    {
      oDAC = (CUI)DAC_NewObject(MC_NULL,MC_NULL);
      
      UI_Init((CUI)oDAC, bMCNum, oMCIList, oMCTList, pUICfg); /* Init UI and link MC obj */
      UI_DACInit((CUI)oDAC); /* Init DAC */
      UI_SetDAC((CUI)oDAC, DAC_CH0, DEFAULT_DAC_CHANNEL_1);
      UI_SetDAC((CUI)oDAC, DAC_CH1, DEFAULT_DAC_CHANNEL_2);  
    }
  #endif


  #if (DAC_OPTION == OPT_DACF3)
    if (cfg & OPT_DAC)
    {
      oDAC = (CUI)DAC_F30X_NewObject(MC_NULL,&DAC_F30XParams_str);
      
      UI_Init((CUI)oDAC, bMCNum, oMCIList, oMCTList, pUICfg); /* Init UI and link MC obj */
      UI_DACInit((CUI)oDAC); /* Init DAC */
      UI_SetDAC((CUI)oDAC, DAC_CH0, DEFAULT_DAC_CHANNEL_1);
      UI_SetDAC((CUI)oDAC, DAC_CH1, DEFAULT_DAC_CHANNEL_2);  
    }
  #endif
    
  #if (DAC_OPTION == OPT_DACS)
    if (cfg & OPT_DACS)
    {
      oDAC = (CUI)DACS_NewObject(MC_NULL,MC_NULL);
      
      UI_Init((CUI)oDAC, bMCNum, oMCIList, oMCTList, pUICfg); /* Init UI and link MC obj */
      UI_DACInit((CUI)oDAC); /* Init DAC */
      UI_SetDAC((CUI)oDAC, DAC_CH0, DEFAULT_DAC_CHANNEL_1);
      UI_SetDAC((CUI)oDAC, DAC_CH1, DEFAULT_DAC_CHANNEL_2);  
    }
  #endif
    
  #if (DAC_OPTION == OPT_DACX)
    if (cfg & OPT_DACX)
    {
      oDAC = (CUI)DACX_NewObject(MC_NULL,MC_NULL);
      
      UI_Init((CUI)oDAC, bMCNum, oMCIList, oMCTList, pUICfg); /* Init UI and link MC obj */
      UI_DACInit((CUI)oDAC); /* Init DAC */
      UI_SetDAC((CUI)oDAC, DAC_CH0, DEFAULT_DAC_CHANNEL_1);
      UI_SetDAC((CUI)oDAC, DAC_CH1, DEFAULT_DAC_CHANNEL_2);  
    }
  #endif
#endif /* DAC_FUNCTIONALITY*/
 
#if (defined(SERIAL_COMMUNICATION))

  if (cfg & OPT_COM)
  {
#if (SERIAL_COM_MODE == COM_BIDIRECTIONAL)
    oMCP = MCP_NewObject(MC_NULL,&MCPParams); /* this call inside UI_NewObject */
    oUSART = USART_NewObject(&USARTParams_str);
    oFCP = FCP_NewObject(&FrameParams_str);

    FCP_Init(oFCP, (CCOM)oUSART);
    MCP_Init(oMCP, oFCP, oDAC, s_fwVer);
    UI_Init((CUI)oMCP, bMCNum, oMCIList, oMCTList, pUICfg); /* Init UI and link MC obj */

    /* UI_SelectMC( (CUI)oMCP, 1); */
    /* UI_SetReg((CUI)oMCP, MC_PROTOCOL_REG_SPEED_KP, 10); */

#endif
    
#if (SERIAL_COM_MODE == COM_UNIDIRECTIONAL)
    oUFC = UFC_NewObject(MC_NULL, &UFCParams_str);
#ifndef SKIP_THIS_CODE
    UFC_Init();
#else
    UFC_Init(oUFC);
#endif
    UI_Init((CUI)oUFC, bMCNum, oMCIList, oMCTList, pUICfg); /* Init UI and link MC obj */
    UFC_StartCom(oUFC); /* Start transmission */
#endif
  }  
#endif

#if (defined(CAN_COMMUNICATION))

  if (cfg & OPT_CAN)
  {
    oMCP = MCP_NewObject(MC_NULL,&MCPParams); /* this call inside UI_NewObject */
    oCAN = CAN_NewObject(&CANParams_str);
    oFCP = FCP_NewObject(&FrameParams_str);

    FCP_Init(oFCP, (CCOM)oCAN);
    MCP_Init(oMCP, oFCP, oDAC, s_fwVer);
    UI_Init((CUI)oMCP, bMCNum, oMCIList, oMCTList, pUICfg); /* Init UI and link MC obj */
}
#endif


}

void UI_WaitForCommand(void)
{
#if (defined(SERIAL_COMMUNICATION))
    while(1) {
        uint16_t msg;
        /* msg = sdGet(&SD1); */
        /* sd_lld_read(&SD1, (uint8_t *)&msg, 1); */
        while (sd_lld_read(&SD1,(uint8_t *)&msg,1)== 0)
        {
            /* sysWaitMicroseconds() */
            osalThreadDelayMilliseconds(1);
        };
    
        msg = (msg >> 8) & 0x00FF;
    
        /* debug: echo the received serial message
           sdPut(&SD1,msg);
           sd_lld_write(&SD1, (uint8_t *)&msg, 1);
        */
    
        USART_IRQHandlerRX(msg);
    
        /* GUI, this section is present only if serial communication is enabled.*/
        if (UI_SerialCommunicationTimeOutHasElapsed()){
            /* Send timeout message */
            Exec_UI_IRQ_Handler(UI_IRQ_USART,3,0); /* Flag 3 = Send timeout error */
        }
    }

#endif

#if (defined(CAN_COMMUNICATION))
    CANRxFrame rxmsg;
    int count =0;

   while(1) {
		if (can_lld_receive(&CAND1, CAN_ANY_MAILBOX, &rxmsg) == CAN_MSG_OK) {

			/* Configured Message */
			/* 0x01 - Set register frame. */
			/* 0x02 - Get register frame. */
			/* 0x03 - Execute command frame. */
			/* 0x06 - Get board info. */
			/* 0x07 - Exec ramp.*/
			/* 0x08 - Get revup data.*/
			/* 0x09 - Set revup data.*/
			/* 0x0A - Set current references.*/
			if ((rxmsg.SID == 0x01U) || (rxmsg.SID == 0x02U) || (rxmsg.SID == 0x03U) ||
                (rxmsg.SID == 0x06U) || (rxmsg.SID == 0x07U) || (rxmsg.SID == 0x08U) || (rxmsg.SID == 0x09U) || (rxmsg.SID == 0x0AU) )
			{
               CAN_IRQHandlerRX(rxmsg.SID);    	           /* CMD Frame */
               CAN_IRQHandlerRX(rxmsg.data8[0]);           /* CMD Length */
			   for (count=0; count < rxmsg.data8[0]; count++)
				   CAN_IRQHandlerRX(rxmsg.data8[1+count]); /* CMD Data[length] */
               CAN_IRQHandlerRX(rxmsg.data8[1+count]);     /* CMD CRC */

               /* GUI, this section is present only if serial communication is enabled.*/
               if (UI_SerialCommunicationTimeOutHasElapsed()){
                   /* Send timeout message */
                   Exec_UI_IRQ_Handler(UI_IRQ_USART,3,0); /* Flag 3 = Send timeout error */
               }

			}
		}
   }

#endif

}


void UI_Scheduler(void)
{
  if(bUITaskCounter > 0u)
  {
    bUITaskCounter--;
  }
  
  if(bCOMTimeoutCounter > 1u)
  {
    bCOMTimeoutCounter--;
  }
}

#define LCD_WELCOME_MESSAGE_DURATION_MS 1000
#define LCD_WELCOME_MESSAGE_DURATION_UI_TICKS (LCD_WELCOME_MESSAGE_DURATION_MS * UI_TASK_FREQUENCY_HZ) / 1000
static uint16_t LCD_welcome_message_UI_counter = LCD_WELCOME_MESSAGE_DURATION_UI_TICKS;

void UI_LCDRefresh(void)
{
    if (LCD_welcome_message_UI_counter == 0)
    {
      UI_LCDExec(oLCD);
      UI_LCDUpdateMeasured(oLCD);
    }
    else
    {
      LCD_welcome_message_UI_counter--;
    }
}

void UI_DACUpdate(uint8_t bMotorNbr)
{
  if (UI_GetSelectedMC((CUI)oDAC) == bMotorNbr)
  {
    UI_DACExec((CUI)oDAC); /* Exec DAC update */
  }
}

void MC_SetDAC(DAC_Channel_t bChannel, MC_Protocol_REG_t bVariable)
{
  UI_SetDAC(oDAC, bChannel, bVariable);
}

void MC_SetUserDAC(DAC_UserChannel_t bUserChNumber, int16_t hValue)
{
  UI_SetUserDAC(oDAC, bUserChNumber, hValue);
}

CUI GetLCD(void)
{
  return oLCD;
}

CMCP_UI GetMCP(void)
{
  return oMCP;
}

CUI GetDAC(void)
{
  return oDAC;
}

bool UI_IdleTimeHasElapsed(void)
{
  bool retVal = FALSE;
  if (bUITaskCounter == 0u)
  {
    retVal = TRUE;
  }
  return (retVal);
}

void UI_SetIdleTime(uint16_t SysTickCount)
{
  bUITaskCounter = SysTickCount;
}

bool UI_SerialCommunicationTimeOutHasElapsed(void)
{
  bool retVal = FALSE;
  if (bCOMTimeoutCounter == 1u)
  {
    bCOMTimeoutCounter = 0u;
    retVal = TRUE;
  }
  return (retVal);
}

#ifndef FREE_RTOS
void UI_SerialCommunicationTimeOutStop(void)
{
  bCOMTimeoutCounter = (uint16_t) 0u;
}
#endif

#ifndef FREE_RTOS
void UI_SerialCommunicationTimeOutStart(void)
{
  bCOMTimeoutCounter = (uint16_t) SERIALCOM_TIMEOUT_OCCURENCE_TICKS;
}
#endif
