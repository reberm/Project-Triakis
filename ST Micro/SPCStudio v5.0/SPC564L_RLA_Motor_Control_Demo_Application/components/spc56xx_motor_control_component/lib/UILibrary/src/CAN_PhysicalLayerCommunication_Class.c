/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    CAN_PhysicalLayerCommunication_Class.c
*   @version BETA 0.9.1
*   @brief   Private implementation for Physical Layer Communication
*          
*   @details Private implementation for Physical Layer Communication.
*
*/

/** @addtogroup SPC5_PMSM_UI_Library
  * @{
  */

/** @addtogroup CAN_PhysicalCommunicationLayer
  * @{
  */

/**
*   @defgroup Can_ph_layer_class_desc Can Physical Layer Communication class Description
*   @brief   CAN Physical Layer Communication class implementation
*   @details Applications on the market, that require an electrical motor to be driven, 
*            usually have the electronics split in two parts: application board and motor drive board.\n
*            To drive the system correctly, the application board requires a method to send a command 
*            to the motor drive board and get a feedback. \n
*            This is usually performed using a CAN communication. \n
*            See Figure 1: "Can communication in motor control application".\n
*            \image html Can_for_mc.PNG "Figure 1: Can Communication in Motor Control application."
*            Motor Control library is designed using an object oriented methodology.\n
*            Interface is managed through a sequence of classes to keep separated the physical 
*             communication(uart, can,..)  the transport protocol and the messages (motor control protocol)\n
*            CAN feature has been added in a coherency with this class structure by adding a specific 
*            CCAN_COM class to manage the can phy communication
*            \image html Can_infrastucture.png "Figure 2: Can Communication Infrastructure."
*            To send a CAN command to the MCTK, it should be set as below: \n
*            - CAN ID = Frame ID, 
*            - CAN Length = 8 or less
*            - Data containing following fields:
*            	- CAN Data[0] = PAYLOAD_LENGTH
*            	- CAN Data[1] = PAYLOAD_ID
*            	- …
*            	- CAN Data[PAYLOAD_LENGTH] = PAYLOAD[n]
*            	- CAN Data[PAYLOAD_LENGTH+1] = CRC
*            	.
*             .
*
*            It means that only command with PAYLOAD_LENGTH <= 6 can be managed by MCTK (Set revup data not available yet)
*
*            Only 2 baud rate are configured in the MCTK:\n
*            - 250 kbps
*            - 500 kbps
*            .
*            They can be selected from: \n 
*            Drive Management -> User Interface -> Can Details -> Baudrate!
*
*            Available frame ID are presented in table below, MCTK has already set the filters to receive those messages:
*

*            \anchor tab1_Can_frame_ex
*            <table border="1" CELLPADDING="5" align = "center">
*            <caption>Table 1: Can Frame Example</caption>
*              <tr>
*                <td><b>Command</b></td>
*                 <td><b>Can ID STD</b></td> 
*                <td><b>Can Data</b></td>
*               </tr>
*              <tr>
*                <td>RAMP FINAL SPEED (2000 rpm)</td>
*                 <td>01</td> 
*                <td>05 5B D0 07 00 00 39</td>
*               </tr>
*              <tr>
*                <td>BUS VOLTAGE</td>
*                 <td>02</td> 
*                <td>01 19 1C</td>
*               </tr>
*              <tr>
*                <td>START/STOP MOTOR</td>
*                 <td>03</td> 
*                <td>01 06 0A</td>
*              </tr>
*              <tr>
*                <td>GET BOARD INFO</td>
*                 <td>06</td> 
*                <td>01 00 07</td>
*               </tr>
*              <tr>
*                <td>EXEC RAMP(1000,2000)</td>
*                 <td>07</td> 
*                <td>06 E8 03 00 00 D0 07 D0</td>
*               </tr>
*              <tr>
*                <td>GET REV UP</td>
*                 <td>08</td> 
*                <td>01 Stage CRC</td>
*               </tr>
*              <tr>
*                <td>SET REF</td>
*                 <td>0A</td> 
*                <td>04 Iq_LB Iq_HB Id_LB Id_HB CRC</td>
*               </tr>
*            </table> 
*			
*/


/**
  * @}
  */
  
/**
  * @}
  */

/* Includes ------------------------------------------------------------------*/

#include "PhysicalLayerCommunication_Class.h"
#include "PhysicalLayerCommunication_Private.h"
#include "CAN_PhysicalLayerCommunication_Class.h"
#include "CAN_PhysicalLayerCommunication_Private.h"
#include "UIIRQHandlerPrivate.h"
#include "UIIRQHandlerClass.h"
#include "UITask.h"
#include "typedefs.h"
#include "Drive parameters.h"
#include "MC_type.h"
#include "can_lld.h"

#ifdef MC_CLASS_DYNAMIC
  #include "stdlib.h" /* Used for dynamic allocation */
#else
  #define MAX_CAN_COM_NUM 1
  static _CCAN_t CAN_COMpool[MAX_CAN_COM_NUM];
  static u8 CAN_COM_Allocated = 0;
#endif

/* Private function prototypes -----------------------------------------------*/
void CAN_HWInit(pCANParams_t pCANParams);
void* CAN_IRQ_Handler(void* this,unsigned char flags, unsigned short rx_data);
static void CAN_StartReceive(CCOM this);
static void CAN_StartTransmit(CCOM this);

CCAN_COM CAN_NewObject(pCANParams_t pCANParams)
{
  _CCOM _oCOM;
  _CCAN _oCAN;
  
  _oCOM = (_CCOM)COM_NewObject();


  #ifdef MC_CLASS_DYNAMIC
    _oCAN = (_CCAN)calloc(1,sizeof(_CCAN_t));
  #else
    if (CAN_COM_Allocated  < MAX_CAN_COM_NUM)
    {
      _oCAN = &CAN_COMpool[CAN_COM_Allocated++];
    }
    else
    {
      _oCAN = MC_NULL;
    }
  #endif
  
  _oCAN->pDParams_str = pCANParams;
  _oCOM->DerivedClass = (void*)_oCAN;
  _oCOM->Methods_str.pStartReceive = &CAN_StartReceive;
  _oCOM->Methods_str.pStartTransmit = &CAN_StartTransmit;
  
  _oCOM->Methods_str.pIRQ_Handler = &CAN_IRQ_Handler;
  Set_UI_IRQ_Handler(pCANParams->bUIIRQn, (_CUIIRQ)_oCOM);
  
  /* Init Struct communication */
  COM_ResetTX((CCOM)_oCOM);
  COM_ResetRX((CCOM)_oCOM);
  
  CAN_HWInit(pCANParams);

  return ((CCAN_COM)_oCOM);
}

void CAN_HWInit(pCANParams_t pCANParams)
{
  /* Start CAN */
  can_lld_start(pCANParams->CANx, pCANParams->CAN_InitStructure);
}

/*******************************************************************************
* Function Name  : CAN_IRQ_Handler
* Description    : Interrupt function for the can communication
* Input          : none 
* Return         : none
*******************************************************************************/
void* CAN_IRQ_Handler(void* this,unsigned char flags, unsigned short rx_data)
{
    void* pRetVal = MC_NULL;
    CANTxFrame canResponseMsg;
    u8 index = 0;
    u8 NumberOfTrasmit = 0;
    u8 count = 0;

    if (flags == 0) /* Flag 0 = RX - Getting Data from GUI */
    {
        /* Read one byte from the receive data register */
        if (((_CCOM)this)->Vars_str.PL_Data.RX.Buffer != MC_NULL &&
        ((_CCOM)this)->Vars_str.PL_Data.RX.BufferTransfer < ((_CCOM)this)->Vars_str.PL_Data.RX.BufferCount)
        {
            ((_CCOM)this)->Vars_str.PL_Data.RX.Buffer[((_CCOM)this)->Vars_str.PL_Data.RX.BufferTransfer++] = (uint16_t)(rx_data);

            pRetVal = ReceivingFrame(((_CCOM)this)->Vars_str.parent, ((_CCOM)this)->Vars_str.PL_Data.RX.BufferTransfer);
        }
    }

    if (flags == 1) /* Flag 1 = TX Sending Data to GUI */
    {
        /* Build the can message to be sent */
    	canResponseMsg.SID = CAN_TX_MSG_ID_START;
    	canResponseMsg.IDE = CAN_IDE_STD;
    	canResponseMsg.RTR = CAN_RTR_DATA;
    	/* Response message has more than 8 byte (FlexCan Limit) */
    	if (((_CCOM)this)->Vars_str.PL_Data.TX.BufferCount > CAN_MAX_MSG_LENGTH)
    	{
    		/* Number of 8 byte message needed to send BufferCount byte */
    		NumberOfTrasmit = ((_CCOM)this)->Vars_str.PL_Data.TX.BufferCount>>3; /* divide by CAN_MAX_MSG_LENGTH*/
    		if( (((_CCOM)this)->Vars_str.PL_Data.TX.BufferCount % CAN_MAX_MSG_LENGTH ) != 0)
    		{
    			/* spare byte to be sent */
    			NumberOfTrasmit++;
    		}

			for (count = 0; count < NumberOfTrasmit; count++)
			{
				while ((((_CCOM)this)->Vars_str.PL_Data.TX.BufferCount > ((_CCOM)this)->Vars_str.PL_Data.TX.BufferTransfer) &&
						(index<(CAN_MAX_MSG_LENGTH-1)) )
				{
					/* fill the message */
					index = ((_CCOM)this)->Vars_str.PL_Data.TX.BufferTransfer % CAN_MAX_MSG_LENGTH;
					canResponseMsg.data8[index] = ((_CCOM)this)->Vars_str.PL_Data.TX.Buffer[((_CCOM)this)->Vars_str.PL_Data.TX.BufferTransfer];
					((_CCOM)this)->Vars_str.PL_Data.TX.BufferTransfer++;

				}
				/* set the length */
				if (index == (CAN_MAX_MSG_LENGTH-1))
					canResponseMsg.LENGTH = CAN_MAX_MSG_LENGTH;
				else
					canResponseMsg.LENGTH = (((_CCOM)this)->Vars_str.PL_Data.TX.BufferCount % CAN_MAX_MSG_LENGTH);
				index = 0;

				/* transmit the message */
				can_lld_transmit(
						((_CCAN)(((_CCOM)this)->DerivedClass))->pDParams_str->CANx,
						(count+1),
						&canResponseMsg
						);
				/* when the buffer exceed 8 byte increase the message ID */
				canResponseMsg.SID++;
			}

    	}
    	else
    	{

			while ((((_CCOM)this)->Vars_str.PL_Data.TX.BufferCount > ((_CCOM)this)->Vars_str.PL_Data.TX.BufferTransfer))
			{
				index = ((_CCOM)this)->Vars_str.PL_Data.TX.BufferTransfer;
				canResponseMsg.data8[index] = ((_CCOM)this)->Vars_str.PL_Data.TX.Buffer[index];
				((_CCOM)this)->Vars_str.PL_Data.TX.BufferTransfer++;
			}

			canResponseMsg.LENGTH = ((_CCOM)this)->Vars_str.PL_Data.TX.BufferCount;
			can_lld_transmit(
					((_CCAN)(((_CCOM)this)->DerivedClass))->pDParams_str->CANx,
					1,
					&canResponseMsg
					);
    	}

        if (((_CCOM)this)->Vars_str.PL_Data.TX.BufferCount <= ((_CCOM)this)->Vars_str.PL_Data.TX.BufferTransfer)
        {

            /* Transmission terminated */

            /* Disable the CAN Transfer interrupt */
            CAN_ITConfig(((_CCAN)(((_CCOM)this)->DerivedClass))->pDParams_str->CANx, CAN_IT_TXE, DISABLE);

            SendingFrame(((_CCOM)this)->Vars_str.parent, ((_CCOM)this)->Vars_str.PL_Data.TX.BufferTransfer);

            /* Init communication for next transfer; */
            /* PL_ResetTX(); */ /* commented even in the original code */
        }
    }
    if (flags == 2) /* Flag 2 = Send overrun error */
    {
        FCP_SendOverrunMeassage(((_CCOM)this)->Vars_str.parent);
    }
    if (flags == 3) /* Flag 3 = Send timeout error */
    {
        FCP_SendTimeoutMeassage(((_CCOM)this)->Vars_str.parent);
    }

    return pRetVal;
}

/**
  * @brief  Start receive from the channel (IRQ enabling implementation)
  * @param  this: COM object 
  * @retval None
  */
static void CAN_StartReceive(CCOM this)
{
  CAN_ITConfig(((_CCAN)(((_CCOM)this)->DerivedClass))->pDParams_str->CANx, CAN_IT_RXNE, ENABLE);
}

/**
  * @brief  Start transmit to the channel (IRQ enabling implementation)
  * @param  this: COM object 
  * @retval None
  */
static void CAN_StartTransmit(CCOM this)
{
  CAN_ITConfig(((_CCAN)(((_CCOM)this)->DerivedClass))->pDParams_str->CANx, CAN_IT_TXE, ENABLE);
}

/**
  * @brief  Enables or disables the specified CAN interrupts.
  * @param  CANx: where x can be 1, 2 to select the CAN peripheral.
  * @param  CAN_IT: specifies the CAN interrupt sources to be enabled or disabled.
  *          This parameter can be one of the following values:
  *            @arg CAN_IT_TXE:  Transmit Data Register empty interrupt
  *            @arg CAN_IT_RXNE: Receive Data register not empty interrupt
  * @param  NewState: new state of the specified CANx interrupts.
  *          This parameter can be: ENABLE or DISABLE.
  * @retval None
  */
void CAN_ITConfig(CANDriver * CANx, uint32_t CAN_IT, FunctionalState NewState) {

	/* Disable TX interrupts */
    if ((CAN_IT == CAN_IT_TXE) && (NewState == DISABLE)){
      /* for SPC5 Studio do nothing */
    }

    /* Enable TX Interrupts */
    if ((CAN_IT == CAN_IT_TXE) && (NewState == ENABLE)) {
        /* simulate the TX INT by calling the TX handler */
        CAN_IRQHandlerTX();
    }

    /* Disable RX interrupt */
    if ((CAN_IT == CAN_IT_RXNE) && (NewState == DISABLE)) {
      /* for SPC5 Studio do nothing */
    }

    /* Enable RX interrupt */
    if ((CAN_IT == CAN_IT_RXNE) && (NewState == ENABLE)) {
      /* for SPC5 Studio do nothing */
    }

    if ((CAN_IT != CAN_IT_RXNE)  &&  (CAN_IT != ((uint16_t)CAN_IT_TXE))){
      /* Else,  Error....do something */
      while (1)
      {
          ;
      }
    }

}

/**
  * @brief  This function handles CAN1 interrupt request.
  * @param  None
  * @retval None
  */
void CAN_IRQHandlerRX(uint16_t Data)
{
  uint16_t retVal;

  retVal = *(uint16_t*)(Exec_UI_IRQ_Handler(UI_IRQ_CAN,0, Data)); /* Flag 0 = RX */

  if (retVal == 1)  {
    UI_SerialCommunicationTimeOutStart();
  }
  if (retVal == 2)
  {
    UI_SerialCommunicationTimeOutStop();
  }

}

void CAN_IRQHandlerTX(void)
{

  Exec_UI_IRQ_Handler(UI_IRQ_CAN,1,0); /* Flag 1 = TX */

}


  
  