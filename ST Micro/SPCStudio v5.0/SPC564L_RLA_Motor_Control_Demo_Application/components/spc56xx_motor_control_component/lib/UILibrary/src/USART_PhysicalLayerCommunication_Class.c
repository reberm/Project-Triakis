/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    USART_PhysicalLayerCommunication_Class.c
*   @version BETA 0.9.1
*   @brief   Private implementation for Physical Layer Communication
*          
*   @details Private implementation for Physical Layer Communication.
*
*/

/* Includes ------------------------------------------------------------------*/

#include "PhysicalLayerCommunication_Class.h"
#include "PhysicalLayerCommunication_Private.h"
#include "USART_PhysicalLayerCommunication_Class.h"
#include "USART_PhysicalLayerCommunication_Private.h"
#include "UIIRQHandlerPrivate.h"
#include "UIIRQHandlerClass.h"
#include "UITask.h"
#include "typedefs.h"
#include "Drive parameters.h"
#include "MC_type.h"
#include "serial_lld.h"

#ifdef MC_CLASS_DYNAMIC
  #include "stdlib.h" /* Used for dynamic allocation */
#else
  #define MAX_USART_COM_NUM 1
  static _CUSART_t USART_COMpool[MAX_USART_COM_NUM];
  static u8 USART_COM_Allocated = 0;
#endif

/* Private function prototypes -----------------------------------------------*/
void USART_HWInit(pUSARTParams_t pUSARTParams);
void* USART_IRQ_Handler(void* this,unsigned char flags, unsigned short rx_data);
static void USART_StartReceive(CCOM this);
static void USART_StartTransmit(CCOM this);

/**
  * @brief  Creates an object of the class "Physical Layer Communication"
  * @param  pSensorParam: Physical Layer parameters
  * @retval oCOM: new Physical Layer object
  */
CUSART_COM USART_NewObject(pUSARTParams_t pUSARTParams)
{
  _CCOM _oCOM;
  _CUSART _oUSART;
  
  _oCOM = (_CCOM)COM_NewObject();


  #ifdef MC_CLASS_DYNAMIC
    _oUSART = (_CUSART)calloc(1,sizeof(_CUSART_t));
  #else
    if (USART_COM_Allocated  < MAX_USART_COM_NUM)
    {
      _oUSART = &USART_COMpool[USART_COM_Allocated++];
    }
    else
    {
      _oUSART = MC_NULL;
    }
  #endif
  
  _oUSART->pDParams_str = pUSARTParams;
  _oCOM->DerivedClass = (void*)_oUSART;
  _oCOM->Methods_str.pStartReceive = &USART_StartReceive;
  _oCOM->Methods_str.pStartTransmit = &USART_StartTransmit;
  
  _oCOM->Methods_str.pIRQ_Handler = &USART_IRQ_Handler;
  Set_UI_IRQ_Handler(pUSARTParams->bUIIRQn, (_CUIIRQ)_oCOM);
  
  /* Init Struct communication */
  COM_ResetTX((CCOM)_oCOM);
  COM_ResetRX((CCOM)_oCOM);
  
  USART_HWInit(pUSARTParams);

  return ((CUSART_COM)_oCOM);
}

void USART_HWInit(pUSARTParams_t pUSARTParams)
{
  /* Start USART */
  sd_lld_start(&SD1, MC_NULL);
}

/*******************************************************************************
* Function Name  : USART_IRQ_Handler
* Description    : Interrupt function for the serial communication
* Input          : none 
* Return         : none
*******************************************************************************/
void* USART_IRQ_Handler(void* this,unsigned char flags, unsigned short rx_data)
{
    void* pRetVal = MC_NULL;
    if (flags == 0) /* Flag 0 = RX - Getting Data from GUI */
    {
        /* Read one byte from the receive data register */
        if (((_CCOM)this)->Vars_str.PL_Data.RX.Buffer != MC_NULL &&
        ((_CCOM)this)->Vars_str.PL_Data.RX.BufferTransfer < ((_CCOM)this)->Vars_str.PL_Data.RX.BufferCount)
        {
            ((_CCOM)this)->Vars_str.PL_Data.RX.Buffer[((_CCOM)this)->Vars_str.PL_Data.RX.BufferTransfer++] = (uint16_t)(rx_data & (uint16_t)0x01FF);

            pRetVal = ReceivingFrame(((_CCOM)this)->Vars_str.parent, ((_CCOM)this)->Vars_str.PL_Data.RX.BufferTransfer);
        }
    }

    if (flags == 1) /* Flag 1 = TX Sending Data to GUI */
    {
        /* Write one byte to the transmit data register */
        /* USART_SendData (((_CUSART)(((_CCOM)this)->DerivedClass))->pDParams_str->USARTx, ((_CCOM)this)->Vars_str.PL_Data.TX.Buffer[((_CCOM)this)->Vars_str.PL_Data.TX.BufferTransfer++]); */

        /* I am putting element to the SD1 output queue */
        while ((((_CCOM)this)->Vars_str.PL_Data.TX.BufferCount > ((_CCOM)this)->Vars_str.PL_Data.TX.BufferTransfer))
        {
            sd_lld_write(
                    &SD1,
                    (uint8_t *)&((_CCOM)this)->Vars_str.PL_Data.TX.Buffer[((_CCOM)this)->Vars_str.PL_Data.TX.BufferTransfer++],
                    1);
        }
        if (((_CCOM)this)->Vars_str.PL_Data.TX.BufferCount <= ((_CCOM)this)->Vars_str.PL_Data.TX.BufferTransfer)
        {

            /* Transmission terminated */

            /* Disable the USART Transfer interrupt */
            USART_ITConfig(((_CUSART)(((_CCOM)this)->DerivedClass))->pDParams_str->USARTx, USART_IT_TXE, DISABLE);

            SendingFrame(((_CCOM)this)->Vars_str.parent, ((_CCOM)this)->Vars_str.PL_Data.TX.BufferTransfer);

            /* Init communication for next transfer; */
            /* PL_ResetTX(); */ /* commented even in the original code */
        }
    }
    if (flags == 2) /* Flag 2 = Send overrun error */
    {
        FCP_SendOverrunMeassage(((_CCOM)this)->Vars_str.parent);
    }
    if (flags == 3) /* Flag 3 = Send timeout error */
    {
        FCP_SendTimeoutMeassage(((_CCOM)this)->Vars_str.parent);
    }
    return pRetVal;
}

/**
  * @brief  Start receive from the channel (IRQ enabling implementation)
  * @param  this: COM object 
  * @retval None
  */
static void USART_StartReceive(CCOM this)
{
  USART_ITConfig(((_CUSART)(((_CCOM)this)->DerivedClass))->pDParams_str->USARTx, USART_IT_RXNE, ENABLE);
}

/**
  * @brief  Start transmit to the channel (IRQ enabling implementation)
  * @param  this: COM object 
  * @retval None
  */
static void USART_StartTransmit(CCOM this)
{
  USART_ITConfig(((_CUSART)(((_CCOM)this)->DerivedClass))->pDParams_str->USARTx, USART_IT_TXE, ENABLE);
}

/**
  * @brief  Enables or disables the specified USART interrupts.
  * @param  USARTx: where x can be 1, 2, 3, 4, 5 or 6 to select the USART or
  *         UART peripheral.
  * @param  USART_IT: specifies the USART interrupt sources to be enabled or disabled.
  *          This parameter can be one of the following values:
  *            @arg USART_IT_CTS:  CTS change interrupt
  *            @arg USART_IT_LBD:  LIN Break detection interrupt
  *            @arg USART_IT_TXE:  Transmit Data Register empty interrupt
  *            @arg USART_IT_TC:   Transmission complete interrupt
  *            @arg USART_IT_RXNE: Receive Data register not empty interrupt
  *            @arg USART_IT_IDLE: Idle line detection interrupt
  *            @arg USART_IT_PE:   Parity Error interrupt
  *            @arg USART_IT_ERR:  Error interrupt(Frame error, noise error, overrun error)
  * @param  NewState: new state of the specified USARTx interrupts.
  *          This parameter can be: ENABLE or DISABLE.
  * @retval None
  */
void USART_ITConfig(USART_TypeDef* USARTx, uint32_t USART_IT, FunctionalState NewState) {
/* This is the call from USART_IRQ_Handler with (flags == 1 =  TX Sending Data to GUI)
 * USART_ITConfig(((_CUSART)(((_CCOM)this)->DerivedClass))->pDParams_str->USARTx, USART_IT_TXE, DISABLE)
 *
 */
    /* Disable TX interrupts */
    if ((USART_IT == USART_IT_TXE) && (NewState == DISABLE)){
      /* for SPC5 Studio do nothing */
    }

    /* Enable TX Interrupts */
    if ((USART_IT == USART_IT_TXE) && (NewState == ENABLE)) {
        /* simulate the TX INT by calling the TX handler */
        USART_IRQHandlerTX();
    }

    /* Disable RX interrupt */
    if ((USART_IT == USART_IT_RXNE) && (NewState == DISABLE)) {
      /* for SPC5 Studio do nothing */
    }

    /* Enable RX interrupt */
    if ((USART_IT == USART_IT_RXNE) && (NewState == ENABLE)) {
      /* for SPC5 Studio do nothing */
    }

    if ((USART_IT != USART_IT_RXNE)  &&  (USART_IT != ((uint16_t)USART_IT_TXE))){
      /* Else,  Error....do something */
      while (1)
      {
          ;
      }
    }

}

  /**
    * @brief  Transmits single data through the USARTx peripheral.
    * @param  USARTx: Select the USART or the UART peripheral.
    *   This parameter can be one of the following values:
    *   USART1, USART2, USART3, UART4 or UART5.
    * @param  Data: the data to transmit.
    * @retval None
    */
  void USART_SendData(USART_TypeDef* USARTx, uint16_t Data)
  {
  /* do nothing because data are already in the SD1 queue */

}


#ifdef SERIAL_COMMUNICATION

/*Start here***********************************************************/
/*GUI, this section is present only if serial communication is enabled*/
/**
  * @brief  This function handles USART1 interrupt request.
  * @param  None
  * @retval None
  */
void USART_IRQHandlerRX(uint16_t Data)
{
  typedef void* (*pExec_UI_IRQ_Handler_t) (unsigned char bIRQAddr, uint16_t retVal);
  uint16_t retVal;

  retVal = *(uint16_t*)(Exec_UI_IRQ_Handler(UI_IRQ_USART,0, Data)); /* Flag 0 = RX */
  if (retVal == 1)  {
   UI_SerialCommunicationTimeOutStart();
  }
  if (retVal == 2)
  {
   UI_SerialCommunicationTimeOutStop();
  }
}

void USART_IRQHandlerTX(void)
{

  Exec_UI_IRQ_Handler(UI_IRQ_USART,1,0); /* Flag 1 = TX */

}
#endif
