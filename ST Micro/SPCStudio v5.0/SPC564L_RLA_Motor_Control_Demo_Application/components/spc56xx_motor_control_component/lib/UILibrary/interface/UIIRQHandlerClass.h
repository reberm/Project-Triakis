/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    UIIRQHandlerClass.h
*   @version BETA 0.9.1
*   @brief   This file contains interface of UI IRQ handler class
*          
*   @details This file contains interface of UI IRQ handler class.
*
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __UI_IRQHANDLERCLASS_H
#define __UI_IRQHANDLERCLASS_H

/** @addtogroup SPC5_PMSM_UI_Library
  * @{
  */

/** @addtogroup UserInterface_IRQHandler
  * @{
  */
  
/** @defgroup UserInterface_class_exported_types UserInterface class exported types
* @{
*/

/* MC IRQ Addresses */
#define UI_IRQ_USART            0u  /*!< Reserved for UIClass serial communication.*/ 
/* MC IRQ Addresses */
#define UI_IRQ_CAN              0u  /*!< Reserved for UIClass serial communication.*/

/**
* @}
*/

/** @defgroup UserInterface_class_exported_methods UserInterface class exported methods
  * @{
  */

/*Methods*/
void* Exec_UI_IRQ_Handler(unsigned char bIRQAddr, unsigned char flag, unsigned short rx_data);

/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

#endif /* __UI_IRQHANDLERCLASS_H */
