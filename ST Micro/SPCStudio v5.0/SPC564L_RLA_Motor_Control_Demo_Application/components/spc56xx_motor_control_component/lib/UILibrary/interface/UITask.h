/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    UITask.h
*   @version BETA 0.9.1
*   @brief   Interface of UITask module
*          
*   @details Interface of UITask module.
*
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __UITASK_H
#define __UITASK_H

#include "UserInterfaceClass.h"
#include "MotorControlProtocolClass.h"
#include "FrameCommunicationProtocolClass.h"
#include "PhysicalLayerCommunication_Class.h"
#include "USART_PhysicalLayerCommunication_Class.h"
#include "UnidirectionalFastCom_UserInterfaceClass.h"
#include "UIIRQHandlerClass.h"

/* Exported functions --------------------------------------------------------*/
void UI_TaskInit(uint8_t cfg, uint32_t* pUICfg, uint8_t bMCNum, CMCI oMCIList[],
                 CMCT oMCTList[],const char* s_fwVer);
void UI_WaitForCommand(void);
void UI_Scheduler(void);
void UI_LCDRefresh(void);
void UI_DACUpdate(uint8_t bMotorNbr);
CUI GetLCD(void);
CMCP_UI GetMCP(void);
CUI GetDAC(void);

bool UI_IdleTimeHasElapsed(void);
void UI_SetIdleTime(uint16_t SysTickCount);
bool UI_SerialCommunicationTimeOutHasElapsed(void);
#ifndef FREE_RTOS
void UI_SerialCommunicationTimeOutStop(void);
void UI_SerialCommunicationTimeOutStart(void);
#endif

/* Exported defines ----------------------------------------------------------*/

#define COM_BIDIRECTIONAL  0x01
#define COM_UNIDIRECTIONAL 0x02

#endif /* __UITASK_H */
