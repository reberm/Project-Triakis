/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    CAN_PhysicalLayerCommunication_Class.h
*   @version BETA 0.9.1
*   @brief   This file contains interface of CAN Physical Layer Communication class
*          
*   @details This file contains interface of CAN Physical Layer Communication class.
*
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __CAN_PLC_H
#define __CAN_PLC_H

#include "MC_type.h"
#include "typedefs.h"
#include "can_lld_cfg.h"

/** @addtogroup SPC5_PMSM_UI_Library
  * @{
  */

/** @addtogroup CAN_PhysicalCommunicationLayer
  * @{
  */
  
/** @defgroup CAN_COM_class_exported_types CAN physical layer communication class exported types
* @{
*/

/* Exported constants --------------------------------------------------------*/



typedef struct
{
  uint32_t CAN_BaudRate;        /*!< This member configures the CAN communication baud rate.
                                     The baud rate is computed using the following formula:
                                     - IntegerDivider = ((PCLKx) / (16 * (CAN_InitStruct->CAN_BaudRate)))
                                     - FractionalDivider = ((IntegerDivider - ((uint32_t) IntegerDivider)) * 16) + 0.5 */

  uint32_t CAN_WordLength;          /*!< Specifies the number of data bits transmitted or received in a frame.
                                           This parameter can be a value of @ref CAN_Word_Length */

} CAN_InitTypeDef;

/* Exported types ------------------------------------------------------------*/
/**
  * @brief  UserInterface class parameters definition
  */
typedef const struct
{
  CANDriver *CANx;
  uint32_t wCANClockSource;
  uint8_t bUIIRQn;
  CANConfig * CAN_InitStructure;
}CANParams_t, *pCANParams_t;


#define CAN_PRE_EMPTION_PRIORITY 3u

#define CAN_IT_TXE                         ((uint32_t)0x00000727)
#define CAN_IT_RXNE                        ((uint32_t)0x00050105)

/** 
  * @brief  Public UserInterface class definition 
  */
typedef struct CCAN_COM_t *CCAN_COM;

/**
  * @}
  */

/** @defgroup CAN_COM_class_exported_methods CAN physical layer communication class exported methods
  * @{
  */

/**
  * @brief  Creates an object of the class CCAN_COM_t
  * @param  pCANParams pointer to pCANParams_t 
  *         structure
  * @retval CCAN_COM new instance of CAN_COM object
  */
CCAN_COM CAN_NewObject(pCANParams_t pCANParams);


void CAN_ITConfig(CANDriver *CANx, uint32_t CAN_IT, FunctionalState NewState);
void CAN_SendData(CANDriver *CANx, uint16_t Data);
void CAN_IRQHandlerRX(uint16_t Data);
void CAN_IRQHandlerTX(void);

/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */
#endif /* __CAN_PLC_H */
