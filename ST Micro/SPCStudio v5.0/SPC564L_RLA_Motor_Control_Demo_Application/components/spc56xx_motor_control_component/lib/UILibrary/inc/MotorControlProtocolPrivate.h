/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    MotorControlProtocolPrivate.h
*   @version BETA 0.9.1
*   @brief   Private definition for MotorControlProtocol class
*          
*   @details Private definition for MotorControlProtocol class.
*
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MCP_MCUI_H
#define __MCP_MCUI_H

/* 
 * SWAP_ENDIAN_32
 * ((value>>24)&0xff)    |    // move byte 3 to byte 0
 * ((value<<8)&0xff0000) |    // move byte 1 to byte 2
 * ((value>>8)&0xff00)   |    // move byte 2 to byte 1
 * ((value<<24)&0xff000000);  // move byte 0 to byte 3
 *
 */

/* To change endianess when read 4 bytes from memory and sent over UART byte by byte */
#define SWAP_ENDIAN_32(value) \
{ \
    int32_t swappedvalue; \
    swappedvalue = ((value>>24)&0xff) | ((value<<8)&0xff0000) |((value>>8)&0xff00) | ((value<<24)&0xff000000); \
    value = swappedvalue; \
}

typedef struct
{
  /* Derived Class variables */
  CFCP oFCP;
  uint8_t BufferFrame[FRAME_MAX_BUFFER_LENGTH];
  uint8_t BufferSize;
  const char *s_fwVer;
  CUI oDAC;
}DVars_t,*pDVars_t;

typedef MCPParams_t DParams_t, *pDParams_t; 

typedef struct
{
  DVars_t DVars_str;
  pDParams_t pDParams_str;
}_DCMCP_t, *_DCMCP;

#endif /* __MCP_MCUI_H */
