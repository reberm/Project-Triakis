/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    CAN_PhysicalLayerCommunication_Private.h
*   @version BETA 0.9.1
*   @brief   Private definition for Physical Layer Communication
*          
*   @details Private definition for Physical Layer Communication.
*
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __CAN_COM_H
#define __CAN_COM_H

typedef CANParams_t DParams_t, *pDParams_t;

typedef struct
{
  pDParams_t pDParams_str;
}_CCAN_t, *_CCAN;

#endif /* __CAN_COM_H */
