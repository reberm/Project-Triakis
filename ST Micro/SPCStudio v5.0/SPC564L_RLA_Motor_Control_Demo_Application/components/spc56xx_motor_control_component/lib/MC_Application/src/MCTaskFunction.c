/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    MCTasksFunction.c
*   @version BETA 0.9.1
*   @brief   This file implementes user functions for High/Medium freq task
*          
*   @details This file implementes user functions for High/Medium freq task.
*
*/

/* Includes ------------------------------------------------------------------*/
#include "MC.h"  
#include "MCTaskFunction.h"

int16_t value_Speed_RPM = 0;

void TSK_MediumFrequencyTaskM1Completed(void)
{
   /* It is editable by the user */
}

void TSK_HighFrequencyTaskCompleted(void)
{
   /* It is editable by the user */
}
