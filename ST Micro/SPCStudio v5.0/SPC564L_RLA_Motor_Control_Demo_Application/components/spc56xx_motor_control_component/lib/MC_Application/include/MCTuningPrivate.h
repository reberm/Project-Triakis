/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    MCTuningPrivate.h
*   @version BETA 0.9.1
*   @brief   This file contains private definition of MCTuning class 
*          
*   @details This file contains private definition of MCTuning class.
*
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MCTUNINGPRIVATE_H
#define __MCTUNINGPRIVATE_H

/** @addtogroup SPC5_PMSM_MC_Application
  * @{
  */
  
/** @addtogroup MCTuning
  * @{
  */

/** @defgroup MCTuning_class_private_types MCTuning class private types
* @{
*/

/** 
  * @brief  MCTuning class members definition
  */
typedef struct
{
    MCTuningInitStruct_t MCTuningInitStruct;
}Vars_t,*pVars_t;

/** 
  * @brief  Redefinition of parameter structure
  */
typedef MCTuningParams_t Params_t, *pParams_t;

/** 
  * @brief  Private MCTuning class definition 
  */
typedef struct
{
    Vars_t Vars_str;        /*!< Class members container */
    pParams_t pParams_str;  /*!< Class parameters container */
}_CMCT_t, *_CMCT;
/**
  * @}
  */
  
/**
  * @}
  */

/**
  * @}
  */

#endif /*__MCTUNINGPRIVATE_H*/
