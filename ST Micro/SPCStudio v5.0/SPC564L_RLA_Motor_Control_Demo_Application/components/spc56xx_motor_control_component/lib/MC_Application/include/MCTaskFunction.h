/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    MCTaskFunction.h
*   @version BETA 0.9.1
*   @brief   This file contains interface of MCTuning class 
*          
*   @details This file contains interface of MCTuning class.
*
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MCTASKSFUNCTION_H
#define __MCTASKSFUNCTION_H

/* Includes ------------------------------------------------------------------*/

void TSK_MediumFrequencyTaskM1Completed(void);
void TSK_HighFrequencyTaskCompleted(void);

#endif /* __MCTASKSFUNCTION_H */
