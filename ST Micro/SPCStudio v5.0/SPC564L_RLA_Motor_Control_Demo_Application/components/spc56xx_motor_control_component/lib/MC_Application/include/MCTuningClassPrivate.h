/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    MCTuningClassPrivate.h
*   @version BETA 0.9.1
*   @brief   This file contains private implementation of MCTuning class 
*          
*   @details This file contains private implementation of MCTuning class.
*
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MCTUNINGCLASSPRIVATE_H
#define __MCTUNINGCLASSPRIVATE_H

/* Includes ------------------------------------------------------------------*/
#include "MC_type.h"

/** @addtogroup SPC5_PMSM_MC_Application
  * @{
  */
  
/** @addtogroup MCTuning
  * @{
  */
  
/** @defgroup MCTuning_class_exported_types MCTuning class exported types
* @{
*/

/** 
  * @brief  MC tuning internal objects initialization structure type;
  */
typedef struct
{
  CPI   oPIDSpeed;
  CPI   oPIDIq;
  CPI   oPIDId;
  CPI   oPIDFluxWeakening;
  CPWMC oPWMnCurrFdbk;
  CRUC  oRevupCtrl;
  CSPD  oSpeedSensorMain;
  CSPD  oSpeedSensorAux;
  CSPD  oSpeedSensorVirtual;
  CSTC  oSpeednTorqueCtrl;
  CSTM  oStateMachine;
  CTSNS oTemperatureSensor;
  CVBS  oBusVoltageSensor;
  CDOUT oBrakeDigitalOutput;
  CDOUT oNTCRelay;
  CMPM  oMPM;
  CFW   oFW;
  CFF   oFF;
  CHFI_FP oHFI;
} MCTuningInitStruct_t;
  
/**
* @}
*/

/** @defgroup MCTuning_class_private_methods MCTuning class private methods
  * @{
  */

/**
  * @brief  Creates an object of the class MCTuning
  * @param  pMCTuningParams pointer to an MCTuning parameters structure
  * @retval CMCT new instance of MCTuning object
  */
CMCT MCT_NewObject(pMCTuningParams_t pMCTuningParams);

/**
  * @brief  Example of public method of the class MCTuning
  * @param  this related object of class CMCT
  * @retval none
  */
void MCT_Init(CMCT this, MCTuningInitStruct_t MCTuningInitStruct);

/**
  * @}
  */
  
/**
  * @}
  */

/**
  * @}
  */

#endif /* __MCTUNINGCLASSPRIVATE_H */
