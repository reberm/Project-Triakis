/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    MC_Lib_Setup.h
*   @version BETA 0.9.1
*   @brief   Motor control library header file
*          
*   @details Motor control library header file.
*
*/

#ifndef MC_LIB_SETUP_H
#define MC_LIB_SETUP_H

#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
*                                        INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/
#include "Drive parameters.h"

/*==================================================================================================
*                                           MACROS
==================================================================================================*/
#define INTC_INTERRUPTS_REQUEST_VECTOR_TABLE_SIZE (308*4)

/*==================================================================================================
*                                      GLOBAL VARIABLES
==================================================================================================*/

/*==================================================================================================
*                                      LOCAL VARIABLES
==================================================================================================*/


/*==================================================================================================
*                                   LOCAL FUNCTION PROTOTYPES
==================================================================================================*/
#if !defined(SPC5_INIT)
void MC_Lib_InitINTC(void);
void MC_Lib_EnableIrq(void);
void WAIT_EN_L9907(void);
void Configure_L9907(void);
#endif /* #if !defined(SPC5_INIT) */
uint8 STM_Configuration(uint32 ticks);

#ifdef __cplusplus
}
#endif

#endif  /* MC_LIB_SETUP_H */
