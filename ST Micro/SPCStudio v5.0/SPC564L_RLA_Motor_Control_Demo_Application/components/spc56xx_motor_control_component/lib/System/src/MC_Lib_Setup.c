/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    MC_Lib_Setup.c
*   @version BETA 0.9.1
*   @brief   Micro Library utilities
*          
*   @details Micro Library utilities.
*
*/

#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
*                                        INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/
#include "platform.h"
#include "MC_Lib_Setup.h"
#include "Reg_eSys_Stm.h"
#include "Reg_Macros.h"

/*===============================================================================================
*                                       CONSTANTS
===============================================================================================*/
#define SPI_SIUL_PCR(PIN)             ((uint32)SIUL_BASEADDR + (uint32)0x40UL + (((uint32)PIN)<<1))
#define SIUL_PCR_PA_ALT1              (uint16)0x0400U
#define SPI_SIUL_PCR_OBE_ON           (uint16)0x0200U
#define SPI_SIUL_PCR_OBE_OFF          (uint16)0x0000U
#define SPI_SIUL_PCR_IBE_ON           (uint16)0x0100U
#define SPI_SIUL_PCR_IBE_OFF          (uint16)0x0000U
#define SPI_SIUL_PCR_ODE_ON           (uint16)0x0020U
#define SPI_SIUL_PCR_ODE_OFF          (uint16)0x0000U
#define SPI_SIUL_PCR_SRC_ON           (uint16)0x0004U

#define SPI_CMD0_OFFSET_1 0x0409
#define SPI_CMD0_RET      0x0800
#define SPI_DIAG_RET      0xC800

/* 1 bit: 0 CSA1 ref. GND, 1 ref. Phase */
#define CMD0_CSA1_OFFSET 1
/* 2 bit  CSA1 gain x10 (00), x30 (01), x50 (10), x100 (11) */
#define CMD0_CSA1_GAIN 0
/* 1 bit: 0 CSA2 ref. GND, 1 ref. Phase */
#define CMD0_CSA2_OFFSET 1
/* 2 bit  CSA2 gain x10 (00), x30 (01), x50 (10), x100 (11) */
#define CMD0_CSA2_GAIN 0      
/* 2 bit  Curr. 25% Imax (00), 50% (01), 75% (10), 100% (11) */
#define CMD0_TURNONOFF_CURR_LIMIT 3
/* 2 bit */
#define CMD0_DEAD_TIME 00

#define CMD0_WRITE_ENABLE 1           /* 1 bit */
#define CMD0_WRITE_DISABLE 0          /* 1 bit */

/* Helpful macros for parity */

#define PARITYodd33(u33) \
    ( \
        ((((((((((((((( \
            (u33) \
        &0x555555555)*5)>>2) \
        &0x111111111)*0x11)>>4) \
        &0x101010101)*0x101)>>8) \
        &0x100010001)*0x10001)>>16) \
        &0x100000001)*0x100000001)>>32) \
    &1)

#define PARITY16(u16) PARITYodd33(((unsigned long long)u16)*0x20001)


#define SPI_EVAL9907_CMD0_WITHOUT_PARITY ( (CMD0_CSA1_OFFSET) | (CMD0_CSA1_GAIN<<1) |\
(CMD0_CSA2_OFFSET<<3) | (CMD0_CSA2_GAIN<<4) |\
(CMD0_TURNONOFF_CURR_LIMIT<<6) | (CMD0_DEAD_TIME<<8) |\
(CMD0_WRITE_ENABLE<<10))

/* Odd parity */
#define CMD0_PARITY (PARITY16(SPI_EVAL9907_CMD0_WITHOUT_PARITY) ^ 1)

#define SPI_EVAL9907_CMD0_WRITE (SPI_EVAL9907_CMD0_WITHOUT_PARITY | (CMD0_PARITY<<11)) 

#define SPI_EVAL9907_CMD0_READ (SPI_EVAL9907_CMD0_WRITE & 0xE3FFu)


#define SPI_CMD0_OFFSET_1 0x0409
#define SPI_CMD1          0x2401
#define SPI_CMD0_RET      0x0800
#define SPI_DIAG_RET      0xC800
#define SPI_DIAG2         0xE000

/*==================================================================================================
*                                      GLOBAL VARIABLES
==================================================================================================*/

/*==================================================================================================
*                                       LOCAL FUNCTIONS
==================================================================================================*/

/*===============================================================================================
*                                       LOCAL VARIABLES
===============================================================================================*/
#if !SPC5_INIT
/**
* @brief   MC_Lib_InitINTC
* @details The function initializes the interrupt controller module.
*
* @return none
* @retval void
*
* @pre none
* @post none
*/
void MC_Lib_InitINTC(void)
 {
    /* Current Sensing -  DMA channel 0 priority - CTU FIFO 1  */
    REG_WRITE8(INTC_PSR(11),15);

    /* TB Scheduler */
    REG_WRITE8(INTC_PSR(30),14); 

    /* ENCODER:  etimer 1 channel 1 (index signal) */
    REG_WRITE8(INTC_PSR(169),13); 

    /* HALL SENSOR:  etimer 1 channel 1 (index signal) */
    REG_WRITE8(INTC_PSR(157),12);
    REG_WRITE8(INTC_PSR(158),12);
    REG_WRITE8(INTC_PSR(159),12);
    
    /* RESOLVER SENSOR:  ADC CTU FIFO 3 ISR (without DMA)*/
    /* REG_WRITE8(INTC_PSR(204),15); */
#if (RES_DMA_DEBUG == TRUE)
    /* RESOLVER SENSOR:  DMA CTU FIFO 3 channel  */
    REG_WRITE8(INTC_PSR(12),15);  
#endif
    
#if(0)
    /* RF1 */
    REG_WRITE8(INTC_PSR(182),10);
#endif

#if(0)
    /* debug RF eTimer 0 channel 1 */
    REG_WRITE8(INTC_PSR(179),15);
#endif

}

/**
* @brief   MC_Lib_EnableIrq
* @details The function enables the interrupts globally.
*
* @return none
* @retval void
*
* @pre none
* @post none
*/
void MC_Lib_EnableIrq(void)
    {
     /* Ensure INTC's current priority is 0 */
     INTC.CPR.B.PRI = 0;
    /* Enable external interrupts */
     asm(" wrteei 1");
    }


/**
* @brief   WAIT_EN_L9906
* @details enable (EN1 to 1) L9906
*
* @return none
* @retval void
*
*/
void WAIT_EN_L9907(void){

}


/**
* @brief   Configure L9907
* @details Configure current sense amplifier 1 and 2 offset (0.5*Vcc offset for phase shunt
*          resistors connection)
*
* @return none
* @retval void
*
*/
void Configure_L9907(void)
{

   uint32_t   j = 0;
   uint16 Arg1 = (LLD_Spi_mode_master | LLD_Spi_clktrans_risingedge | LLD_Spi_clkdelay_on | 0x4);
   uint8 br = 0x3;
   
   SIU.PCR[43].R    = 0x0200;
   SIU.GPDO[43].R  = 0x0;
   for (j=0;j<0x00FFFF;j++) {asm ("nop");}

   /* DPSI 0 /CS0 - PC4 - PCR36 - AF1 */
   REG_WRITE16( SPI_SIUL_PCR(36) , (SIUL_PCR_PA_ALT1 | SPI_SIUL_PCR_OBE_ON | SPI_SIUL_PCR_SRC_ON) );
   /* DPSI 0 RX - PC7 - PCR39 - AF */
   REG_WRITE16( SPI_SIUL_PCR(39) , (SIUL_PCR_PA_ALT1 |SPI_SIUL_PCR_IBE_ON) );
   /* DPSI 0 TX - PC6 - PCR38 - AF1 */
   REG_WRITE16( SPI_SIUL_PCR(38) , (SIUL_PCR_PA_ALT1 | SPI_SIUL_PCR_OBE_ON | SPI_SIUL_PCR_SRC_ON) );
   /* DPSI 0 CLK - PC5 - PCR37 - AF1 */
   REG_WRITE16( SPI_SIUL_PCR(37) , (SIUL_PCR_PA_ALT1 | SPI_SIUL_PCR_OBE_ON | SPI_SIUL_PCR_SRC_ON) );

   /* Initialize the driver */
   LLF_SPI_INIT(Arg1);
   LLF_SPI_BAUDRATE(br);

   receivedData = LLF_SPI_VALVEACTIVATION(SPI_CMD0_RET);
   receivedData = LLF_SPI_VALVEACTIVATION(SPI_EVAL9907_CMD0_WRITE);
   receivedData = LLF_SPI_VALVEACTIVATION(SPI_EVAL9907_CMD0_WRITE);
   receivedData = LLF_SPI_VALVEACTIVATION(SPI_CMD0_RET);   
   receivedData = LLF_SPI_VALVEACTIVATION(SPI_CMD0_RET);

   SIU.GPDO[43].R = 0x1;
   for (j=0;j<0x00FFFF;j++) {asm ("nop");}
}
#endif /* #if !SPC5_INIT */

/**
* @brief   STM_Configuration
* @details Configure STM timer for task management
*
*/
uint8 STM_Configuration(uint32 ticks)
{
#if defined _SPC56ELxx_
   REG_WRITE32( STM_CR(0) , 0x00000000 );
   REG_WRITE32( STM_CMP(0) , ticks );
   REG_WRITE32( STM_CCR(0) , 0x00000001 );
   REG_WRITE32( STM_CR(0) , 0x00000001 );
#else
   REG_WRITE32( STM_CR , 0x00000000 );
   REG_WRITE32( STM_CMP0 , ticks );
   REG_WRITE32( STM_CCR0 , 0x00000001 );
   REG_WRITE32( STM_CR , 0x00000001 );
#endif

   return (0);
}
#ifdef __cplusplus
}
#endif
