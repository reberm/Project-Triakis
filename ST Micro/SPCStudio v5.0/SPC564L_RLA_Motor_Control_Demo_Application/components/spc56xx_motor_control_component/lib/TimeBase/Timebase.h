/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    Timebase.h
*   @version BETA 0.9.1
*   @brief   This file implements the time base utilized for running MC tasks
*          
*   @details This file implements the time base utilized for running MC tasks.
*
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __TIMEBASE_H
#define __TIMEBASE_H

/* Includes ------------------------------------------------------------------*/
#include "MC_type.h"

/** @addtogroup SPC5_PMSM_MC_Timebase
  * @{
  */

/** @defgroup Timebase_exported_functions Timebase exported functions
  * @{
  */
    
/**
  * @brief  Use this function to know whether the user time base is elapsed
  * has elapsed 
  * @param  none
  * @retval bool TRUE if time is elapsed, FALSE otherwise
  */
bool TB_UserTimebaseHasElapsed(void);

/**
  * @brief  It set a counter intended to be used for counting the user time base 
  *         time
  * @param  SysTickCount number of System ticks to be counted
  * @retval void
  */
void TB_SetUserTimebaseTime(uint16_t SysTickCount);

/**
  * @brief  It is the task scheduler.
  * @param  none
  * @retval none
  */
void TB_Scheduler(void);

void TB_Set_DebounceDelay_500us(uint8_t hDelay);
bool TB_DebounceDelay_IsElapsed(void);

/**
  * @}
  */
  
/**
  * @}
  */

/**
  * @}
  */

#endif /* __MCBOOTSINGLEMOTOR_H */
