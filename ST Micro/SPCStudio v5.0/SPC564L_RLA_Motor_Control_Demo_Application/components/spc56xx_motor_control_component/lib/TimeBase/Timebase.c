/*
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    Timebase.c
*   @version BETA 0.9.1
*   @brief   This file implements the time base utilized for running MC tasks
*          
*   @details This file implements the time base utilized for running MC tasks.
*
*/

#include "Timebase.h"
#if defined(PFC_ENABLED)
  #include "PIRegulatorClass.h"
#endif
#include "MCTuningClass.h"
#include "MCInterfaceClass.h"
#if defined(PFC_ENABLED)
  #include "PFCApplication.h"
#endif
#include "MCTasks.h"
#include "Parameters conversion.h"
#if (defined(DAC_FUNCTIONALITY) || defined(SERIAL_COMMUNICATION))
#include "UITask.h"
#endif

#ifdef USE_L9907
#include "spc5_L9907.h"
#endif

#ifdef USE_STGAP1S
#include "GAPApplication.h"
#endif

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
static volatile uint16_t  bUDTBTaskCounter;
static volatile uint16_t  hKey_debounce_500us = 0;

/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Use this function to know whether the user time base is elapsed
  * has elapsed 
  * @param  none
  * @retval bool TRUE if time is elapsed, FALSE otherwise
  */
bool TB_UserTimebaseHasElapsed(void)
{
  bool retVal = FALSE;
  if (bUDTBTaskCounter == 0u)
  {
    retVal = TRUE;
  }
  return (retVal);
}

/**
  * @brief  It set a counter intended to be used for counting the user time base 
  *         time
  * @param  SysTickCount number of System ticks to be counted
  * @retval void
  */
void TB_SetUserTimebaseTime(uint16_t SysTickCount)
{
  bUDTBTaskCounter = SysTickCount;
}

/**
  * @brief  It is the task scheduler and updates counters used by wait functions.
  *         Note: The tasks are not called by this function but -through the SVC 
  *         call handler- by TB_TaskExec.
  * @param  none
  * @retval none
  */
void TB_Scheduler(void)
{
  TSK_SafetyTask();
  
#ifdef PFC_ENABLED
  {
    PFC_Scheduler();
  }
#endif
  
  MC_Scheduler();
  
#if (defined(DAC_FUNCTIONALITY) || defined(SERIAL_COMMUNICATION))
  UI_Scheduler();
#endif

#ifdef USE_L9907
  L9907_Schedule();
#endif

#ifdef USE_STGAP1S
  GAPSchedule();
#endif

  if(bUDTBTaskCounter > 0u)
  {
    bUDTBTaskCounter--;
  }
  
  if (hKey_debounce_500us != 0)  
  {
    hKey_debounce_500us --;
  }
}

void TB_Set_DebounceDelay_500us(uint8_t hDelay)
{
  hKey_debounce_500us = hDelay;
}  

bool TB_DebounceDelay_IsElapsed(void)
{
 if (hKey_debounce_500us == 0)
 {
   return (TRUE);
 }
 else 
 {
   return (FALSE);
 }
} 
