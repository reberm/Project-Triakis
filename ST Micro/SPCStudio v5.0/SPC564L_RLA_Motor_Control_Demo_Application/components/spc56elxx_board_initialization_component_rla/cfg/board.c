/*
    SPC5 RLA - Copyright (C) 2015 STMicroelectronics

    Licensed under the Apache License, Version 2.0 (the "License").
    You may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

#include "board.h"
#include "clock.h"

/* Initial setup of all defined pads, the list is terminated by a {-1, 0, 0}.*/
static const spc_siu_init_t spc_siu_init[] = {
  {(int32_t)PCR(PORT_A, MC_PIN_eTimer0_HALL1), PAL_LOW,   
   (iomode_t)(PAL_SPC5_PA(1) | PAL_SPC5_IBE | PAL_SPC5_SRC)},
  {(int32_t)PCR(PORT_A, MC_PIN_eTimer0_HALL2), PAL_LOW,   
   (iomode_t)(PAL_SPC5_PA(1) | PAL_SPC5_IBE | PAL_SPC5_SRC)},
  {(int32_t)PCR(PORT_A, MC_PIN_eTimer0_HALL3), PAL_LOW,   
   (iomode_t)(PAL_SPC5_PA(1) | PAL_SPC5_IBE | PAL_SPC5_SRC)},
  {(int32_t)PCR(PORT_D, MC_PIN_PWM_L1), PAL_LOW,    (iomode_t)(PAL_MODE_OUTPUT_ALTERNATE(1))},
  {(int32_t)PCR(PORT_G, MC_PIN_PWM_L2), PAL_LOW,    (iomode_t)(PAL_MODE_OUTPUT_ALTERNATE(1))},
  {(int32_t)PCR(PORT_G, MC_PIN_PWM_L3), PAL_LOW,    (iomode_t)(PAL_MODE_OUTPUT_ALTERNATE(1))},
  {(int32_t)PCR(PORT_C, MC_PIN_PWM_H1), PAL_LOW,    (iomode_t)(PAL_MODE_OUTPUT_ALTERNATE(3))},
  {(int32_t)PCR(PORT_G, MC_PIN_PWM_H2), PAL_LOW,    (iomode_t)(PAL_MODE_OUTPUT_ALTERNATE(1))},
  {(int32_t)PCR(PORT_G, MC_PIN_PWM_H3), PAL_LOW,    (iomode_t)(PAL_MODE_OUTPUT_ALTERNATE(1))},
  {(int32_t)PCR(PORT_B, PIN_CAN0TXD), PAL_LOW,    (iomode_t)(PAL_MODE_OUTPUT_ALTERNATE(1))},
  {(int32_t)PCR(PORT_B, PIN_CAN0RXD), PAL_LOW,    (iomode_t)(PAL_MODE_INPUT)},
  {(int32_t)PCR(PORT_B, PIN_LIN0TXD), PAL_LOW,    (iomode_t)(PAL_MODE_OUTPUT_ALTERNATE(1))},
  {(int32_t)PCR(PORT_B, PIN_LIN0RXD), PAL_LOW,    (iomode_t)(PAL_MODE_INPUT)},
  {(int32_t)PCR(PORT_B, MC_RESOLVER_CH_A), PAL_LOW,    (iomode_t)(PAL_MODE_INPUT_ANALOG)},
  {(int32_t)PCR(PORT_B, MC_RESOLVER_CH_B), PAL_LOW,    (iomode_t)(PAL_MODE_INPUT_ANALOG)},
  {(int32_t)PCR(PORT_A, MC_PIN_eTIM1_EncoderIndex), PAL_LOW,   
   (iomode_t)(PAL_SPC5_PA(1) | PAL_SPC5_IBE)},
  {(int32_t)PCR(PORT_D, MC_PIN_eTIM1_EncoderA), PAL_LOW,   
   (iomode_t)(PAL_SPC5_PA(2) | PAL_SPC5_IBE)},
  {(int32_t)PCR(PORT_D, MC_PIN_eTIM1_EncoderB), PAL_LOW,   
   (iomode_t)(PAL_SPC5_PA(2) | PAL_SPC5_IBE)},
  {(int32_t)PCR(PORT_C, MC_PIN_DSPI0_CS0), PAL_LOW,    (iomode_t)(PAL_MODE_OUTPUT_ALTERNATE(1))},
  {(int32_t)PCR(PORT_C, MC_PIN_DSPI0_CLK), PAL_LOW,    (iomode_t)(PAL_MODE_OUTPUT_ALTERNATE(1))},
  {(int32_t)PCR(PORT_C, MC_PIN_DSPI0_TX_SOUT), PAL_LOW,    (iomode_t)(PAL_MODE_OUTPUT_ALTERNATE(1))},
  {(int32_t)PCR(PORT_C, MC_PIN_DSPI0_RX_SIN), PAL_LOW,    (iomode_t)(PAL_MODE_INPUT)},
  {(int32_t)PCR(PORT_B, MC_PIN_ADC_0_IB1), PAL_LOW,    (iomode_t)(PAL_MODE_INPUT_ANALOG)},
  {(int32_t)PCR(PORT_B, MC_PIN_ADC_1_IB2), PAL_LOW,    (iomode_t)(PAL_MODE_INPUT_ANALOG)},
  {(int32_t)PCR(PORT_A, L9907_EN1),  PAL_LOW,    (iomode_t)(PAL_MODE_OUTPUT_PUSHPULL)},
  {(int32_t)PCR(PORT_C, L9907_EN2),  PAL_LOW,    (iomode_t)(PAL_MODE_OUTPUT_PUSHPULL)},
  {(int32_t)PCR(PORT_G, L9907_FS_FLAG), PAL_LOW,    (iomode_t)(PAL_MODE_INPUT)},
  {(int32_t)PCR(PORT_A, DEBUG_PIN_0), PAL_LOW,    (iomode_t)(PAL_MODE_OUTPUT_PUSHPULL)},
  {(int32_t)PCR(PORT_A, DEBUG_PIN_1), PAL_LOW,    (iomode_t)(PAL_MODE_OUTPUT_PUSHPULL)},
  {(int32_t)PCR(PORT_C, ADC_VBUS_PIN), PAL_LOW,    (iomode_t)(PAL_MODE_INPUT_ANALOG)},
  {(int32_t)PCR(PORT_E, ADC_USER_PIN_0), PAL_LOW,    (iomode_t)(PAL_MODE_INPUT_ANALOG)},
  {(int32_t)PCR(PORT_C, ADC_USER_PIN_1), PAL_LOW,    (iomode_t)(PAL_MODE_INPUT_ANALOG)},
  {(int32_t)PCR(PORT_E, ADC_USER_PIN_2), PAL_LOW,    (iomode_t)(PAL_MODE_INPUT_ANALOG)},
  {(int32_t)PCR(PORT_E, ADC_USER_PIN_3), PAL_LOW,    (iomode_t)(PAL_MODE_INPUT_ANALOG)},
  {-1, 0, 0}
};

/* Initialization array for the PSMI registers.*/
static const uint8_t spc_padsels_init[SPC5_SIUL_NUM_PADSELS] = {
  0,   0,   0,   0,   0,   0,   0,   0,   
  0,   0,   0,   0,   1,   1,   0,   0,   
  0,   0,   0,   0,   0,   1,   2,   3,   
  0,   2,   2,   3,   0,   0,   0,   0,   
  0,   1,   0,   0,   0,   0,   0,   0,   
  0,   0,   0,   0,   
};

/**
 * @brief   PAL setup.
 */
static const PALConfig pal_default_config = {
  (iomode_t)(PAL_MODE_RESET),
  spc_siu_init,
  spc_padsels_init
};

/*
 * Board-specific initialization code.
 */
void boardInit(void) {

  pal_init(&pal_default_config);
}
