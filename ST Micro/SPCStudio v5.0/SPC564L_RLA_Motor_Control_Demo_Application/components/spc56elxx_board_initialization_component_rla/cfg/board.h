/*
    SPC5 RLA - Copyright (C) 2015 STMicroelectronics

    Licensed under the Apache License, Version 2.0 (the "License").
    You may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

#ifndef _BOARD_H_
#define _BOARD_H_

#include "pal.h"

/*
 * Setup for a generic SPC56ELxx board.
 */

/*
 * Board identifiers.
 */
#define BOARD_SPC56EL_DISCOVERY
#define BOARD_NAME                  "STMicroelectronics SPC56EL Discovery"

/*
 * PIN definitions.
 */
#define MC_PIN_eTimer0_HALL1        0U
#define MC_PIN_eTimer0_HALL2        2U
#define MC_PIN_eTimer0_HALL3        1U
#define MC_PIN_PWM_L1               14U
#define MC_PIN_PWM_L2               4U
#define MC_PIN_PWM_L3               7U
#define MC_PIN_PWM_H1               15U
#define MC_PIN_PWM_H2               3U
#define MC_PIN_PWM_H3               6U
#define PIN_CAN0TXD                 0U
#define PIN_CAN0RXD                 1U
#define PIN_LIN0TXD                 2U
#define PIN_LIN0RXD                 3U
#define MC_RESOLVER_CH_A            10U
#define MC_RESOLVER_CH_B            15U
#define MC_PIN_eTIM1_EncoderIndex   4U
#define MC_PIN_eTIM1_EncoderA       2U
#define MC_PIN_eTIM1_EncoderB       3U
#define MC_PIN_DSPI0_CS0            4U
#define MC_PIN_DSPI0_CLK            5U
#define MC_PIN_DSPI0_TX_SOUT        6U
#define MC_PIN_DSPI0_RX_SIN         7U
#define MC_PIN_ADC_0_IB1            7U
#define MC_PIN_ADC_1_IB2            13U
#define L9907_EN1                   3U
#define L9907_EN2                   11U
#define L9907_FS_FLAG               1U
#define DEBUG_PIN_0                 5U
#define DEBUG_PIN_1                 6U
#define ADC_VBUS_PIN                2U
#define ADC_USER_PIN_0              11U
#define ADC_USER_PIN_1              1U
#define ADC_USER_PIN_2              4U
#define ADC_USER_PIN_3              3U

/*
 * PORT definitions.
 */
#define PORT_MC_PIN_eTimer0_HALL1   PORT_A
#define PORT_MC_PIN_eTimer0_HALL2   PORT_A
#define PORT_MC_PIN_eTimer0_HALL3   PORT_A
#define PORT_MC_PIN_PWM_L1          PORT_D
#define PORT_MC_PIN_PWM_L2          PORT_G
#define PORT_MC_PIN_PWM_L3          PORT_G
#define PORT_MC_PIN_PWM_H1          PORT_C
#define PORT_MC_PIN_PWM_H2          PORT_G
#define PORT_MC_PIN_PWM_H3          PORT_G
#define PORT_PIN_CAN0TXD            PORT_B
#define PORT_PIN_CAN0RXD            PORT_B
#define PORT_PIN_LIN0TXD            PORT_B
#define PORT_PIN_LIN0RXD            PORT_B
#define PORT_MC_RESOLVER_CH_A       PORT_B
#define PORT_MC_RESOLVER_CH_B       PORT_B
#define PORT_MC_PIN_eTIM1_EncoderIndex PORT_A
#define PORT_MC_PIN_eTIM1_EncoderA  PORT_D
#define PORT_MC_PIN_eTIM1_EncoderB  PORT_D
#define PORT_MC_PIN_DSPI0_CS0       PORT_C
#define PORT_MC_PIN_DSPI0_CLK       PORT_C
#define PORT_MC_PIN_DSPI0_TX_SOUT   PORT_C
#define PORT_MC_PIN_DSPI0_RX_SIN    PORT_C
#define PORT_MC_PIN_ADC_0_IB1       PORT_B
#define PORT_MC_PIN_ADC_1_IB2       PORT_B
#define PORT_L9907_EN1              PORT_A
#define PORT_L9907_EN2              PORT_C
#define PORT_L9907_FS_FLAG          PORT_G
#define PORT_DEBUG_PIN_0            PORT_A
#define PORT_DEBUG_PIN_1            PORT_A
#define PORT_ADC_VBUS_PIN           PORT_C
#define PORT_ADC_USER_PIN_0         PORT_E
#define PORT_ADC_USER_PIN_1         PORT_C
#define PORT_ADC_USER_PIN_2         PORT_E
#define PORT_ADC_USER_PIN_3         PORT_E

/*
 * Support macros.
 */
#define PCR(port, pin)  (((port) * 16U) + (pin))

#if !defined(_FROM_ASM_)
#ifdef __cplusplus
extern "C" {
#endif
  void boardInit(void);
#ifdef __cplusplus
}
#endif
#endif /* _FROM_ASM_ */

#endif /* _BOARD_H_ */
