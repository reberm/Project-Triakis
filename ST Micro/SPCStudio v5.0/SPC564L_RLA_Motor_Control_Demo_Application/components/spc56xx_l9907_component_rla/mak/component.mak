
#################################################################################
# Source and include file located under ./lib and ./cfg  are automatically added.
#################################################################################

# C specific options here (added to USE_OPT).
USE_COPT += -DAUTOSAR_OS_NOT_USED

# List of library files exported by the component.
LIB_C_SRC       += ./components/spc56xx_l9907_component_rla/lib/src/spc5_L9907.c

LIB_CPP_SRC     +=

LIB_ASM_SRC     +=

LIB_INCLUDES    += ./components/spc56xx_l9907_component_rla/lib/include

APP_C_SRC       += ./components/spc56xx_l9907_component_rla/cfg/spc5_L9907_cfg.c

APP_CPP_SRC     +=

APP_ASM_SRC     +=

APP_INCLUDES    += ./components/spc56xx_l9907_component_rla/cfg
