/**
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    spc5_L9907_cfg.c
*   @version BETA 0.9.1
*   @brief   L9907 faultHandler function implementation
*          
*   @details L9907 faultHandler function implementation.
*
*/

/**
 * @addtogroup L9907
 * @{
 */
 
/*
VERY IMPORTANT: in SPC560Pxx IRQ Component RLA create a new  Enabled Vector with Number 44 and Priority higher than 10, and insert  a call to the function L9907_irqHandler in the IRQ Body's code.
*/

void L9907_faultRecovery(void) {

	/* faultRecovery body here*/

} // end faultRecovery
/** @} */
