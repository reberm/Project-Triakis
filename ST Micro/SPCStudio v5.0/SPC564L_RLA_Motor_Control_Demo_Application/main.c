/**
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ADG License Agreement, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://goo.gl/28pHKW
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
*/

/*
*   @file    main.c
*   @version BETA 0.9.1
*   @brief   Main program body
*          
*   @details Main program body.
*
*/
#include "MC_Lib_Setup.h"
#include "components.h"
#include "xpc56el.h"
#if defined(PFC_ENABLED)
  #include "PIRegulatorClass.h"
#endif /* #if defined(PFC_ENABLED) */
#include "MCTuningClass.h"
#include "MCInterfaceClass.h"
#if defined(PFC_ENABLED)
  #include "PFCInit.h"
  #include "PFCApplication.h"
#endif /* #if defined(PFC_ENABLED) */
#include "MCTasks.h"
#include "Parameters conversion.h"
#include "Control stage parameters.h"
#ifdef DUALDRIVE
#include "Parameters conversion motor 2.h"
#endif /* #ifdef DUALDRIVE */
#include "Timebase.h"
#include "MC.h"
#include "MCTuningClass.h"
#include "MCInterfaceClass.h"
#if (defined(DAC_FUNCTIONALITY) || defined(SERIAL_COMMUNICATION) || defined(CAN_COMMUNICATION))
#include "UITask.h"
#endif
/* CTU is missing in SPC5 */
#include "ast_ctu_lld.h"
#include "Reg_eSys_Stm.h"
/*===============================================================================================
*                          LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
===============================================================================================*/

/*===============================================================================================
*                                       CONSTANTS
===============================================================================================*/

/* Enable/Disable the demo mode */
#define DEMOMODE FALSE

/* DEMO MODE variables and macros */
#if (DEMOMODE == TRUE)
#define USER_TIMEBASE_FREQUENCY_HZ        10
#define USER_TIMEBASE_OCCURENCE_TICKS     (SYS_TICK_FREQUENCY/USER_TIMEBASE_FREQUENCY_HZ)-1u
#define RUN_DURATION_SEC                  5
#define STOP_DURATION_SEC                 3
#define RUN_DURATION                      (RUN_DURATION_SEC * USER_TIMEBASE_FREQUENCY_HZ)
#define STOP_DURATION                     (STOP_DURATION_SEC * USER_TIMEBASE_FREQUENCY_HZ)
#define US_RESET                          0x00
#define US_POSITIVE_RUN                   0x01
#define US_POSITIVE_RUN2                  0x02
#define US_POSITIVE_RUN3                  0x03
#define US_STOP1                          0x04
#define US_NEGATIVE_RUN                   0x05
#define US_STOP2                          0x06
#define US_STOP3                          0x07
#define US_NEGATIVE_RUN2                  0x08
#define US_NEGATIVE_RUN3                  0x09
#define MANUAL_MODE                       0x00
#define DEMO_MODE                         0x01
static uint16_t UserCnt = 0;
static uint8_t User_State = US_RESET;
static volatile uint8_t Mode = DEMO_MODE;
#endif /* #if (DEMOMODE == TRUE) */

/*===============================================================================================
*                                       LOCAL VARIABLES
===============================================================================================*/
#define FIRMWARE_VERS "SPC56P Discovery FOC\0EAR.0.9.1"
const char s_fwVer[32] = FIRMWARE_VERS;

/*===============================================================================================
*                                       GLOBAL VARIABLES
===============================================================================================*/
State_t StateMotor;
bool startMot = FALSE;
bool TestAlign = FALSE;

CMCI oMCI[MC_NUM];
CMCT oMCT[MC_NUM];
#if (defined(DAC_FUNCTIONALITY) || defined(SERIAL_COMMUNICATION) || defined(CAN_COMMUNICATION))
uint32_t wConfig[MC_NUM] = {UI_CONFIG_M1,UI_CONFIG_M2};
#endif
#ifdef ADC_USER_ENABLE
#define TEST_ADC_USER_CON_NUM 16
#define TEST_ADC_REQ_TIME 128000 /* 2ms */
uint16_t ADC_User_data = 0;
uint16_t ADC_User_data_test[TEST_ADC_USER_CON_NUM];
uint16_t test_adc_index = 0;

#endif

/*===============================================================================================
*                                   LOCAL FUNCTION PROTOTYPES
===============================================================================================*/
void SysTasks_Configuration(void);
void spc5_late_init(void);

#ifdef ADC_USER_ENABLE
void TimerADC_Conv(uint32_t ticks);
#endif

#if (DEMOMODE == TRUE)
void Demo(void);
#endif /* #if (DEMOMODE == TRUE) */

/*===============================================================================================
*                                             FUNCTIONS
===============================================================================================*/
/*
 * Application entry point.
 */
int main(void)
{
   /* SWT disabled.*/
   SWT.SR.R = 0xC520U;
   SWT.SR.R = 0xD928U;
   SWT.CR.R = 0xFF00000AU;

   /* INTC init, Enable IRQ, Rla Init, Smart Power Init */
   componentsInit();

#if 0
    /* implement terminal echo to debug serial data transmission with GUI */
    uint8_t msg;

    sd_lld_start(&SD1, NULL);

    for ( ; ; )
    {
        while (sd_lld_read(&SD1,&msg,1)== 0);
        sd_lld_write(&SD1,&msg,1);
    }
#endif

   /* ADC, CTU and PWM are not clocked by SPC5 Studio */
   spc5_late_init();

   /*MCInterface and MCTuning boot*/
   MCboot(oMCI,oMCT);

   /*SysTasks configuration.*/
   SysTasks_Configuration();

#ifdef ADC_USER_ENABLE
   TimerADC_Conv(TEST_ADC_REQ_TIME);
#endif

/* Start the Demo application or not */
#if (DEMOMODE == FALSE)
   if(StateMotor == IDLE)
   {
   #if(0)
      TestAlign = MCI_EncoderAlign(oMCI[0]);
      if(TestAlign == TRUE)
   #endif
      {
   #if(1)
         while(IDLE != MCI_GetSTMState(oMCI[0]))
         {
         }
         /* This command sets what will be the first speed ramp after the
         MCI_StartMotor command. It requires as first parameter the oMCI[0], as
         second parameter the target mechanical speed in thenth of Hz and as
         third parameter the speed ramp duration in milliseconds. */
         MCI_ExecSpeedRamp(oMCI[0], (int16_t)0, (uint16_t)2000);

         /* This is a user command used to start the motor. The speed ramp shall be
            pre programmed before the command.*/
         startMot = MCI_StartMotor(oMCI[0]);
   #endif
      }
   }
#else
   /* Start the Demo application */
   Demo();
#endif /* #if (DEMOMODE == FALSE) */

/* Start here ***************************************************************/
/* GUI, this section is present only if DAC or serial communication is enabled. */
/* SERIAL_COMMUNICATION and DAC_FUNCTIONALITY set as undefined in Parameter conversion.h */
#if (defined(DAC_FUNCTIONALITY) || defined(SERIAL_COMMUNICATION)|| defined(CAN_COMMUNICATION))
   UI_TaskInit(UI_INIT_CFG,wConfig,MC_NUM,oMCI,oMCT,s_fwVer);
#endif
/* End here******************************************************************/
#if defined(SERIAL_COMMUNICATION) || defined(CAN_COMMUNICATION)
   UI_WaitForCommand(); /* waiting loop for GUI commands */
#else
   while(1)
   {
      /* Do nothing */
   }

#endif

   return 0;
}

/**
  * @brief  Configures the Tasks with STM IP.
  * @param  None
  * @retval None
  */
void SysTasks_Configuration(void)
{
   STM_Configuration((SYSCLK_FREQ_120MHz) / SYS_TICK_FREQUENCY);
}

#if (DEMOMODE == TRUE)
/**
  * @brief  Start the Demo application.
  * @param  None
  * @retval None
  */
void Demo(void)
{
   while(1)
   {
      if (Mode == DEMO_MODE)
      {
         /* User defined Timebase */
         if (TB_UserTimebaseHasElapsed())
         {
            /* User defined code */
            switch (User_State)
            {
            case US_RESET:
            {
               /* Next state */
               /* This command sets what will be the first speed ramp after the
               MCI_StartMotor command. It requires as first parameter the oMCI[0], as
               second parameter the target mechanical speed in thenth of Hz and as
               third parameter the speed ramp duration in milliseconds. */
               MCI_ExecSpeedRamp(oMCI[0], 1500/6, 2000);

               /* This is a user command used to start the motor. The speed ramp shall be
                  pre programmed before the command.*/
               MCI_StartMotor(oMCI[0]);
               User_State = US_POSITIVE_RUN;
               UserCnt = 0;
            }
            break;
            case US_POSITIVE_RUN:
            {
               if (UserCnt < RUN_DURATION)
               {
                  UserCnt++;
               }
               else
               {
                  MCI_ExecSpeedRamp(oMCI[0], 4000/6, 2000);
                  User_State = US_POSITIVE_RUN2;
                  UserCnt = 0;
               }
            }
            break;
            case US_POSITIVE_RUN2:
            {
              if (UserCnt < RUN_DURATION)
               {
                  UserCnt++;
               }
               else
               {
                  MCI_ExecSpeedRamp(oMCI[0], 1000/6, 2000);
                  User_State = US_POSITIVE_RUN3;
                  UserCnt = 0;
               }
            }
            break;
            case US_POSITIVE_RUN3:
            {
               if (UserCnt < RUN_DURATION)
               {
                  UserCnt++;
               }
               else
               {
                  /* Next state */
                  /* This is a user command used to stop the motor. */
                  MCI_StopMotor(oMCI[0]);
                  User_State = US_STOP1;
                  UserCnt = 0;
               }
            }
            break;
            case US_STOP1:
            {
               if (UserCnt < STOP_DURATION)
               {
                  UserCnt++;
               }
               else
               {
                  /* Next state */
                  User_State = US_NEGATIVE_RUN;
               }
            }
            break;
            case US_NEGATIVE_RUN:
            {
               if (UserCnt < RUN_DURATION)
               {
                  UserCnt++;
               }
               else
               {
                  MCI_ExecSpeedRamp(oMCI[0], -2000/6, 2000);
                  /* This is a user command used to start the motor. The speed ramp shall be
                     pre programmed before the command.*/
                  MCI_StartMotor(oMCI[0]);
                  User_State = US_NEGATIVE_RUN2;
                  UserCnt = 0;
               }
            }
            break;
            case US_NEGATIVE_RUN2:
            {
               if (UserCnt < RUN_DURATION)
               {
                  UserCnt++;
               }
               else
               {
                  MCI_ExecSpeedRamp(oMCI[0], -1500/6, 2000);
                  User_State = US_NEGATIVE_RUN3;
                  UserCnt = 0;
               }
            }
            break;
            case US_NEGATIVE_RUN3:
            {
               if (UserCnt < RUN_DURATION)
               {
                  UserCnt++;
               }
               else
               {
                  MCI_ExecSpeedRamp(oMCI[0], -4000/6, 2000);
                  User_State = US_STOP2;
                  UserCnt = 0;
               }
            }
            break;
            case US_STOP2:
            {
               if (UserCnt < RUN_DURATION)
               {
                  UserCnt++;
               }
               else
               {
                  /* Next state */
                  /* This is a user command used to stop the motor. */
                  MCI_StopMotor(oMCI[0]);
                  User_State = US_STOP3;
                  UserCnt = 0;
               }
            }
            break;
            case US_STOP3:
            {
               if (UserCnt < STOP_DURATION)
               {
                  UserCnt++;
               }
               else
               {
                  /* Next state */
                  User_State = US_RESET;
               }
            }
            break;
            default:
            {
               User_State = US_RESET;
            }
            break;
            }

            /* If a fault condition occurs the user defined state macchine is forced
               to reset the state */
            if (MCI_GetSTMState(oMCI[0]) == FAULT_OVER)
            {
               User_State = US_RESET;
               MCI_FaultAcknowledged(oMCI[0]);
            }
            /* Reload the user defined timebase */
            TB_SetUserTimebaseTime(USER_TIMEBASE_OCCURENCE_TICKS);
         }
      }
      else
      {
         User_State = US_RESET;
      }
   }
}
#endif /* #if (DEMOMODE == TRUE) */



/**
  * @brief  Initialization of motor Control IPs.
  * @param  None
  * @retval None
  */
/* Note: It is not possible to use the late init of SPC5Studio because of missing header
 * in the weak function generated by configurator */
void spc5_late_init(void) {

  /* ADC inits */
   SPCSetPeripheralClockMode(SPC5_ADC0_PCTL, SPC5_ADC_ADC0_START_PCTL);
   SPCSetPeripheralClockMode(SPC5_ADC1_PCTL, SPC5_ADC_ADC1_START_PCTL);

   /* PWM init */
   SPCSetPeripheralClockMode(SPC5_FLEXPWM0_PCTL, SPC5_ADC_ADC0_START_PCTL);

   /* CTU init that was lacking in SPC5 */
   SPCSetPeripheralClockMode(SPC5_AST_CTU_PCTL, SPC5_AST_CTU_START_PCTL);

   /* eTimers init */
   SPCSetPeripheralClockMode(SPC5_ETIMER0_PCTL, SPC5_ADC_ADC0_START_PCTL);
   SPCSetPeripheralClockMode(SPC5_ETIMER1_PCTL, SPC5_ADC_ADC0_START_PCTL);

   /* PIT init */
   SPCSetPeripheralClockMode(92, SPC5_ADC_ADC0_START_PCTL);
}

#ifdef ADC_USER_ENABLE

void TimerADC_Conv(uint32_t ticks)
{
  REG_WRITE32( STM_CMP(1) , ticks );
  REG_WRITE32( STM_CCR(1) , 0x00000001 );
}
#endif

void ADC_STM_User_Handler(void)
{
#ifdef ADC_USER_ENABLE
  uint32_t counter;
#ifdef  ADC_MULTIPLE_CONV_ENABLED
  static uint8_t multiple_adc_conv = 0;
#endif
  REG_WRITE32( STM_CIR(1) , 0x00000001 );
  REG_WRITE32( STM_CCR(1) , 0x00000000 );
  counter = REG_READ32(STM_CNT(1));
  REG_WRITE32( STM_CMP(1) , counter + TEST_ADC_REQ_TIME );
  REG_WRITE32( STM_CCR(1) , 0x00000001 );

#ifdef  ADC_MULTIPLE_CONV_ENABLED
  if(test_adc_index < TEST_ADC_USER_CON_NUM)
  {
    ADC_User_data_test[test_adc_index] = MC_GetMultipleRegularConv(multiple_adc_conv);
    ADC_User_data = ADC_User_data_test[test_adc_index];
    test_adc_index++;
    multiple_adc_conv++;
    if(multiple_adc_conv >= ADC_USER_CONV_MAX)
    {
        multiple_adc_conv = 0;
    }
  }
  else
  {
     test_adc_index = 0;
  }
#else
  if(UDRC_STATE_IDLE == MC_RegularConvState())
  {
      /* Function parameters (ADC module, ADC channel and ADC sample time conv)  */
      /* not used in this release (fixed in the configuration).                  */
      /*  Only one ADC conversion can be configured.                             */
      MC_RequestRegularConv(ADC_USER_MODULE,ADC_USER_CH,0);
  }
  if(UDRC_STATE_EOC == MC_RegularConvState())
  {
      if(test_adc_index < TEST_ADC_USER_CON_NUM)
      {
        ADC_User_data_test[test_adc_index] = MC_GetRegularConv();
        ADC_User_data = ADC_User_data_test[test_adc_index];
        test_adc_index++;
      }
      else
      {
         test_adc_index = 0;
      }
  }
#endif /* ADC_MULTIPLE_CONV_ENABLED */
#endif /* ADC_USER_ENABLE */
}

